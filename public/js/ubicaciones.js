var geocoder;
var mapaPropiedad;
var marker;
var markerEstacion;
var bandera=false;

function cargaMapa() 
{
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(20.656890903633094, -103.34968945386208);
    var mapOptions = 
    {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    }
    mapaPropiedad = new google.maps.Map(document.getElementById('ubicaciones-mapa'), mapOptions);

}

function marcadorPropiedad(lat,longitud) 
{
    var ubicacion = new google.maps.LatLng(lat, longitud);
    marker = new google.maps.Marker({
              position: ubicacion,
              map: mapaPropiedad
    });
    mapaPropiedad.setCenter(ubicacion);
    bandera=true;
}


  



function marcarEstacion(id,lat,longitud)
{
    var ubicacion = new google.maps.LatLng(lat, longitud);
    $("#ubicaciones article").removeClass("ubicaciones-item-hover");
    $("#estacion-"+id).addClass("ubicaciones-item-hover");
    mapaPropiedad.setCenter(ubicacion);
    mapaPropiedad.setZoom(16);

    var infowindow=eval('infowindow'+id);
    var markerEstacion=eval('markerEstacion'+id);
    
    infowindow.open(mapaPropiedad,markerEstacion);

}
