var doc = null;
var registrofrm4 = null;
var BCargaMejoramiento=0;

$(document).ready(function(){
	
});

function cargarMejoramientoDeFertilidadDelSuelo(){
	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
		if(BCargaMejoramiento==0){
			BCargaMejoramiento=1;
			$('.mejoramiento tr').each(function(index, element) {
				if(index<'3' || index>'11'){
					if(index=='0' || index=='12'){
						$(this).find('th:last').remove();
						if(index==0){
							$(this).find('th:last').attr("colspan","3");
						}
					}else{
						if(index<3){
							$(this).find('td:last').remove();
							$(this).find('td:last').attr("colspan","3");
						}else{
							$(this).find('td:last').remove();
						}
						
					}
				}
			});
		}
	}
	var conceptos = ["muestreo_suelo", "analisis_suelo", "incorporacion_rastrojo", "abonos_verdes", "composta", "lombricomposta", "lixiviados", "remineralizacion", "descompactadores", "microorganismos", "cal", "dolomita", "yeso", "estiercol","otro"];
	var conceptos_internos_fisica = ["arcilla", "arena", "profundidad", "textura", "limo"];
	var conceptos_internos_quimica = ["cah", "cic", "iht", "mo", "ph", "sb"];

	for (var i = 0; i < conceptos.length; i++) {
		//alert(conceptos[i]);
		//$("#" + conceptos[i] + "_precio", "#frm-4").once('keyup', function(){
		if(conceptos[i]=="muestreo_suelo" || conceptos[i]=="analisis_suelo"){
			$("#" + conceptos[i] + "_precio"/*, #" + conceptos[i] + "_cantidad", */,"#frm-4").once('keyup', function(){
				//alert( this.id);
				calculaImporteFrm44(this.id);
			});
		}else{
			$("#" + conceptos[i] + "_precio, #" + conceptos[i] + "_cantidad", "#frm-4").once('keyup', function(){
				//alert( this.id);
				calculaImporteFrm44(this.id);
			});
		}
		

		$("#" + conceptos[i] + "_No", "#frm-4").once('click', function(){ selectNo(this.id); }).trigger('click');
	};

	if($("#gen-type").val() == 1) 
		for (var i = 0; i < conceptos.length; i++) { 
				//$("#" + conceptos[i] + "_innovacion", "#frm-4").attr("disabled","disabled"); 
		}
	else 
		for (var i = 0; i < conceptos.length; i++) { 
				$("#" + conceptos[i] + "_innovacion", "#frm-4").removeAttr('disabled'); 
		}
	
	$("#muestreo_suelo_Si", "#frm-4").once('click', function(){
		$("#analisis_suelo_mdfds", "#frm-4").show("slow", function() {
		    $("#analisis_doc", "#frm-4").removeAttr("disabled");
			
			
		});
	});
	
	$("#div_up1", "#frm-4").once('click', function(){
		$("#div_up1", "#frm-4").addClass('col-xs-7');
        $("#div_up2, #div_up3", "#frm-4").hide();
        $("#span_documento", "#frm-4").html('&nbsp;Adjuntar el análisis del suelo');
		var input = $("#documento");
  		input.replaceWith(input.val('').clone(true));
  		doc = null;
	});
	
	$("#div_up3", "#frm-4").once('click', function(){ 
		$.ajax({
        url: "/backend/diagnosticos/eliminararchivo/",
        type: "POST",
		data:{archivo:doc,dato_general:$("#gen-id").val(),tipo:$("#gen-type").val()},
		}).done(function(data){
			data = jQuery.parseJSON(data);
			if(data.ok=="Archivo eliminado"){
				
				$("#div_up1", "#frm-4").removeClass('col-xs-3').addClass('col-xs-7');
				$("#div_up2, #div_up3", "#frm-4").hide();
				$("#span_documento", "#frm-4").html('&nbsp;Adjuntar el análisis del suelo');
				var input = $("#documento");
				input.replaceWith(input.val('').clone(true));
				doc = null;
				
				ShowDialogBox("Eliminado", "El archivo se eliminó correctamente", "Ok", "", false, "");
			}else if(data.ok!== undefined){
				ShowDialogBox("Eliminado", "El archivo no se pudo eliminar", "Ok", "", false, "");
			}
		});
		
		
	});

	$("#div_up2", "#frm-4").once('click', function(){
		if(doc == null) 
			alert("No existe aun el archivo en el servidor")
		else
			window.open("http://agricultura-eat.mx" + doc, "_target");
	});

	$("#muestreo_suelo_No", "#frm-4").once('click', function(){
		$("#analisis_suelo_mdfds", "#frm-4").hide("slow", function() {
			selectNo("muestreo_suelo_No");
			$("#analisis_suelo_No", "#frm-4").prop('checked', true).trigger('click');
		    $("#matriz_mdfds", "#frm-4").fadeOut(1200,function(){
		    	$("#matriz_mdfds :input", "#frm-4").attr("disabled");
		    });
		});
	});

	$("#analisis_suelo_Si", "#frm-4").once('click', function(){
		$("#matriz_mdfds", "#frm-4").fadeIn(1200,function(){
			$("#analisis_tecnica", "#frm-4").removeAttr("disabled");
			if(registrofrm4.congelado == 1) $("#matriz_mdfds :input", "#frm-4").removeAttr("disabled");
				for (var i = 0; i < conceptos_internos_fisica.length; i++) {
					$("#analisis_suelo_fisica_" + conceptos_internos_fisica[i]).removeAttr("disabled");
					
				};
		
				for (var i = 0; i < conceptos_internos_quimica.length; i++) {
					$("#analisis_suelo_quimica_" + conceptos_internos_quimica[i]).removeAttr("disabled");
				};
		});
	});

	$("#analisis_suelo_No", "#frm-4").once('click', function(){
		selectNo("analisis_suelo_No");
		$("#matriz_mdfds", "#frm-4").fadeOut(1200,function(){
			$("#div_up3", "#frm-4").trigger('click');
			$("#matriz_mdfds :input", "#frm-4").attr("disabled");
			$("#matriz_mdfds :input", "#frm-4").val('');
		});
	});

	//asignamos las unidades a los conceptos dependiendo la configuracion general
	obtenerUnidadesPorFormularioConceptoFrm4(conceptos);

	/*$.ajax({
        url: "/backend/diagnosticos/obtenerunidades/",
        type: "POST",
        async: false,
        processData: false,  
        contentType: false
    }).done(function(data){
    	for (var i = 0; i < conceptos.length; i++) {
    		$("#" + conceptos[i] + "_id_unidad",  "#frm-4").html(data);
    	};
    });*/

	$('#btn-save-mdfds', "#frm-4").once('click', function(){
		if(validaFrmMejoramiento(conceptos, conceptos_internos_fisica, conceptos_internos_quimica))
		{
			for (var i = 0; i < conceptos.length; i++) { if($("#" + conceptos[i] + "_Si", "#frm-4").is(":checked")) $("#" + conceptos[i] + "_fecha", "#frm-4").removeAttr('disabled'); }
			var frm = new FormData(document.getElementById("frm-4"));	
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			frm.append("hay_documento", doc);
			$.ajax({
	            url: "/backend/diagnosticos/guardarmejoramientofertilidad/",
	            type: "POST",
	            data: frm,
	            enctype: 'multipart/form-data',
	            contentType: false,
			    processData: false
	        }).done(function(respuesta){
	        	try
	        	{
	        		respuesta = jQuery.parseJSON(respuesta);
		        	for (var i = 0; i < conceptos.length; i++) { 
						$("#" + conceptos[i] + "_fecha", "#frm-4").attr("disabled","disabled"); 
					}
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        	}else if(respuesta.error=="Error saving file"){
						ShowDialogBox("Archivo adjunto", "No se ha cargado ningún archivo para el análisis de suelo", "Ok", "", false, "");
					}
		        	else 
						ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					for (var i = 0; i < conceptos.length; i++) { 
						$("#" + conceptos[i] + "_fecha", "#frm-4").attr("disabled","disabled"); 
					}
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });
		}
	});
	
	$("#btn-congelar-mdfds", "#frm-4").once('click', function(){
		if(validaFrmMejoramiento(conceptos, conceptos_internos_fisica, conceptos_internos_quimica)){
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm4);
		}
	});

	$('#btn-clean-mdfds', "#frm-4").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			for (var i = 0; i < conceptos.length; i++) {
				$("#" + conceptos[i] + "_Si", "#frm-4").prop('checked', false);
				$("#" + conceptos[i] + "_No", "#frm-4").trigger("click").prop('checked', false);
				$("#" + conceptos[i] + "_id_unidad",  "#frm-4").val('0');
				$("#" + conceptos[i] + "_cantidad",  "#frm-4").val('');
				$("#" + conceptos[i] + "_precio",  "#frm-4").val('');
				$("#" + conceptos[i] + "_importe",  "#frm-4").val('');
				$("#" + conceptos[i] + "_fecha",  "#frm-4").val('');
				$("#" + conceptos[i] + "_innovacion",  "#frm-4").val('');
			};
		});
		
	});
function obtenerUnidadesPorFormularioConceptoFrm4(conceptos){
	for (var i = 0; i < conceptos.length; i++) {
			$.ajax({
				url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
				type: "POST",
				data:{formulario:"MejoramientoDeFertilidadDelSuelo",concepto:conceptos[i]}
			}).done(function(data){
				concepto = data.split("|")[0];
				datos = data.split("|")[1]
				$('#'+concepto+'_id_unidad', '#frm-4').html(datos);
			});
	}
	
}

	function selectNo(concepto)
	{
		concepto = concepto.split("_");
		var _concepto = "";
		for (var i = 0; i < concepto.length-1; i++) {
			_concepto += concepto[i] + "_";
		};
		concepto = _concepto.substring(0, _concepto.length-1).trim();
		$("#" + concepto + "_id_unidad",  "#frm-4").val('0');
		$("#" + concepto + "_cantidad",  "#frm-4").val('');
		$("#" + concepto + "_precio",  "#frm-4").val('');
		$("#" + concepto + "_importe",  "#frm-4").val('');
		$("#" + concepto + "_fecha",  "#frm-4").val('');
		$("#" + concepto + "_innovacion",  "#frm-4").val('');
	}

	$.ajax({
        url: "/backend/diagnosticos/obtenerdatosmejoramientofertilidad/",
        type: "POST",
        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
    }).done(function(data){
    	try
    	{
	    	var registro = jQuery.parseJSON(data);
	    	registrofrm4 = registro;
	    	if(registro.error !== undefined) 
	    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
	    	else if(registro.noexiste === undefined)
	    	{
				$("#analisis_tecnica",  "#frm-4").val(registro.tecnica);
				for (var i = 0; i < conceptos.length; i++) {
					//alert(registro[conceptos[i] + "_cantidad"]);
	        		if(registro[conceptos[i] + "_realizado"] == 1)
	        		{
	        			$("#" + conceptos[i] + "_Si", "#frm-4").prop('checked', true);
	        			$("#" + conceptos[i] + "_Si", "#frm-4").trigger( "click" );
	        		}
	        		else
	        		{
						$("#" + conceptos[i] + "_No", "#frm-4").prop('checked', true);
	        			$("#" + conceptos[i] + "_No", "#frm-4").trigger( "click" );
					}
					if(registro[conceptos[i] + "_id_unidad"] == null) 
						$("#" + conceptos[i] + "_id_unidad",  "#frm-4").val('0');
					else $("#" + conceptos[i] + "_id_unidad", "#frm-4").val(registro[conceptos[i] + "_id_unidad"]);
					$("#" + conceptos[i] + "_cantidad", "#frm-4").val(registro[conceptos[i] + "_cantidad"]);
					$("#" + conceptos[i] + "_precio", "#frm-4").val(registro[conceptos[i] + "_precio"]);
					$("#" + conceptos[i] + "_importe", "#frm-4").val(registro[conceptos[i] + "_importe"]);
					$("#" + conceptos[i] + "_fecha", "#frm-4").val(registro[conceptos[i] + "_fecha"]);
					$("#" + conceptos[i] + "_innovacion", "#frm-4").val(registro[conceptos[i] + "_innovacion"]);
				}
				if($("#analisis_suelo_Si").is(":checked"))
				{
					for (var i = 0; i < conceptos_internos_fisica.length; i++) {
						$("#analisis_suelo_fisica_" + conceptos_internos_fisica[i], "#frm-4").val(registro["analisis_suelo_fisica_" + conceptos_internos_fisica[i]]);
					};

					for (var i = 0; i < conceptos_internos_quimica.length; i++) {
						$("#analisis_suelo_quimica_" + conceptos_internos_quimica[i], "#frm-4").val(registro["analisis_suelo_quimica_" + conceptos_internos_quimica[i]]);
					};
					
					doc = registro.documento;
					if(doc!='' && doc!="NULL" && doc!=null){
						arch = doc.split("/");
						arch = arch[2];
						$("#span_documento", "#frm-4").html('&nbsp; Adjuntar el análisis del suelo');
						$("#div_up1", "#frm-4").removeClass('col-xs-7').addClass('col-xs-3');
						$("#div_up2, #div_up3", "#frm-4").show();
					}else{
						$("#span_documento", "#frm-4").html('&nbsp; Adjuntar el análisis del suelo');
						//$("#div_up1", "#frm-4").removeClass('col-xs-7');
        				$("#div_up2, #div_up3", "#frm-4").hide();
						doc=null;					
					}
					
        			
				}
	    	}
			if(registro.congelado == 2)
			{
				$('#frm-4 *').attr("disabled", "disabled").unbind();
				dg_congelado = true;
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente: " + err.message, "Ok", "", false, "");
		}
    });

}

function congelarFrm4(){
			var conceptos = ["muestreo_suelo", "analisis_suelo", "incorporacion_rastrojo", "abonos_verdes", "composta", "lombricomposta", "lixiviados", "remineralizacion", "descompactadores", "microorganismos", "cal", "dolomita", "yeso", "estiercol", "otro"];
			var conceptos_internos_fisica = ["arcilla", "arena", "profundidad", "textura", "limo"];
			var conceptos_internos_quimica = ["cah", "cic", "iht", "mo", "ph", "sb"];

			for (var i = 0; i < conceptos.length; i++) { if($("#" + conceptos[i] + "_Si", "#frm-4").is(":checked")) $("#" + conceptos[i] + "_fecha", "#frm-4").removeAttr('disabled'); }
			var frm = new FormData(document.getElementById("frm-4"));	
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			frm.append("hay_documento", doc);
			$.ajax({
	            url: "/backend/diagnosticos/guardarmejoramientofertilidad/",
	            type: "POST",
	            data: frm,
	            enctype: 'multipart/form-data',
	            contentType: false,
			    processData: false
	        }).done(function(respuesta){
	        	try
	        	{
	        		respuesta = jQuery.parseJSON(respuesta);
		        	for (var i = 0; i < conceptos.length; i++) { 
						$("#" + conceptos[i] + "_fecha", "#frm-4").attr("disabled","disabled"); 
					}
		        	if(respuesta.ok !== undefined)
		        	{
							$.ajax({
							url: "/backend/diagnosticos/congelarmejoramientofertilidad/",
							type: "POST",
							data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
							async: false
							}).done(function(respuesta){
								try
								{
									respuesta = jQuery.parseJSON(respuesta);
									if(respuesta.ok !== undefined){
										ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", false, "");
										$('#frm-4 *').attr("disabled", "disabled").unbind();	
									}
									else
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
								catch(err)
								{
									ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
							});

		        		//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
		        	}else if(respuesta.error=="Error saving file"){
						ShowDialogBox("Archivo adjunto", "No se ha cargado ningún archivo para el análisis de suelo", "Ok", "", false, "");
					}
		        	else 
						ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					for (var i = 0; i < conceptos.length; i++) { 
						$("#" + conceptos[i] + "_fecha", "#frm-4").attr("disabled","disabled"); 
					}
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });

	
}

function validaFrmMejoramiento(conceptos, fisica, quimica)
{
	return true;
	if($("#gen-type").val()==1){
		var conceptos_internos = ["_cantidad", "_precio", "_importe", "_fecha"];
	}else{
		var conceptos_internos = ["_cantidad", "_precio", "_importe", "_fecha","_innovacion"];
	}
	var conceptos_internos = ["_cantidad", "_precio", "_importe", "_fecha","_innovacion"];
	for (var i = 0; i < conceptos.length; i++)
	{
		
		if($("#" + conceptos[i] + "_Si").is(':checked'))
		{  
			if(conceptos[i]!="muestreo_suelo" && conceptos[i]!="analisis_suelo"){
				if($("#" + conceptos[i] + "_id_unidad").val() == 0){
					ShowDialogBox("Validación", "El campo Unidad es obligatorio", "Ok", "", false, "");
					return false;
				}
			}
			
			

			for (var j = 0; j < conceptos_internos.length; j++)
			{
				//alert(conceptos[i].toString() +conceptos_internos[j].toString());
				if($("#" + conceptos[i] + conceptos_internos[j]).val() == "")
				{
					var nombreCampo='';
					nombreCampo = conceptos_internos[j].substring(1, conceptos_internos[j].length);
					if(nombreCampo=="precio"){
						nombreCampo="Costo unitario";
					}else if(nombreCampo=="importe"){
						nombreCampo="Costo total";
					}
					
					ShowDialogBox("Validación", "El campo " + nombreCampo  + " es obligatorio", "Ok", "", false, "");
					return false;
				}
			}

			if($("#" + conceptos[i] + "_innovacion").val() == '' && $("#gen-type").val() != 1)
			{
				ShowDialogBox("Validación", "El campo innovación es obligatorio", "Ok", "", false, "");
				return false;
			}
        } 
	};

	if($("#analisis_suelo_Si").is(":checked"))
	{
		//$('#analisis_tecnica',"#frm-4").css("background-color","#F00");
		if($('#analisis_tecnica',"#frm-4").val()==''){
			ShowDialogBox("Validación", "El campo Técnica utilizada es obligatorio", "Ok", "", false, "");
			return false;
		}
		
		for (var i = 0; i < fisica.length; i++) {
			if($("#analisis_suelo_fisica_" + fisica[i]).val().trim() == '')
			{
				ShowDialogBox("Validación", "El campo " + fisica[i]  + " es obligatorio", "Ok", "", false, "");
				return false;
			}
		};

		for (var i = 0; i < quimica.length; i++) {
			if($("#analisis_suelo_quimica_" + quimica[i]).val().trim() == '')
			{
				ShowDialogBox("Validación", "El campo " + quimica[i]  + " es obligatorio", "Ok", "", false, "");
				return false;
			}
		};
	}
	
	return true;
}

function calculaImporteFrm44(concepto){
	//alert("hola mundo");
	concepto = concepto.split("_");
	var _concepto = "";
	for (var i = 0; i < concepto.length-1; i++) {
		_concepto += concepto[i] + "_";
	};
	
	concepto = _concepto.substring(0, _concepto.length-1).trim();
	if(concepto=="muestreo_suelo" || concepto=="analisis_suelo"){
		if(parseFloat($("#" + concepto + "_precio", "#frm-4").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-4").val(parseFloat($("#" + concepto + "_precio", "#frm-4").val()));
	else
		$("#" + concepto + "_importe", "#frm-4").val('');
	}else{
		if(parseFloat($("#" + concepto + "_cantidad", "#frm-4").val()) >= 0 && parseFloat($("#" + concepto + "_precio", "#frm-4").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-4").val(parseFloat($("#" + concepto + "_cantidad", "#frm-4").val()) * parseFloat($("#" + concepto + "_precio", "#frm-4").val()));
	else
		$("#" + concepto + "_importe", "#frm-4").val('');
	}
	
	//alert("split"+concepto);
	/*if(/*parseFloat($("#" + concepto + "_cantidad", "#frm-4").val()) >= 0 && parseFloat($("#" + concepto + "_precio", "#frm-4").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-4").val(parseFloat($("#" + concepto + "_cantidad", "#frm-4").val()) * parseFloat($("#" + concepto + "_precio", "#frm-4").val()));
	else
		$("#" + concepto + "_importe", "#frm-4").val('');*/
}