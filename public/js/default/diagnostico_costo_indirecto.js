$(document).ready(function(){

});

function cargarCostoIndirecto()
{
	$('#btn-save-ci', "#frm-17").once('click', function(){
		if(validaFrm17())
		{
			var frm = new FormData(document.getElementById("frm-17"));	
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarcostoindirecto/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	try
	        	{
	        		respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        	}
		        	else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });
		}
	});
	
	$("#btn-congelar-ci", "#frm-17").once('click', function(){
		if(validaFrm17()){
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podra revertise, ¿Desea continuar?", congelarFrm17);
		}
	});

	$('#btn-clean-ci', "#frm-17").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$("#frm-17").get(0).reset();
		});
	});

	$.ajax({
        url: "/backend/diagnosticos/obtenerdatoscostoindirecto/",
        type: "POST",
        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
    }).done(function(data){
    	try
    	{
	    	var registro = jQuery.parseJSON(data);
	    	if(registro.error !== undefined) 
	    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, intentelo nuevamente", "Ok", "", false, "");
	    	else if(registro.nodata === undefined)
	    	{
	    		$("#trilla_corte", "#frm-17").val(registro.trilla_corte);
	    		$("#flete", "#frm-17").val(registro.flete);
	    		$("#seguro_agricola_input", "#frm-17").val(registro.seguro_agricola);
	    		$("#asistencia_tecnica", "#frm-17").val(registro.asistencia_tecnica);
	    		$("#gasto_almacen", "#frm-17").val(registro.gasto_almacen);
	    		$("#cobertura_precio", "#frm-17").val(registro.cobertura_precio);
	    		$("#garantia_liquida", "#frm-17").val(registro.garantia_liquida);
				
				//recorremos los radios y los seteamos
				$("#frm-17 input[type='radio']").each(function(index, element) {
					id=this.id;
					trozos = id.split("-");
					concepto = trozos[0];
					input = trozos[1];
					if(input=="Si"){
						habilitaCI(this);
					}
					
				});
				if(registro.congelado == 2)
					$('#frm-17 *').attr("disabled", "disabled").unbind();
	    	}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, intentelo nuevamente: " + err.message, "Ok", "", false, "");
		}
    });




	

}

function congelarFrm17(){
	
			var frm = new FormData(document.getElementById("frm-17"));	
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarcostoindirecto/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	try
	        	{
	        		respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
							$.ajax({
							url: "/backend/diagnosticos/congelarcostoindirecto/",
							type: "POST",
							data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
							async: false
							}).done(function(respuesta){
								try
								{
									respuesta = jQuery.parseJSON(respuesta);
									if(respuesta.ok !== undefined){
										ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", false, "");
										$('#frm-17 *').attr("disabled", "disabled").unbind();	
									}
									else
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
								catch(err)
								{
									ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
							});

		        		//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        	}
		        	else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });

}

function validaFrm17()
{
	return true;
	var bandera = true;
	//recorremos los radios y los seteamos
	$("#frm-17 input[type='radio']").each(function(index, element) {
        id= this.id;
		trozos = id.split("-");
		
		concepto = trozos[0];
		input = trozos[1];
		if($("#"+id).is(':checked') && $("#"+concepto).val()<=0 && $("#"+id).val()=='1'){
			ShowDialogBox("Datos incompletos", "Hay datos faltantes, por favor revise los datos que este completos", "Ok", "", false, "");
			bandera= false;
		}
    });
	return bandera;
}

function habilitaCI(selector){
	id= selector.id;
	trozos = selector.id.split("-");
	concepto = trozos[0];
	input = trozos[1];
	//alert(concepto);
	//comprobamos el valor del selector para habilitar o deshabilitar
	if($("#"+concepto+'-Si').is(':checked')){
		//habilitamos el campo
		$('#'+concepto+'-Si').attr('checked', true);
		$("#"+concepto+'-No').attr('checked', false);
		//quitamos el attr readonly del elemento
		$('#'+concepto).removeAttr("readonly");
	}else if($("#"+concepto+'-No').is(':checked')){
		//deshabilitamos el campo
		$('#'+concepto+'-Si').attr('checked', false);
		$("#"+concepto+'-No').attr('checked', true);
		//deshabilitamos el attr readonly del elemento
		$('#'+concepto).attr("readonly");
		$('#'+concepto).val('0');
	}else{
		if($('#'+concepto).val()>0){
			//habilitamos el campo
			$('#'+concepto+'-Si').attr('checked', true);
			$("#"+concepto+'-No').attr('checked', false);
			//quitamos el attr readonly del elemento
			$('#'+concepto).removeAttr("readonly");
		}else{
			//deshabilitamos el campo
			$('#'+concepto+'-Si').attr('checked', false);
			$("#"+concepto+'-No').attr('checked', true);
			//deshabilitamos el attr readonly del elemento
			$('#'+concepto).attr("readonly");
			$('#'+concepto).val('0');
		}
		
	}
	
}