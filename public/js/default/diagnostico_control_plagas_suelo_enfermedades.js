var dg_congelado = false;
var MisDatos = new Array();
var cargado = 0;



$(document).ready(function(){
	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
			$('#frm-13_enfermedades #tabla tr:eq(0)').each(function(index, element) {
					//$(this).find('th:eq(7)').remove();
					$(this).find('th:eq(7)').remove();
			});
			$('#frm-13_enfermedades #tabla tr').not('#frm-13_enfermedades #tabla tr:eq(0)').each(function(index, element) {
					//$(this).find('th:eq(7)').remove();
					$(this).find('td:eq(6)').remove();
			});
	}
	
	
	
	if($("#frm-13_enfermedades #congelado").val() == 2){
		$('#frm-13_enfermedades *').attr("disabled", "disabled").unbind();	
	}
	
	$('#btnGuardarControlPlagasSueloEnfermedades').once('click', function()
	{
		if(validarFrm13())
		{
			$.ajax({
				url: "/backend/diagnosticostemp/guardar-control-plagas-de-suelo/",
				type: "POST",
				data: $("#frm-13_enfermedades").serialize()
			}).done(function(respuesta)
			{
				$('#frm-13_enfermedades #id').val(respuesta);
				ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", false, "", false);
			});
		}else
		{
			ShowDialogBox("Validación", "El formulario está incompleto; favor de completarlo.", "Ok", "", false, "");
		}
	});


	$('#btnCongelarControlPlagasSuelo').once('click', function()
	{
		if(validarFrm13())
		{
			$.ajax({
			url: "/backend/diagnosticostemp/guardar-control-plagas-de-suelo/",
			type: "POST",
			data: $("#frm-13_enfermedades").serialize()
			}).done(function(respuesta)
			{
				$('#frm-13_enfermedades #id').val(respuesta);
				$.ajax({
				url: "/backend/diagnosticostemp/congelar-control-plagas-de-suelo/",
				type: "POST",
				data: $("#frm-13_enfermedades").serialize()
				}).done(function(respuesta)
				{
					if(respuesta==1){
						$('#frm-13_enfermedades #congelado').val('2');
						$('#frm-13_enfermedades *').attr("disabled", "disabled").unbind();	
						ShowDialogBox("Guardado", "La información se ha congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/plagas_suelo/1");
					}
				});

				//ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", false, "", false);
			});
			
		}
		else
		{
			ShowDialogBox("Validación", "El formulario está incompleto; favor de completarlo.", "Ok", "", false, "");
		}
	});
	
	$('#btnLimpiarControlPlagasSuelo').once('click', function()
	{
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			//$('#frm-13_enfermedades input').val('');
			$('input','#frm-13_enfermedades').val('');
			$('#frm-13_enfermedades select').not("[id^='tipo_ap_select_gran_1']").not("[id^='tipo_ap_select_trat_1']").not("[id^='tipo_ap_select_rec_1']").not("[id^='tipo_ap_select_ctrl_1']").not("[id^='tipo_ap_select_otr_1']").each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
			$('#frm-13_enfermedades [id^="tipo_ap_select_gran_1"], #frm-13_enfermedades [id^="tipo_ap_select_trat_1"], #frm-13_enfermedades [id^="tipo_ap_select_rec_1"], #frm-13_enfermedades [id^="tipo_ap_select_ctrl_1"], #frm-13_enfermedades [id^="tipo_ap_select_otr_1"]').each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).val('2').change();
			});
		});
	});
	
});

function validarFrm13()
{
	return true;
	var bandera=true;
	//console.log("entra: ");
	$("#frm-13_enfermedades #tabla tr").not("#frm-13_enfermedades #tabla #fila-general-gran, #frm-13_enfermedades #tabla #fila-general-trat, #frm-13_enfermedades #tabla #fila-general-rec").each(function(i){	
		//console.log(this.id);
		$(this).find(".required").each(function(index, element) {
			if($(this).val()=="" || $(this).val()=="0")
			{
				console.log(this.id);
				$(this).focus();	
				bandera=false;
			}
		});

	});

	return bandera;
	
	/*var bandera=true;
	$("#frm-13_enfermedades .required").each(function(i){

		//console.log($(this).val());
		if($(this).val()=="" || $(this).val()=="0")
		{
			$(this).focus();	
			bandera=false;
		}

	});

	return bandera;*/
 
}
function evaluaCambioAplicacionFrm13Enf(elemento){
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").removeClass("required");
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").removeClass("required");
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").removeClass("required");
	
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").removeClass("required");
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").removeClass("required");

	
	if($("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #tipo_ap_select_" + elemento+"_1").val() == 1 || $("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #tipo_ap_select_" + elemento+"_1").val() == 3){
		//mecanizada
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #manual_div_"+elemento+"_1").addClass("hide");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_"+elemento+"_1").removeClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").addClass("hide");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_"+elemento+"_1 option:eq(0)").prop('selected', true);
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').addClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').addClass("required");

	}else{
		//manual
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #manual_div_"+elemento+"_1").removeClass("hide").addClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #mecanizada_div_"+elemento+"_1").addClass("hide").removeClass("required");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").val('').addClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").val('').addClass("required");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').removeClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').removeClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo"+elemento+"_1").removeClass("required");
	}
	
}
function evaluaCambioAplicacion2Frm13(elemento){
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').removeClass("required");
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').removeClass("required");
	$("#frm-13_enfermedades #despues_"+elemento+"_apli_enf_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").removeClass("required");

	
	if($("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_"+elemento+"_1").val() == 1){
		//mecanizada
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").val('').removeClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").val('').removeClass("required");

		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').addClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').addClass("required");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").addClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").val('').removeClass("required");


	}else{
		//manual
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_"+elemento+"_1").addClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").removeClass("hide").removeClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").val('').removeClass("required");
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").val('').removeClass("required");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").val('').removeClass("required");
		
		
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").removeClass("hide");
		$("#frm-13_enfermedades #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").val('').addClass("required");

		
	}
}
/*function calculaImporteFrm13(obj,id,hermano,receptor){
	
	var parent = $(obj).parents().get(1);
		//$(parent).css("background-color","#F00");
		if(id==undefined){
			var cantidad = $(parent).find(' td:eq(1) input').attr("value");
			var precio = $(parent).find(' td:eq(2) input').attr("value");
			var importe = $(parent).find(' td:eq(3) input');

		}else{
			var cantidad = $("#"+id).val();
			var precio = $("#"+hermano).val();
			var importe = $("#"+receptor);
		}
		alert(cantidad+"-"+precio+"-"+importe+"")
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		
		if(cantidad>0 && precio>0){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}*/
function calculaImporteFrm13Enf(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);

			if($(parent).attr("id")!=undefined && $(parent).attr("id").valueOf()=='despues_enf_gran_marca_1'){
				var cantidad = $(parent).find(' td:eq(1) input').attr("value");
				var precio = $(parent).find(' td:eq(2) input').attr("value");
				var importe = $(parent).find(' td:eq(3) input');
			}else{
				var cantidad = $(parent2).find(' td:eq(1) input').attr("value");
				var precio = $(parent2).find(' td:eq(2) input').attr("value");
				var importe = $(parent2).find(' td:eq(3) input');
			}
			
			//var hectareas = $('.semilla').attr("value");
			//alert(hectareas);
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		//hectareas = parseFloat(hectareas);
		
		if(cantidad>0 && precio>0){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}