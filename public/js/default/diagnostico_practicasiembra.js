var cnt = 0;

function cargarPracticaSiembra(){

	//limpiarFrm8();

	//alert("limpia");
	cnt++;
	
    $('#btn-save-pds', "#frm-8").once('click', function(){
    	if(validaFrm8())
    	{
	        var preId = $("#gen-id").val();
	        var frm = new FormData(document.getElementById("frm-8"));
	        frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
	        $.ajax({
	            url: "/backend/diagnosticos/guardarpracticadesiembra/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(data){
	        	try
	        	{
		            data = jQuery.parseJSON(data);
		            if(data.ok !== undefined)
		            	ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		            else
		            	ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });
	    }
    });
	
	$("#btn-congelar-pds", "#frm-8").once('click', function(){
		if(validaFrm8())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm8);
	});
	
    $('#btn-clean-pds', "#frm-8").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?",limpiarFrm8);
    });

    $.ajax({
        url: "/backend/diagnosticos/obtenerdatospracticadesiembra/",
        type: "POST",
        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
    }).done(function(data){
    	try
    	{
    		limpiarFrm8();
	        var registro = jQuery.parseJSON(data);
			//alert(registro.mecanizada_tipo);
	        if(registro.error !== undefined) 
	            ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
	        else if(registro.noexiste === undefined)
	        {
	           	$("#tipo_ap_select_prasie_1", "#frm-8").val(registro.aplicacion_tipo);
				//alert(registro.mecanizada_tipo);
				if(registro.aplicacion_tipo==1){
						$("#manual_div_prasie_1", "#frm-8").addClass("hide");
						$("#mecanizada_div_prasie_1", "#frm-8").removeClass("hide");
						if(registro.mecanizada_tipo==1){
							$("#mecanizada_div_select_div_propia_prasie_1", "#frm-8").removeClass("hide");
							$("#mecanizada_div_select_div_rentada_prasie_1", "#frm-8").addClass("hide");
						}else{
							$("#mecanizada_div_select_div_propia_prasie_1", "#frm-8").addClass("hide");
							$("#mecanizada_div_select_div_rentada_prasie_1", "#frm-8").removeClass("hide");
							
							$("#manual_div_no_jornales_prasie_1", "#frm-8").removeClass("hide");
							//$("#mecanizada_div_select_prasie_1", "#frm-8").addClass("hide");
						}
					}else{
						$("#manual_div_no_jornales_prasie_1", "#frm-8").removeClass("hide");
						//$("#mecanizada_div_select_prasie_1", "#frm-8").addClass("hide");
						
					}
	            //$("#tipo_ap_select_prasie_1", "#frm-8").change();
	            $("#manual_div_no_jornales_prasie_1", "#frm-8").val(registro.manual_no_jornales);
	            $("#manual_div_costo_jornal_prasie_1", "#frm-8").val(registro.manual_costo_jornal);
	            $("#mecanizada_div_select_prasie_1", "#frm-8").val(registro.mecanizada_tipo);
	            //$("#mecanizada_div_select_prasie_1", "#frm-8").change();
	            $("#mecanizada_div_select_div_propia_combustible_prasie_1", "#frm-8").val(registro.mecanizada_propia_costo_combustible);
	            $("#mecanizada_div_select_div_propia_operador_prasie_1", "#frm-8").val(registro.mecanizada_propia_costo_operador);
	            $("#mecanizada_div_select_div_rentada_costo_prasie_1", "#frm-8").val(registro.mecanizada_rentada_monto);
	        	if(registro.congelado == 2) $('#frm-8 *').attr("disabled", "disabled").unbind();
	        }
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function validaFrm8()
{
	return true;
	if($("#tipo_ap_select_prasie_1").val() == 1 ) //Meanizada
	{  
		if($("#mecanizada_div_select_prasie_1").val() == 1 ) //propia
		{
			if($("#mecanizada_div_select_div_propia_combustible_prasie_1").val() == "")
			{
				ShowDialogBox("Validación", "El Costo del combustible es obligatorio", "Ok", "", false, "");
				$("#mecanizada_div_select_div_propia_combustible_prasie_1").focus();
				return false;
			}
			if($("#mecanizada_div_select_div_propia_operador_prasie_1").val() == ""){
				ShowDialogBox("Validación", "El Costo del operador es obligatoria", "Ok", "", false, "");
				$("#manual_div_costo_jornal_prasie_1").focus();
			
				return false;
			}
		}
			
		if($("#mecanizada_div_select_prasie_1").val() == 2 ) //rentada
		{
			if($("#mecanizada_div_select_div_rentada_costo_prasie_1").val() == "")
			{
				ShowDialogBox("Validación", "El Monto de la renta es obligatorio", "Ok", "", false, "");
				$("#mecanizada_div_select_div_propia_combustible_prasie_1").focus();
				return false;
			}
		}
	}
		
	if($("#tipo_ap_select_prasie_1").val() == 2) //manual
	{  
		if($("#manual_div_no_jornales_prasie_1").val() == "")
		{
			ShowDialogBox("Validación", "El número de jornales es obligatorio", "Ok", "", false, "");
			$("#manual_div_no_jornales_prasie_1").focus();
			return false;
		}
		
		if($("#manual_div_costo_jornal_prasie_1").val() == "")
		{
			ShowDialogBox("Validación", "El Costo por jornal es obligatoria", "Ok", "", false, "");
			$("#manual_div_costo_jornal_prasie_1").focus();
			return false;
		}		
	}
	return true;
}

function congelarFrm8(){
	var preId = $("#gen-id").val();
	var frm = new FormData(document.getElementById("frm-8"));
	frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
	$.ajax({
	url: "/backend/diagnosticos/guardarpracticadesiembra/",
	type: "POST",
	data: frm,
	processData: false,  
	contentType: false
	}).done(function(data){
		try
		{
		  data = jQuery.parseJSON(data);
		 if(data.ok !== undefined){
			 	$.ajax({
				url: "/backend/diagnosticos/congelarpracticasiembra/",
				type: "POST",
				data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
				async: false 
				}).done(function(respuesta){
					try
					{
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
							ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", false, "");
							$('#frm-8 *').attr("disabled", "disabled").unbind();
						}
						else
							ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
					}
					catch(err)
					{
						ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
					}	
				});
			//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		 }else
			ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
		}
	});
	
}

function limpiarFrm8(){
	$('#frm-8 *').removeAttr("disabled");
	$("#tipo_ap_select_prasie_1", "#frm-8").val('2');
    $("#manual_div_no_jornales_prasie_1", "#frm-8").val('');
    $("#manual_div_costo_jornal_prasie_1", "#frm-8").val('');
    $("#mecanizada_div_select_prasie_1", "#frm-8").val('1');
    $("#mecanizada_div_select_div_propia_combustible_prasie_1", "#frm-8").val('');
    $("#mecanizada_div_select_div_propia_operador_prasie_1", "#frm-8").val('');
    $("#mecanizada_div_select_div_rentada_costo_prasie_1", "#frm-8").val('');
    $("#tipo_ap_select_prasie_1", "#frm-8").change();
    $("#mecanizada_div_select_prasie_1", "#frm-8").change();
}