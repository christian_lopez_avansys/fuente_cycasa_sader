$(document).ready(function(){
	$("#cantidad, #ph").once('keyup', function(){
		calculaImporteFrm2();
	});
});

function cargarRentaTierra(){
	
	var conceptos = ["renta"];
	$('#btn-save-rdlt', "#frm-2").once('click', function(){
		//$("#fecha", "#frm-2").removeAttr('disabled');
		
		if(validaFrm2()){
		
		var preId = $("#gen-id").val();
		var frm = new FormData(document.getElementById("frm-2"));
		frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
		
		$.ajax({
            url: "/backend/diagnosticos/guardarrentadelatierra/",
            type: "POST",
            data: frm,
            processData: false,  
            contentType: false
        }).done(function(data){
        	try {
	        	data = jQuery.parseJSON(data);
	        	//$("#fecha", "#frm-2").attr("disabled","disabled");
				if(data.ok !== undefined)
	        	{
	        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
	        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data+"/renta_tierra/1";
	        	}
	        	else ShowDialogBox("Ha ocurrido un error", "No se ha podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error", "No se ha podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
			}
        });
		
		}
	});

	$("#btn-congelar-rdlt", "#frm-2").once('click', function(){		
		if(validaFrm2())
		ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm2);
	});


	$('#btn-clean-rdlt', "#frm-2").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$("#rbSi", "#frm-2").prop('checked', false);
			$("#rbNo", "#frm-2").prop('checked', false);
			$("#id_unidad",  "#frm-2").val('0');
			$("#cantidad",  "#frm-2").val('');
			$("#ph",  "#frm-2").val('');
			$("#importe",  "#frm-2").val('');
			$("#fecha",  "#frm-2").val('');
		});
		
	});

	$('#rbNo', "#frm-2").once('click', function(){
		$("#id_unidad",  "#frm-2").val('0');
		$("#cantidad",  "#frm-2").val('');
		$("#ph",  "#frm-2").val('');
		$("#importe",  "#frm-2").val('');
		$("#fecha",  "#frm-2").val('');
	
	});
	
	//asignamos las unidades a los conceptos dependiendo la configuracion general
	obtenerUnidadesPorFormularioConceptoFrm2(conceptos);
	/*$.ajax({
        url: "/backend/diagnosticos/obtenerunidades/",
        type: "POST",
        async: false,
        processData: false,  
        contentType: false
    }).done(function(data){
        $("#id_unidad",  "#frm-2").html(data);
    });*/
    //[2020-06-04]Agregado por Christian López para resover el problema de asincronía 
    //con respecto a las unidades de medida
    setTimeout(function()
    { 	
		$.ajax({
	        url: "/backend/diagnosticos/obtenerdatosrentadelatierra/",
	        type: "POST",
	        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
	    }).done(function(data)
	    {
	    	try 
	    	{
		    	var registro = jQuery.parseJSON(data);

		    	if(registro.error !== undefined) 
		    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
		    	else if(registro.nodata === undefined)
		    	{
		    		$('#frm-2 *').attr("disabled", false)
		    		if(registro.rentado == 1)
		    		{
		    			$("#rbSi", "#frm-2").prop('checked', true);
		    			$("#rbSi", "#frm-2").trigger( "click" );
		    		}
		    		else
		    		{
						$("#rbNo", "#frm-2").prop('checked', true);
						$("#rbNo", "#frm-2").trigger( "click" );
					}
					if(registro.id_unidad == null) 
						$("#id_unidad",  "#frm-2").val('0');
					else 
						$("#id_unidad", "#frm-2").val(registro.id_unidad);
						$("#cantidad", "#frm-2").val(registro.cantidad);
						$("#ph", "#frm-2").val(registro.precio);
						$("#importe", "#frm-2").val(registro.importe);
						$("#fecha", "#frm-2").val(registro.fecha_realizacion);
					
					if(registro.congelado == 2) 
						$('#frm-2 *').attr("disabled", "disabled").unbind();
		    	}
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
			}
			
	    });
    }, 2000);

}
function obtenerUnidadesPorFormularioConceptoFrm2(conceptos){
	for (var i = 0; i < conceptos.length; i++) {
			$.ajax({
				url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
				type: "POST",
				data:{formulario:"RentaDeLaTierra",concepto:conceptos[i]}
			}).done(function(data){
				concepto = data.split("|")[0];
				datos = data.split("|")[1]
				$('#id_unidad', '#frm-2').html(datos);
			});
	}
	
}
function congelarFrm2(){
	$("#fecha", "#frm-2").removeAttr('disabled');
		var preId = $("#gen-id").val();
		var frm = new FormData(document.getElementById("frm-2"));
		frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
		
		$.ajax({
            url: "/backend/diagnosticos/guardarrentadelatierra/",
            type: "POST",
            data: frm,
            processData: false,  
            contentType: false
        }).done(function(data){
        	try {
	        	data = jQuery.parseJSON(data);
	        	//$("#fecha", "#frm-2").attr("disabled","disabled");
				if(data.ok !== undefined)
	        	{
	        			$.ajax({
						url: "/backend/diagnosticos/congelarrentatierra/",
						type: "POST",
						data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
						async: false
						}).done(function(respuesta){
							respuesta = jQuery.parseJSON(respuesta);
							
							try {
							if(respuesta.ok !== undefined){
								$('#frm-2 *').attr("disabled", "disabled").unbind();
								ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true,"/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/"+$("#gen-id").val()+"/renta_tierra/1");
								
							}
								
							else
								ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
							}
							catch(err) {
								ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
							}
								
						});

					//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
	        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data+"/renta_tierra/1";
	        	}
	        	else ShowDialogBox("Ha ocurrido un error", "No se ha podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error", "No se ha podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
			}
        });

}

function validaFrm2(){
return true;
if($("#rbSi").is(':checked')) {  
			if($("#id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
				$("#id_unidad").focus();
				
				return false;
			}
			
			if($("#cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#cantidad").focus();
				
				return false;
			}
			
			if($("#ph").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#ph").focus();
				
				return false;
			}
			
			if($("#importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatoria  es obligatorio", "Ok", "", false, "");
				$("#importe").focus();
				
				return false;
			}
			
			if($("#fecha").val() == ""){
				ShowDialogBox("Validación", "El Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#fecha').attr('readonly', 'readonly');
			}
        } 
		
		return true;

}

function calculaImporteFrm2(){
	if(parseFloat($("#cantidad", "#frm-2").val()) >= 0 && parseFloat($("#ph", "#frm-2").val()) >= 0)
		$("#importe", "#frm-2").val(parseFloat($("#cantidad", "#frm-2").val()) * parseFloat($("#ph", "#frm-2").val()));
	else
		$("#importe", "#frm-2").val('');
}