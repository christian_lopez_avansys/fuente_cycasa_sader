var dg_congelado = false;
var MisDatos = new Array();
var cargadoMaleza = 0;
var pre_unidad ="";
var post_unidad ="";
var des_unidad ="";
var otro_unidad ="";


$(document).ready(function(){



});

function cargarControlMalezas()
{
	if($("#gen-type").val() == 1) 
		$(".inno", "#frm-11").attr("disabled", "disabled");
	else 
		$(".inno", "#frm-11").removeAttr("disabled");
	
	
	
	$.ajax({
        url: "/backend/diagnosticos/obtenerdatoscontroldemaleza/",
        type: "POST",
        data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
    	var info;
    	try
    	{
	        respuesta = jQuery.parseJSON(respuesta);
			
	        if(respuesta.ok !== undefined)
	        {
				if(respuesta.congelado == 2){
					$('#frm-12 *').attr("disabled", "disabled").unbind();
				}
				//lleno los clones
				//alert($("#frm-12 #tabla #pre_marca_marca_").html().toString());
				$("#frm-12 #registro").val(respuesta.general.id);
				$("#frm-12 #tabla #pre_marca_empresa_").html(respuesta.select_empresa);
				$("#frm-12 #tabla #pre_marca_marca_").html();
				//$("#frm-12 #tabla #pre_marca_marca_").html(respuesta.select_marca);
				//$("#frm-12 #tabla #pre_marca_fuente_").html(respuesta.select_fuente);
				$("#frm-12 #tabla #pre_marca_unidad_").html(respuesta.select_pre_unidad);//comentada
				$("#frm-12 #tabla #pre_marca_etapa_fenologica_").html(respuesta.select_etapa_fenologica);
				$("#frm-12 #tabla #pre_coad_unidad_").html(respuesta.select_unidad);
				$("#frm-12 #tabla #pre_coad_etapa_fenologica_").html(respuesta.select_etapa_fenologica);
				
				pre_unidad =respuesta.select_pre_unidad;
				post_unidad =respuesta.select_post_unidad;
				des_unidad =respuesta.select_des_unidad;
				otro_unidad =respuesta.select_otro_unidad;
				
				$("#frm-12 #tabla #pre_acidificacion_unidad_1").html(respuesta.select_post_unidad); //comentada
				$("#frm-12 #tabla #pre_acidificacion_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);

				if(cargadoMaleza != 1)
				{//validacion para no cargar 2 veces el form
					cargadoMaleza=1;
					//desaparecemos la columna de innovaacion para cuando sea diagnostico
					if($("#gen-type").val()==1){
						$('#frm-12 #tabla tr:eq(0)').each(function(index, element) {
									//$(this).find('th:eq(7)').remove();
									$(this).find('th:eq(7)').remove();
						});
						$('.pre').each(function(index, element) {
									//$(this).find('th:eq(7)').remove();
									$(this).find('td:eq(6)').remove();
						});
						$('.post').each(function(index, element) {
									//$(this).find('th:eq(7)').remove();
									$(this).find('td:eq(6)').remove();
						});
						$('.des').each(function(index, element) {
									//$(this).find('th:eq(7)').remove();
									$(this).find('td:eq(6)').remove();
						});
						$('.otro').each(function(index, element) {
									//$(this).find('th:eq(7)').remove();
									$(this).find('td:eq(6)').remove();
						});
						$('.hpre').each(function(index, element) {
									//$(this).find('th:eq(7)').remove();
									$(this).find('td:eq(6)').remove();
						});
					}
					//asignamos los datos generales
					if(respuesta.general!=null){
						$("#frm-12 #tabla #otro_unidad").html(respuesta.general.lunidad);
						$("#frm-12 #tabla #otro_cantidad").val(respuesta.general.lcantidad);
						$("#frm-12 #tabla #otro_precio").val(respuesta.general.lprecio);
						$("#frm-12 #tabla #otro_importe").val(respuesta.general.limporte);
						$("#frm-12 #tabla #otro_frealizacion").val(respuesta.general.lfrealizacion);
						$("#frm-12 #tabla #otro_etapa_fenologica").html(respuesta.general.letapa_fenologica);
						$("#frm-12 #tabla #otro_innovacion").val(respuesta.general.linnovacion);
						$("#frm-12 #tabla #otro_tipo_ap").val(respuesta.general.laplicacion).trigger("change");
						$("#frm-12 #tabla #manual_div_otro #manual_div_no_jornales_otro").val(respuesta.general.lnumero_jornales);
						$("#frm-12 #tabla #manual_div_otro #manual_div_costo_jornal_otro").val(respuesta.general.lcosto_jornal);
						$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_otro").val(respuesta.general.ltipo_mecanizacion).trigger("change");
						//alert(respuesta.general.lcosto_combustible);
						$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_otro #mecanizada_div_select_div_propia_combustible_otro").val(respuesta.general.lcosto_combustible);
						$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_otro #mecanizada_div_select_div_propia_operador_otro").val(respuesta.general.lcosto_operador);
						$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_rentada_otro #mecanizada_div_select_div_rentada_costo_otro").val(respuesta.general.lmonto_renta);
						
						$("#frm-12 #tabla #otro_escarda_unidad").html(respuesta.general.eunidad);
						$("#frm-12 #tabla #otro_escarda_cantidad").val(respuesta.general.ecantidad);
						$("#frm-12 #tabla #otro_escarda_precio").val(respuesta.general.eprecio);
						$("#frm-12 #tabla #otro_escarda_importe").val(respuesta.general.eimporte);
						$("#frm-12 #tabla #otro_escarda_frealizacion").val(respuesta.general.efrealizacion);
						$("#frm-12 #tabla #otro_escarda_etapa_fenologica").html(respuesta.general.eetapa_fenologica);
						$("#frm-12 #tabla #otro_escarda_innovacion").val(respuesta.general.einnovacion);
						$("#frm-12 #tabla #otro_escarda_tipo_ap").val(respuesta.general.eaplicacion).trigger("change");
						$("#frm-12 #tabla #manual_div_otro_escarda #manual_div_no_jornales_otro_escarda").val(respuesta.general.enumero_jornales);
						$("#frm-12 #tabla #manual_div_otro_escarda #manual_div_costo_jornal_otro_escarda").val(respuesta.general.ecosto_jornal);
						$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_otro_escarda").val(respuesta.general.etipo_mecanizacion).trigger("change");
						$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda #mecanizada_div_select_div_propia_combustible_otro_escarda").val(respuesta.general.ecosto_combustible);
						$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda #mecanizada_div_select_div_propia_operador_otro_escarda").val(respuesta.general.ecosto_operador);
						$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda #mecanizada_div_select_div_rentada_costo_otro_escarda").val(respuesta.general.emonto_renta);
						
						$('#realizo_otro').val(respuesta.general.realizo_otro).trigger("change");
					}else{						
						$("#frm-12 #tabla #otro_unidad").html(respuesta.select_otro_unidad);
						$("#frm-12 #tabla #otro_etapa_fenologica").html(respuesta.select_etapa_fenologica);
						$("#frm-12 #tabla #otro_escarda_unidad").html(respuesta.select_unidad);
						$("#frm-12 #tabla #otro_escarda_etapa_fenologica").html(respuesta.select_etapa_fenologica);	
						
					}
					if(respuesta.general.acificacion!=null)
					{
						if(respuesta.general.acidificacion.pre!=null){
							$("#frm-12 #tabla #pre_acidificacion_unidad_1").html(respuesta.general.acidificacion.pre.unidad);
							$("#frm-12 #tabla #pre_acidificacion_cantidad_1").val(respuesta.general.acidificacion.pre.cantidad);
							$("#frm-12 #tabla #pre_acidificacion_precio_1").val(respuesta.general.acidificacion.pre.precio);
							$("#frm-12 #tabla #pre_acidificacion_importe_1").val(respuesta.general.acidificacion.pre.importe);
							$("#frm-12 #tabla #pre_acidificacion_frealizacion_1").val(respuesta.general.acidificacion.pre.frealizacion);
							$("#frm-12 #tabla #pre_acidificacion_etapa_fenologica_1").html(respuesta.general.acidificacion.pre.etapa_fenologica);
							$("#frm-12 #tabla #pre_acidificacion_innovacion_1").val(respuesta.general.acidificacion.pre.innovacion);
						}
						
						if(respuesta.general.acidificacion.post!=null){
							$("#frm-12 #tabla #post_acidificacion_unidad").html(respuesta.general.acidificacion.post.unidad);
							$("#frm-12 #tabla #post_acidificacion_cantidad").val(respuesta.general.acidificacion.post.cantidad);
							$("#frm-12 #tabla #post_acidificacion_precio").val(respuesta.general.acidificacion.post.precio);
							$("#frm-12 #tabla #post_acidificacion_importe").val(respuesta.general.acidificacion.post.importe);
							$("#frm-12 #tabla #post_acidificacion_frealizacion").val(respuesta.general.acidificacion.post.frealizacion);
							$("#frm-12 #tabla #post_acidificacion_etapa_fenologica").html(respuesta.general.acidificacion.post.etapa_fenologica);
							$("#frm-12 #tabla #post_acidificacion_innovacion").val(respuesta.general.acidificacion.post.innovacion);
						}
						
						if(respuesta.general.acidificacion.des!=null){
							$("#frm-12 #tabla #des_acidificacion_unidad").html(respuesta.general.acidificacion.des.unidad);
							$("#frm-12 #tabla #des_acidificacion_cantidad").val(respuesta.general.acidificacion.des.cantidad);
							$("#frm-12 #tabla #des_acidificacion_precio").val(respuesta.general.acidificacion.des.precio);
							$("#frm-12 #tabla #des_acidificacion_importe").val(respuesta.general.acidificacion.des.importe);
							$("#frm-12 #tabla #des_acidificacion_frealizacion").val(respuesta.general.acidificacion.des.frealizacion);
							$("#frm-12 #tabla #des_acidificacion_etapa_fenologica").html(respuesta.general.acidificacion.des.etapa_fenologica);
							$("#frm-12 #tabla #des_acidificacion_innovacion").val(respuesta.general.acidificacion.des.innovacion);
						}
					}
					if(respuesta.general.pre!=null){
						$('#realizo_pre').val('1').trigger("change");
							for( var i =0;  i < respuesta.general.pre.length; i++){
								if((i+1)< respuesta.general.pre.length){
										agregarFilaExtraFrm12('pre-marca');
								}
								//alert(respuesta.general.pre[i].empresa);
								//$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_empresa_"+(i+1)).css("background-color","#f00");
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #id").val(respuesta.general.pre[i].id);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_empresa_"+(i+1)).html(respuesta.general.pre[i].empresa); //borrar
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_marca_"+(i+1)).html(respuesta.general.pre[i].marca);
								//$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_fuente_"+(i+1)).html(respuesta.general.pre[i].fuente);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_unidad_"+(i+1)).html(respuesta.general.pre[i].unidad);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_cantidad_"+(i+1)).val(respuesta.general.pre[i].cantidad);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_precio_"+(i+1)).val(respuesta.general.pre[i].precio);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_importe_"+(i+1)).val(respuesta.general.pre[i].importe);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_frealizacion_"+(i+1)).val(respuesta.general.pre[i].frealizacion);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_etapa_fenologica_"+(i+1)).html(respuesta.general.pre[i].etapa_fenologica);
								$("#frm-12 #tabla #pre-marca_"+(i+1)+" #pre_marca_innovacion_"+(i+1)).val(respuesta.general.pre[i].innovacion);
								
		/*						$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_marca_"+(i+1)).html(respuesta.general.pre[i].cmarca);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_fuente_"+(i+1)).html(respuesta.general.pre[i].cfuente);
		*/						$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_unidad_"+(i+1)).html(respuesta.general.pre[i].cunidad);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_cantidad_"+(i+1)).val(respuesta.general.pre[i].ccantidad);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_precio_"+(i+1)).val(respuesta.general.pre[i].cprecio);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_importe_"+(i+1)).val(respuesta.general.pre[i].cimporte);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_frealizacion_"+(i+1)).val(respuesta.general.pre[i].cfrealizacion);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_etapa_fenologica_"+(i+1)).html(respuesta.general.pre[i].cetapa_fenologica);
								$("#frm-12 #tabla #pre-coad_"+(i+1)+" #pre_coad_innovacion_"+(i+1)).val(respuesta.general.pre[i].cinnovacion);
								
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #pre_tipo_apli").val(respuesta.general.pre[i].atipo).trigger("change");//('selected',true);
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #mecanizada_div #mecanizada_div_select").val(respuesta.general.pre[i].atipo_mecanizacion).trigger("change");
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #manual_div #manual_div_no_jornales").val(respuesta.general.pre[i].anumero_jornales);
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #manual_div #manual_div_costo_jornal").val(respuesta.general.pre[i].acosto_jornal);
								
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(respuesta.general.pre[i].acosto_combustible);
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(respuesta.general.pre[i].acosto_operador);
								$("#frm-12 #tabla #pre-apli_"+(i+1)+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(respuesta.general.pre[i].amonto_renta);
								//agregarTablaFrm12();
								
							}
					}else{
						$('#realizo_pre').val('2').trigger("change");
					}
					
					if(respuesta.general.post!=null){
						$('#realizo_post').val('1').trigger("change");
							for( var j =0;  j < respuesta.general.post.length; j++){
								if((j+1)< respuesta.general.post.length){
										agregarFilaExtraFrm12('post-marca');
								}
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #id").val(respuesta.general.post[j].id);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_empresa_"+(j+1)).html(respuesta.general.post[j].empresa);//borra
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_marca_"+(j+1)).html(respuesta.general.post[j].marca);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_fuente_"+(j+1)).html(respuesta.general.post[j].fuente);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_unidad_"+(j+1)).html(respuesta.general.post[j].unidad);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_cantidad_"+(j+1)).val(respuesta.general.post[j].cantidad);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_precio_"+(j+1)).val(respuesta.general.post[j].precio);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_importe_"+(j+1)).val(respuesta.general.post[j].importe);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_frealizacion_"+(j+1)).val(respuesta.general.post[j].frealizacion);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_etapa_fenologica_"+(j+1)).html(respuesta.general.post[j].etapa_fenologica);
								$("#frm-12 #tabla #post-marca_"+(j+1)+" #post_marca_innovacion_"+(j+1)).val(respuesta.general.post[j].innovacion);
								
		/*						$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_marca_"+(j+1)).html(respuesta.general.post[j].cmarca);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_fuente_"+(j+1)).html(respuesta.general.post[j].cfuente);
		*/						$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_unidad_"+(j+1)).html(respuesta.general.post[j].cunidad);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_cantidad_"+(j+1)).val(respuesta.general.post[j].ccantidad);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_precio_"+(j+1)).val(respuesta.general.post[j].cprecio);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_importe_"+(j+1)).val(respuesta.general.post[j].cimporte);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_frealizacion_"+(j+1)).val(respuesta.general.post[j].cfrealizacion);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_etapa_fenologica_"+(j+1)).html(respuesta.general.post[j].cetapa_fenologica);
								$("#frm-12 #tabla #post-coad_"+(j+1)+" #post_coad_innovacion_"+(j+1)).val(respuesta.general.post[j].cinnovacion);
								
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #post_tipo_apli").val(respuesta.general.post[j].atipo).trigger("change");
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #manual_div #manual_div_no_jornales").val(respuesta.general.post[j].anumero_jornales);
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #manual_div #manual_div_costo_jornal").val(respuesta.general.post[j].acosto_jornal);
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #mecanizada_div #mecanizada_div_select").val(respuesta.general.post[j].atipo_mecanizacion).trigger("change");
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(respuesta.general.post[j].acosto_combustible);
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(respuesta.general.post[j].acosto_operador);
								$("#frm-12 #tabla #post-apli_"+(j+1)+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(respuesta.general.post[j].amonto_renta);
								//agregarTablaFrm12();
								
							}
					}else{
						//lleno los select para post
						$("#frm-12 #tabla #post_marca_empresa_1").html(respuesta.select_empresa); //borraar
						$("#frm-12 #tabla #post_marca_marca_1").html(respuesta.select_marca);
						//$("#frm-12 #tabla #post_marca_fuente_1").html(respuesta.select_fuente);
						$("#frm-12 #tabla #post_marca_unidad_1").html(respuesta.select_post_unidad);
						$("#frm-12 #tabla #post_marca_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
						$("#frm-12 #tabla #post_coad_unidad_1").html(respuesta.select_post_unidad);
						$("#frm-12 #tabla #post_coad_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
						$("#frm-12 #tabla #post_acidificacion_unidad").html(respuesta.select_post_unidad);
						$("#frm-12 #tabla #post_acidificacion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
						$('#realizo_post').val('2').trigger("change");
					}
					
					if(respuesta.general.des!=null){
						$('#realizo_des').val('1').trigger("change");
							for( var w =0;  w < respuesta.general.des.length; w++){
								if((w+1)< respuesta.general.des.length){
										agregarFilaExtraFrm12('des-marca');
								}
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #id").val(respuesta.general.des[w].id);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_marca_"+(w+1)).html(respuesta.general.des[w].marca);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_empresa_"+(w+1)).html(respuesta.general.des[w].empresa);//borra
								//$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_fuente_"+(w+1)).html(respuesta.general.des[w].fuente);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_unidad_"+(w+1)).html(respuesta.general.des[w].unidad);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_cantidad_"+(w+1)).val(respuesta.general.des[w].cantidad);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_precio_"+(w+1)).val(respuesta.general.des[w].precio);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_importe_"+(w+1)).val(respuesta.general.des[w].importe);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_frealizacion_"+(w+1)).val(respuesta.general.des[w].frealizacion);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_etapa_fenologica_"+(w+1)).html(respuesta.general.des[w].etapa_fenologica);
								$("#frm-12 #tabla #des-marca_"+(w+1)+" #des_marca_innovacion_"+(w+1)).val(respuesta.general.des[w].innovacion);
								
								/*$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_marca_"+(w+1)).html(respuesta.general.des[w].cmarca);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_fuente_"+(w+1)).html(respuesta.general.des[w].cfuente);*/
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_unidad_"+(w+1)).html(respuesta.general.des[w].cunidad);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_cantidad_"+(w+1)).val(respuesta.general.des[w].ccantidad);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_precio_"+(w+1)).val(respuesta.general.des[w].cprecio);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_importe_"+(w+1)).val(respuesta.general.des[w].cimporte);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_frealizacion_"+(w+1)).val(respuesta.general.des[w].cfrealizacion);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_etapa_fenologica_"+(w+1)).html(respuesta.general.des[w].cetapa_fenologica);
								$("#frm-12 #tabla #des-coad_"+(w+1)+" #des_coad_innovacion_"+(w+1)).val(respuesta.general.des[w].cinnovacion);
								
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #des_tipo_apli").val(respuesta.general.des[w].atipo).trigger("change");
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #manual_div #manual_div_no_jornales").val(respuesta.general.des[w].anumero_jornales);
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #manual_div #manual_div_costo_jornal").val(respuesta.general.des[w].acosto_jornal);
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #mecanizada_div #mecanizada_div_select").val(respuesta.general.des[w].atipo_mecanizacion).trigger("change");
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(respuesta.general.des[w].acosto_combustible);
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(respuesta.general.des[w].acosto_operador);
								$("#frm-12 #tabla #des-apli_"+(w+1)+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(respuesta.general.des[w].amonto_renta);
								//agregarTablaFrm12();
								
							}
					}else{
						//lleno los select para des
						$("#frm-12 #tabla #des_marca_marca_1").html(respuesta.select_marca);
						$("#frm-12 #tabla #des_marca_empresa_1").html(respuesta.select_empresa);
						//$("#frm-12 #tabla #des_marca_fuente_1").html(respuesta.select_fuente);
						$("#frm-12 #tabla #des_marca_unidad_1").html(respuesta.select_des_unidad);
						$("#frm-12 #tabla #des_marca_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
						
						$("#frm-12 #tabla #des_coad_unidad_1").html(respuesta.select_des_unidad);
						$("#frm-12 #tabla #des_coad_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
						$("#frm-12 #tabla #des_acidificacion_unidad").html(respuesta.select_des_unidad);
						$("#frm-12 #tabla #des_acidificacion_etapa_fenologica").html(respuesta.select_etapa_fenologica);

						$('#realizo_des').val('2').trigger("change");
					}
					
	        	}//validacion de cargado
				
			}else if(respuesta.noexiste === undefined){
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el formulario, por favor intentelo nuevamente", "Ok", "", false, "");
			}else{//formulario nuevo, cargo la informacion de los catalogos en los select
				//lleno los select para pre y los clones tambien
				$("#frm-12 #tabla #pre_marca_empresa_1").html(respuesta.select_empresa);
				$("#frm-12 #tabla #pre_marca_marca_1").html();
				//$("#frm-12 #tabla #pre_marca_marca_1").html(respuesta.select_marca);
				//$("#frm-12 #tabla #pre_marca_fuente_1").html(respuesta.select_fuente);
				//alert(respuesta.select_pre_unidad);
				$("#frm-12 #tabla #pre_marca_unidad_1").html(respuesta.select_pre_unidad);
				$("#frm-12 #tabla #pre_marca_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #tabla #pre_coad_unidad_1").html(respuesta.select_pre_unidad);
				$("#frm-12 #tabla #pre_coad_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				$("#frm-12 #tabla #pre_acidificacion_unidad_1").html(respuesta.select_pre_unidad);
				$("#frm-12 #tabla #pre_acidificacion_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				//llenado de clones
				//alert($("#frm-12 #tabla #pre_marca_marca_").html().toString());
				
				$("#frm-12 #tabla #pre_marca_empresa_").html(respuesta.select_empresa);		
				$("#frm-12 #tabla #pre_marca_marca_").html();
				//$("#frm-12 #tabla #pre_marca_marca_").html(respuesta.select_marca);
				//$("#frm-12 #tabla #pre_marca_fuente_").html(respuesta.select_fuente);
				$("#frm-12 #tabla #pre_marca_unidad_").html(respuesta.select_pre_unidad);
				$("#frm-12 #tabla #pre_marca_etapa_fenologica_").html(respuesta.select_etapa_fenologica);
				$("#frm-12 #tabla #pre_coad_unidad_").html(respuesta.select_unidad);
				$("#frm-12 #tabla #pre_coad_etapa_fenologica_").html(respuesta.select_etapa_fenologica);
				
				
				//lleno los select para post
				$("#frm-12 #tabla #post_marca_empresa_1").html(respuesta.select_empresa);
				$("#frm-12 #tabla #post_marca_marca_1").html();
				//$("#frm-12 #tabla #post_marca_marca_1").html(respuesta.select_marca);
				//$("#frm-12 #tabla #post_marca_fuente_1").html(respuesta.select_fuente);
				$("#frm-12 #tabla #post_marca_unidad_1").html(respuesta.select_post_unidad);
				$("#frm-12 #tabla #post_marca_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #tabla #post_coad_unidad_1").html(respuesta.select_post_unidad);
				$("#frm-12 #tabla #post_coad_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				$("#frm-12 #tabla #post_acidificacion_unidad").html(respuesta.select_post_unidad);
				$("#frm-12 #tabla #post_acidificacion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				//lleno los select para des
				$("#frm-12 #tabla #des_marca_empresa_1").html(respuesta.select_empresa);  //borrar
				$("#frm-12 #tabla #des_marca_marca_1").html();
				//$("#frm-12 #tabla #des_marca_marca_1").html(respuesta.select_marca);
				//$("#frm-12 #tabla #des_marca_fuente_1").html(respuesta.select_fuente);
				$("#frm-12 #tabla #des_marca_unidad_1").html(respuesta.select_des_unidad);
				$("#frm-12 #tabla #des_marca_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #tabla #des_coad_unidad_1").html(respuesta.select_des_unidad);
				$("#frm-12 #tabla #des_coad_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				$("#frm-12 #tabla #des_acidificacion_unidad").html(respuesta.select_des_unidad);
				$("#frm-12 #tabla #des_acidificacion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				//lleno los select para otro
				$("#frm-12 #tabla #otro_unidad").html(respuesta.select_otro_unidad);
				$("#frm-12 #tabla #otro_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #tabla #otro_escarda_unidad").html(respuesta.select_otro_unidad);
				$("#frm-12 #tabla #otro_escarda_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				//ejecutamos el trigger de los select para comenzar
				$("#frm-12 #tabla #realizo_pre").trigger("change");
				$("#frm-12 #tabla #realizo_post").trigger("change");
				$("#frm-12 #tabla #realizo_des").trigger("change");
				$("#frm-12 #tabla #realizo_otro").trigger("change");
				
				
						//desaparecemos la columna de innovaacion para cuando sea diagnostico
								if($("#gen-type").val()==1){
									$('#frm-12 #tabla tr:eq(0)').each(function(index, element) {
												//$(this).find('th:eq(7)').remove();
												$(this).find('th:eq(7)').remove();
									});
									$('.pre').each(function(index, element) {
												//$(this).find('th:eq(7)').remove();
												$(this).find('td:eq(6)').remove();
									});
									$('.post').each(function(index, element) {
												//$(this).find('th:eq(7)').remove();
												$(this).find('td:eq(6)').remove();
									});
									$('.des').each(function(index, element) {
												//$(this).find('th:eq(7)').remove();
												$(this).find('td:eq(6)').remove();
									});
									$('.otro').each(function(index, element) {
												//$(this).find('th:eq(7)').remove();
												$(this).find('td:eq(6)').remove();
									});
									$('.hpre').each(function(index, element) {
												//$(this).find('th:eq(7)').remove();
												$(this).find('td:eq(6)').remove();
									});
								}
					

				
				
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor intentelo nuevamente: " + err.message, "Ok", "", false, "");
		}
	
	});

    $('#btn-save-maleza', "#frm-12").once('click', function(){
		if(validaFrm12()==1){
			var id_generico = $("#gen-id").val();
			$(".date", "#frm-12").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-12"));
			frm.append("id", $("#gen-id").val()); 
			frm.append("tipo", $("#gen-type").val());
			frm.append("registro", $("#frm-12 #registro").val());
			//alert($("#frm-12 #registro").val());
			
				$.ajax({
					url: "/backend/diagnosticos/guardarcontroldemaleza/",
					type: "POST",
					data: frm,
					processData: false,  
					contentType: false,
				}).done(function(respuesta){
					
					try
					{
						$(".date", "#frm-12").attr('disabled', 'disabled');
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
							ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
						}
						else 
							ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
					}
					catch(error)
					{
						ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
					}
				});
	    }else{
			ShowDialogBox("Campos faltantes", "Hay campos incorrectos o faltantes. Revise los campos velva a inténtarlo.", "Ok", "", false, "");
		}
	});


	$("#btn-congelar-maleza", "#frm-12").once('click', function(){
		//if(validaFrm3())
			ShowConfirmBox("Confirmacion de congelacion", "¿Realmente desea congelar este formulario?, si lo congela no podra revertise, ¿Desea continuar?", congelarFrm12);
	});
	
	
	$("#btn-clean-maleza", "#frm-12").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			var registro = $("#frm-12 #registro").val();
			$('input','#frm-12').val('');
			$('#frm-12 select').not("[id^='realizo_pre']").not("[id^='realizo_post']").not("[id^='realizo_des']").not("[id^='realizo_otro']").each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
			$('#frm-12 [id^="realizo_pre"], #frm-12 [id^="realizo_post"], #frm-12 [id^="realizo_des"], #frm-12 [id^="realizo_otro"]').each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).val('2').change();
			});
			$("#frm-12 #registro").val(registro);
		});

	});
}
/*function cargaSelectClones(respuesta){
	//renglon generico
				$("#frm-12 #fsiembra-concepto_1 #fsiembra_tipo_fu_select_1").html(respuesta.select_fuente);
				$("#frm-12 #fsiembra-concepto_1 #fsiembra_tipo_unidad_1").html(respuesta.select_unidad);
				$("#frm-12 #fsiembra-concepto_1 #fsiembra_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				//renglon de costo por mezclado
				$("#frm-12 #fsiembra_mezclado_tipo_unidad_1").html(respuesta.select_unidad);
				$("#frm-12 #fsiembra_mezclado_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				//renglon generico clon
				$("#frm-12 #generico-fsiembra #fsiembra_tipo_fu_select_").html(respuesta.select_fuente);
				$("#frm-12 #generico-fsiembra #fsiembra_tipo_unidad_").html(respuesta.select_unidad);
				$("#frm-12 #generico-fsiembra #fsiembra_etapa_fenologica_").html(respuesta.select_etapa_fenologica);
				
				//cargamos los select en la tabla de foliar aunque no existe para cuando se clone la fila ya tenga los valores
				$("#frm-12 #generico-foliar #generico-quimica_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-12 #generico-foliar #generico-quimica_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-12 #generico-foliar #generico-quimica_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #generico-foliar #generico-organica_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-12 #generico-foliar #generico-organica_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-12 #generico-foliar #generico-organica_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #generico-foliar #generico-mineral_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-12 #generico-foliar #generico-mineral_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-12 #generico-foliar #generico-mineral_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-12 #generico-foliar #generico-acondicionador_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-12 #generico-foliar #generico-acondicionador_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-12 #generico-foliar #generico-acondicionador_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
	
}*/
function congelarFrm12(){
			var id_generico = $("#gen-id").val();
			$(".date", "#frm-12").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-12"));
			frm.append("id", $("#gen-id").val()); 
			frm.append("tipo", $("#gen-type").val());
			frm.append("registro", $("#frm-12 #registro").val());
			//alert($("#frm-12 #registro").val());
				$.ajax({
					url: "/backend/diagnosticos/guardarcontroldemaleza/",
					type: "POST",
					data: frm,
					processData: false,  
					contentType: false,
				}).done(function(respuesta){
					try
					{
						$(".date", "#frm-12").attr('disabled', 'disabled');
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
								$.ajax({
								url: "/backend/diagnosticos/congelarcontroldemaleza/",
								type: "POST",
								data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
								async: false
								}).done(function(respuesta){
									try
									{
										respuesta = jQuery.parseJSON(respuesta);
										if(respuesta.ok !== undefined){
											ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/control_maleza/1");
											$('#frm-12 *').attr("disabled", "disabled").unbind();	
										}
										else
											ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
									}
									catch(err)
									{
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
									}
								});
							//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
						}
						else 
							ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
					}
					catch(error)
					{
						ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
					}
				});
	
}

function validaFrm12()
{
	return true;
	var count = 0;
	var faltante =0;
	//recorremos las tablas de fsiembra
	//alert($("[class^='pre']").length+" "+$("[class^='post']").length+" "+	$("[class^='des']").length+" "+$("[class^='otro']").length);
	//validmos la prte de pre herbicida
	if($('#realizo_pre').val()=='1'){
		$("[class^='pre']").each(function(index, element) {
			id = $(this).attr("id").valueOf();
			actual = id.split("_");
			prefijo =id.split("-");
			prefijo = prefijo[0];
			elemento = actual[0];
			actual = parseInt(actual[1]);
			name = elemento.replace("-", "_"); 
			
			//alert(actual);
			//faltante = 1;
			elemento_marca = "#frm-12 #tabla #"+id+" #"+name+"_marca_"+actual;
			elemento_fuente = "#frm-12 #tabla #"+id+" #"+name+"_fuente_"+actual;
			elemento_unidad = "#frm-12 #tabla #"+id+" #"+name+"_unidad_"+actual;
			elemento_cantidad = "#frm-12 #tabla #"+id+" #"+name+"_cantidad_"+actual;
			elemento_precio = "#frm-12 #tabla #"+id+" #"+name+"_precio_"+actual;
			elemento_frealizacion = "#frm-12 #tabla #"+id+" #"+name+"_frealizacion_"+actual;
			elemento_etapa_fenologia = "#frm-12 #tabla #"+id+" #"+name+"_etapa_fenologica_"+actual;
			elemento_innovacion = "#frm-12 #tabla #"+id+" #"+name+"_innovacion_"+actual;
			elemento_tipo_ap ="#frm-12 #tabla #" + id + " #pre_tipo_apli";
			numero_jornales = " #frm-12 #tabla #" + id + " #manual_div #manual_div_no_jornales";
			costo_jornal = "#frm-12 #tabla #" + id + " #manual_div #manual_div_costo_jornal";
			tipo_mecanizacion = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select";
			costo_combustible = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_propia_combustible";
			costo_operador = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_propia_operador";
			monto_renta = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo";
	
						
			if($(elemento_marca).val() !== undefined){
				if($(elemento_marca).val()=='' || $(elemento_marca).val()=='0'){
					//$(elemento_marca).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_marca).css("background-color","#FFF");
				}
			}
			if($(elemento_fuente).val() !== undefined){
				if($(elemento_fuente).val()=='' || $(elemento_fuente).val()=='0'){
					//$(elemento_fuente).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_fuente).css("background-color","#FFF");
				}
			}
			if($(elemento_unidad).val() !== undefined){	
				if($(elemento_unidad).val()=='' || $(elemento_unidad).val()=='0'){
					//$(elemento_unidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_unidad).css("background-color","#FFF");
				}
			}
			if($(elemento_cantidad).val() !== undefined){	
				if($(elemento_cantidad).val()==''){
					//$(elemento_cantidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_cantidad).css("background-color","#FFF");
				}
			}
			if($(elemento_precio).val() !== undefined){
				if($(elemento_precio).val()==''){
					//$(elemento_precio).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_precio).css("background-color","#FFF");
				}
			}
			if($(elemento_frealizacion).val() !== undefined){
				if($(elemento_frealizacion).val()==''){
					//$(elemento_frealizacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_frealizacion).css("background-color","#FFF");
				}
			}
			if($(elemento_etapa_fenologia).val() !== undefined){
				if($(elemento_etapa_fenologia).val()=='' || $(elemento_etapa_fenologia).val()==0){
					//$(elemento_etapa_fenologia).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_etapa_fenologia).css("background-color","#FFF");
				}
			}
			if($(elemento_innovacion).val() !== undefined){
				if($(elemento_innovacion).val()=='' && $("#gen-type").val()!=1){
					//$(elemento_innovacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_innovacion).css("background-color","#FFF");
				}
			}
			if($(elemento_tipo_ap).val() !== undefined){
					if($(elemento_tipo_ap).val()==2){
						//manual
						if($(numero_jornales).val()==''){
							//$(numero_jornales).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(numero_jornales).css("background-color","#FFF");
						}
								
						if($(costo_jornal).val()==''){
							//$(costo_jornal).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(costo_jornal).css("background-color","#FFF");
						}
								
					}else if($(elemento_tipo_ap).val()==1){
						//mecanizada
						if($(tipo_mecanizacion).val()==1){
							if($(costo_combustible).val()==''){
								$(costo_combustible).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(costo_combustible).css("background-color","#FFF");
							}
									
							if($(costo_operador).val()==''){
								$(costo_operador).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(costo_operador).css("background-color","#FFF");
							}
						}else{
							if($(monto_renta).val()==''){
								$(monto_renta).css("background-color","#FFD7D7");
								faltante++;
							}else{
								$(monto_renta).css("background-color","#FFF");
							}
						}
						/*if($(tipo_mecanizacion).val()==''){
							//$(tipo_mecanizacion).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(tipo_mecanizacion).css("background-color","#FFF");
						}*/
						
						
					}else{
						
					}
			}
			acidificacion_unidad = "#frm-12 #tabla #pre_acidificacion_unidad_1";
			acidificacion_cantidad = "#frm-12 #tabla #pre_acidificacion_cantidad_1";
			acidificacion_precio = "#frm-12 #tabla #pre_acidificacion_precio_1";
			acidificacion_frealizacion = "#frm-12 #tabla #pre_acidificacion_frealizacion_1";
			acidificacion_etapa_fenologia = "#frm-12 #tabla #pre_acidificacion_etapa_fenologica_1";
			acidificacion_innovacion = "#frm-12 #tabla #pre_acidificacion_innovacion_1";
	
			if($(acidificacion_unidad).val() !== undefined){	
				if($(acidificacion_unidad).val()=='' || $(acidificacion_unidad).val()=='0'){
					//$(acidificacion_unidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(acidificacion_unidad).css("background-color","#FFF");
				}
			}
			if($(acidificacion_cantidad).val() !== undefined){	
				if($(acidificacion_cantidad).val()==''){
					//$(acidificacion_cantidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(acidificacion_cantidad).css("background-color","#FFF");
				}
			}
			if($(acidificacion_precio).val() !== undefined){
				if($(acidificacion_precio).val()==''){
					//$(acidificacion_precio).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(acidificacion_precio).css("background-color","#FFF");
				}
			}
			if($(acidificacion_frealizacion).val() !== undefined){
				if($(acidificacion_frealizacion).val()==''){
					//$(acidificacion_frealizacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(acidificacion_frealizacion).css("background-color","#FFF");
				}
			}
			if($(acidificacion_etapa_fenologia).val() !== undefined){
				if($(acidificacion_etapa_fenologia).val()=='' || $(acidificacion_etapa_fenologia).val()==0){
					//$(acidificacion_etapa_fenologia).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(acidificacion_etapa_fenologia).css("background-color","#FFF");
				}
			}
			if($(acidificacion_innovacion).val() !== undefined){
				if($(acidificacion_innovacion).val()=='' && $("#gen-type").val()!=1){
					//$(acidificacion_innovacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(acidificacion_innovacion).css("background-color","#FFF");
				}
			}
		});
	}
	//validmos la post herbicida
	if($('#realizo_post').val()=='1'){
		$("[class^='post']").each(function(index, element) {
			id = $(this).attr("id").valueOf();
			actual = id.split("_");
			prefijo =id.split("-");
			prefijo = prefijo[0];
			elemento = actual[0];
			actual = parseInt(actual[1]);
			name = elemento.replace("-", "_"); 
			
			elemento_marca = "#frm-12 #tabla #"+id+" #"+name+"_marca_"+actual;
			elemento_fuente = "#frm-12 #tabla #"+id+" #"+name+"_fuente_"+actual;
			elemento_unidad = "#frm-12 #tabla #"+id+" #"+name+"_unidad_"+actual;
			elemento_cantidad = "#frm-12 #tabla #"+id+" #"+name+"_cantidad_"+actual;
			elemento_precio = "#frm-12 #tabla #"+id+" #"+name+"_precio_"+actual;
			elemento_frealizacion = "#frm-12 #tabla #"+id+" #"+name+"_frealizacion_"+actual;
			elemento_etapa_fenologia = "#frm-12 #tabla #"+id+" #"+name+"_etapa_fenologica_"+actual;
			elemento_innovacion = "#frm-12 #tabla #"+id+" #"+name+"_innovacion_"+actual;
			elemento_tipo_ap ="#frm-12 #tabla #" + id + " #post_tipo_apli";
			numero_jornales = " #frm-12 #tabla #" + id + " #manual_div #manual_div_no_jornales";
			costo_jornal = "#frm-12 #tabla #" + id + " #manual_div #manual_div_costo_jornal";
			tipo_mecanizacion = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select";
			costo_combustible = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_propia_combustible";
			costo_operador = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_propia_operador";
			monto_renta = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo";
			
			
			if($(elemento_marca).val() !== undefined){
				if($(elemento_marca).val()=='' || $(elemento_marca).val()=='0'){
					//$(elemento_marca).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_marca).css("background-color","#FFF");
				}
			}
			if($(elemento_fuente).val() !== undefined){
				if($(elemento_fuente).val()=='' || $(elemento_fuente).val()=='0'){
					//$(elemento_fuente).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_fuente).css("background-color","#FFF");
				}
			}
			if($(elemento_unidad).val() !== undefined){	
				if($(elemento_unidad).val()=='' || $(elemento_unidad).val()=='0'){
					//$(elemento_unidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_unidad).css("background-color","#FFF");
				}
			}
			if($(elemento_cantidad).val() !== undefined){	
				if($(elemento_cantidad).val()==''){
					//$(elemento_cantidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_cantidad).css("background-color","#FFF");
				}
			}
			if($(elemento_precio).val() !== undefined){
				if($(elemento_precio).val()==''){
					//$(elemento_precio).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_precio).css("background-color","#FFF");
				}
			}
			if($(elemento_frealizacion).val() !== undefined){
				if($(elemento_frealizacion).val()==''){
					//$(elemento_frealizacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_frealizacion).css("background-color","#FFF");
				}
			}
			if($(elemento_etapa_fenologia).val() !== undefined){
				if($(elemento_etapa_fenologia).val()=='' || $(elemento_etapa_fenologia).val()=='0'){
					//$(elemento_etapa_fenologia).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_etapa_fenologia).css("background-color","#FFF");
				}
			}
			if($(elemento_innovacion).val() !== undefined){
				if($(elemento_innovacion).val()==''){
					//$(elemento_innovacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_innovacion).css("background-color","#FFF");
				}
			}
			
			
				if($(elemento_tipo_ap).val() !== undefined){
					if($(elemento_tipo_ap).val()==2){
						//manual
						if($(numero_jornales).val()==''){
							//$(numero_jornales).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(numero_jornales).css("background-color","#FFF");
						}
								
						if($(costo_jornal).val()==''){
							//$(costo_jornal).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(costo_jornal).css("background-color","#FFF");
						}
								
					}else if($(elemento_tipo_ap).val()==1){
						//mecanizada
						if($(tipo_mecanizacion).val()==1){
							if($(costo_combustible).val()==''){
								$(costo_combustible).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(costo_combustible).css("background-color","#FFF");
							}
									
							if($(costo_operador).val()==''){
								$(costo_operador).css("background-color","#FFD7D7");
								faltante++;
							}else{
								$(costo_operador).css("background-color","#FFF");
							}
						}else{
							if($(monto_renta).val()==''){
								$(monto_renta).css("background-color","#FFD7D7");
								faltante++;
							}else{
								$(monto_renta).css("background-color","#FFF");
							}
						}
					}
				}
				
				
			acidificacion_unidad = "#frm-12 #tabla #post_acidificacion_unidad";
			acidificacion_cantidad = "#frm-12 #tabla #post_acidificacion_cantidad";
			acidificacion_precio = "#frm-12 #tabla #post_acidificacion_precio";
			acidificacion_frealizacion = "#frm-12 #tabla #post_acidificacion_frealizacion";
			acidificacion_etapa_fenologia = "#frm-12 #tabla #post_acidificacion_etapa_fenologica";
			acidificacion_innovacion = "#frm-12 #tabla #post_acidificacion_innovacion";
					
					
						if($(acidificacion_unidad).val() !== undefined){	
							if($(acidificacion_unidad).val()=='' || $(acidificacion_unidad).val()=='0'){
								//$(acidificacion_unidad).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_unidad).css("background-color","#FFF");
							}
						}
						if($(acidificacion_cantidad).val() !== undefined){	
							if($(acidificacion_cantidad).val()==''){
								//$(acidificacion_cantidad).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_cantidad).css("background-color","#FFF");
							}
						}
						if($(acidificacion_precio).val() !== undefined){
							if($(acidificacion_precio).val()==''){
								//$(acidificacion_precio).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_precio).css("background-color","#FFF");
							}
						}
						if($(acidificacion_frealizacion).val() !== undefined){
							if($(acidificacion_frealizacion).val()==''){
								//$(acidificacion_frealizacion).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_frealizacion).css("background-color","#FFF");
							}
						}
						if($(acidificacion_etapa_fenologia).val() !== undefined){
							if($(acidificacion_etapa_fenologia).val()=='' || $(acidificacion_etapa_fenologia).val()=='0'){
								//$(acidificacion_etapa_fenologia).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_etapa_fenologia).css("background-color","#FFF");
							}
						}
						if($(acidificacion_innovacion).val() !== undefined){
							if($(acidificacion_innovacion).val()==''){
								//$(acidificacion_innovacion).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_innovacion).css("background-color","#FFF");
							}
						}
	
		});
	}
	//validmos la desecante
	if($('#realizo_des').val()=='1'){
		$("[class^='des']").each(function(index, element) {
			id = $(this).attr("id").valueOf();
			actual = id.split("_");
			prefijo =id.split("-");
			prefijo = prefijo[0];
			elemento = actual[0];
			actual = parseInt(actual[1]);
			name = elemento.replace("-", "_"); 
			
			elemento_marca = "#frm-12 #tabla #"+id+" #"+name+"_marca_"+actual;
			elemento_fuente = "#frm-12 #tabla #"+id+" #"+name+"_fuente_"+actual;
			elemento_unidad = "#frm-12 #tabla #"+id+" #"+name+"_unidad_"+actual;
			elemento_cantidad = "#frm-12 #tabla #"+id+" #"+name+"_cantidad_"+actual;
			elemento_precio = "#frm-12 #tabla #"+id+" #"+name+"_precio_"+actual;
			elemento_frealizacion = "#frm-12 #tabla #"+id+" #"+name+"_frealizacion_"+actual;
			elemento_etapa_fenologia = "#frm-12 #tabla #"+id+" #"+name+"_etapa_fenologica_"+actual;
			elemento_innovacion = "#frm-12 #tabla #"+id+" #"+name+"_innovacion_"+actual;
			elemento_tipo_ap ="#frm-12 #tabla #" + id + " #des_tipo_apli";
			numero_jornales = " #frm-12 #tabla #" + id + " #manual_div #manual_div_no_jornales";
			costo_jornal = "#frm-12 #tabla #" + id + " #manual_div #manual_div_costo_jornal";
			tipo_mecanizacion = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select";
			costo_combustible = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_propia_combustible";
			costo_operador = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_propia_operador";
			monto_renta = "#frm-12 #tabla #" + id + " #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo";
			
				if($(elemento_marca).val()=='' || $(elemento_fuente).val()=='0'){
					//$(elemento_marca).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_marca).css("background-color","#FFF");
				}
				if($(elemento_fuente).val()=='' || $(elemento_fuente).val()=='0'){
					//$(elemento_fuente).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_fuente).css("background-color","#FFF");
				}
				
				if($(elemento_unidad).val()=='' || $(elemento_unidad).val()=='0'){
					//$(elemento_unidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_unidad).css("background-color","#FFF");
				}
				
				if($(elemento_cantidad).val()==''){
					//$(elemento_cantidad).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_cantidad).css("background-color","#FFF");
				}
				if($(elemento_precio).val()==''){
					//$(elemento_precio).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_precio).css("background-color","#FFF");
				}
						
				if($(elemento_frealizacion).val()==''){
					//$(elemento_frealizacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_frealizacion).css("background-color","#FFF");
				}
						
				if($(elemento_etapa_fenologia).val()=='' || $(elemento_etapa_fenologia).val()=='0'){
					//$(elemento_etapa_fenologia).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_etapa_fenologia).css("background-color","#FFF");
				}
						
				if($(elemento_innovacion).val()==''){
					//$(elemento_innovacion).css("background-color","#FFD7D7");
					faltante++;
				}else{
					//$(elemento_innovacion).css("background-color","#FFF");
				}
				
				
					if($(elemento_tipo_ap).val() !== undefined){
					if($(elemento_tipo_ap).val()==2){
						//manual
						if($(numero_jornales).val()==''){
							//$(numero_jornales).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(numero_jornales).css("background-color","#FFF");
						}
								
						if($(costo_jornal).val()==''){
							//$(costo_jornal).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(costo_jornal).css("background-color","#FFF");
						}
								
					}else if($(elemento_tipo_ap).val()==1){
						//mecanizada
						if($(tipo_mecanizacion).val()==1){
							if($(costo_combustible).val()==''){
								$(costo_combustible).css("background-color","#FFD7D7");
								faltante++;
							}else{
								$(costo_combustible).css("background-color","#FFF");
							}
									
							if($(costo_operador).val()==''){
								$(costo_operador).css("background-color","#FFD7D7");
								faltante++;
							}else{
								$(costo_operador).css("background-color","#FFF");
							}
						}else{
							if($(monto_renta).val()==''){
								$(monto_renta).css("background-color","#FFD7D7");
								faltante++;
							}else{
								$(monto_renta).css("background-color","#FFF");
							}
						}
						
						
					}
			
			acidificacion_unidad = "#frm-12 #tabla #des_acidificacion_unidad";
			acidificacion_cantidad = "#frm-12 #tabla #des_acidificacion_cantidad";
			acidificacion_precio = "#frm-12 #tabla #des_acidificacion_precio";
			acidificacion_frealizacion = "#frm-12 #tabla #des_acidificacion_frealizacion";
			acidificacion_etapa_fenologia = "#frm-12 #tabla #des_acidificacion_etapa_fenologica";
			acidificacion_innovacion = "#frm-12 #tabla #des_acidificacion_innovacion";
			
						if($(acidificacion_unidad).val() !== undefined){	
							if($(acidificacion_unidad).val()=='' || $(acidificacion_unidad).val()=='0'){
								//$(acidificacion_unidad).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_unidad).css("background-color","#FFF");
							}
						}
						if($(acidificacion_cantidad).val() !== undefined){	
							if($(acidificacion_cantidad).val()==''){
								//$(acidificacion_cantidad).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_cantidad).css("background-color","#FFF");
							}
						}
						if($(acidificacion_precio).val() !== undefined){
							if($(acidificacion_precio).val()==''){
								//$(acidificacion_precio).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_precio).css("background-color","#FFF");
							}
						}
						if($(acidificacion_frealizacion).val() !== undefined){
							if($(acidificacion_frealizacion).val()==''){
								//$(acidificacion_frealizacion).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_frealizacion).css("background-color","#FFF");
							}
						}
						if($(acidificacion_etapa_fenologia).val() !== undefined){
							if($(acidificacion_etapa_fenologia).val()=='' || $(acidificacion_etapa_fenologia).val()=='0'){
								//$(acidificacion_etapa_fenologia).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_etapa_fenologia).css("background-color","#FFF");
							}
						}
						if($(acidificacion_innovacion).val() !== undefined){
							if($(acidificacion_innovacion).val()==''){
								//$(acidificacion_innovacion).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(acidificacion_innovacion).css("background-color","#FFF");
							}
						}
	
			}
		});
	}
	
	//validacion de la parte de otro
	if($('#realizo_otro').val()=='1'){
		elemento_unidad = "#frm-12 #tabla #otro_unidad";
		elemento_cantidad = "#frm-12 #tabla #otro_cantidad";
		elemento_precio = "#frm-12 #tabla #otro_precio";
		elemento_frealizacion = "#frm-12 #tabla #otro_frealizacion";
		elemento_etapa_fenologia = "#frm-12 #tabla #otro_etapa_fenologica";
		elemento_innovacion = "#frm-12 #tabla #otro_innovacion";
		elemento_tipo_ap ="#frm-12 #tabla #otro_tipo_ap";
		numero_jornales = " #frm-12 #tabla #manual_div_otro #manual_div_no_jornales_otro";
		costo_jornal = "#frm-12 #tabla #manual_div_otro #manual_div_costo_jornal_otro";
		tipo_mecanizacion = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_otro";
		costo_combustible = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_otro #mecanizada_div_select_div_propia_combustible_otro";
		costo_operador = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_otro #mecanizada_div_select_div_propia_operador_otro";
		monto_renta = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_rentada_otro #mecanizada_div_select_div_rentada_costo_otro";
		
		if($(elemento_marca).val() !== undefined){
			if($(elemento_marca).val()=='' || $(elemento_marca).val()=='0'){
				//$(elemento_marca).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_marca).css("background-color","#FFF");
			}
		}
		if($(elemento_fuente).val() !== undefined){
			if($(elemento_fuente).val()=='' || $(elemento_fuente).val()=='0'){
				//$(elemento_fuente).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_fuente).css("background-color","#FFF");
			}
		}
		if($(elemento_unidad).val() !== undefined){	
			if($(elemento_unidad).val()=='' || $(elemento_unidad).val()=='0'){
				//$(elemento_unidad).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_unidad).css("background-color","#FFF");
			}
		}
		if($(elemento_cantidad).val() !== undefined){	
			if($(elemento_cantidad).val()==''){
				//$(elemento_cantidad).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_cantidad).css("background-color","#FFF");
			}
		}
		if($(elemento_precio).val() !== undefined){
			if($(elemento_precio).val()==''){
				//$(elemento_precio).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_precio).css("background-color","#FFF");
			}
		}
		if($(elemento_frealizacion).val() !== undefined){
			if($(elemento_frealizacion).val()==''){
				//$(elemento_frealizacion).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_frealizacion).css("background-color","#FFF");
			}
		}
		if($(elemento_etapa_fenologia).val() !== undefined){
			if($(elemento_etapa_fenologia).val()=='' || $(elemento_etapa_fenologia).val()=='0'){
				//$(elemento_etapa_fenologia).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_etapa_fenologia).css("background-color","#FFF");
			}
		}
		if($(elemento_innovacion).val() !== undefined){
			if($(elemento_innovacion).val()==''){
				//$(elemento_innovacion).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(elemento_innovacion).css("background-color","#FFF");
			}
		}
		
		
			if($(elemento_tipo_ap).val() !== undefined){
				if($(elemento_tipo_ap).val()==2){
					//manual
					if($(numero_jornales).val()==''){
						//$(numero_jornales).css("background-color","#FFD7D7");
						faltante++;
					}else{
						//$(numero_jornales).css("background-color","#FFF");
					}
							
					if($(costo_jornal).val()==''){
						//$(costo_jornal).css("background-color","#FFD7D7");
						faltante++;
					}else{
						//$(costo_jornal).css("background-color","#FFF");
					}
							
				}else if($(elemento_tipo_ap).val()==1){
					//mecanizada
					if($(tipo_mecanizacion).val()==1){
							if($(costo_combustible).val()==''){
								//$(costo_combustible).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(costo_combustible).css("background-color","#FFF");
							}
									
							if($(costo_operador).val()==''){
								//$(costo_operador).css("background-color","#FFD7D7");
								faltante++;
							}else{
								//$(costo_operador).css("background-color","#FFF");
							}
					}else{
						if($(monto_renta).val()==''){
							//$(monto_renta).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(monto_renta).css("background-color","#FFF");
						}
					}
					
					
				}
			}
			
		escarda_unidad = "#frm-12 #tabla #otro_escarda_unidad";
		escarda_cantidad = "#frm-12 #tabla #otro_escarda_cantidad";
		escarda_precio = "#frm-12 #tabla #otro_escarda_precio";
		escarda_frealizacion = "#frm-12 #tabla #otro_escarda_frealizacion";
		escarda_etapa_fenologia = "#frm-12 #tabla #otro_escarda_etapa_fenologica";
		escarda_innovacion = "#frm-12 #tabla #otro_escarda_innovacion";
		escarda_tipo_ap ="#frm-12 #tabla #otro_escarda_tipo_ap";
		escarda_numero_jornales = " #frm-12 #tabla #manual_div_otro_escarda #manual_div_no_jornales_otro_escarda";
		escarda_costo_jornal = "#frm-12 #tabla #manual_div_otro_escarda #manual_div_costo_jornal_otro_escarda";
		escarda_tipo_mecanizacion = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_otro_escarda";
		escarda_costo_combustible = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda #mecanizada_div_select_div_propia_combustible_otro_escarda";
		escarda_costo_operador = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda #mecanizada_div_select_div_propia_operador_otro_escarda";
		escarda_monto_renta = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_rentada_otro_escarda #mecanizada_div_select_div_rentada_costo_otro_escarda";
		
		if($(escarda_unidad).val() !== undefined){	
			if($(escarda_unidad).val()=='' || $(escarda_unidad).val()=='0'){
				//$(escarda_unidad).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(escarda_unidad).css("background-color","#FFF");
			}
		}
		if($(escarda_cantidad).val() !== undefined){	
			if($(escarda_cantidad).val()==''){
				//$(escarda_cantidad).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(escarda_cantidad).css("background-color","#FFF");
			}
		}
		if($(escarda_precio).val() !== undefined){
			if($(escarda_precio).val()==''){
				//$(escarda_precio).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(escarda_precio).css("background-color","#FFF");
			}
		}
		if($(escarda_frealizacion).val() !== undefined){
			if($(escarda_frealizacion).val()==''){
				//$(escarda_frealizacion).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(escarda_frealizacion).css("background-color","#FFF");
			}
		}
		if($(escarda_etapa_fenologia).val() !== undefined){
			if($(escarda_etapa_fenologia).val()=='' || $(escarda_etapa_fenologia).val()=='0'){
				//$(escarda_etapa_fenologia).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(escarda_etapa_fenologia).css("background-color","#FFF");
			}
		}
		if($(escarda_innovacion).val() !== undefined){
			if($(escarda_innovacion).val()==''){
				//$(escarda_innovacion).css("background-color","#FFD7D7");
				faltante++;
			}else{
				//$(escarda_innovacion).css("background-color","#FFF");
			}
		}
		
		
			if($(escarda_tipo_ap).val() !== undefined){
				if($(escarda_tipo_ap).val()==2){
					//manual
					if($(escarda_numero_jornales).val()==''){
						//$(escarda_numero_jornales).css("background-color","#FFD7D7");
						faltante++;
					}else{
						$(escarda_numero_jornales).css("background-color","#FFF");
					}
							
					if($(escarda_costo_jornal).val()==''){
						//$(escarda_costo_jornal).css("background-color","#FFD7D7");
						faltante++;
					}else{
						//$(escarda_costo_jornal).css("background-color","#FFF");
					}
							
				}else if($(escarda_tipo_ap).val()==1){
					//mecanizada
					if($(escarda_tipo_mecanizacion).val()==1){
						if($(escarda_costo_combustible).val()==''){
							//$(escarda_costo_combustible).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(escarda_costo_combustible).css("background-color","#FFF");
						}
								
						if($(escarda_costo_operador).val()==''){
							//$(escarda_costo_operador).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(escarda_costo_operador).css("background-color","#FFF");
						}
					}else{
						if($(escarda_monto_renta).val()==''){
							//$(escarda_monto_renta).css("background-color","#FFD7D7");
							faltante++;
						}else{
							//$(escarda_monto_renta).css("background-color","#FFF");
						}
						
					}
					
					
				}
			}
	}
	//Evitamos valida a petición de Raul el 18/06/2020
	return 1;
	if(faltante>0){
		return 0;
	}else{
		return 1;
	}
}
function evaluaCambioAplicacion2Frm12(tabla,elemento){
	mecanizada_div="#frm-12 #tabla #"+tabla+" #mecanizada_div";
	
	manual_div = "#frm-12 #tabla #"+tabla+" #manual_div";
	mecanizada_tipo = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select";
	
	mecanizada_tipo_otro="#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_otro";
	mecanizada_tipo_otro_escarda="#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_otro_escarda";
	
	mecanizada_combustible = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible";
	mecanizada_operador = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador";
	

	if(elemento ==''){
		
		if($(mecanizada_tipo).val() == 1){
	
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia").removeClass("hide");
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada").addClass("hide");
	
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select option:eq(0)").prop('selected', true);
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('');
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador").val('');
			
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val('');
	
		}else{
	
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia").addClass("hide");
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada").removeClass("hide");
			
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('');
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador").val('');
			
			$("#frm-12 #tabla #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val('');
			
	
		}
	}else if(elemento =='otro'){
		//alert($(mecanizada_tipo_otro).val());
		if($(mecanizada_tipo_otro).val() == 1){
	
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_otro").removeClass("hide");
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_rentada_otro").addClass("hide");
	
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_otro option:eq(0)").prop('selected', true);
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_combustible_otro").val('');
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_operador_otro").val('');
			
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_rentada_otro #mecanizada_div_select_div_rentada_costo_otro").val('');
	
		}else{
	
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_otro").addClass("hide");
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_rentada_otro").removeClass("hide");
			
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_combustible_otro").val('');
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_operador_otro").val('');
			
			$("#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo_otro").val('');
			
	
		}
	}else if(elemento =='escarda'){
		if($(mecanizada_tipo_otro_escarda).val() == 1){
	
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda").removeClass("hide");
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_rentada_otro_escarda").addClass("hide");
	
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_otro_escarda option:eq(0)").prop('selected', true);
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_combustible_otro_escarda").val('');
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_operador_otro_escarda").val('');
			
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_rentada_otro_escarda #mecanizada_div_select_div_rentada_costo_otro_escarda").val('');
	
		}else{
	
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda").addClass("hide");
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_rentada_otro_escarda").removeClass("hide");
			
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_combustible_otro_escarda").val('');
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_operador_otro_escarda").val('');
			
			$("#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_rentada_otro_escarda #mecanizada_div_select_div_rentada_costo_otro_escarda").val('');
			
	
		}
	}
}
function evaluaCambioAplicacionFrm12(tabla,elemento){
	var valor = '';
	var manual = '';
	var mecanizada = '';
	
	if(elemento!='otro' && elemento!='escarda'){
		valor =  $("#frm-12 #tabla #"+tabla+" #"+elemento).val();
		mecanizada_div="#frm-12 #tabla #"+tabla+" #mecanizada_div";
		manual_div = "#frm-12 #tabla #"+tabla+" #manual_div";
		propia_div = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select_div_propia";
		renta_div ="#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select_div_rentada";
		mecanizada_tipo = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select";
		mecanizada_combustible = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible";
		mecanizada_operador = "#frm-12 #tabla #"+tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador";
	}else if(elemento=='otro'){
		valor =  $("#frm-12 #tabla #"+tabla).val();
		mecanizada_div="#frm-12 #tabla #mecanizada_div_otro";
		manual_div="#frm-12 #tabla #manual_div_otro";
		propia_div = "#frm-12 #tabla #"+tabla+" #mecanizada_div_otro #mecanizada_div_select_div_propia_otro";
		renta_div ="#frm-12 #tabla #"+tabla+" #mecanizada_div_otro #mecanizada_div_select_div_rentada_otro";
		mecanizada_tipo = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_otro";
		mecanizada_combustible = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_combustible_otro";
		mecanizada_operador = "#frm-12 #tabla #mecanizada_div_otro #mecanizada_div_select_div_propia_operador_otro";
	}else{
		valor =  $("#frm-12 #tabla #"+tabla).val();
		mecanizada_div="#frm-12 #tabla #mecanizada_div_otro_escarda";
		manual_div="#frm-12 #tabla #manual_div_otro_escarda";
		propia_div = "#frm-12 #tabla #"+tabla+" #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_otro_escarda";
		renta_div ="#frm-12 #tabla #"+tabla+" #mecanizada_div_otro_escarda #mecanizada_div_select_div_rentada_otro_escarda";
		mecanizada_tipo = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_otro_escarda";
		mecanizada_combustible = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_combustible_otro_escarda";
		mecanizada_operador = "#frm-12 #tabla #mecanizada_div_otro_escarda #mecanizada_div_select_div_propia_operador_otro_escarda";
	}

	if(valor == 1){
		//mecanizada
		$(mecanizada_div).removeClass("hide");
		$(manual_div).addClass("hide");
		
		if(elemento!='otro' && elemento!='escarda'){
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}else if(elemento=='otro'){
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}else{
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}
		

	}else if(valor ==2){
		//manual
		$(manual_div).removeClass("hide");
		$(mecanizada_div).addClass("hide");
		if(elemento!='otro' && elemento!='escarda'){
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}else if(elemento=='otro'){
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}else{
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}
	}else{
		//aerea
		$(mecanizada_div).removeClass("hide");
		$(manual_div).addClass("hide");

		if(elemento!='otro' && elemento!='escarda'){
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}else if(elemento=='otro'){
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}else{
			$(mecanizada_tipo+" option:eq(0)").prop('selected', true);
			$(mecanizada_combustible).val('');
			$(mecanizada_operador).val('');
		}

	}
	if($(mecanizada_tipo).val()==1){
		$(propia_div).removeClass("hide");
		$(renta_div).addClass("hide");
	}else{
		
		$(propia_div).addClass("hide");
		$(renta_div).removeClass("hide");
	}
}
function calculaImporteFrm12(id,hermano,receptor)
{
	//obtenemos el valor del campo
/*	if(idPadre==''){
*/		//es un valor de costo por mezclado
		selector = "#frm-12  #"+id;
		selector2 = "#frm-12 #"+hermano;
		selector3 = "#frm-12 #"+receptor;
	/*}else{
		//es un valor de la fila de concepto
		selector = "#frm-12 #"+tabla+" #"+idPadre+" #"+id;
		selector2 = "#frm-12 #"+tabla+" #"+idPadre+" #"+hermano;
		selector3 = "#frm-12 #"+tabla+" #"+idPadre+" #"+receptor;
		
	}*/
	var valor = parseFloat($(selector).val());
	var hermano = parseFloat($(selector2).val());
	if(valor>0 && hermano>0){
		$(selector3).val(valor*hermano);
	}else{
		$(selector3).val('');
	}
}

function agregarFilaExtraFrm12(elemento)
{
	//en base al elemento bbuscamos la antidad de elementos que hay
	var ultimo_id = $("#frm-12 #tabla [id^='"+elemento+"']").last().attr("id").valueOf();
	ultimo_id = ultimo_id.split("_");
	actual = parseInt(ultimo_id[1]);
	var contador = (actual+1);
	
	if(elemento.indexOf("pre")>=0){
		$("#frm-12 #tabla #pre-apli_"+actual).after( 
			$("#frm-12 #tabla #pre-marca").clone().removeClass("hide").attr("id", "pre-marca_"+contador),
			$("#frm-12 #tabla #pre-coad").clone().removeClass("hide").attr("id", "pre-coad_"+contador),
			$("#frm-12 #tabla #pre-apli").clone().removeClass("hide").attr("id", "pre-apli_"+contador)
		);

		var elem = "pre_marca";//elemento.split("-");
		$("#frm-12 #tabla #pre-marca_"+contador).addClass("pre");
		$("#frm-12 #tabla #pre-marca_"+contador+" #eliminarFila_").attr("id", elem+"_"+contador+"_eliminar").attr("onClick", "eliminarPorId('"+elemento+"_"+contador+"')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_marca_").attr("id",elem+"_marca_"+ contador ).attr("name", "pre[marca]["+contador+"][marca]");
		//$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_fuente_").attr("id",elem+"_fuente_"+ contador ).attr("name","pre[marca]["+contador+"][fuente]");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_unidad_").attr("id",elem+"_unidad_"+ contador ).attr("name", "pre[marca]["+contador+"][unidad]");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_cantidad_").attr("id",elem+"_cantidad_"+ contador ).attr("name", "pre[marca]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm12('"+elem+"_cantidad_"+contador+"', '"+elem+"_precio_"+contador+"','"+elem+"_importe_"+contador+"')");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_precio_").attr("id",elem+"_precio_"+ contador ).attr("name", "pre[marca]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm12('"+elem+"_precio_"+contador+"', '"+elem+"_cantidad_"+contador+"','"+elem+"_importe_"+contador+"')");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_importe_").attr("id",elem+"_importe_"+ contador ).attr("name", "pre[marca]["+contador+"][importe]");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_frealizacion_").attr("id",elem+"_frealizacion_"+ contador ).attr("name", "pre[marca]["+contador+"][frealizacion]");
		$("#frm-12 #tabla #pre-marca_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_etapa_fenologica_").attr("id",elem+"_etapa_fenologica_"+ contador ).attr("name", "pre[marca]["+contador+"][etapa_fenologica]");
		$("#frm-12 #tabla #pre-marca_"+contador+" #pre_marca_innovacion_").attr("id",elem+"_innovacion_"+ contador ).attr("name", "pre[marca]["+contador+"][innovacion]");
		
		var elem2 = "pre_coad";
		$("#frm-12 #tabla #pre-coad_"+contador).addClass("pre");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_marca_").attr("id",elem2+"_marca_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][marca]");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_fuente_").attr("id",elem2+"_fuente_"+ contador ).attr("name","pre[coadyudantes]["+contador+"][fuente]");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_unidad_").attr("id",elem2+"_unidad_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][unidad]");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_cantidad_").attr("id",elem2+"_cantidad_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm12('"+elem2+"_cantidad_"+contador+"', '"+elem2+"_precio_"+contador+"','"+elem2+"_importe_"+contador+"')");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_precio_").attr("id",elem2+"_precio_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm12('"+elem2+"_precio_"+contador+"', '"+elem2+"_cantidad_"+contador+"','"+elem2+"_importe_"+contador+"')");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_importe_").attr("id",elem2+"_importe_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][importe]");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_frealizacion_").attr("id",elem2+"_frealizacion_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][frealizacion]");
		$("#frm-12 #tabla #pre-coad_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_etapa_fenologica_").attr("id",elem2+"_etapa_fenologica_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][etapa_fenologica]");
		$("#frm-12 #tabla #pre-coad_"+contador+" #pre_coad_innovacion_").attr("id",elem2+"_innovacion_"+ contador ).attr("name", "pre[coadyudantes]["+contador+"][innovacion]");
		
		var elem3 = "pre_apli";
		$("#frm-12 #tabla #pre-apli_"+contador).addClass("pre");
		$("#frm-12 #tabla #pre-apli_"+contador+" #pre_tipo_apli").attr("onchange","evaluaCambioAplicacionFrm12('pre-apli_"+contador+"', 'pre_tipo_apli')");
		//asigno names a los de aplicaciones
		$("#frm-12 #tabla #pre-apli_"+contador+" #pre_tipo_apli").attr("name", "pre[aplicacion]["+contador+"][aplicacion]");
		$("#frm-12 #tabla #pre-apli_"+contador+" #manual_div #manual_div_no_jornales").attr("name", "pre[aplicacion]["+contador+"][numero_jornales]");
		$("#frm-12 #tabla #pre-apli_"+contador+" #manual_div #manual_div_costo_jornal").attr("name", "pre[aplicacion]["+contador+"][costo_jornal]");
		$("#frm-12 #tabla #pre-apli_"+contador+" #mecanizada_div #mecanizada_div_select").attr("name", "pre[aplicacion]["+contador+"][tipo_mecanizacion]").attr("onchange","evaluaCambioAplicacion2Frm12('pre-apli_"+contador+"','')");
		$("#frm-12 #tabla #pre-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").attr("name", "pre[aplicacion]["+contador+"][costo_combustible]");
		$("#frm-12 #tabla #pre-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").attr("name", "pre[aplicacion]["+contador+"][costo_operador]");
		$("#frm-12 #tabla #pre-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").attr("name", "pre[aplicacion]["+contador+"][monto_renta]");
		
		
		
		
	}else if(elemento.indexOf("post")>=0){
		$("#frm-12 #tabla #post-apli_"+actual).after( 
			$("#frm-12 #tabla #pre-marca").clone().removeClass("hide").attr("id", "post-marca_"+contador),
			$("#frm-12 #tabla #pre-coad").clone().removeClass("hide").attr("id", "post-coad_"+contador),
			$("#frm-12 #tabla #pre-apli").clone().removeClass("hide").attr("id", "post-apli_"+contador)
		);

		var elem = "post_marca";//elemento.split("-");
		$("#frm-12 #tabla #post-marca_"+contador).addClass("post");
		$("#frm-12 #tabla #post-marca_"+contador+" #eliminarFila_").attr("id", elem+"_"+contador+"_eliminar").attr("onClick", "eliminarPorId('"+elemento+"_"+contador+"')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_marca_").attr("id",elem+"_marca_"+ contador ).attr("name", "post[marca]["+contador+"][marca]");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_fuente_").attr("id",elem+"_fuente_"+ contador ).attr("name","post[marca]["+contador+"][fuente]");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_unidad_").attr("id",elem+"_unidad_"+ contador ).attr("name", "post[marca]["+contador+"][unidad]");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_cantidad_").attr("id",elem+"_cantidad_"+ contador ).attr("name", "post[marca]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm12('"+elem+"_cantidad_"+contador+"', '"+elem+"_precio_"+contador+"','"+elem+"_importe_"+contador+"')");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_precio_").attr("id",elem+"_precio_"+ contador ).attr("name", "post[marca]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm12('"+elem+"_precio_"+contador+"', '"+elem+"_cantidad_"+contador+"','"+elem+"_importe_"+contador+"')");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_importe_").attr("id",elem+"_importe_"+ contador ).attr("name", "post[marca]["+contador+"][importe]");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_frealizacion_").attr("id",elem+"_frealizacion_"+ contador ).attr("name", "post[marca]["+contador+"][frealizacion]");
		$("#frm-12 #tabla #post-marca_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_etapa_fenologica_").attr("id",elem+"_etapa_fenologica_"+ contador ).attr("name", "post[marca]["+contador+"][etapa_fenologica]");
		$("#frm-12 #tabla #post-marca_"+contador+" #pre_marca_innovacion_").attr("id",elem+"_innovacion_"+ contador ).attr("name", "post[marca]["+contador+"][innovacion]");
		
		var elem2 = "post_coad";
		$("#frm-12 #tabla #post-coad_"+contador).addClass("post");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_marca_").attr("id",elem2+"_marca_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][marca]");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_fuente_").attr("id",elem2+"_fuente_"+ contador ).attr("name","post[coadyudantes]["+contador+"][fuente]");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_unidad_").attr("id",elem2+"_unidad_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][unidad]");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_cantidad_").attr("id",elem2+"_cantidad_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm12('"+elem2+"_cantidad_"+contador+"', '"+elem2+"_precio_"+contador+"','"+elem2+"_importe_"+contador+"')");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_precio_").attr("id",elem2+"_precio_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm12('"+elem2+"_precio_"+contador+"', '"+elem2+"_cantidad_"+contador+"','"+elem2+"_importe_"+contador+"')");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_importe_").attr("id",elem2+"_importe_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][importe]");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_frealizacion_").attr("id",elem2+"_frealizacion_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][frealizacion]");
		$("#frm-12 #tabla #post-coad_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_etapa_fenologica_").attr("id",elem2+"_etapa_fenologica_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][etapa_fenologica]");
		$("#frm-12 #tabla #post-coad_"+contador+" #pre_coad_innovacion_").attr("id",elem2+"_innovacion_"+ contador ).attr("name", "post[coadyudantes]["+contador+"][innovacion]");
		
		var elem3 = "post_apli";
		$("#frm-12 #tabla #post-apli_"+contador).addClass("post");
		$("#frm-12 #tabla #post-apli_"+contador+" #pre_tipo_apli").attr("onchange","evaluaCambioAplicacionFrm12('post-apli_"+contador+"', 'pre_tipo_apli')");
		
		//asigno names a los de aplicaciones
		$("#frm-12 #tabla #post-apli_"+contador+" #pre_tipo_apli").attr("name", "post[aplicacion]["+contador+"][aplicacion]");
		$("#frm-12 #tabla #post-apli_"+contador+" #manual_div #manual_div_no_jornales").attr("name", "post[aplicacion]["+contador+"][numero_jornales]");
		$("#frm-12 #tabla #post-apli_"+contador+" #manual_div #manual_div_costo_jornal").attr("name", "post[aplicacion]["+contador+"][costo_jornal]");
		$("#frm-12 #tabla #post-apli_"+contador+" #mecanizada_div #mecanizada_div_select").attr("name", "post[aplicacion]["+contador+"][tipo_mecanizacion]").attr("onchange","evaluaCambioAplicacion2Frm12('post-apli_"+contador+"','')");
		$("#frm-12 #tabla #post-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").attr("name", "post[aplicacion]["+contador+"][costo_combustible]");
		$("#frm-12 #tabla #post-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").attr("name", "post[aplicacion]["+contador+"][costo_operador]");
		$("#frm-12 #tabla #post-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").attr("name", "post[aplicacion]["+contador+"][monto_renta]");

	}else if(elemento.indexOf("des")>=0){
		$("#frm-12 #tabla #des-apli_"+actual).after( 
			$("#frm-12 #tabla #pre-marca").clone().removeClass("hide").attr("id", "des-marca_"+contador),
			$("#frm-12 #tabla #pre-coad").clone().removeClass("hide").attr("id", "des-coad_"+contador),
			$("#frm-12 #tabla #pre-apli").clone().removeClass("hide").attr("id", "des-apli_"+contador)
		);

		var elem = "des_marca";//elemento.split("-");
		$("#frm-12 #tabla #des-marca_"+contador).addClass("des");
		$("#frm-12 #tabla #des-marca_"+contador+" #eliminarFila_").attr("id", elem+"_"+contador+"_eliminar").attr("onClick", "eliminarPorId('"+elemento+"_"+contador+"')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_marca_").attr("id",elem+"_marca_"+ contador ).attr("name", "des[marca]["+contador+"][marca]");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_fuente_").attr("id",elem+"_fuente_"+ contador ).attr("name","des[marca]["+contador+"][fuente]");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_unidad_").attr("id",elem+"_unidad_"+ contador ).attr("name", "des[marca]["+contador+"][unidad]");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_cantidad_").attr("id",elem+"_cantidad_"+ contador ).attr("name", "des[marca]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm12('"+elem+"_cantidad_"+contador+"', '"+elem+"_precio_"+contador+"','"+elem+"_importe_"+contador+"')");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_precio_").attr("id",elem+"_precio_"+ contador ).attr("name", "des[marca]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm12('"+elem+"_precio_"+contador+"', '"+elem+"_cantidad_"+contador+"','"+elem+"_importe_"+contador+"')");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_importe_").attr("id",elem+"_importe_"+ contador ).attr("name", "des[marca]["+contador+"][importe]");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_frealizacion_").attr("id",elem+"_frealizacion_"+ contador ).attr("name", "des[marca]["+contador+"][frealizacion]");
		$("#frm-12 #tabla #des-marca_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_etapa_fenologica_").attr("id",elem+"_etapa_fenologica_"+ contador ).attr("name", "des[marca]["+contador+"][etapa_fenologica]");
		$("#frm-12 #tabla #des-marca_"+contador+" #pre_marca_innovacion_").attr("id",elem+"_innovacion_"+ contador ).attr("name", "des[marca]["+contador+"][innovacion]");
		
		var elem2 = "des_coad";
		$("#frm-12 #tabla #des-coad_"+contador).addClass("des");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_marca_").attr("id",elem2+"_marca_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][marca]");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_fuente_").attr("id",elem2+"_fuente_"+ contador ).attr("name","des[coadyudantes]["+contador+"][fuente]");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_unidad_").attr("id",elem2+"_unidad_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][unidad]");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_cantidad_").attr("id",elem2+"_cantidad_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm12('"+elem2+"_cantidad_"+contador+"', '"+elem2+"_precio_"+contador+"','"+elem2+"_importe_"+contador+"')");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_precio_").attr("id",elem2+"_precio_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm12('"+elem2+"_precio_"+contador+"', '"+elem2+"_cantidad_"+contador+"','"+elem2+"_importe_"+contador+"')");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_importe_").attr("id",elem2+"_importe_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][importe]");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_frealizacion_").attr("id",elem2+"_frealizacion_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][frealizacion]");
		$("#frm-12 #tabla #des-coad_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_etapa_fenologica_").attr("id",elem2+"_etapa_fenologica_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][etapa_fenologica]");
		$("#frm-12 #tabla #des-coad_"+contador+" #pre_coad_innovacion_").attr("id",elem2+"_innovacion_"+ contador ).attr("name", "des[coadyudantes]["+contador+"][innovacion]");
		
		var elem3 = "des_apli";
		$("#frm-12 #tabla #des-apli_"+contador).addClass("des");
		$("#frm-12 #tabla #des-apli_"+contador+" #pre_tipo_apli").attr("onchange","evaluaCambioAplicacionFrm12('des-apli_"+contador+"', 'pre_tipo_apli')");
		
		//asigno names a los de aplicaciones
		$("#frm-12 #tabla #des-apli_"+contador+" #pre_tipo_apli").attr("name", "des[aplicacion]["+contador+"][aplicacion]");
		$("#frm-12 #tabla #des-apli_"+contador+" #manual_div #manual_div_no_jornales").attr("name", "des[aplicacion]["+contador+"][numero_jornales]");
		$("#frm-12 #tabla #des-apli_"+contador+" #manual_div #manual_div_costo_jornal").attr("name", "des[aplicacion]["+contador+"][costo_jornal]");
		$("#frm-12 #tabla #des-apli_"+contador+" #mecanizada_div #mecanizada_div_select").attr("name", "des[aplicacion]["+contador+"][tipo_mecanizacion]").attr("onchange","evaluaCambioAplicacion2Frm12('des-apli_"+contador+"','')");
		$("#frm-12 #tabla #des-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").attr("name", "des[aplicacion]["+contador+"][costo_combustible]");
		$("#frm-12 #tabla #des-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").attr("name", "des[aplicacion]["+contador+"][costo_operador]");
		$("#frm-12 #tabla #des-apli_"+contador+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").attr("name", "des[aplicacion]["+contador+"][monto_renta]");

	}else{
		alert("Elemento incorrecto: Error al agregar campos nuevos");
	}
	
	

}
// Evento que selecciona la fila y la elimina 
function eliminarPorId(id)
{	//pre-marca_2
	//pre,marca_2
	prefijo = id.split("-");
	partes = prefijo[1].split("_");
	prefijo[0] = prefijo[0];//pre
	prefijo[1] = partes[0];//marca
	prefijo[2] = partes[1];//2
	
	mi_id = $("#frm-12 #tabla #"+id+" #id").val();

	if(parseInt(mi_id)>0){
		$.ajax({
			url: "/backend/diagnosticos/eliminarcontroldemaleza/",
			type: "POST",
			data: { id: mi_id },
			async: false
		}).done(function(respuesta){
			respuesta = jQuery.parseJSON(respuesta);
			if(respuesta !== undefined){
				$("#frm-12 #tabla #"+prefijo[0]+"-marca_"+prefijo[2]).remove();
				$("#frm-12 #tabla #"+prefijo[0]+"-coad_"+prefijo[2]).remove();
				$("#frm-12 #tabla #"+prefijo[0]+"-apli_"+prefijo[2]).remove();
			}else{
				ShowDialogBox("Ha ocurrido un error", "No se han podido eliminar las tablas, inténtelo nuevamente", "Ok", "", false, "");	
			}
		});
 
	}else{
		$("#frm-12 #tabla #"+prefijo[0]+"-marca_"+prefijo[2]).remove();
		$("#frm-12 #tabla #"+prefijo[0]+"-coad_"+prefijo[2]).remove();
		$("#frm-12 #tabla #"+prefijo[0]+"-apli_"+prefijo[2]).remove();
	}
/*	$("#frm-12 #tabla #"+prefijo[0]+"-marca_"+prefijo[2]).remove();
	$("#frm-12 #tabla #"+prefijo[0]+"-coad_"+prefijo[2]).remove();
	$("#frm-12 #tabla #"+prefijo[0]+"-apli_"+prefijo[2]).remove();*/
}

function realizo_cm(elemento){
	if($('#realizo_'+elemento).val()=='2'){//no
		//deshabilitamos los dates
		$("."+elemento).find('.input-group-addon').addClass("hide");
		//deshabilitamos los demas inputs
		$("."+elemento).find('select, input, i').each(function(index, element){
			$(this).attr("disabled","disabled").not('.input-group-addon');
			//quitamos la clase requerida
			$(this).removeClass("required").not('.input-group-addon');
			//borramos la informacion de los inputs o select
			if($(this).is('select')){
				if($(this).find('option[value="0"]').length>0){
					$(this).val('0');//.css("background-color","#F00");
				}else{
					$(this).val('2').trigger("change");//.css("background-color","#FF0");
				}
			}else if($(this).is('input')){
				$(this).val('');
			}
		});
		
	}else{//si
		//habilitamos los dates
		$("."+elemento).find('.input-group-addon').removeClass("hide");
		//habilitamos los demas inputs
		$("."+elemento).find('select, input, i').each(function(index, element){
			$(this).removeAttr("disabled").not('.input-group-addon');
			//habilitamos la clase requerida
			$(this).addClass("required").not('.input-group-addon');
		});
		
	}
	
	
}
