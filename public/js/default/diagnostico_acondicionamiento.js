$(document).ready(function(){
	$("#canal_precio, #canal_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('canal');
	});
	$("#trazo_precio, #trazo_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('trazo');
	});
	$("#emparejamiento_precio, #emparejamiento_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('emparejamiento');
	});
	$("#contreo_precio, #contreo_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('contreo');
	});
	$("#despiedre_precio, #despiedre_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('despiedre');
	});
	$("#reforzamiento_precio, #reforzamiento_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('reforzamiento');
	});
	$("#otros_precio, #otros_cantidad", "#frm-3").once('keyup', function(){
		calculaImporteFrm3('otros');
	});
});

function cargarAcondicionamientoDelPredio(){

	var conceptos = ["canal", "trazo", "emparejamiento", "contreo", "despiedre", "reforzamiento", "otros"];
	$('#btn-save-adp', "#frm-3").once('click', function(){
		if(validaFrm3())
		{
			for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-3").removeAttr('disabled'); }
			var preId = $("#gen-id").val();
			var frm = new FormData(document.getElementById("frm-3"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardaracondicionamientodelpredio/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	respuesta = jQuery.parseJSON(respuesta);
	        	for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-3").attr("disabled","disabled"); }
				try {
					if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data+"/acondicionamiento/1/predio/"+$("#predio_id").val();
		        	}
				}
				catch(err) {
	        		ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, intentelo nuevamente", "Ok", "", false, "");
				}
	        });
	    }
	});


	$("#btn-congelar-adp", "#frm-3").once('click', function(){
		if(validaFrm3())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm3);
	});

	$('#btn-clean-adp', "#frm-3").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
		for (var i = 0; i < conceptos.length; i++) {
			$("#" + conceptos[i] + "_Si", "#frm-3").prop('checked', false);
			$("#" + conceptos[i] + "_No", "#frm-3").prop('checked', false);
			$("#" + conceptos[i] + "_id_unidad",  "#frm-3").val('0');
			$("#" + conceptos[i] + "_cantidad",  "#frm-3").val('');
			$("#" + conceptos[i] + "_precio",  "#frm-3").val('');
			$("#" + conceptos[i] + "_importe",  "#frm-3").val('');
			$("#" + conceptos[i] + "_fecha",  "#frm-3").val('');
		};
		});
	});

	function selectNo(concepto)
	{
		//alert(concepto);
		$("#" + concepto + "_id_unidad",  "#frm-3").val('0');
		$("#" + concepto + "_cantidad",  "#frm-3").val('');
		$("#" + concepto + "_precio",  "#frm-3").val('');
		$("#" + concepto + "_importe",  "#frm-3").val('');
		$("#" + concepto + "_fecha",  "#frm-3").val('');
	}

	$('#canal_No', "#frm-3").once('click', function(){ selectNo("canal"); });
	$('#trazo_No', "#frm-3").once('click', function(){ selectNo("trazo"); });
	$('#emparejamiento_No', "#frm-3").once('click', function(){ selectNo("emparejamiento"); });
	$('#contreo_No', "#frm-3").once('click', function(){ selectNo("contreo"); });
	$('#despiedre_No', "#frm-3").once('click', function(){ selectNo("despiedre"); });
	$('#reforzamiento_No', "#frm-3").once('click', function(){ selectNo("reforzamiento"); });
	$('#otros_No', "#frm-3").once('click', function(){ selectNo("otros"); });
	
	//asignamos las unidades a los conceptos dependiendo la configuracion general
	obtenerUnidadesPorFormularioConceptoFrm3(conceptos);
	
	/*$.ajax({
        url: "/backend/diagnosticos/obtenerunidades/",
        type: "POST",
        async: false,
        processData: false,  
        contentType: false
    }).done(function(data){
        $("#canal_id_unidad",  "#frm-3").html(data);
        $("#trazo_id_unidad",  "#frm-3").html(data);
        $("#emparejamiento_id_unidad",  "#frm-3").html(data);
        $("#contreo_id_unidad",  "#frm-3").html(data);
        $("#despiedre_id_unidad",  "#frm-3").html(data);
        $("#reforzamiento_id_unidad",  "#frm-3").html(data);
        $("#otros_id_unidad",  "#frm-3").html(data);
    });*/

    //[2020-06-04]Agregado por Christian López para resover el problema de asincronía 
    //con respecto a las unidades de medida
    setTimeout(function()
    { 
		$.ajax({
	        url: "/backend/diagnosticos/obtenerdatosacondicionamientodelpredio/id/" + $("#gen-id").val() + "/tipo/" + $("#gen-type").val(),
	        type: "POST",
	        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
	    }).done(function(data){
			var registro = jQuery.parseJSON(data);
			try
			{
		    	if(registro.error !== undefined) 
		    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
		    	else if(registro.nodata === undefined)
		    	{
					for (var i = 0; i < conceptos.length; i++)
					{
						//alert(registro[conceptos[i] + "_cantidad"]);
		        		if(registro[conceptos[i] + "_rentado"] == 1)
		        		{
		        			$("#" + conceptos[i] + "_Si", "#frm-3").prop('checked', true);
		        			$("#" + conceptos[i] + "_Si", "#frm-3").trigger( "click" );
		        		}
		        		else
		        		{
							$("#" + conceptos[i] + "_No", "#frm-3").prop('checked', true);
		        			$("#" + conceptos[i] + "_No", "#frm-3").trigger( "click" );
						}
						if(registro[conceptos[i] + "_id_unidad"] == null) $("#" + conceptos[i] + "_id_unidad",  "#frm-3").val('0');
						else $("#" + conceptos[i] + "_id_unidad", "#frm-3").val(registro[conceptos[i] + "_id_unidad"]);
						$("#" + conceptos[i] + "_cantidad", "#frm-3").val(registro[conceptos[i] + "_cantidad"]);
						$("#" + conceptos[i] + "_precio", "#frm-3").val(registro[conceptos[i] + "_precio"]);
						$("#" + conceptos[i] + "_importe", "#frm-3").val(registro[conceptos[i] + "_importe"]);
						$("#" + conceptos[i] + "_fecha", "#frm-3").val(registro[conceptos[i] + "_fecha"]);
					}
					if(registro.congelado == 2) $('#frm-3 *').attr("disabled", "disabled").unbind();
	    		}
			}
			catch(err)
			{
	    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
			}
	    });
	}, 3000);
}
function obtenerUnidadesPorFormularioConceptoFrm3(conceptos){
	for (var i = 0; i < conceptos.length; i++) {
			$.ajax({
				url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
				type: "POST",
				data:{formulario:"AcondicionamientoDelPredio",concepto:conceptos[i]}
			}).done(function(data){
				concepto = data.split("|")[0];
				datos = data.split("|")[1]
				$('#'+concepto+'_id_unidad', '#frm-3').html(datos);
			});
	}
	
}
function congelarFrm3(){
		var conceptos = ["canal", "trazo", "emparejamiento", "contreo", "despiedre", "reforzamiento", "otros"];
			for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-3").removeAttr('disabled'); }
			var preId = $("#gen-id").val();
			var frm = new FormData(document.getElementById("frm-3"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardaracondicionamientodelpredio/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	respuesta = jQuery.parseJSON(respuesta);
	        	for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-3").attr("disabled","disabled"); }
				try {
					if(respuesta.ok !== undefined)
		        	{
							$.ajax({
							url: "/backend/diagnosticos/congelaracondicionamientopredio/",
							type: "POST",
							data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
							async: false
							}).done(function(respuesta){
								try {
									respuesta = jQuery.parseJSON(respuesta)
									if(respuesta.ok !== undefined)
									{
										ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true,"/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/"+$("#gen-id").val()+"/acondicionamiento/1");
										$('#frm-3 *').attr("disabled", "disabled").unbind();
									}
									else
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
								catch(err) {
									ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
									
							});

		        		//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data;
		        	}
				}
				catch(err) {
	        		ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, intentelo nuevamente", "Ok", "", false, "");
				}
	        });
}

function validaFrm3(){
	return true;
	if($("#canal_Si").is(':checked')) {  

			if($("#canal_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de canal es obligatoria", "Ok", "", false, "");
				$("#canal_id_unidad").focus();
				
				return false;
			}
			
			if($("#canal_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad de canal es obligatoria", "Ok", "", false, "");
				$("#canal_cantidad").focus();
				
				return false;
			}
			
			if($("#canal_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de canal es obligatorio", "Ok", "", false, "");
				$("#canal_precio").focus();
				
				return false;
			}
			
			if($("#canal_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de canal es obligatorio", "Ok", "", false, "");
				$("#canal_importe").focus();
				
				return false;
			}
			
			if($("#canal_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de canal es obligatoria", "Ok", "", false, "");
				$("#canal_fecha").focus();
				
				return false;
			}
        } 
		
		if($("#trazo_Si").is(':checked')) {  

			if($("#trazo_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de trazo de surcado es obligatoria", "Ok", "", false, "");
				$("#trazo_id_unidad").focus();
				
				return false;
			}
			
			if($("#trazo_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad de trazo de surcado es obligatoria", "Ok", "", false, "");
				$("#trazo_cantidad").focus();
				
				return false;
			}
			
			if($("#trazo_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de trazo de surcado es obligatorio", "Ok", "", false, "");
				$("#trazo_precio").focus();
				return false;
			}
			
			if($("#trazo_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de trazo de surcado es obligatorio ", "Ok", "", false, "");
				$("#trazo_importe").focus();
				
				return false;
			}
			
			if($("#trazo_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de trazo de surcado es obligatoria", "Ok", "", false, "");
				$("#trazo_fecha").focus();
				
				return false;
			}
        } 
		
		if($("#emparejamiento_Si").is(':checked')) {  

			if($("#emparejamiento_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de emparejamiento es obligatoria", "Ok", "", false, "");
				$("#emparejamiento_id_unidad").focus();
				
				return false;
			}
			
			if($("#emparejamiento_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad emparejamiento es obligatoria", "Ok", "", false, "");
				$("#emparejamiento_cantidad").focus();
				
				return false;
			}
			
			if($("#emparejamiento_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de emparejamiento es obligatorio", "Ok", "", false, "");
				$("#emparejamiento_precio").focus();
				
				return false;
			}
			
			if($("#emparejamiento_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de emparejamiento es obligatorio ", "Ok", "", false, "");
				$("#emparejamiento_importe").focus();
				
				return false;
			}
			
			if($("#emparejamiento_fecha").val() == ""){
				ShowDialogBox("Validación", "La realización de emparejamiento es obligatoria", "Ok", "", false, "");
				$("#emparejamiento_fecha").focus();
				
				return false;
			}
        } 
		
		
		if($("#contreo_Si").is(':checked')) {  

			if($("#contreo_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de contreo es obligatoria", "Ok", "", false, "");
				$("#contreo_id_unidad").focus();
				
				return false;
			}
			
			if($("#contreo_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad contreo es obligatoria", "Ok", "", false, "");
				$("#contreo_cantidad").focus();
				
				return false;
			}
			
			if($("#contreo_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de contreo es obligatorio", "Ok", "", false, "");
				$("#contreo_precio").focus();
				
				return false;
			}
			
			if($("#contreo_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de contreo es obligatorio", "Ok", "", false, "");
				$("#contreo_importe").focus();
				
				return false;
			}
			
			if($("#contreo_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de contreo es obligatoria", "Ok", "", false, "");
				$("#contreo_fecha").focus();
				
				return false;
			}
        } 
		
		if($("#despiedre_Si").is(':checked')) {  

			if($("#despiedre_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de despiedre es obligatoria", "Ok", "", false, "");
				$("#despiedre_id_unidad").focus();
				
				return false;
			}
			
			if($("#despiedre_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad despiedre es obligatoria", "Ok", "", false, "");
				$("#despiedre_cantidad").focus();
				
				return false;
			}
			
			if($("#despiedre_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de despiedre es obligatorio", "Ok", "", false, "");
				$("#despiedre_precio").focus();
				
				return false;
			}
			
			if($("#despiedre_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de despiedre es obligatorio", "Ok", "", false, "");
				$("#despiedre_importe").focus();
				
				return false;
			}
			
			if($("#despiedre_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de despiedre es obligatoria", "Ok", "", false, "");
				$("#despiedre_fecha").focus();
				
				return false;
			}
        } 
		
		
		if($("#reforzamiento_Si").is(':checked')) {  

			if($("#reforzamiento_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de reforzamiento de cercado es obligatoria", "Ok", "", false, "");
				$("#reforzamiento_id_unidad").focus();
				
				return false;
			}
			
			if($("#reforzamiento_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad reforzamiento de cercado es obligatoria", "Ok", "", false, "");
				$("#reforzamiento_cantidad").focus();
				
				return false;
			}
			
			if($("#reforzamiento_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de reforzamiento de cercado es obligatorio", "Ok", "", false, "");
				$("#reforzamiento_precio").focus();
				
				return false;
			}
			
			if($("#reforzamiento_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de reforzamiento de cercado es obligatorio", "Ok", "", false, "");
				$("#reforzamiento_importe").focus();
				
				return false;
			}
			
			if($("#reforzamiento_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de reforzamiento de cercado es obligatoria", "Ok", "", false, "");
				$("#reforzamiento_fecha").focus();
				
				return false;
			}
        } 
		
		if($("#otros_Si").is(':checked')) {  

			if($("#otros_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de otros es obligatoria", "Ok", "", false, "");
				$("#otros_id_unidad").focus();
				return false;
			}
			
			if($("#otros_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad otros es obligatoria", "Ok", "", false, "");
				$("#otros_cantidad").focus();
				return false;
			}
			
			if($("#otros_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de otros es obligatorio", "Ok", "", false, "");
				$("#otros_precio").focus();
				return false;
			}
			
			if($("#otros_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de otros es obligatorio", "Ok", "", false, "");
				$("#otros_importe").focus();
				return false;
			}
			
			if($("#otros_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de otros es obligatoria", "Ok", "", false, "");
				$("#otros_fecha").focus();
				return false;
			}
        } 

        return true;
}

function calculaImporteFrm3(concepto){
	if(parseFloat($("#" + concepto + "_cantidad", "#frm-3").val()) >= 0 && parseFloat($("#" + concepto + "_precio", "#frm-3").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-3").val(parseFloat($("#" + concepto + "_cantidad", "#frm-3").val()) * parseFloat($("#" + concepto + "_precio", "#frm-3").val()));
	else
		$("#" + concepto + "_importe", "#frm-3").val('');
}