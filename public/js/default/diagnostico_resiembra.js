var doc = null;
var registrofrm4 = null;
var bandera_bio2 =0;
var bandera_ins2 =0;
var BCargaResiembra=0;
$(document).ready(function(){
	
});

function cargarResiembra()
{
	$(".remFrm9", "#frm-9").remove();
	formularios.frm9.cultivos = [];

	if($("#gen-type").val() == 1) $(".inno", "#frm-9").attr("disabled", "disabled");
	else $(".inno", "#frm-9").removeAttr("disabled");

	$.ajax({
        url: "/backend/diagnosticos/obtenerdatosresiembra/",
        type: "POST",
        data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        respuesta = jQuery.parseJSON(respuesta);
	        var info = { cultivo: respuesta.html_cultivo, uso: respuesta.html_uso, marca: { semilla: respuesta.html_marca_semilla, bio: respuesta.html_marca_biofertilizante, ins: respuesta.html_marca_insecticida}, unidad_bio: respuesta.unidad_bio, unidad_ins: respuesta.unidad_ins, tabla: null };
	       //alert(respuesta.unidad_bio.toSource());
		    $('#frm-9 #total_hectareas').val( respuesta.total_hectareas );
	        if(respuesta.ok !== undefined)
	        {
	        	for (var i = 0; i < respuesta.cultivos.length; i++)
	        	{
	        		info.tabla = respuesta.cultivos[i];
	        		agregarFilaCultivoFrm9(info);
					
					if(i==(respuesta.cultivos.length-1)){
						$("#dialog").dialog('close');
						$("#dialog").dialog('destroy');
						//desaparecemos la columna de innovaacion para cuando sea diagnostico
								if($("#gen-type").val()==1){
									if(BCargaResiembra==0){
										BCargaResiembra=1;
										$('.trsiembra tr').each(function(index, element) {
											if(this.id=="fila_generica_0" ){
												$(this).find('th:eq(5)').remove();
											}else if(this.id=="fila_generica_00" ){
												$(this).find('th:eq(6)').remove();
											}else if(this.id.indexOf("gen_despues_")>=0){
												if($(this).find('th').length==7){
													$(this).find('th:eq(5)').remove();
												}else{
													$(this).find('th:eq(6)').remove();
												}
											}else if(this.id.indexOf("bio_despues_")>=0 || this.id.indexOf("ins_despues_")>=0 || this.id=="fila_generica_3" || this.id=="fila_generica_2"){
												$(this).find('td:eq(5)').remove();
											}else{
												$(this).find('td:eq(4)').remove();
											}
											
										});
									}
								}
					}
					bandera_bio2 =0;
					bandera_ins2 =0;
	        	}
				
				
	        	if(respuesta.congelado == 2) $('#frm-9 *').attr("disabled", "disabled").unbind();
			}
	        else if(respuesta.noexiste === undefined){
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
			}else{
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
				//desaparecemos la columna de innovaacion para cuando sea diagnostico
								if($("#gen-type").val()==1){
									if(BCargaResiembra==0){
										BCargaResiembra=1;
										$('.trsiembra tr').each(function(index, element) {
											if(this.id=="fila_generica_0" ){
												$(this).find('th:eq(5)').remove();
											}else if(this.id=="fila_generica_00" ){
												$(this).find('th:eq(6)').remove();
											}else if(this.id.indexOf("gen_despues_")>=0){
												if($(this).find('th').length==7){
													$(this).find('th:eq(5)').remove();
												}else{
													$(this).find('th:eq(6)').remove();
												}
											}else if(this.id.indexOf("bio_despues_")>=0 || this.id.indexOf("ins_despues_")>=0 || this.id=="fila_generica_3" || this.id=="fila_generica_2"){
												$(this).find('td:eq(5)').remove();
											}else{
												$(this).find('td:eq(4)').remove();
											}
											
										});
									}
								}
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente: " + err.message, "Ok", "", false, "");
		}
    });

    $('#btn-save-rsmr', "#frm-9").once('click', function(){
		if(validaFrm9()){
			var preId = $("#gen-id").val();
			$(".frm9date", "#frm-9").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-9"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarresiembra/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	try
	        	{
		        	$(".frm9date", "#frm-9").attr('disabled', 'disabled');
		        	respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/"+$("#gen-id").val()+"/resie/1");
		        	}
		        	else 
		        		ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", intentelo nuevamente", "Ok", "", false, "");
	        	}
	        	catch(error)
	        	{
	        		ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
	        	}
	        });
	    }
	});

	$("#btn-agregar-rsmr", "#frm-9").once('click', function(){
		bandera_bio2 =0;
		bandera_ins2 =0;
		agregarFilaCultivoFrm9();
	});
	$("#btn-clear-rsmr", "#frm-9").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$('input','#frm-9').not("input[type='hidden']").val('');
			$('#frm-9 select').each(function() {
				//$(this).css("background-color","#F00");
				 $(this).find('option:selected').not('.realizo').removeAttr("selected");
				 $(this).eq(0).not('.realizo').prop('selected', 'selected').change();
			});
		});
	});

	$("#btn-congelar-rsmr", "#frm-9").once('click', function(){
		if($("#frm-9 #tabla [id^='gen_despues_']").length>0){
		if(validaFrm9()){
			var preId = $("#gen-id").val();
			$(".frm9date", "#frm-9").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-9"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarresiembra/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	try
	        	{
		        	$(".frm9date", "#frm-9").attr('disabled', 'disabled');
		        	respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm9);
		        	}
		        	else 
		        		ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
	        	}
	        	catch(error)
	        	{
	        		ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
	        	}
	        });
	    }
		}//if validacion
		else{
			ShowDialogBox("Ha ocurrido un error", "Para congelar es necesario que haya por lo menos un registro", "Ok", "", false, "");
		}
	});
	
	
	$('#frm-9 .verificarRango').blur(function(){
		var valor = $(this).val();
		var id = $(this).attr("id").valueOf();
		//alert(id);
		if(valor<1001){
			ShowExecuteBox("Número de semillas por hectárea", "Debe ser mayor a 1,000 el número de semillas por hectárea",function(){
				$('#'+id).focus();
			});
		}else if(valor>100000){
			ShowExecuteBox("Número de semillas por hectárea", "Debe ser menor a 100,000 el número de semillas por hectárea", function(){
				$('#'+id).focus();
			});
		}
	});
	$('#frm-9 .verificarRango2').blur(function(){
		var valor = $(this).val();
		var id = $(this).attr("id").valueOf();
		//alert(id);
		if(valor<0.01){
			ShowExecuteBox("Costo por semilla", "Debe ser mayor a 0.01 el Costo por semilla",function(){
				$('#'+id).focus();
			});
		}else if(valor>10){
			ShowExecuteBox("Costo por semilla", "Debe ser menor a 10 el Costo por semilla", function(){
				$('#'+id).focus();
			});
		}
	});
	
	
	
}
function seleccionaCultivo(obj){
	
		//alert("entra "+$(obj).val());
		$.ajax({
        url: "/backend/diagnosticos/obtenerhectareasdecultivo/",
        type: "POST",
		data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val(), id_cultivo:$(obj).val() },
        async: false
		}).done(function(respuesta){
			//alert(respuesta);
			$('#frm-9 #total_hectareas').val( $('#frm-9 #total_hectareas').val()+parseFloat(respuesta) );
		});

}
function congelarFrm9(){
	$.ajax({
        url: "/backend/diagnosticos/congelarresiembra/",
        type: "POST",
		data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        respuesta = jQuery.parseJSON(respuesta);
	        if(respuesta.ok !== undefined){
	        	ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/resie/1");
				$('#frm-9 *').attr("disabled", "disabled").unbind();	
			}
	        else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function validaFrm9()
{
	return true;
	//validamos los select
	var validacion = true;
	var i=1;
	var misCultivos = [];
	misCultivos.length = 0;
	var apariciones=0;
	var suma =0;
	$("#frm-9 .sem").each(function(index, element) {
		
		misCultivos[i-1]=$('#frm-9 #id_cultivo_'+i).val();
		suma += parseFloat($("#frm-9 #hectareas_sembradas_" + i).val());
    	if($('#id_cultivo_'+i).val()=='' || $('#id_cultivo_'+i).val()=='0' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar un cultivo válido para poder guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_cultivo_'+i).focus();
		}else if($('#id_uso_'+i).val()=='0' || $('#id_uso_'+i).val()=='' && validacion!=false ){
			ShowDialogBox("Validación", "Debe seleccionar un uso en cultivo antes de guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_uso_'+i).focus();
		}else if($('#id_marca_'+i).val()=='0' || $('#id_uso_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar una marca en cultivo antes de guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_marca_'+i).focus();
		}else if($('#id_hov_'+i).val()=='0' || $('#id_uso_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar un nombre de hibrido o variedad en cultivo antes de guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_hov_'+i).focus();
		}else if($('#hectareas_sembradas_'+i).val()<=0 && validacion!=false){
			ShowDialogBox("Validación", "Debe ingresar el número de hectareas sembradas", "Ok", "", false, "");
			validacion = false;
			$('#hectareas_sembradas_'+i).focus();
		}else if($('#cantidad_cultivo_'+i).val()<1001 || $('#cantidad_cultivo_'+i).val()>100000 || $('#cantidad_cultivo_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "El Número de semillas por hectárea debe ser mayor a 1,000 y menor a 100,000", "Ok", "", false, "");
			validacion = false;
			//$('#cantidad_cultivo_'+i).focus();
		}else if($('#precio_cultivo_'+i).val()<0.01 || $('#precio_cultivo_'+i).val()>10 || $('#precio_cultivo_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "El Costo por semilla debe ser mayor a 0.01 y menor a 10", "Ok", "", false, "");
			validacion = false;
			//$('#precio_cultivo_'+i).focus();
		}/*else if($('#id_unidad_cultivo_'+i).val()=='0' || $('#id_unidad_cultivo_'+i).val()=='' ){
			ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en cultivo", "Ok", "", false, "");
			validacion = false;
			$('#id_unidad_cultivo_'+i).focus();
		}*/else{
			//validamos que las hectareas sembradas no superen a las hectareas del cultivo.
			var cultivo = $('#id_cultivo_'+i).val();
			var hectareas_sembradas = parseFloat($("#frm-9 #hectareas_sembradas_" + i).val());//$("#hectareas_sembradas_" + i, "#frm-9").val().trim();
			$.ajax({
			url: "/backend/diagnosticos/comprobarhectareas/",
			type: "POST",
			data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val(), cultivo: cultivo, hectareas:hectareas_sembradas },
			async: false
			}).done(function(respuesta){
				
				if(respuesta == 1 && validacion!=false){
					ShowDialogBox("Validación", "El número de hectáreas sembradas es mayor que el cultivo", "Ok", "", false, "");
					validacion = false;
					$('#hectareas_sembradas_'+i).focus();
				}
			});
		}
		
			var j=1;
			$(".bio1").each(function(index, element) {
				if($('#ino_con_bio_'+i).val()=='1'){
					if($('#id_empresa_bio_'+i+'_'+j).val()=='0' || $('#id_empresa_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Validación", "Debe seleccionar una empresa en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_empresa_bio_'+i).focus();
					}else if($('#id_marca_bio_'+i+'_'+j).val()=='0' || $('#id_marca_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Validación", "Debe seleccionar una marca en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_marca_bio_'+i).focus();
					}else if($('#id_activo_bio_'+i+'_'+j).val()=='0' || $('#id_activo_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Ha ocurrido un error", "Debe seleccionar un activo en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_activo_bio_'+i).focus();
					}else if($('#id_unidad_bio_'+i+'_'+j).val()=='0' || $('#id_unidad_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_unidad_bio_'+i).focus();
					}else if($('#fecha_bio_'+i+'_'+j).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar una fecha para la Inoculación con biofertilizante", "Ok", "", false, "");
							validacion = false;
							$('#fecha_bio_'+i).focus();
					}
				}
				
				j++;
			});
			
				var k=1;
				$(".ins1").each(function(index, element) {
					if($('#ino_con_ins_'+i).val()=='1'){
						if($('#id_empresa_ins_'+i+'_'+j).val()=='0' || $('#id_empresa_ins_'+i+'_'+j).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una empresa en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_empresa_ins_'+i).focus();
						}else if($('#id_marca_ins_'+i+'_'+k).val()=='0' || $('#id_marca_ins_'+i+'_'+k).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una marca en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_marca_ins_'+i).focus();
						}else if($('#id_activo_ins_'+i+'_'+k).val()=='0' || $('#id_activo_ins_'+i+'_'+k).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar un activo en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_activo_ins_'+i).focus();
						}else if($('#id_unidad_ins_'+i+'_'+k).val()=='0' || $('#id_unidad_ins_'+i+'_'+k).val()=='' && validacion!=false ){
							ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_unidad_ins_'+i).focus();
						}else if($('#fecha_ins_'+i+'_'+k).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar una fecha para la Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#fecha_ins_'+i).focus();
						}
						
					}
					k++;
				});
		i++;
	});
	//alert(suma+" - "+$('#total_hectareas').val());
	//ordenamos los valores del array y comparamos si el de la posicion + 1 es igual o duplicado
	var sorted_arr = misCultivos.sort(); 
	var results = [];
	for (var i = 0; i < misCultivos.length - 1; i++) {
		if (sorted_arr[i + 1] == sorted_arr[i]) {
			results.push(sorted_arr[i]);
		}
	}

	if(results.length>0 && validacion!=false){
		ShowDialogBox("Validación", "No es posible agregar cultivos duplicados", "Ok", "", false, "");
		validacion = false;
	}

	if(suma>$('#total_hectareas').val() && validacion!=false){
		ShowDialogBox("Validación", "Verifique el número de hectáreas sembradas.", "Ok", "", false, "");
		validacion = false;
	}	
	
	
	return validacion;
}


function calculaImporteFrm9(obj){
		
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
			var cantidad = $(parent2).find(' td:eq(0) input').attr("value");
			var precio = $(parent2).find(' td:eq(1) input').attr("value");
			var importe = $(parent2).find(' td:eq(2) input');
			var hectareas = $(parent2).find('th input').not("input[type='hidden']").attr("value");

		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		hectareas = parseFloat(hectareas);
		//alert(hectareas);
		if(cantidad>0 && precio>0 && hectareas>0){
			importe.val(cantidad*precio*hectareas);
		}else{
			importe.val('');
		}
}
function calculaImporte2Frm9(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
	//var parent3 = $(obj).parents().get(3);
	
			var cantidad = $(parent2).find(' td:eq(1) input').attr("value");
			var precio = $(parent2).find(' td:eq(2) input').attr("value");
			var importe = $(parent2).find(' td:eq(3) input');
			//var hectareas = $('.semilla').attr("value");
			//alert(hectareas);
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		//hectareas = parseFloat(hectareas);
		
		if(cantidad>0 && precio>0 /*&& hectareas>0*/){
			importe.val(cantidad*precio/**hectareas*/);
		}else{
			importe.val('');
		}
}

function agregarFilaCultivoFrm9(info_tabla)
{
	formularios.frm9.cultivos.push( { bio: 0, insect: 0 } );
	var act = formularios.frm9.cultivos.length;
	var div0 = "gen_despues_" + act;
	var div1= "sem_despues_" + act;
	var div2 = "bio_despues_" + act + "_1";
	var div3 = "ins_despues_" + act + "_1";

	$("#frm-9 #tabla").append(
		$("#frm-9 #tabla tbody #fila_generica_0").clone().removeClass("hide").attr("id", div0).addClass("gen remFrm9")
	);

	$("#frm-9 #tabla").append(
		$("#frm-9 #tabla tbody #fila_generica_1").clone().removeClass("hide").attr("id", div1).addClass("sem remFrm9")
	);
	
	//colocamos el registro header para las inoculaciones
	$("#frm-9 #tabla").append(
		$("#frm-9 #tabla tbody #fila_generica_00").clone().removeClass("hide").attr("id", div0+"_"+act).addClass("gen remFrm9")
	);

	$("#frm-9 #tabla #" + div1 + " #id_registro_").attr("id", "id_registro_" + act).attr("name", "tabla[" + (act-1) + "][id_registro]");
	$("#frm-9 #tabla #" + div1 + " #id_cultivo_").attr("id", "id_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][id_cultivo]");
	$("#frm-9 #tabla #" + div1 + " #id_uso_").attr("id", "id_uso_" + act).attr("name", "tabla[" + (act-1) + "][id_uso]");
	$("#frm-9 #tabla #" + div1 + " #id_marca_").attr("id", "id_marca_" + act).attr("name", "tabla[" + (act-1) + "][id_marca]").attr("onChange", "javascript:loadHOV(" + act + ")");
	$("#frm-9 #tabla #" + div1 + " #thov_").attr("id", "thov_" + act).attr("name", "tabla[" + (act-1) + "][thov]").attr("onChange", "javascript:loadHOV(" + act + ")");
	$("#frm-9 #tabla #" + div1 + " #id_hov_").attr("id", "id_hov_" + act).attr("name", "tabla[" + (act-1) + "][id_hov]");
	$("#frm-9 #tabla #" + div1 + " #hectareas_sembradas_").attr("id", "hectareas_sembradas_" + act).attr("name", "tabla[" + (act-1) + "][hectareas_sembradas]");
	$("#frm-9 #tabla #" + div1 + " #id_unidad_cultivo_").attr("id", "id_unidad_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][id_unidad_cultivo]");
	$("#frm-9 #tabla #" + div1 + " #cantidad_cultivo_").attr("id", "cantidad_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][cantidad_cultivo]");
	$("#frm-9 #tabla #" + div1 + " #precio_cultivo_").attr("id", "precio_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][precio_cultivo]");
	$("#frm-9 #tabla #" + div1 + " #importe_cultivo_").attr("id", "importe_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][importe_cultivo]");
	$("#frm-9 #tabla #" + div1 + " #fecha_cultivo_").attr("id", "fecha_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][fecha_cultivo]").addClass('frm9date');
	$("#frm-9 #tabla #" + div1 + " #innovacion_cultivo_").attr("id", "innovacion_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][innovacion_cultivo]");

	$('#frm-9 .date').datepicker({ format: 'yyyy/mm/dd' });

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////// Llenar los campos de cada tabla ////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//alert("aqui "+info_tabla.toSource() );
	if(info_tabla !== undefined)
	{
		$("#frm-9 #tabla #" + div1 + " #id_registro_" + act).val(info_tabla.tabla.registro);
		//$("#frm-9 #tabla #" + div1 + " #hectareas_sembradas_" + act+" .semilla").val(info_tabla.tabla.numero_hectareas);
		
		$("#frm-9 #tabla #" + div1 + " #id_cultivo_" + act).html("<option value='" + info_tabla.tabla.id + "' selected>" + info_tabla.tabla.cultivo + "</option>");
		/*if ( $("#frm-9 #tabla #" + div1 + " #id_cultivo_" + act + " option[value='" + info_tabla.tabla.cultivo + "']").length > 0 )
		{
			//alert(info_tabla.tabla.cultivo);
	 		$("#frm-9 #tabla #" + div1 + " #id_cultivo_" + act).val(info_tabla.tabla.cultivo);
		}
		else
		{
			//alert("unico "+info_tabla.tabla.cultivo+"-"+info_tabla.tabla.id);
			obtenerunico("#frm-9 #tabla #" + div1 + " #id_cultivo_" + act, 'cultivo', info_tabla.tabla.id);
		}*/
		
		$("#frm-9 #tabla #" + div1 + " #id_uso_" + act).html(info_tabla.uso);
		if ( $("#frm-9 #tabla #" + div1 + " #id_uso_" + act + " option[value='" + info_tabla.tabla.uso + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div1 + " #id_uso_" + act).val(info_tabla.tabla.uso);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div1 + " #id_uso_" + act, 'uso', info_tabla.tabla.uso);
		}
		
		$("#frm-9 #tabla #" + div1 + " #id_marca_" + act).html(info_tabla.marca.semilla);
		if ( $("#frm-9 #tabla #" + div1 + " #id_marca_" + act + " option[value='" + info_tabla.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div1 + " #id_marca_" + act).val(info_tabla.tabla.marca);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div1 + " #id_marca_" + act, 'marca', info_tabla.tabla.marca);
		}

		$("#frm-9 #tabla #" + div1 + " #id_hov_" + act).html(info_tabla.tabla.html_hov);
		if ( $("#frm-9 #tabla #" + div1 + " #id_hov_" + act + " option[value='" + info_tabla.tabla.hov + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div1 + " #id_hov_" + act).val(info_tabla.tabla.hov);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div1 + " #id_hov_" + act, 'hov', info_tabla.tabla.hov);
		}

		$("#frm-9 #tabla #" + div1 + " #id_unidad_cultivo_" + act).html(info_tabla.unidad);
		if ( $("#frm-9 #tabla #" + div1 + " #id_unidad_cultivo_" + act + " option[value='" + info_tabla.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div1 + " #id_unidad_cultivo_" + act).val(info_tabla.tabla.concepto.unidad);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div1 + " #id_unidad_cultivo_" + act, 'unidad', info_tabla.tabla.concepto.unidad);
		}

		$("#frm-9 #tabla #" + div1 + " #thov_" + act).val(info_tabla.tabla.thov);
		//$("#frm-9 #tabla #" + div1 + " #hectareas_sembradas_" + act).val(info_tabla.tabla.hectareas);
		$("#frm-9 #tabla #" + div1 + " .resemilla").val(info_tabla.tabla.numero_hectareas);
		$("#frm-9 #tabla #" + div1 + " #cantidad_cultivo_" + act).val(info_tabla.tabla.concepto.cantidad);
		$("#frm-9 #tabla #" + div1 + " #precio_cultivo_" + act).val(info_tabla.tabla.concepto.precio);
		$("#frm-9 #tabla #" + div1 + " #importe_cultivo_" + act).val(info_tabla.tabla.concepto.importe);
		$("#frm-9 #tabla #" + div1 + " #fecha_cultivo_" + act).val(info_tabla.tabla.concepto.fecha_realizacion);
		$("#frm-9 #tabla #" + div1 + " #innovacion_cultivo_" + act).val(info_tabla.tabla.concepto.innovacion);

		var info_inoculacion = { uso: info_tabla.uso, marca: null, hibrido_variedad: info_tabla.hibrido_variedad, unidad_bio: info_tabla.unidad_bio,unidad_ins: info_tabla.unidad_ins, tabla: null };
		//alert(info_tabla.tabla.toSource());
		if(info_tabla.tabla.inoculaciones!=null ){
			if( info_tabla.tabla.cuantos_bio<1 ){
				if($('#frm-9 .bio'+act).length==0){
					if(bandera_bio2==0){
						$("#frm-9 #tabla").append(
							$("#frm-9 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm9")
						);
					}
				agregarFilaBioFrm9(act, null);
				}
			}
			for (var i = 0; i < info_tabla.tabla.inoculaciones.length; i++)
			{
				info_inoculacion.tabla = info_tabla.tabla.inoculaciones[i];
				if(info_tabla.tabla.inoculaciones[i].tipo == 1)
				{
					if(bandera_bio2==0){
						$("#frm-9 #tabla").append(
							$("#frm-9 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm9")
						);
					}
					info_inoculacion.marca = info_tabla.marca.bio;
					agregarFilaBioFrm9(act, info_inoculacion);
				}
				else
				{
					if(bandera_ins2==0){
						$("#frm-9 #tabla").append(
							$("#frm-9 #tabla tbody #fila_generica_ins").clone().removeClass("hide").attr("id","realizo_inoculacion_ins_"+act).addClass("ino_ins_"+act +" remFrm9")
						);
					}
					info_inoculacion.marca = info_tabla.marca.ins;
					agregarFilaInsFrm9(act, info_inoculacion);
				}
			}
			//si ya termino de recorrer, revisamos si hay inoculaciones de los  tipos
			//sino ponemos uno vacio
			if(info_tabla.tabla.cuantos_ins<1){
				if($('#frm-9 .ins'+act).length==0){
					if(bandera_ins2==0){
						$("#frm-9 #tabla").append(
							$("#frm-9 #tabla tbody #fila_generica_ins").clone().removeClass("hide").attr("id","realizo_inoculacion_ins_"+act).addClass("ino_ins_"+act +" remFrm9")
						);
					}
					agregarFilaInsFrm9(act, null);
				}	
			}
		}else{//if is null inoculaciones
			//alert("es null");
			if(bandera_bio2==0){
				$("#frm-9 #tabla").append(
					$("#frm-9 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm9")
				);
			}
			//info_inoculacion.marca = info_tabla.marca.bio;
			agregarFilaBioFrm9(act, null);
			
			if(bandera_ins2==0){
				$("#frm-9 #tabla").append(
					$("#frm-9 #tabla tbody #fila_generica_ins").clone().removeClass("hide").attr("id","realizo_inoculacion_ins_"+act).addClass("ino_ins_"+act +" remFrm9")
				);
			}
			//info_inoculacion.marca = info_tabla.marca.ins;
			agregarFilaInsFrm9(act, null);
		}
	}else{
		loadCultivos2(act);
		loadUsos2(act);
		loadMarcas2(act);
		//loadUnidades2(act,0,"");
		/*agregarFilaBioFrm9(act);
		agregarFilaInsFrm9(act);*/
		if(bandera_bio2==0){
				$("#frm-9 #tabla").append(
					$("#frm-9 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm9")
				);
			}
			//info_inoculacion.marca = info_tabla.marca.bio;
			agregarFilaBioFrm9(act, null);
			
			if(bandera_ins2==0){
				$("#frm-9 #tabla").append(
					$("#frm-9 #tabla tbody #fila_generica_ins").clone().removeClass("hide").attr("id","realizo_inoculacion_ins_"+act).addClass("ino_ins_"+act +" remFrm9")
				);
			}
			//info_inoculacion.marca = info_tabla.marca.ins;
			agregarFilaInsFrm9(act, null);
	}
	
	//separamos los cultivos
	$("#frm-9 #tabla").append("<tr class='remFrm9'><td colspan='8'></td></tr>");
}

function loadCultivos2(padre)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenercultivoscatalogo/",
        type: "POST",
		data:{ id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta.indexOf('value="0"') > -1)
	        	$("#frm-9 #id_cultivo_" + padre).html(respuesta);
	        else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function loadUsos2(padre)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenerusos/",
        type: "POST",
        async: false
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta.indexOf('value="0"') > -1)
	        	$("#id_uso_" + padre).html(respuesta);
	        else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function loadMarcas2(padre)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenermarcas/",
        type: "POST",
		data: { tipo: 1 },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta.indexOf('value="0"') > -1)
	        	$("#id_marca_" + padre).html(respuesta);
	        else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function loadUnidades2(padre,hijo,tipo)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
        type: "POST",
        data:{formulario:"Resiembra",concepto:tipo}
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta.indexOf('value="0"') > -1){
	        	//$("#id_unidad_cultivo_" + padre).html(respuesta);
				$("#id_unidad_" + tipo + "_" + padre + "_" + hijo).html(respuesta);
			}else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}
function agregarFilaBioFrm9(padre, info = null)
{
	//alert(info.toSource());
	var ultimo = null
	$('#frm-9 .bio' + padre).each(function(){
		ultimo = this;
	});


	formularios.frm9.cultivos[padre-1].bio = formularios.frm9.cultivos[padre-1].bio + 1;
	var act = formularios.frm9.cultivos[padre-1].bio;
	var div = "bio_despues_" + padre + "_" + act;

	if(ultimo == null)
	{
		$("#frm-9 #tabla").append(
			$("#frm-9 #tabla tbody #fila_generica_2").clone().removeClass("hide").attr("id", div).addClass("bio" + padre + " remFrm9")
		);
		$("#frm-9 #tabla #" + div + " #eliminarFila").attr("id", "agregarFilaBio_" + padre + "_1").removeClass("btn-danger fa-times-circle").addClass("btn-warning fa-plus-circle").attr("onClick", "javascript:agregarFilaBioFrm9(" + padre + ")");
	}
	else
	{
		$("#frm-9 #" + ultimo.id).after(
			$("#frm-9 #tabla tbody #fila_generica_2").clone().removeClass("hide").attr("id", div).addClass("bio" + padre + " remFrm9")
		);
		if(info == null)
			$("#frm-9 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaBio_" + padre + "_1").attr("onClick", "eliminarItem('" + div + "')");
		else
			$("#frm-9 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaBio_" + padre + "_1").attr("onClick", "eliminarItem('" + div + "', " + info.tabla.id + ")");
	}

	$("#frm-9 #tabla #" + div + " #id_bio_").attr("id", "id_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id]");
	$("#frm-9 #tabla #" + div + " #tipo_bio_").attr("id", "tipo_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][tipo]").attr("value", "1");
	$("#frm-9 #tabla #" + div + " #id_marca_bio_").attr("id", "id_marca_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id_marca]");//.attr("onChange", "javascript:loadActivo(" + padre + "," + act + ", 'bio')");
	$("#frm-9 #tabla #" + div + " #id_empresa_bio_").attr("id", "id_empresa_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][empresa_id]").attr("onChange", "loadEmpresas('5',this)");
	//$("#frm-9 #tabla #" + div + " #id_activo_bio_").attr("id", "id_activo_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id_activo]");
	$("#frm-9 #tabla #" + div + " #id_unidad_bio_").attr("id", "id_unidad_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id_unidad]");
	$("#frm-9 #tabla #" + div + " #cantidad_bio_").attr("id", "cantidad_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][cantidad]");
	$("#frm-9 #tabla #" + div + " #precio_bio_").attr("id", "precio_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][precio]");
	$("#frm-9 #tabla #" + div + " #importe_bio_").attr("id", "importe_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][importe]");
	$("#frm-9 #tabla #" + div + " #fecha_bio_").attr("id", "fecha_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][fecha]").addClass("frm9date");
	$("#frm-9 #tabla #" + div + " #innovacion_bio_").attr("id", "innovacion_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][innovacion]");
	
	if(bandera_bio2==0){
		$("#frm-9 #tabla #realizo_inoculacion_bio_"+padre+" #ino_con_bio").attr("id", "ino_con_bio_" + padre).attr("name", "tabla[" + (padre-1) + "][bio]["+(act-1)+"][realizo_ino_bio]").trigger("change");
		bandera_bio2=1;	
	}
	$('.date').datepicker({ format: 'yyyy/mm/dd' });

	if(info != null)
	{
		//alert(info.tabla.marca);
		$("#frm-9 #tabla #realizo_inoculacion_bio_"+padre+" #ino_con_bio_"+ padre).val(info.realizo).trigger("change");
		$("#frm-9 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act).html(info.tabla.marca);
		$("#frm-9 #tabla #" + div + " #id_empresa_bio_" + padre + "_" + act).html(info.tabla.empresa);

		/*$("#frm-9 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act).html(info.marca);
		if ( $("#frm-9 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act + " option[value='" + info.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act).val(info.tabla.marca);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act, 'marca', info.tabla.marca);
		}

		$("#frm-9 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act).html(info.tabla.marcaactivo);
		if ( $("#frm-9 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act + " option[value='" + info.tabla.activo + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act).val(info.tabla.activo);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act, 'activo', info.tabla.activo);
		}
*/
		$("#frm-9 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act).html(info.unidad_bio);
		//alert(info.unidad_bio);
		if ( $("#frm-9 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act + " option[value='" + info.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act).val(info.tabla.concepto.unidad);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act, 'unidad', info.tabla.concepto.unidad);
		}

		$("#frm-9 #tabla #" + div + " #id_bio_" + padre + "_" + act).val(info.tabla.id);
		$("#frm-9 #tabla #" + div + " #tipo_bio_" + padre + "_" + act).val(info.tabla.tipo);
		$("#frm-9 #tabla #" + div + " #cantidad_bio_" + padre + "_" + act).val(info.tabla.concepto.cantidad);
		$("#frm-9 #tabla #" + div + " #precio_bio_" + padre + "_" + act).val(info.tabla.concepto.precio);
		$("#frm-9 #tabla #" + div + " #importe_bio_" + padre + "_" + act).val(info.tabla.concepto.importe);
		$("#frm-9 #tabla #" + div + " #fecha_bio_" + padre + "_" + act).val(info.tabla.concepto.fecha_realizacion);
		$("#frm-9 #tabla #" + div + " #innovacion_bio_" + padre + "_" + act).val(info.tabla.concepto.innovacion);
	}
	else
	{
		//loadMarcas(padre, act, 'bio', 2);
		loadUnidades2(padre, act, 'bio', 2);
	}
}

function agregarFilaInsFrm9(padre, info = null)
{
	var ultimo = null
	$('#frm-9 .ins' + padre).each(function(){
		ultimo = this;
	});

	formularios.frm9.cultivos[padre-1].insect = formularios.frm9.cultivos[padre-1].insect + 1;
	var act = formularios.frm9.cultivos[padre-1].insect;
	var div = "ins_despues_" + padre + "_" + formularios.frm9.cultivos[padre-1].insect;

	if(ultimo == null)
	{
		$("#frm-9 #tabla").append(
			$("#frm-9 #tabla tbody #fila_generica_3").clone().removeClass("hide").attr("id", div).addClass("ins" + padre + " remFrm9")
		);
		$("#frm-9 #tabla #" + div + " #eliminarFila").attr("id", "agregarFilaIns_" + padre + "_1").removeClass("btn-danger fa-times-circle").addClass("btn-warning fa-plus-circle").attr("onClick", "javascript:agregarFilaInsFrm9(" + padre + ")");
	}
	else
	{
		$("#frm-9 #" + ultimo.id).after(
			$("#frm-9 #tabla tbody #fila_generica_3").clone().removeClass("hide").attr("id", div).addClass("ins" + padre + " remFrm9")
		);
		if(info == null)
			$("#frm-9 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaIns_" + padre + "_1").attr("onClick", "eliminarItem('" + div + "')");
		else
			$("#frm-9 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaIns_" + padre + "_1").attr("onClick", "eliminarItem('" + div + "', " + info.tabla.id + ")");
	}

	$("#frm-9 #tabla #" + div + " #id_ins_").attr("id", "id_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id]");
	$("#frm-9 #tabla #" + div + " #tipo_ins_").attr("id", "tipo_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][tipo]").attr("value", "2");
	$("#frm-9 #tabla #" + div + " #id_marca_ins_").attr("id", "id_marca_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id_marca]");//.attr("onChange", "javascript:loadActivo(" + padre + "," + act + ", 'ins')");
	$("#frm-9 #tabla #" + div + " #id_empresa_ins_").attr("id", "id_empresa_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][empresa_id]").attr("onChange", "loadEmpresas('1',this)");
	//$("#frm-9 #tabla #" + div + " #id_activo_ins_").attr("id", "id_activo_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id_activo]");
	$("#frm-9 #tabla #" + div + " #id_unidad_ins_").attr("id", "id_unidad_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id_unidad]");
	$("#frm-9 #tabla #" + div + " #cantidad_ins_").attr("id", "cantidad_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][cantidad]");
	$("#frm-9 #tabla #" + div + " #precio_ins_").attr("id", "precio_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][precio]");
	$("#frm-9 #tabla #" + div + " #importe_ins_").attr("id", "importe_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][importe]");
	$("#frm-9 #tabla #" + div + " #fecha_ins_").attr("id", "fecha_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][fecha]").addClass("frm9date");
	$("#frm-9 #tabla #" + div + " #innovacion_ins_").attr("id", "innovacion_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][innovacion]");
	
	
	if(bandera_ins2==0){
		$("#frm-9 #tabla #realizo_inoculacion_ins_"+padre+" #ino_con_ins").attr("id", "ino_con_ins_"+ padre).attr("name", "tabla[" + (padre-1) + "][ins]["+(act-1)+"][realizo_ino_ins]").trigger("change");
		bandera_ins2=1;	
	}
	
	$('#frm-9 .date').datepicker({ format: 'yyyy/mm/dd' });

	if(info != null)
	{
		$("#frm-9 #tabla #realizo_inoculacion_ins_"+padre+" #ino_con_ins_"+ padre).val(info.realizo).trigger("change");
		$("#frm-9 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act).html(info.tabla.marca);
		$("#frm-9 #tabla #" + div + " #id_empresa_ins_" + padre + "_" + act).html(info.tabla.empresa);

		/*$("#frm-9 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act).html(info.marca);
		if ( $("#frm-9 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act + " option[value='" + info.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act).val(info.tabla.marca);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act, 'marca', info.tabla.marca);
		}

		$("#frm-9 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act).html(info.tabla.marcaactivo);
		if ( $("#frm-9 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act + " option[value='" + info.tabla.activo + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act).val(info.tabla.activo);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act, 'activo', info.tabla.activo);
		}
*/
		$("#frm-9 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act).html(info.unidad_ins);
		if ( $("#frm-9 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act + " option[value='" + info.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-9 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act).val(info.tabla.concepto.unidad);
		}
		else
		{
			obtenerunico("#frm-9 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act, 'unidad', info.tabla.concepto.unidad);
		}

		$("#frm-9 #tabla #" + div + " #id_ins_" + padre + "_" + act).val(info.tabla.id);
		$("#frm-9 #tabla #" + div + " #tipo_ins_" + padre + "_" + act).val(info.tabla.tipo);
		$("#frm-9 #tabla #" + div + " #cantidad_ins_" + padre + "_" + act).val(info.tabla.concepto.cantidad);
		$("#frm-9 #tabla #" + div + " #precio_ins_" + padre + "_" + act).val(info.tabla.concepto.precio);
		$("#frm-9 #tabla #" + div + " #importe_ins_" + padre + "_" + act).val(info.tabla.concepto.importe);
		$("#frm-9 #tabla #" + div + " #fecha_ins_" + padre + "_" + act).val(info.tabla.concepto.fecha_realizacion);
		$("#frm-9 #tabla #" + div + " #innovacion_ins_" + padre + "_" + act).val(info.tabla.concepto.innovacion);
	}
	else
	{
		//loadMarcas(padre, act, 'ins', 3);
		loadUnidades2(padre, act, 'ins', 3);
	}
	
		
	
}

function eliminarItem(div, id = null)
{
	if(id != null){
		ShowConfirmBox("Registro almacenado", "Este registro ya se encuentra almacenado, si lo elimina no podrá revertise, ¿Desea continuar?", eliminarItem_2, id, div);
	}
	else{
		$("#" + div).remove();
	}
}

function eliminarItem_2(registro, div){
	var url = "/backend/diagnosticos/eliminarinoculacionfrm9/";
	$.ajax({
        url: url,
        type: "POST",
        data: { id: registro },
        async: false
    }).done(function(respuesta){
        respuesta = jQuery.parseJSON(respuesta);
        if(respuesta.ok === undefined)
        	ShowDialogBox("Ha ocurrido un error", "No se han podido eliminar el registro, inténtelo nuevamente", "Ok", "", false, "");
        else
        {
        	ShowDialogBox("Registro eliminado", "Se eliminó correctamente el registro", "Ok", "", false, "");
        	$("#" + div).remove();
        }
    });
}