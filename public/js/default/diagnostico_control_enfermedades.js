var dg_congelado = false;
var MisDatos = new Array();
var cargadoCenf = 0;



$(document).ready(function(){

//cargar los catalogos en los select
//validar el formulario
//editar formulario

});

function cargarControlEnfermedades()
{
	//var conceptos = ["fungicida", "bactericida", "viricida", "bioestimulante"];


	if($("#gen-type").val() == 1) 
		$(".inno", "#frm-15").attr("disabled", "disabled");
	else 
		$(".inno", "#frm-15").removeAttr("disabled");
	
	if($("#frm-15 #congelado").val() == 2){
		$('#frm-15 *').attr("disabled", "disabled").unbind();	
	}
	
		//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
			$('#frm-15 #tabla tr:eq(0)').each(function(index, element) {
					//$(this).find('th:eq(7)').remove();
					$(this).find('th:eq(7)').remove();
			});
			$('#frm-15 #tabla tr').not('#frm-15 #tabla tr:eq(0)').each(function(index, element) {
					//$(this).find('th:eq(7)').remove();
					$(this).find('td:eq(6)').remove();
			});
	}


	//asignamos las unidades a los conceptos dependiendo la configuracion general
	//obtenerUnidadesPorFormularioConceptoFrm15(conceptos);
/*	$.ajax({
        url: "/backend/diagnosticos/obtenerdatoscontroldeenfermedades/",
        type: "POST",
        data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
    	var info;
    	try
    	{
	        respuesta = jQuery.parseJSON(respuesta);
	        //alert(info.cultivo);
	        if(respuesta.ok !== undefined)
	        {
				//lleno los clones
				//alert($("#frm-12 #tabla #pre_marca_marca_").html().toString());
				if(cargadoCenf != 1){//validacion para no cargar 2 veces el form
					cargadoCenf=1;
					//asignamos los datos generales
					
	        	}//validacion de cargado
				//agregarFilaExtraFrm11('','',respuesta);
	        	if(respuesta.congelado == 2){
					$('#frm-15 *').attr("disabled", "disabled").unbind();
				}
			}else if(respuesta.noexiste === undefined){
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el formulario, por favor intentelo nuevamente", "Ok", "", false, "");
			}else{
				//formulario nuevo, cargo la informacion de los catalogos en los select
				//alert($('#frm-15 #tabla #fungicida_id_unidad').html());
				$('#frm-15 #tabla #fungicida_id_unidad').html(respuesta.select_fungicida_unidad);
				$('#frm-15 #tabla #fungicida_id_etapa_fenologica').html(respuesta.select_fungicida_etapa_fenologica);
				$('#frm-15 #tabla #bactericida_id_unidad').html(respuesta.select_bactericida_unidad);
				$('#frm-15 #tabla #bactericida_id_etapa_fenologica').html(respuesta.select_bactericida_etapa_fenologica);
				$('#frm-15 #tabla #viricida_id_unidad').html(respuesta.select_viricida_unidad);
				$('#frm-15 #tabla #viricida_id_etapa_fenologica').html(respuesta.select_viricida_etapa_fenologica);
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor intentelo nuevamente: " + err.message, "Ok", "", false, "");
		}
	
	});*/

    $('#btn-save-cenf', "#frm-15").once('click', function(){
		if(validarFrm15())
		{
			$.ajax({
				url: "/backend/diagnosticos/guardarcontroldeenfermedades/",
				type: "POST",
				data: $("#frm-15").serialize()
			}).done(function(respuesta)
			{
				ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/control_enfermedades/1");
			});
		}
		else
		{
			ShowDialogBox("Validación", "El formulario está incompleto; favor de completarlo.", "Ok", "", false, "");
		}
	});


	$("#btn-congelar-cenf", "#frm-15").once('click', function(){
		if(validarFrm15())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertirse, ¿Desea continuar?", congelarFrm15);
	});
	
	$("#btn-clean-cenf", "#frm-15").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$('input,textarea','#frm-15').val('');
			$('#frm-15 select').not("[id^='tipo_ap']").each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
			$('#frm-15 [id^="tipo_ap"]').each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).val('2').change();
			});
		});
	});
}

function obtenerUnidadesPorFormularioConceptoFrm15(conceptos){
	for (var i = 0; i < conceptos.length; i++) {
			$.ajax({
				url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
				type: "POST",
				data:{formulario:"ControlDeEnfermedades",concepto:conceptos[i]}
			}).done(function(data){
				concepto = data.split("|")[0];
				datos = data.split("|")[1]
				$('#'+concepto+'_id_unidad', '#frm-15').html(datos);
			});
	}
	
}

function congelarFrm15(){
			$.ajax({
				url: "/backend/diagnosticos/guardarcontroldeenfermedades/",
				type: "POST",
				data: $("#frm-15").serialize()
			}).done(function(data)
			{
				//resp = jQuery.parseJSON(data);
					$.ajax({
					url: "/backend/diagnosticos/congelarcontroldeenfermedades/",
					type: "POST",
					data: { id: $("#gen-id").val(),  tipo:$("#gen-type").val() },
					async: false
					}).done(function(respuesta){
						try
						{
							respuesta = jQuery.parseJSON(respuesta);
							if(respuesta.ok !== undefined){
								ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/control_enfermedades/1");
								$('#frm-15 *').attr("disabled", "disabled").unbind();	
							}
							else
								ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
						}
						catch(err)
						{
							ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
						}
					});

				//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "", false);
			});
}

function validarFrm15()
{
	return true;
	var bandera=true;
	$("#frm-15 .required").each(function(i){
		//console.log($(this).val());
		if($(this).val()=="" || $(this).val()=="0")
		{
			$(this).focus();	
			bandera=false;
		}

	});

	return bandera;

}
function evaluaCambioAplicacionFrm15(id){
	if($("#frm-15 #"+id).val() == 1 || $("#frm-15 #"+id).val() == 3){
		//mecanizada
		$("#frm-15 #tabla #mecanizada_div").removeClass("hide");
		$("#frm-15 #tabla #manual_div").addClass("hide");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia").removeClass("hide");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_rentada").addClass("hide");
		
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select option:eq(0)").prop('selected', true);
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('').addClass("required");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_operador").val('').addClass("required");

		$("#frm-15 #tabla #manual_div #manual_div_no_jornales").removeClass("required");
		$("#frm-15 #tabla #manual_div #manual_div_costo_jornal").removeClass("required");
	}else{
		//manual
		$("#frm-15 #tabla #manual_div ").removeClass("hide");
		$("#frm-15 #tabla #mecanizada_div").addClass("hide");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
		$("#frm-15 #tabla #manual_div #manual_div_no_jornales").addClass("validar");
		$("#frm-15 #tabla #manual_div #manual_div_costo_jornal").addClass("validar");
		
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select option:eq(0)").prop('selected', true);
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('').removeClass("required");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_operador").val('').removeClass("required");

		$("#frm-15 #tabla #manual_div #manual_div_no_jornales").val('');
		$("#frm-15 #tabla #manual_div #manual_div_costo_jornal").val('');
	}
}
function evaluaCambioAplicacion2Frm15(id){
	if($("#frm-15 #"+id).val() == 1){
		//propia
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia").removeClass("hide");
		$("#frm-15 #tabla #manual_div").addClass("hide");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_rentada").addClass("hide");
		
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('').addClass("required");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_operador").val('').addClass("required");
		
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_rentada").removeClass("required");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val('').removeClass("required");
		
		
		$("#frm-15 #tabla #manual_div #manual_div_no_jornales").removeClass("required");
		$("#frm-15 #tabla #manual_div #manual_div_costo_jornal").removeClass("required");
	}else{
		//rentada
		$("#frm-15 #tabla #manual_div ").addClass("hide");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia").addClass("hide");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_rentada").removeClass("hide");
		
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select option:eq(2)").prop('selected', true);
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val('').addClass("required");
		
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_combustible").removeClass("required");
		$("#frm-15 #tabla #mecanizada_div #mecanizada_div_select_div_propia_operador").removeClass("required");

		$("#frm-15 #tabla #manual_div #manual_div_no_jornales").val('');
		$("#frm-15 #tabla #manual_div #manual_div_costo_jornal").val('');
	}
}

function calculaImporteFrm15(id,hermano,receptor)
{

	selector = "#frm-15  #"+id;
	selector2 = "#frm-15 #"+hermano;
	selector3 = "#frm-15 #"+receptor;

	var valor = parseFloat($(selector).val());
	var hermano = parseFloat($(selector2).val());
	if(valor>0 && hermano>0){
		$(selector3).val(valor*hermano);
	}else{
		$(selector3).val('');
	}
}
