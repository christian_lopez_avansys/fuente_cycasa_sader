var doc = null;
var registrofrm4 = null;
var bandera_bio =0;
var bandera_ins =0;
var bandera_bios =0;
var bandera_insq =0;
var BCargaSiembra=0;


$(document).ready(function(){
	
});

function cargarSiembra()
{
	
	$(".remFrm7", "#frm-7").remove();
	formularios.frm7.cultivos = [];

	if($("#gen-type").val() == 1) $(".inno", "#frm-7").attr("disabled", "disabled");
	else $(".inno", "#frm-7").removeAttr("disabled");
	
			$.ajax({
				url: "/backend/diagnosticos/obtenerdatossiembra/",
				type: "POST",
				data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
				async: false
			}).done(function(respuesta){
				try
				{
					respuesta = jQuery.parseJSON(respuesta);
					var info = { uso: respuesta.html_uso, marca: { semilla: respuesta.html_marca_semilla, bio: respuesta.html_marca_biofertilizante, ins: respuesta.html_marca_insecticida}, unidad_bio: respuesta.unidad_bio, unidad_ins: respuesta.unidad_ins, unidad_bios: respuesta.unidad_bios, unidad_insq: respuesta.unidad_insq, tabla: null };
					//alert(respuesta.unidad_bio.toSource());
					if(respuesta.ok !== undefined)
					{
						for (var i = 0; i < respuesta.cultivos.length; i++)
						{
							info.tabla = respuesta.cultivos[i];
							agregarFilaCultivoFrm7(info);
							
							//cerramos la ventana de carga
							if(i==(respuesta.cultivos.length-1)){
								$("#dialog").dialog('close');
								$("#dialog").dialog('destroy');
								
								//desaparecemos la columna de innovaacion para cuando sea diagnostico
								if($("#gen-type").val()==1){
									if(BCargaSiembra==0){
										BCargaSiembra=1;
										$('.tsemilla tr').each(function(index, element) {
											if(this.id=="fila_generica_0" ){
												$(this).find('th:eq(5)').remove();
											}else if(this.id=="fila_generica_00" ){
												$(this).find('th:eq(6)').remove();
											}else if(this.id.indexOf("gen_despues_")>=0){
												if($(this).find('th').length==7){
													$(this).find('th:eq(5)').remove();
												}else{
													$(this).find('th:eq(6)').remove();
												}
											}else if(this.id.indexOf("bio_despues_")>=0 || this.id.indexOf("ins_despues_")>=0 || this.id.indexOf("bios_despues_")>=0 || this.id.indexOf("insq_despues_")>=0 || this.id=="fila_generica_5" || this.id=="fila_generica_4" || this.id=="fila_generica_3" || this.id=="fila_generica_2"){
												$(this).find('td:eq(5)').remove();
											}else{
												$(this).find('td:eq(4)').remove();
											}
										
											/*if($(this).hasClass("gen")){
												th = $(this).find('th').length-2;
												$(this).find('th:eq('+th+')').remove();
											}else if($(this).hasClass("sem")){
												td = $(this).find('td').length-2
												$(this).find('td:eq('+td+')').remove();
											}*/
										
									});
									}
								}
							}
							bandera_bio =0;
							bandera_ins =0;
							bandera_bios =0;
							bandera_insq =0;
						}
						if(respuesta.congelado == 2){
							 $('#frm-7 *').attr("disabled", "disabled").unbind();
						}
					}
					else if(respuesta.noexiste == undefined){
						$("#dialog").dialog('close');
						$("#dialog").dialog('destroy');
						ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente. ", "Ok", "", false, "");
					}else{
						$("#dialog").dialog('close');
						$("#dialog").dialog('destroy');
					}
						
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente: " + err.message, "Ok", "", false, "");
				}
			});


    $('#btn-save-smr', "#frm-7").once('click', function(){
		if(validaFrm7()){
			var preId = $("#gen-id").val();
			$(".frm7date", "#frm-7").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-7"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarsiembra/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	try
	        	{
		        	$(".frm7date", "#frm-7").attr('disabled', 'disabled');
		        	respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "", false);
		        	}
		        	else 
		        		ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
	        	}
	        	catch(error)
	        	{
	        		ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
	        	}
	        });
	    }
	});

	$("#btn-congelar-smr", "#frm-7").once('click', function(){
		if(validaFrm7())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm7);
	});
	
	$("#btn-clear-smr", "#frm-7").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$('input','#frm-7').val('');
			$('#frm-7 select').each(function() {
				//$(this).css("background-color","#F00");
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
		});
	});
	
	$('#frm-7 .verificarRango').blur(function(){
		var valor = $(this).val();
		var id = $(this).attr("id").valueOf();
		//alert(id);
		if(valor<1001){
			/*
			ShowExecuteBox("Número de semillas por hectárea", "Debe ser mayor a 1,000 el número de semillas por hectárea",function(){
				$('#'+id).focus();
			});
			*/
		}else if(valor>100000){
			ShowExecuteBox("Número de semillas por hectárea", "Debe ser menor a 100,000 el número de semillas por hectárea", function(){
				$('#'+id).focus();
			});
		}
	});
	/*$('#frm-7 .verificarRango2').blur(function(){
		var valor = $(this).val();
		var id = $(this).attr("id").valueOf();
		//alert(id);
		if(valor<0.01){
			ShowExecuteBox("Costo por semilla", "Debe ser mayor a 0.01 el Costo por semilla",function(){
				$('#'+id).focus();
			});
		}else if(valor>10){
			ShowExecuteBox("Costo por semilla", "Debe ser menor a 10 el Costo por semilla", function(){
				$('#'+id).focus();
			});
		}
	});*/
	
	
	
}

function congelarFrm7(){
	$.ajax({
        url: "/backend/diagnosticos/congelarsiembra/",
        type: "POST",
		data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        respuesta = jQuery.parseJSON(respuesta);
	        if(respuesta.ok !== undefined){
	        	ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", false, "");
				$('#frm-7 *').attr("disabled", "disabled").unbind();	
			}
	        else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function validaFrm7()
{
	return true;
	//validamos los select
	var validacion = true;
	//return true;
	var i=1;
	$("#frm-7 .sem").each(function(index, element) {
		//alert("i:"+i +" - "+  $('#cantidad_cultivo_'+i).val());
    	if($('#id_cultivo_'+i).val()=='' || $('#id_cultivo_'+i).val()=='0' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar un cultivo válido para poder guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_cultivo_'+i).focus();
		}else if($('#id_uso_'+i).val()=='0' || $('#id_uso_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar un uso en cultivo antes de guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_uso_'+i).focus();
		}else if($('#id_marca_'+i).val()=='0' || $('#id_uso_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar un proceso en cultivo antes de guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_marca_'+i).focus();
		}else if($('#id_hov_'+i).val()=='0' || $('#id_uso_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "Debe seleccionar un nombre de hibrido o variedad en cultivo antes de guardar", "Ok", "", false, "");
			validacion = false;
			$('#id_hov_'+i).focus();
		}else if($('#cantidad_cultivo_'+i).val()<1001 || $('#cantidad_cultivo_'+i).val()>100000 || $('#cantidad_cultivo_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "El Número de semillas por hectárea debe ser mayor a 1,000 y menor a 100,000", "Ok", "", false, "");
			validacion = false;
			//$('#cantidad_cultivo_'+i).focus();
		}else if($('#precio_cultivo_'+i).val()<0.01 || $('#precio_cultivo_'+i).val()>10 || $('#precio_cultivo_'+i).val()=='' && validacion!=false){
			ShowDialogBox("Validación", "El Costo por semilla debe ser mayor a 0.01 y menor a 10", "Ok", "", false, "");
			validacion = false;
			//$('#precio_cultivo_'+i).focus();
		}/*else if($('#id_unidad_cultivo_'+i).val()=='0' || $('#id_unidad_cultivo_'+i).val()=='' ){
			ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en cultivo", "Ok", "", false, "");
			validacion = false;
			$('#id_unidad_cultivo_'+i).focus();
		}*/
		
			var j=1;
			$(".bio1").each(function(index, element) {
				if($('#ino_con_bio_'+i).val()=='1'){
					if($('#id_empresa_bio_'+i+'_'+j).val()=='0' || $('#id_empresa_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Validación", "Debe seleccionar una empresa en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_empresa_bio_'+i).focus();
					}else if($('#id_marca_bio_'+i+'_'+j).val()=='0' || $('#id_marca_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Validación", "Debe seleccionar una marca en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_marca_bio_'+i).focus();
					}else if($('#id_activo_bio_'+i+'_'+j).val()=='0' || $('#id_activo_bio_'+i+'_'+j).val()=='' && validacion!=false){
						ShowDialogBox("Ha ocurrido un error", "Debe seleccionar un activo en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_activo_bio_'+i).focus();
					}else if($('#id_unidad_bio_'+i+'_'+j).val()=='0' || $('#id_unidad_bio_'+i+'_'+j).val()=='' ){
						ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en Inoculación con biofertilizante", "Ok", "", false, "");
						validacion = false;
						$('#id_unidad_bio_'+i).focus();
					}else if($('#fecha_bio_'+i+'_'+j).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar una fecha para la Inoculación con biofertilizante", "Ok", "", false, "");
							validacion = false;
							$('#fecha_bio_'+i).focus();
					}
				}
				
				j++;
				
			});
			
				var k=1;
				$(".ins1").each(function(index, element) {
					if($('#ino_con_ins_'+i).val()=='1'){
						if($('#id_empresa_ins_'+i+'_'+j).val()=='0' || $('#id_empresa_ins_'+i+'_'+j).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una empresa en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_empresa_bio_'+i).focus();
						}else if($('#id_marca_ins_'+i+'_'+k).val()=='0' || $('#id_marca_ins_'+i+'_'+k).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una marca en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_marca_ins_'+i).focus();
						}else if($('#id_activo_ins_'+i+'_'+k).val()=='0' || $('#id_activo_ins_'+i+'_'+k).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar un activo en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_activo_ins_'+i).focus();
						}else if($('#id_unidad_ins_'+i+'_'+k).val()=='0' || $('#id_unidad_ins_'+i+'_'+k).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#id_unidad_ins_'+i).focus();
						}else if($('#fecha_ins_'+i+'_'+k).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar una fecha para la Inoculación con insecticida", "Ok", "", false, "");
							validacion = false;
							$('#fecha_ins_'+i).focus();
						}
					}
					k++;
				});
				
				var l=1;
				$(".bios1").each(function(index, element) {
					if($('#ino_con_bios_'+i).val()=='1'){
						if($('#id_empresa_bios_'+i+'_'+j).val()=='0' || $('#id_empresa_bios_'+i+'_'+j).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una empresa en Inoculación con bioestimulante", "Ok", "", false, "");
							validacion = false;
							$('#id_empresa_bios_'+i).focus();
						}else if($('#id_marca_bios_'+i+'_'+l).val()=='0' || $('#id_marca_bios_'+i+'_'+l).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una marca en Inoculación con bioestimulante", "Ok", "", false, "");
							validacion = false;
							$('#id_marca_bios_'+i).focus();
						}else if($('#id_activo_bios_'+i+'_'+l).val()=='0' || $('#id_activo_bios_'+i+'_'+l).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar un activo en Inoculación con bioestimulante", "Ok", "", false, "");
							validacion = false;
							$('#id_activo_ins_'+i).focus();
						}else if($('#id_unidad_bios_'+i+'_'+l).val()=='0' || $('#id_unidad_bios_'+i+'_'+l).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en Inoculación con bioestimulante", "Ok", "", false, "");
							validacion = false;
							$('#id_unidad_bios_'+i).focus();
						}else if($('#fecha_bios_'+i+'_'+k).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar una fecha para la Inoculación con bioestimulante", "Ok", "", false, "");
							validacion = false;
							$('#fecha_bios_'+i).focus();
						}
					}
					l++;
				});
				
				var m=1;
				$(".insq1").each(function(index, element) {
					if($('#ino_con_bios_'+i).val()=='1'){
						if($('#id_empresa_insq_'+i+'_'+j).val()=='0' || $('#id_empresa_insq_'+i+'_'+j).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una empresa en Inoculación química", "Ok", "", false, "");
							validacion = false;
							$('#id_empresa_insq_'+i).focus();
						}else if($('#id_marca_insq_'+i+'_'+m).val()=='0' || $('#id_marca_insq_'+i+'_'+m).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar una marca en Inoculación química", "Ok", "", false, "");
							validacion = false;
							$('#id_marca_insq_'+i).focus();
						}else if($('#id_activo_insq_'+i+'_'+m).val()=='0' || $('#id_activo_insq_'+i+'_'+m).val()=='' && validacion!=false){
							ShowDialogBox("Validación", "Debe seleccionar un activo en Inoculación química", "Ok", "", false, "");
							validacion = false;
							$('#id_activo_insq_'+i).focus();
						}else if($('#id_unidad_insq_'+i+'_'+m).val()=='0' || $('#id_unidad_insq_'+i+'_'+m).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar la unidad de medida en Inoculación química", "Ok", "", false, "");
							validacion = false;
							$('#id_unidad_insq_'+i).focus();
						}else if($('#fecha_insq_'+i+'_'+m).val()=='' ){
							ShowDialogBox("Validación", "Debe seleccionar una fecha para la Inoculación química", "Ok", "", false, "");
							validacion = false;
							$('#fecha_insq_'+i).focus();
						}
					}
					m++;
				});
		
		
		
		i++;
	});

	return validacion;
}
/*
function calculaImporteFrm4(concepto)
{
	concepto = concepto.split("_");
	var _concepto = "";
	for (var i = 0; i < concepto.length-1; i++) {
		_concepto += concepto[i] + "_";
	};
	concepto = _concepto.substring(0, _concepto.length-1).trim();
	if(parseFloat($("#" + concepto + "_cantidad", "#frm-4").val()) >= 0 && parseFloat($("#" + concepto + "_precio", "#frm-4").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-4").val(parseFloat($("#" + concepto + "_cantidad", "#frm-4").val()) * parseFloat($("#" + concepto + "_precio", "#frm-4").val()));
	else
		$("#" + concepto + "_importe", "#frm-4").val('');
}*/

function agregarFilaCultivoFrm7(info_tabla)
{
	formularios.frm7.cultivos.push( { bio: 0, insect: 0, bios:0, insq:0 } );
	var act = formularios.frm7.cultivos.length;
	var div0 = "gen_despues_" + act;
	var div1= "sem_despues_" + act;
	var div2 = "bio_despues_" + act + "_1";
	var div3 = "ins_despues_" + act + "_1";
	var div4 = "bios_despues_" + act + "_1";
	var div5 = "insq_despues_" + act + "_1";
	
	if($('#frm-7 .sem').length>0){
		$("#frm-7 #tabla").append("<tr><td colspan='8'></td></tr>");
	}
	$("#frm-7 #tabla").append(
		$("#frm-7 #tabla tbody #fila_generica_0").clone().removeClass("hide").attr("id", div0).addClass("gen remFrm7")
	);

	$("#frm-7 #tabla").append(
		$("#frm-7 #tabla tbody #fila_generica_1").clone().removeClass("hide").attr("id", div1).addClass("sem remFrm7")
	);
	
	//colocamos el registro header para las inoculaciones
	$("#frm-7 #tabla").append(
		$("#frm-7 #tabla tbody #fila_generica_00").clone().removeClass("hide").attr("id", div0+"_"+act).addClass("gen remFrm7")
	);
	

	$("#frm-7 #tabla #" + div1 + " #id_cultivo_").attr("id", "id_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][id_cultivo]");
	$("#frm-7 #tabla #" + div1 + " #id_uso_").attr("id", "id_uso_" + act).attr("name", "tabla[" + (act-1) + "][id_uso]");
	$("#frm-7 #tabla #" + div1 + " #id_marca_").attr("id", "id_marca_" + act).attr("name", "tabla[" + (act-1) + "][id_marca]").attr("onChange", "javascript:loadHOV(" + act + ")");
	$("#frm-7 #tabla #" + div1 + " #thov_").attr("id", "thov_" + act).attr("name", "tabla[" + (act-1) + "][thov]").attr("onChange", "javascript:loadHOV(" + act + ")");
	$("#frm-7 #tabla #" + div1 + " #id_hov_").attr("id", "id_hov_" + act).attr("name", "tabla[" + (act-1) + "][id_hov]");
	$("#frm-7 #tabla #" + div1 + " #hectareas_sembradas_").attr("id", "hectareas_sembradas_" + act).attr("name", "tabla[" + (act-1) + "][hectareas_sembradas]");
	$("#frm-7 #tabla #" + div1 + " #id_unidad_cultivo_").attr("id", "id_unidad_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][id_unidad_cultivo]");
	$("#frm-7 #tabla #" + div1 + " #cantidad_cultivo_").attr("id", "cantidad_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][cantidad_cultivo]");
	$("#frm-7 #tabla #" + div1 + " #precio_cultivo_").attr("id", "precio_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][precio_cultivo]");
	$("#frm-7 #tabla #" + div1 + " #importe_cultivo_").attr("id", "importe_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][importe_cultivo]");
	$("#frm-7 #tabla #" + div1 + " #fecha_cultivo_").attr("id", "fecha_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][fecha_cultivo]").addClass('frm7date');
	$("#frm-7 #tabla #" + div1 + " #innovacion_cultivo_").attr("id", "innovacion_cultivo_" + act).attr("name", "tabla[" + (act-1) + "][innovacion_cultivo]");

	$('.date').datepicker({ format: 'yyyy/mm/dd' });

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////// Llenar los campos de cada tabla ////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
	$("#frm-7 #tabla #" + div1 + " #id_cultivo_" + act).html("<option value='" + info_tabla.tabla.id + "' selected>" + info_tabla.tabla.cultivo + "</option>");
	
	$("#frm-7 #tabla #" + div1 + " #id_uso_" + act).html(info_tabla.uso);
	if ( $("#frm-7 #tabla #" + div1 + " #id_uso_" + act + " option[value='" + info_tabla.tabla.uso + "']").length > 0 )
	{
 		$("#frm-7 #tabla #" + div1 + " #id_uso_" + act).val(info_tabla.tabla.uso);
	}
	else
	{
		obtenerunico("#frm-7 #tabla #" + div1 + " #id_uso_" + act, 'uso', info_tabla.tabla.uso);
	}
	
	//$("#frm-7 #tabla #" + div1 + " #id_marca_" + act).html(info_tabla.marca.semilla);
	//alert(info_tabla.tabla.marca);
	if ( $("#frm-7 #tabla #" + div1 + " #id_marca_" + act + " option[value='" + info_tabla.tabla.marca + "']").length > 0 )
	{
 		$("#frm-7 #tabla #" + div1 + " #id_marca_" + act).val(info_tabla.tabla.marca);
	}
	else
	{
		obtenerunico("#frm-7 #tabla #" + div1 + " #id_marca_" + act, 'marca', info_tabla.tabla.marca);
	}

	$("#frm-7 #tabla #" + div1 + " #id_hov_" + act).html(info_tabla.tabla.html_hov);
	if ( $("#frm-7 #tabla #" + div1 + " #id_hov_" + act + " option[value='" + info_tabla.tabla.hov + "']").length > 0 )
	{
 		$("#frm-7 #tabla #" + div1 + " #id_hov_" + act).val(info_tabla.tabla.hov);
	}
	else
	{
		obtenerunico("#frm-7 #tabla #" + div1 + " #id_hov_" + act, 'hov', info_tabla.tabla.hov);
	}

	$("#frm-7 #tabla #" + div1 + " #id_unidad_cultivo_" + act).html(info_tabla.unidad);
	if ( $("#frm-7 #tabla #" + div1 + " #id_unidad_cultivo_" + act + " option[value='" + info_tabla.tabla.concepto.unidad + "']").length > 0 )
	{
 		$("#frm-7 #tabla #" + div1 + " #id_unidad_cultivo_" + act).val(info_tabla.tabla.concepto.unidad);
	}
	else
	{
		obtenerunico("#frm-7 #tabla #" + div1 + " #id_unidad_cultivo_" + act, 'unidad', info_tabla.tabla.concepto.unidad);
	}

	$("#frm-7 #tabla #" + div1 + " #thov_" + act).val(info_tabla.tabla.thov);
	//$("#frm-7 #tabla #" + div1 + " #hectareas_sembradas_" + act).val(info_tabla.tabla.hectareas);
	//alert(info_tabla.tabla.numero_hectareas);
	$("#frm-7 #tabla #" + div1 + " .semilla").val(info_tabla.tabla.numero_hectareas);
	$("#frm-7 #tabla #" + div1 + " #cantidad_cultivo_" + act).val(info_tabla.tabla.concepto.cantidad);
	$("#frm-7 #tabla #" + div1 + " #precio_cultivo_" + act).val(info_tabla.tabla.concepto.precio);
	$("#frm-7 #tabla #" + div1 + " #importe_cultivo_" + act).val(info_tabla.tabla.concepto.importe);
	$("#frm-7 #tabla #" + div1 + " #fecha_cultivo_" + act).val(info_tabla.tabla.concepto.fecha_realizacion);
	$("#frm-7 #tabla #" + div1 + " #innovacion_cultivo_" + act).val(info_tabla.tabla.concepto.innovacion);

	var info_inoculacion = { uso: info_tabla.uso, marca: null, hibrido_variedad: info_tabla.hibrido_variedad, unidad_bio: info_tabla.unidad_bio,unidad_ins: info_tabla.unidad_ins,unidad_bios: info_tabla.unidad_bios,unidad_insq: info_tabla.unidad_insq, tabla: null };
	//alert("abc"+info_tabla.tabla.cuantos_bio);
	//verificamos si hay inoculaciones sino ponemos uno vacio
	if(info_tabla.tabla.inoculaciones!=null ){
		if( info_tabla.tabla.cuantos_bio<1 ){
			
				if($('.bio'+act).length==0){
					if(bandera_bio==0){
						$("#frm-7 #tabla").append(
							$("#frm-7 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm7")
						);
					}
					agregarFilaBioFrm7(act, null);
				}
		}else{
			for (var i = 0; i < info_tabla.tabla.inoculaciones.length; i++){
				info_inoculacion.tabla = info_tabla.tabla.inoculaciones[i];
				if(info_tabla.tabla.inoculaciones[i].tipo == 1)
				{
					if(bandera_bio==0){
						$("#frm-7 #tabla").append(
							$("#frm-7 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm7")
						);
					}
					info_inoculacion.marca = info_tabla.marca.bio;
					//alert(info_inoculacion.toSource());
					agregarFilaBioFrm7(act, info_inoculacion);
				}
	
			}
		}
			
			if(info_tabla.tabla.cuantos_ins<1){
				if($('.ins'+act).length==0){
					if(bandera_ins==0){
						$("#frm-7 #tabla").append(
							$("#frm-7 #tabla tbody #fila_generica_ins").clone().removeClass("hide").attr("id","realizo_inoculacion_ins_"+act).addClass("ino_ins_"+act +" remFrm7")
						);
					}
					agregarFilaInsFrm7(act, null);
				}	
			}else{
				for (var i = 0; i < info_tabla.tabla.inoculaciones.length; i++){
					info_inoculacion.tabla = info_tabla.tabla.inoculaciones[i];
					if(info_tabla.tabla.inoculaciones[i].tipo == 1)
					{
						if(bandera_bio==0){
							$("#frm-7 #tabla").append(
								$("#frm-7 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm7")
							);
						}
						info_inoculacion.marca = info_tabla.marca.bio;
						//alert(info_inoculacion.toSource());
						agregarFilaBioFrm7(act, info_inoculacion);
						
						
					}
				}
			}
			if(info_tabla.tabla.cuantos_bios<1){
				if($('.bios'+act).length==0){
					if(bandera_bios==0){
						$("#frm-7 #tabla").append(
							$("#frm-7 #tabla tbody #fila_generica_bios").clone().removeClass("hide").attr("id","realizo_inoculacion_bios_"+act).attr("biostimulante","1").addClass("ino_bios_"+act +" remFrm7")
						);
					}
					agregarFilaBiosFrm7(act, null);
				}
			}else{
				for (var i = 0; i < info_tabla.tabla.inoculaciones.length; i++){
					info_inoculacion.tabla = info_tabla.tabla.inoculaciones[i];
					if(info_tabla.tabla.inoculaciones[i].tipo == 3)
					{
						if(bandera_bios==0){
							$("#frm-7 #tabla").append(
								$("#frm-7 #tabla tbody #fila_generica_bios").clone().removeClass("hide").attr("id","realizo_inoculacion_bios_"+act).attr("biostimulante","1").addClass("ino_bios_"+act +" remFrm7")
							);
						}
						info_inoculacion.marca = info_tabla.marca.bios;
						//alert(info_inoculacion.toSource());
						agregarFilaBiosFrm7(act, info_inoculacion);
						
						
					}
				}
			}
			if(info_tabla.tabla.cuantos_insq<1){
				if($('.insq'+act).length==0){
					if(bandera_insq==0){
						$("#frm-7 #tabla").append(
							$("#frm-7 #tabla tbody #fila_generica_insq").clone().removeClass("hide").attr("id","realizo_inoculacion_insq_"+act).attr("quimico","1").addClass("ino_insq_"+act +" remFrm7")
						);
					}
					agregarFilaInsqFrm7(act, null);
				}
			}else{
				for (var i = 0; i < info_tabla.tabla.inoculaciones.length; i++){
					info_inoculacion.tabla = info_tabla.tabla.inoculaciones[i];
					if(info_tabla.tabla.inoculaciones[i].tipo == 4)
					{
						if(bandera_insq==0){
							$("#frm-7 #tabla").append(
								$("#frm-7 #tabla tbody #fila_generica_insq").clone().removeClass("hide").attr("id","realizo_inoculacion_insq_"+act).attr("quimico","1").addClass("ino_insq_"+act +" remFrm7")
							);
						}
						info_inoculacion.marca = info_tabla.marca.insq;
						//alert(info_inoculacion.toSource());
						agregarFilaInsqFrm7(act, info_inoculacion);
						
						
					}
				}
			}
		
	}else{//if is null inoculaciones
	//alert("es null");
		if(bandera_bio==0){
			$("#frm-7 #tabla").append(
				$("#frm-7 #tabla tbody #fila_generica_bio").clone().removeClass("hide").attr("id","realizo_inoculacion_bio_"+act).addClass("ino_bio_"+act+" remFrm7")
			);
		}
		//info_inoculacion.marca = info_tabla.marca.bio;
		agregarFilaBioFrm7(act, null);
		
		if(bandera_ins==0){
			$("#frm-7 #tabla").append(
				$("#frm-7 #tabla tbody #fila_generica_ins").clone().removeClass("hide").attr("id","realizo_inoculacion_ins_"+act).addClass("ino_ins_"+act +" remFrm7")
			);
		}
		//info_inoculacion.marca = info_tabla.marca.ins;
		agregarFilaInsFrm7(act, null);
		
		if(bandera_bios==0){
			$("#frm-7 #tabla").append(
				$("#frm-7 #tabla tbody #fila_generica_bios").clone().removeClass("hide").attr("id","realizo_inoculacion_bios_"+act).attr("biostimulante","1").addClass("ino_bios_"+act +" remFrm7")
			);
		}
		//info_inoculacion.marca = info_tabla.marca.ins;
		agregarFilaBiosFrm7(act, null);
		
		if(bandera_insq==0){
			$("#frm-7 #tabla").append(
				$("#frm-7 #tabla tbody #fila_generica_insq").clone().removeClass("hide").attr("id","realizo_inoculacion_insq_"+act).attr("quimico","1").addClass("ino_insq_"+act +" remFrm7")
			);
		}
		//info_inoculacion.marca = info_tabla.marca.ins;
		agregarFilaInsqFrm7(act, null);
	}
}

function loadMarcas(padre, hijo, tipo, tipo2)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenermarcas/",
        type: "POST",
		data: { tipo: tipo2 },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta.indexOf('value="0"') > -1)
	        	$("#id_marca_" + tipo + "_" + padre + "_" + hijo).html(respuesta);
	        /*else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");*/
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error Marcas", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function loadUnidades(padre, hijo, tipo)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
        type: "POST",
        data:{formulario:"Semilla", concepto:tipo}
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta.indexOf('value="0"') > -1)
	        	$("#id_unidad_" + tipo + "_" + padre + "_" + hijo).html(respuesta);
	        /*else
	        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");*/
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error Unidades", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
}

function loadHOV(padre)
{
	$("#id_hov_" + padre).html('<option value="0">Seleccione un nombre</option>');
	if($("#id_marca_" + padre).val() > 0)
	{
		$.ajax({
	        url: "/backend/diagnosticos/obtenerhibridosovariedades/",
	        type: "POST",
			data: { id_marca: $("#id_marca_" + padre).val(),  tipo: $("#thov_" + padre).val() },
	        async: false
	    }).done(function(respuesta){
	    	try
	    	{
		        if(respuesta.indexOf('value="0"') > -1)
		        	$("#id_hov_" + padre).html(respuesta);
		        /*else
		        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");*/
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error Hibrido o variedad", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
			}
	    });
	}
}

/*function loadActivo(padre, hijo, tipo)
{
	$("#id_activo_" + tipo + "_" + padre + "_" + hijo).html('<option value="0">Seleccione un activo</option>');

	if($("#id_marca_" + tipo + "_" + padre + "_" + hijo).val() > 0)
	{
		$.ajax({
	        url: "/backend/diagnosticos/obteneractivos/",
	        type: "POST",
			data: { id_marca: $("#id_marca_" + tipo + "_" + padre + "_" + hijo).val() },
	        async: false
	    }).done(function(respuesta){
	    	try
	    	{
		        if(respuesta.indexOf('value="0"') > -1)
		        	$("#id_activo_" + tipo + "_" + padre + "_" + hijo).html(respuesta);
		        
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error Activos", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
			}
	    });
	}
}*/

function agregarFilaBioFrm7(padre, info = null)
{
	//alert(info.toSource());
	var ultimo = null
	$('#frm-7 .bio' + padre).each(function(){
		ultimo = this;
	});

	formularios.frm7.cultivos[padre-1].bio = formularios.frm7.cultivos[padre-1].bio + 1;
	var act = formularios.frm7.cultivos[padre-1].bio;
	var div = "bio_despues_" + padre + "_" + act;

	if(ultimo == null)
	{
		$("#frm-7 #tabla").append(
			$("#frm-7 #tabla tbody #fila_generica_2").clone().removeClass("hide").attr("id", div).addClass("bio" + padre + " remFrm7")
		);
		$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "agregarFilaBio_" + padre + "_1").removeClass("btn-danger fa-times-circle").addClass("btn-warning fa-plus-circle").attr("onClick", "javascript:agregarFilaBioFrm7(" + padre + ")");
	}
	else
	{
		$("#frm-7 #" + ultimo.id).after(
			$("#frm-7 #tabla tbody #fila_generica_2").clone().removeClass("hide").attr("id", div).addClass("bio" + padre + " remFrm7")
		);
		if(info == null)
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaBio_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "')");
		else
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaBio_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "', " + info.tabla.id + ")");
	}

	$("#frm-7 #tabla #" + div + " #id_bio_").attr("id", "id_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id]");
	$("#frm-7 #tabla #" + div + " #tipo_bio_").attr("id", "tipo_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][tipo]").attr("value", "1");
	$("#frm-7 #tabla #" + div + " #id_marca_bio_").attr("id", "id_marca_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id_marca]");
	$("#frm-7 #tabla #" + div + " #id_empresa_bio_").attr("id", "id_empresa_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][empresa_id]").attr("onChange", "cargarSubcatalogo(this)");
	//$("#frm-7 #tabla #" + div + " #id_activo_bio_").attr("id", "id_activo_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id_activo]");
	$("#frm-7 #tabla #" + div + " #id_unidad_bio_").attr("id", "id_unidad_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][id_unidad]");
	$("#frm-7 #tabla #" + div + " #cantidad_bio_").attr("id", "cantidad_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][cantidad]");
	$("#frm-7 #tabla #" + div + " #precio_bio_").attr("id", "precio_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][precio]");
	$("#frm-7 #tabla #" + div + " #importe_bio_").attr("id", "importe_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][importe]");
	$("#frm-7 #tabla #" + div + " #fecha_bio_").attr("id", "fecha_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][fecha]").addClass("frm7date");
	$("#frm-7 #tabla #" + div + " #innovacion_bio_").attr("id", "innovacion_bio_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][innovacion]");
	//$("#frm-7 #tabla #" + div + " #ino_con_bio").attr("id", "ino_con_bio").attr("name", "tabla[" + (padre-1) + "][bio][" + (act-1) + "][realizo_ino_bio]").trigger("change");

	if(bandera_bio==0){
		$("#frm-7 #tabla #realizo_inoculacion_bio_"+padre+" #ino_con_bio").attr("id", "ino_con_bio_" + padre).attr("name", "tabla[" + (padre-1) + "][bio]["+(act-1)+"][realizo_ino_bio]").trigger("change");
		bandera_bio=1;	
	}
	$('.date').datepicker({ format: 'yyyy/mm/dd' });
	//alert(info.toSource());
	if(info != null)
	{
		//alert(info.tabla.concepto.toSource());
		$("#frm-7 #tabla #realizo_inoculacion_bio_"+padre+" #ino_con_bio_"+ padre).val(info.realizo).trigger("change");
		$("#frm-7 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act).html(info.tabla.marca);
		$("#frm-7 #tabla #" + div + " #id_empresa_bio_" + padre + "_" + act).html(info.tabla.empresa);
		/*if ( $("#frm-7 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act + " option[value='" + info.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act).val(info.tabla.marca);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_marca_bio_" + padre + "_" + act, 'marca', info.tabla.marca);
		}

		$("#frm-7 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act).html(info.tabla.marcaactivo);
		if ( $("#frm-7 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act + " option[value='" + info.tabla.activo + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act).val(info.tabla.activo);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_activo_bio_" + padre + "_" + act, 'activo', info.tabla.activo);
		}*/

		
		$("#frm-7 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act).html(info.unidad_bio);
		//alert(info.unidad_bio);
		if ( $("#frm-7 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act + " option[value='" + info.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act).val(info.tabla.concepto.unidad);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_unidad_bio_" + padre + "_" + act, 'unidad', info.tabla.concepto.unidad);
		}

		$("#frm-7 #tabla #" + div + " #id_bio_" + padre + "_" + act).val(info.tabla.id);
		$("#frm-7 #tabla #" + div + " #tipo_bio_" + padre + "_" + act).val(info.tabla.tipo);
		$("#frm-7 #tabla #" + div + " #cantidad_bio_" + padre + "_" + act).val(info.tabla.concepto.cantidad);
		$("#frm-7 #tabla #" + div + " #precio_bio_" + padre + "_" + act).val(info.tabla.concepto.precio);
		$("#frm-7 #tabla #" + div + " #importe_bio_" + padre + "_" + act).val(info.tabla.concepto.importe);
		$("#frm-7 #tabla #" + div + " #fecha_bio_" + padre + "_" + act).val(info.tabla.concepto.fecha_realizacion);
		$("#frm-7 #tabla #" + div + " #innovacion_bio_" + padre + "_" + act).val(info.tabla.concepto.innovacion);
	}
	else
	{
		//loadMarcas(padre, act, 'bio', 2);
		loadUnidades(padre, act, 'bio', 2);
	}
}

function agregarFilaInsFrm7(padre, info = null)
{
	var ultimo = null
	$('#frm-7 .ins' + padre).each(function(){
		ultimo = this;
	});

	formularios.frm7.cultivos[padre-1].insect = formularios.frm7.cultivos[padre-1].insect + 1;
	var act = formularios.frm7.cultivos[padre-1].insect;
	var div = "ins_despues_" + padre + "_" + formularios.frm7.cultivos[padre-1].insect;

	if(ultimo == null)
	{
		$("#frm-7 #tabla").append(
			$("#frm-7 #tabla tbody #fila_generica_3").clone().removeClass("hide").attr("id", div).addClass("ins" + padre + " remFrm7")
		);
		$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "agregarFilaIns_" + padre + "_1").removeClass("btn-danger fa-times-circle").addClass("btn-warning fa-plus-circle").attr("onClick", "javascript:agregarFilaInsFrm7(" + padre + ")");
	}
	else
	{
		$("#frm-7 #" + ultimo.id).after(
			$("#frm-7 #tabla tbody #fila_generica_3").clone().removeClass("hide").attr("id", div).addClass("ins" + padre + " remFrm7")
		);
		if(info == null)
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaIns_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "')");
		else
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaIns_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "', " + info.tabla.id + ")");
	}
	
	$("#frm-7 #tabla #" + div + " #id_ins_").attr("id", "id_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id]");
	$("#frm-7 #tabla #" + div + " #tipo_ins_").attr("id", "tipo_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][tipo]").attr("value", "2");
	$("#frm-7 #tabla #" + div + " #id_marca_ins_").attr("id", "id_marca_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id_marca]");
	$("#frm-7 #tabla #" + div + " #id_empresa_ins_").attr("id", "id_empresa_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][empresa_id]").attr("onChange", "cargarSubcatalogo(this)");
	//$("#frm-7 #tabla #" + div + " #id_activo_ins_").attr("id", "id_activo_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id_activo]");
	$("#frm-7 #tabla #" + div + " #id_unidad_ins_").attr("id", "id_unidad_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][id_unidad]");
	$("#frm-7 #tabla #" + div + " #cantidad_ins_").attr("id", "cantidad_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][cantidad]");
	$("#frm-7 #tabla #" + div + " #precio_ins_").attr("id", "precio_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][precio]");
	$("#frm-7 #tabla #" + div + " #importe_ins_").attr("id", "importe_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][importe]");
	$("#frm-7 #tabla #" + div + " #fecha_ins_").attr("id", "fecha_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][fecha]").addClass("frm7date");
	$("#frm-7 #tabla #" + div + " #innovacion_ins_").attr("id", "innovacion_ins_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][innovacion]");
	//$("#frm-7 #tabla #realizo_inoculacion_ins_"+act+" #ino_con_ins").attr("id", "ino_con_ins_"+ act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][realizo_ino_ins]").trigger("change");	

	if(bandera_ins==0){
		$("#frm-7 #tabla #realizo_inoculacion_ins_"+padre+" #ino_con_ins").attr("id", "ino_con_ins_"+ padre).attr("name", "tabla[" + (padre-1) + "][ins]["+(act-1)+"][realizo_ino_ins]").trigger("change");
		bandera_ins=1;	
	}
	
	$('.date').datepicker({ format: 'yyyy/mm/dd' });

	if(info != null)
	{
		$("#frm-7 #tabla #realizo_inoculacion_ins_"+padre+" #ino_con_ins_"+ padre).val(info.realizo).trigger("change");
		$("#frm-7 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act).html(info.tabla.marca);
		$("#frm-7 #tabla #" + div + " #id_empresa_ins_" + padre + "_" + act).html(info.tabla.empresa);
		/*$("#frm-7 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act).html(info.marca);
		if ( $("#frm-7 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act + " option[value='" + info.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act).val(info.tabla.marca);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_marca_ins_" + padre + "_" + act, 'marca', info.tabla.marca);
		}

		$("#frm-7 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act).html(info.tabla.marcaactivo);
		if ( $("#frm-7 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act + " option[value='" + info.tabla.activo + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act).val(info.tabla.activo);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_activo_ins_" + padre + "_" + act, 'activo', info.tabla.activo);
		}*/

		$("#frm-7 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act).html(info.unidad_ins);
		if ( $("#frm-7 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act + " option[value='" + info.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act).val(info.tabla.concepto.unidad);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_unidad_ins_" + padre + "_" + act, 'unidad', info.tabla.concepto.unidad);
		}

		$("#frm-7 #tabla #" + div + " #id_ins_" + padre + "_" + act).val(info.tabla.id);
		$("#frm-7 #tabla #" + div + " #tipo_ins_" + padre + "_" + act).val(info.tabla.tipo);
		$("#frm-7 #tabla #" + div + " #cantidad_ins_" + padre + "_" + act).val(info.tabla.concepto.cantidad);
		$("#frm-7 #tabla #" + div + " #precio_ins_" + padre + "_" + act).val(info.tabla.concepto.precio);
		$("#frm-7 #tabla #" + div + " #importe_ins_" + padre + "_" + act).val(info.tabla.concepto.importe);
		$("#frm-7 #tabla #" + div + " #fecha_ins_" + padre + "_" + act).val(info.tabla.concepto.fecha_realizacion);
		$("#frm-7 #tabla #" + div + " #innovacion_ins_" + padre + "_" + act).val(info.tabla.concepto.innovacion);
	}
	else
	{
		//loadMarcas(padre, act, 'ins', 3);
		loadUnidades(padre, act, 'ins', 3);
	}
}

function agregarFilaBiosFrm7(padre, info = null)
{
	var ultimo = null
	$('#frm-7 .bios' + padre).each(function(){
		ultimo = this;
	});

	formularios.frm7.cultivos[padre-1].bios = formularios.frm7.cultivos[padre-1].bios + 1;
	var act = formularios.frm7.cultivos[padre-1].bios;
	var div = "bios_despues_" + padre + "_" + formularios.frm7.cultivos[padre-1].bios;

	if(ultimo == null)
	{
		$("#frm-7 #tabla").append(
			$("#frm-7 #tabla tbody #fila_generica_4").clone().removeClass("hide").attr("id", div).addClass("bios" + padre + " remFrm7")
		);
		$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "agregarFilaBios_" + padre + "_1").removeClass("btn-danger fa-times-circle").addClass("btn-warning fa-plus-circle").attr("onClick", "javascript:agregarFilaBiosFrm7(" + padre + ")");
	}
	else
	{
		$("#frm-7 #" + ultimo.id).after(
			$("#frm-7 #tabla tbody #fila_generica_4").clone().removeClass("hide").attr("id", div).addClass("bios" + padre + " remFrm7")
		);
		if(info == null)
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaBios_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "')");
		else
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaBios_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "', " + info.tabla.id + ")");
	}
	
	$("#frm-7 #tabla #" + div + " #id_bios_").attr("id", "id_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][id]");
	$("#frm-7 #tabla #" + div + " #tipo_bios_").attr("id", "tipo_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][tipo]").attr("value", "3");
	$("#frm-7 #tabla #" + div + " #id_marca_bios_").attr("id", "id_marca_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][id_marca]");
	$("#frm-7 #tabla #" + div + " #id_empresa_bios_").attr("id", "id_empresa_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][empresa_id]").attr("onChange", "cargarSubcatalogo(this)");
	//$("#frm-7 #tabla #" + div + " #id_activo_bios_").attr("id", "id_activo_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][id_activo]");
	$("#frm-7 #tabla #" + div + " #id_unidad_bios_").attr("id", "id_unidad_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][id_unidad]");
	$("#frm-7 #tabla #" + div + " #cantidad_bios_").attr("id", "cantidad_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][cantidad]");
	$("#frm-7 #tabla #" + div + " #precio_bios_").attr("id", "precio_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][precio]");
	$("#frm-7 #tabla #" + div + " #importe_bios_").attr("id", "importe_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][importe]");
	$("#frm-7 #tabla #" + div + " #fecha_bios_").attr("id", "fecha_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][fecha]").addClass("frm7date");
	$("#frm-7 #tabla #" + div + " #innovacion_bios_").attr("id", "innovacion_bios_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][bios][" + (act-1) + "][innovacion]");
	//$("#frm-7 #tabla #realizo_inoculacion_ins_"+act+" #ino_con_ins").attr("id", "ino_con_ins_"+ act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][realizo_ino_ins]").trigger("change");	

	if(bandera_bios==0){
		$("#frm-7 #tabla #realizo_inoculacion_bios_"+padre+" #ino_con_bios").attr("id", "ino_con_bios_"+ padre).attr("name", "tabla[" + (padre-1) + "][bios]["+(act-1)+"][realizo_ino_bios]").trigger("change");
		bandera_bios=1;	
	}
	
	$('.date').datepicker({ format: 'yyyy/mm/dd' });

	if(info != null)
	{
		$("#frm-7 #tabla #realizo_inoculacion_bios_"+padre+" #ino_con_bios_"+ padre).val(info.realizo).trigger("change");
		$("#frm-7 #tabla #" + div + " #id_marca_bios_" + padre + "_" + act).html(info.tabla.marca);
		$("#frm-7 #tabla #" + div + " #id_empresa_bios_" + padre + "_" + act).html(info.tabla.empresa);
		/*$("#frm-7 #tabla #" + div + " #id_marca_bios_" + padre + "_" + act).html(info.marca);
		if ( $("#frm-7 #tabla #" + div + " #id_marca_bios_" + padre + "_" + act + " option[value='" + info.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_marca_bios_" + padre + "_" + act).val(info.tabla.marca);

		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_marca_bios_" + padre + "_" + act, 'marca', info.tabla.marca);
		}

		$("#frm-7 #tabla #" + div + " #id_activo_bios_" + padre + "_" + act).html(info.tabla.marcaactivo);
		if ( $("#frm-7 #tabla #" + div + " #id_activo_bios_" + padre + "_" + act + " option[value='" + info.tabla.activo + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_activo_bios_" + padre + "_" + act).val(info.tabla.activo);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_activo_bios_" + padre + "_" + act, 'activo', info.tabla.activo);
		}*/

		$("#frm-7 #tabla #" + div + " #id_unidad_bios_" + padre + "_" + act).html(info.unidad_bios);
		if ( $("#frm-7 #tabla #" + div + " #id_unidad_bios_" + padre + "_" + act + " option[value='" + info.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_unidad_bios_" + padre + "_" + act).val(info.tabla.concepto.unidad);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_unidad_bios_" + padre + "_" + act, 'unidad', info.tabla.concepto.unidad);
		}

		$("#frm-7 #tabla #" + div + " #id_bios_" + padre + "_" + act).val(info.tabla.id);
		$("#frm-7 #tabla #" + div + " #tipo_bios_" + padre + "_" + act).val(info.tabla.tipo);
		$("#frm-7 #tabla #" + div + " #cantidad_bios_" + padre + "_" + act).val(info.tabla.concepto.cantidad);
		$("#frm-7 #tabla #" + div + " #precio_bios_" + padre + "_" + act).val(info.tabla.concepto.precio);
		$("#frm-7 #tabla #" + div + " #importe_bios_" + padre + "_" + act).val(info.tabla.concepto.importe);
		$("#frm-7 #tabla #" + div + " #fecha_bios_" + padre + "_" + act).val(info.tabla.concepto.fecha_realizacion);
		$("#frm-7 #tabla #" + div + " #innovacion_bios_" + padre + "_" + act).val(info.tabla.concepto.innovacion);
	}
	else
	{
		//loadMarcas(padre, act, 'bios', 3);
		loadUnidades(padre, act, 'bios', 3);
	}
}

function agregarFilaInsqFrm7(padre, info = null)
{
	var ultimo = null
	$('#frm-7 .insq' + padre).each(function(){
		ultimo = this;
	});

	formularios.frm7.cultivos[padre-1].insq = formularios.frm7.cultivos[padre-1].insq + 1;
	var act = formularios.frm7.cultivos[padre-1].insq;
	var div = "insq_despues_" + padre + "_" + formularios.frm7.cultivos[padre-1].insq;

	if(ultimo == null)
	{
		$("#frm-7 #tabla").append(
			$("#frm-7 #tabla tbody #fila_generica_5").clone().removeClass("hide").attr("id", div).addClass("insq" + padre + " remFrm7")
		);
		$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "agregarFilaInsq_" + padre + "_1").removeClass("btn-danger fa-times-circle").addClass("btn-warning fa-plus-circle").attr("onClick", "javascript:agregarFilaInsqFrm7(" + padre + ")");
	}
	else
	{
		$("#frm-7 #" + ultimo.id).after(
			$("#frm-7 #tabla tbody #fila_generica_5").clone().removeClass("hide").attr("id", div).addClass("insq" + padre + " remFrm7")
		);
		if(info == null)
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaInsq_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "')");
		else
			$("#frm-7 #tabla #" + div + " #eliminarFila").attr("id", "eliminarFilaInsq_" + padre + "_1").attr("onClick", "eliminarItemFrm7('" + div + "', " + info.tabla.id + ")");
	}
	
	$("#frm-7 #tabla #" + div + " #id_insq_").attr("id", "id_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][id]");
	$("#frm-7 #tabla #" + div + " #tipo_insq_").attr("id", "tipo_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][tipo]").attr("value", "4");
	$("#frm-7 #tabla #" + div + " #id_marca_insq_").attr("id", "id_marca_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][id_marca]");
	$("#frm-7 #tabla #" + div + " #id_empresa_insq_").attr("id", "id_empresa_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][empresa_id]").attr("onChange", "cargarSubcatalogo(this)");
	//$("#frm-7 #tabla #" + div + " #id_activo_insq_").attr("id", "id_activo_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][id_activo]");
	$("#frm-7 #tabla #" + div + " #id_unidad_insq_").attr("id", "id_unidad_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][id_unidad]");
	$("#frm-7 #tabla #" + div + " #cantidad_insq_").attr("id", "cantidad_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][cantidad]");
	$("#frm-7 #tabla #" + div + " #precio_insq_").attr("id", "precio_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][precio]");
	$("#frm-7 #tabla #" + div + " #importe_insq_").attr("id", "importe_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][importe]");
	$("#frm-7 #tabla #" + div + " #fecha_insq_").attr("id", "fecha_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][fecha]").addClass("frm7date");
	$("#frm-7 #tabla #" + div + " #innovacion_insq_").attr("id", "innovacion_insq_" + padre + "_" + act).attr("name", "tabla[" + (padre-1) + "][insq][" + (act-1) + "][innovacion]");
	//$("#frm-7 #tabla #realizo_inoculacion_ins_"+act+" #ino_con_ins").attr("id", "ino_con_ins_"+ act).attr("name", "tabla[" + (padre-1) + "][ins][" + (act-1) + "][realizo_ino_ins]").trigger("change");	

	if(bandera_insq==0){
		$("#frm-7 #tabla #realizo_inoculacion_insq_"+padre+" #ino_con_insq").attr("id", "ino_con_insq_"+ padre).attr("name", "tabla[" + (padre-1) + "][insq]["+(act-1)+"][realizo_ino_insq]").trigger("change");
		bandera_insq=1;	
	}
	
	$('.date').datepicker({ format: 'yyyy/mm/dd' });

	if(info != null)
	{
		$("#frm-7 #tabla #realizo_inoculacion_insq_"+padre+" #ino_con_insq_"+ padre).val(info.realizo).trigger("change");
		$("#frm-7 #tabla #" + div + " #id_marca_insq_" + padre + "_" + act).html(info.tabla.marca);
		$("#frm-7 #tabla #" + div + " #id_empresa_insq_" + padre + "_" + act).html(info.tabla.empresa);
		/*$("#frm-7 #tabla #" + div + " #id_marca_insq_" + padre + "_" + act).html(info.marca);
		if ( $("#frm-7 #tabla #" + div + " #id_marca_insq_" + padre + "_" + act + " option[value='" + info.tabla.marca + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_marca_insq_" + padre + "_" + act).val(info.tabla.marca);

		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_marca_insq_" + padre + "_" + act, 'marca', info.tabla.marca);
		}

		$("#frm-7 #tabla #" + div + " #id_activo_insq_" + padre + "_" + act).html(info.tabla.marcaactivo);
		if ( $("#frm-7 #tabla #" + div + " #id_activo_insq_" + padre + "_" + act + " option[value='" + info.tabla.activo + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_activo_insq_" + padre + "_" + act).val(info.tabla.activo);
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_activo_insq_" + padre + "_" + act, 'activo', info.tabla.activo);
		}*/
		//alert(info.unidad.toSource());
		
		//$("#frm-7 #tabla #" + div + " #id_marca_insq_" + padre + "_" + act).html(info.marca_insq);
		$("#frm-7 #tabla #" + div + " #id_unidad_insq_" + padre + "_" + act).html(info.unidad_insq);
		if ( $("#frm-7 #tabla #" + div + " #id_unidad_insq_" + padre + "_" + act + " option[value='" + info.tabla.concepto.unidad + "']").length > 0 )
		{
	 		$("#frm-7 #tabla #" + div + " #id_unidad_insq_" + padre + "_" + act).val(info.tabla.concepto.unidad);
			//alert(info.tabla.concepto.unidad.toSource());
		}
		else
		{
			obtenerunico("#frm-7 #tabla #" + div + " #id_unidad_insq_" + padre + "_" + act, 'unidad', info.tabla.concepto.unidad);
		}

		$("#frm-7 #tabla #" + div + " #id_insq_" + padre + "_" + act).val(info.tabla.id);
		$("#frm-7 #tabla #" + div + " #tipo_insq_" + padre + "_" + act).val(info.tabla.tipo);
		$("#frm-7 #tabla #" + div + " #cantidad_insq_" + padre + "_" + act).val(info.tabla.concepto.cantidad);
		$("#frm-7 #tabla #" + div + " #precio_insq_" + padre + "_" + act).val(info.tabla.concepto.precio);
		$("#frm-7 #tabla #" + div + " #importe_insq_" + padre + "_" + act).val(info.tabla.concepto.importe);
		$("#frm-7 #tabla #" + div + " #fecha_insq_" + padre + "_" + act).val(info.tabla.concepto.fecha_realizacion);
		$("#frm-7 #tabla #" + div + " #innovacion_insq_" + padre + "_" + act).val(info.tabla.concepto.innovacion);
	}
	else
	{
		//loadMarcas(padre, act, 'insq', 3);
		loadUnidades(padre, act, 'insq', 3);
	}
}

function eliminarItemFrm7(div, id = null)
{
	if(id != null){
		ShowConfirmBox("Registro almacenado", "Este registro ya se encuentra almacenado, si lo elimina no podrá revertise, ¿Desea continuar?", eliminarItem_2Frm7, id, div);
	}
	else{
		$("#" + div).remove();
	}
}

function eliminarItem_2Frm7(registro, div){
	var url = "/backend/diagnosticos/eliminarinoculacionfrm7/";
	$.ajax({
        url: url,
        type: "POST",
        data: { id: registro },
        async: false
    }).done(function(respuesta){
        respuesta = jQuery.parseJSON(respuesta);
        if(respuesta.ok === undefined)
        	ShowDialogBox("Ha ocurrido un error", "No se han podido eliminar el registro, inténtelo nuevamente", "Ok", "", false, "");
        else
        {
        	ShowDialogBox("Registro eliminado", "Se eliminó correctamente el registro", "Ok", "", false, "");
        	$("#" + div).remove();
        }
    });
}


function obtenerunico(id_selector, tipo, id_objeto)
{
	$.ajax({
        url: "/backend/diagnosticos/obtenerunico/",
        type: "POST",
		data: { tipo: tipo, id: id_objeto }
		}).done(function(respuesta){
			try
			{
				if(respuesta.indexOf('value=') > -1){
					//alert(respuesta);
					$(id_selector).append(respuesta);
				}
				/*else
					ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");*/
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error Unico", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente: "+err, "Ok", "", false, "");
			}
		});
}

function calculaImporteFrm7(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
	
			var cantidad = $(parent2).find(' td:eq(0) input').attr("value");
			var precio = $(parent2).find(' td:eq(1) input').attr("value");
			var importe = $(parent2).find(' td:eq(2) input');
			//var hectareas = $(parent2).find('th input').attr("value");
//alert(hectareas);
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		//hectareas = parseFloat(hectareas);
		
		if(cantidad>0 && precio>0 ){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}
function calculaImporte2Frm7(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
	//var parent3 = $(obj).parents().get(3);
	
			var cantidad = $(parent2).find(' td:eq(1) input').attr("value");
			var precio = $(parent2).find(' td:eq(2) input').attr("value");
			var importe = $(parent2).find(' td:eq(3) input');
			//var hectareas = $('.semilla').attr("value");
			//alert(hectareas);
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		//hectareas = parseFloat(hectareas);
		
		if(cantidad>0 && precio>0 /*&& hectareas>0*/){
			importe.val(cantidad*precio/**hectareas*/);
		}else{
			importe.val('');
		}
}

function realizo(obj){
	
	var parent = $(obj).parents().get(1);
	identificador = $(parent).attr("id").valueOf();
	if($(parent).attr("biostimulante")!=undefined){
		identificador2 = $(parent).attr("biostimulante").valueOf();
	}else{
		identificador2=0;
	}
	if($(parent).attr("quimico")!=undefined){
		identificador3 = $(parent).attr("quimico").valueOf();
	}else{
		identificador3=0;
	}
	
	
	//obtenemos el identificador del id para saber a cuales elementos hay que deshabilitar o habilitar
	if(identificador.indexOf("realizo_inoculacion_bio")>=0 && identificador2==0){
		id = identificador.replace("realizo_inoculacion_bio_","");
		inoculacion = "bio";
	}else if(identificador.indexOf("realizo_inoculacion_ins_")>=0){
		id = identificador.replace("realizo_inoculacion_ins_","");
		inoculacion = "ins";
	}else if(identificador2!=undefined && identificador2=='1'){
		id = identificador.replace("realizo_inoculacion_bios_","");
		inoculacion = "bios";
	}else if(identificador3!=undefined && identificador3=='1'){
		id = identificador.replace("realizo_inoculacion_insq_","");
		inoculacion = "insq";
	}
	
	
	if($(obj).val()==2){
		//alert(id);
		if(inoculacion=="bio"){
			$('#ino_con_bio_'+id).val('2');
			//bloqueamos los campos para bio
			$(".bio"+id).find('select').val('0');
			$(".bio"+id).find('input').not('input[type="hidden"]').val('');
			
			//alert($("#frm-7 #tabla .bio"+id).length);
			$(".bio"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			
		}else if(inoculacion=="ins"){
			$('#ino_con_ins_'+id).val('2');
			//bloqueamos los campos para bio
			$(".ins"+id).find('select').val('0');
			$(".ins"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".ins"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
		}else if(inoculacion=="bios"){
			$('#ino_con_bios_'+id).val('2');
			//bloqueamos los campos para bio
			$(".bios"+id).find('select').val('0');
			$(".bios"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bios"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
		}else if(inoculacion=="insq"){
			$('#ino_con_insq_'+id).val('2');
			//bloqueamos los campos para bio
			$(".insq"+id).find('select').val('0');
			$(".insq"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".insq"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
		}
		

	}else{
		if(inoculacion=="bio"){
			//habilitamos los campos para bio
			$(".bio"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').addClass("date").datepicker({ format: 'yyyy/mm/dd' });
				 $(this).find('td:eq(4) div .input-group-addon').show();
				 $(this).find('*').not('.inno').each(function(index, element) {
					$(this).removeAttr("disabled");
				});
			});
			
			//deshabilitamos los campos para ins
			$('#ino_con_ins_'+id).val('2');
			$(".ins"+id).find('select').val('0');
			$(".ins"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".ins"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			$('#ino_con_bios_'+id).val('2');
			$(".bios"+id).find('select').val('0');
			$(".bios"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bios"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			//bloqueamos los campos para ins
			$('#ino_con_insq_'+id).val('2');
			$(".insq"+id).find('select').val('0');
			$(".insq"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".insq"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
		}else if(inoculacion=="ins"){
			
			//habilitamos los campos para ins
			$(".ins"+id).each(function(index, element) {
				$(this).find('td:eq(4) div').addClass("date").datepicker({ format: 'yyyy/mm/dd' });
				 $(this).find('td:eq(4) div .input-group-addon').show();
				 $(this).find('*').not('.inno').each(function(index, element) {
					$(this).removeAttr("disabled");
				});
			});
			
			//bloqueamos los campos para bio y bios
			$('#ino_con_bio_'+id).val('2');
			$(".bio"+id).find('select').val('0');
			$(".bio"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bio"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			$('#ino_con_bios_'+id).val('2');
			$(".bios"+id).find('select').val('0');
			$(".bios"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bios"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			//bloqueamos los campos para ins
			$('#ino_con_insq_'+id).val('2');
			$(".insq"+id).find('select').val('0');
			$(".insq"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".insq"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
		}else if(inoculacion=="bios"){
			
			//habilitamos los campos para inse
			$(".bios"+id).each(function(index, element) {
				//alert($(this).html());
				
				$(this).find('td:eq(4) div').addClass("date").datepicker({ format: 'yyyy/mm/dd' });
				 $(this).find('td:eq(4) div .input-group-addon').show();
				 $(this).find('*').not('.inno').each(function(index, element) {
					$(this).removeAttr("disabled");
				});
			});
			
			//bloqueamos los campos para bio  ins e insq
			$('#ino_con_bio_'+id).val('2');
			$(".bio"+id).find('select').val('0');
			$(".bio"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bio"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			
			//bloqueamos los campos para bio
			$('#ino_con_ins_'+id).val('2');
			$(".ins"+id).find('select').val('0');
			$(".ins"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".ins"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			//bloqueamos los campos para ins
			$('#ino_con_insq_'+id).val('2');
			$(".insq"+id).find('select').val('0');
			$(".insq"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".insq"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
		}else if(inoculacion=="insq"){
			
			//habilitamos los campos para insq
			$(".insq"+id).each(function(index, element) {
				//alert($(this).html());
				
				$(this).find('td:eq(4) div').addClass("date").datepicker({ format: 'yyyy/mm/dd' });
				 $(this).find('td:eq(4) div .input-group-addon').show();
				 $(this).find('*').not('.inno').each(function(index, element) {
					$(this).removeAttr("disabled");
				});
			});
			
			//bloqueamos los campos para bio
			$('#ino_con_bio_'+id).val('2');
			$(".bio"+id).find('select').val('0');
			$(".bio"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bio"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			//bloqueamos los campos para ins
			$('#ino_con_ins_'+id).val('2');
			$(".ins"+id).find('select').val('0');
			$(".ins"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".ins"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
			
			//bloqueamos los campos para bios
			$('#ino_con_bios_'+id).val('2');
			$(".bios"+id).find('select').val('0');
			$(".bios"+id).find('input').not('input[type="hidden"]').val('');
			
			$(".bios"+id).each(function(index, element) {
				//alert($(this).html());
				$(this).find('td:eq(4) div').removeClass("date");
				$(this).find('td:eq(4) div .input-group-addon').hide();
				$(this).find('*').not('input[type="hidden"]').each(function(index, element) {
					$(this).not(".realizo").attr("disabled","disabled");
				});
			});
		}

	}
}