 var sup_predio = null;

function cargarDatosGenerales(){ 
	$("#campo_etnia").hide();
    $("#ocultaragro-2").hide();
	$('.dgCultivo','#frm-1').not(":first").remove();
	$("#flexigrid-filtro").flexigrid({
        url: "/backend/productor/griddiagnostico"
        ,dataType: "xml"
        ,colModel: [
            {display: "Seleccionar", name: "seleccionar", width: 75, sortable: false, align: "center"}
            ,{display: "Nombre", name: "nombre", width: 270, sortable: true, align: "center"}
            ,{display: "RFC", name: "rfc", width: 80, sortable: true, align: "center"}
			,{display: "CURP", name: "curp", width: 120, sortable: true, align: "center"}
			,{display: "Domicilio", name: "domicilio", width: 193, sortable: true, align: "center"}
        ]
        ,sortname: "id"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,height: 350
        ,onSuccess: function(){

        }
        ,onError: function(respuesta){
            _mensaje("#_mensaje-1", "Ocurri&oacute; un error al tratar de seleccionar los contactos, int&eacute;ntelo de nuevo");
        }
    });

	function limpiarproductor(){
		$("#nombre_productor", "#frm-1").val('');
		$("#curp_productor", "#frm-1").val('');
		$("#domicilio_productor", "#frm-1").val('');
		$("#id_predio", "#frm-1").html('<option value="0">Seleccione un predio</option>');
		limpiarpredio();
	}

	function limpiarpredio(){
		$("#dependencia", "#frm-1").val('');
		$("#integradora", "#frm-1").val('');
		$("#organizacion", "#frm-1").val('');
		$("#estado", "#frm-1").val('');
		$("#municipio", "#frm-1").val('');
		$("#localidad", "#frm-1").val('');
		$("#potrero", "#frm-1").val('');
		$("#superficie", "#frm-1").val('');
	}

	$("#filtro-avanzado", "#frm-1").once('click', function(){
		$("#frm-1-div").hide("slow", function() {
		    $("#frm-1-filtro").show("slow", function() {
			    
			});
		});
	});

	$("#filtro-regresar", "#frm-1-filtro").once('click', function(){
		$("#frm-1-filtro").hide("slow", function() {
		    $("#frm-1-div").show("slow", function() {
			    
			});
		});
	});
	$("#superficie", "#frm-1").keyup(function(){
		sup_predio = $(this).val();
	});

	$("#search_rfc", "#frm-1").once('click', function(){
		if($("#rfc_productor").val().trim() == '')
			ShowDialogBox("Mensaje", "El campo RFC es requerido para esta busqueda", "Ok", "", false, "");
		else
			$.ajax({
	            type: "POST"
	            ,url: "/backend/diagnosticos/obtenerproductor"
	            ,data: { rfc_productor : $("#rfc_productor").val().trim()  }
	            ,success: function(respuesta){	
	            	respuesta = jQuery.parseJSON(respuesta);		
					if(respuesta.noexiste)
					{
						limpiarproductor();
						ShowConfirmBox("Productor no encontrado", "No se ha encontrado un productor con el RFC solicitado, ¿Desea agregar un nuevo productor?", agregarProductor, $("#rfc_productor").val().trim(), null);
					}
					else
					{
						$("#nombre_productor", "#frm-1").val(respuesta.nombre);
						$("#id_productor", "#frm-1").val(respuesta.id);
						$("#curp_productor", "#frm-1").val(respuesta.curp);
						$("#domicilio_productor", "#frm-1").val(respuesta.domicilio);
						$("#id_predio", "#frm-1").html(respuesta.predios);
						limpiarpredio();
					}
	            }
			});
	});
	$(".SelectEstado", "#frm-1").change('change', function(){
		$.ajax({
	    	url: "/backend/diagnosticos/obtenermunicipiosporestado/",
	      	type: "POST",
	      	data: { id_estado : $(this).val() }
	    }).done(function(respuesta){
	    	$(".SelectMunicipio", "#frm-1").html(respuesta);
	    });
	});
	$(".SelectMunicipio", "#frm-1").change('change', function(){
		$.ajax({
	    	url: "/backend/diagnosticos/obtenerlocalidadespormunicipio/",
	      	type: "POST",
	      	data: { id_municipio : $(this).val() }
	    }).done(function(respuesta){
	    	$(".SelectLocalidad", "#frm-1").html(respuesta);
	    });
	});
	$("#id_predio", "#frm-1").once('change', function(){
		$.ajax({
	    	url: "/backend/predio/obtenerinformacion/",
	      	type: "POST",
	      	data: { id : $("#id_predio", "#frm-1").val() }
	    }).done(function(respuesta){
	    	respuesta = jQuery.parseJSON(respuesta);		
			if(respuesta.noexiste)
			{
				ShowDialogBox("Mensaje", respuesta.noexiste, "Ok", "", false, "");
				limpiarpredio();
			}
			else
			{
				$("#dependencia", "#frm-1").val(respuesta.dependencia);
				$("#integradora", "#frm-1").val(respuesta.integradora);
				$("#organizacion", "#frm-1").val(respuesta.organizacion);
				$("#estado", "#frm-1").val(respuesta.estado);
				$("#municipio", "#frm-1").val(respuesta.municipio);
				$("#localidad", "#frm-1").val(respuesta.localidad);
				$("#potrero", "#frm-1").val(respuesta.potrero);
				$("#superficie", "#frm-1").val(respuesta.superficie);
				sup_predio = respuesta.superficie;
				//alert(sup_predio);
			}
	    });
	});

	$("#btn-congelar-dg", "#frm-1").once('click', function(){
		if(validaFrm1())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podra revertise, ¿Desea continuar?", congelarFrm1);
	});

	$("#frm-1").validate(
	{
	   submitHandler: function(form) 
	   {    
	    //$("#Aplicar,#Aceptar").hide();
	      
	     $(form).ajaxSubmit(
	     {

	        success:    function(response) 
	        { 
	            if(!isNaN(response))
	            {                
	                alert("Guardado correcto");
	                $('#modalProductor').modal('toggle');                
	            }
	            else
	            {         
	                alert("Error al guardar");
	            }
	        }
	     });
	   }
	});
	$("#frm-predio").validate(
	{
	   submitHandler: function(form) 
	   {    
	    //$("#Aplicar,#Aceptar").hide();
	      
	     $(form).ajaxSubmit(
	     {

	        success:    function(response) 
	        { 
	            if(!isNaN(response))
	            {                
	                alert("Guardado correcto");
	                //location.href='/backend/diagnosticos/agregar/tipo/1/id/-/predio/'+response
	                $('#modalProductor').modal('toggle');                
	            }
	            else
	            {         
	                alert("Error al guardar");
	            }
	        }
	     });
	   }
	});
	$('#btn-save-dg').once('click', function(){
		var faltante = false;
		var suma = 0; 
		var sup_predio = $("#superficie").val();
		/*if($('#tabla_cultivo tr').length>=3){
			//validamos que la superficie del predio sea igual que la suma de hectareas de los cultivos			
			for (var i = 1; i <= formularios.frm1.cultivo; i++) {
				if($("#dgCultivo_despues_" + i, "#frm-1").length > 0)
				{
					if($("#select_cultivo_dg_" + i, "#frm-1").val().trim() == '0' || $("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim() == ''|| $("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim() == '0')
					{
						faltante = true;
						break;
					}
					suma += parseFloat($("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim());
				}
			}
			if(faltante){
				ShowDialogBox("Datos incompletos", "Por favor verifique que todos los campos esten llenados correctamente", "Ok", "", false, "");
				return false;
			}else if(suma < parseFloat(sup_predio) || suma > parseFloat(sup_predio)){				
				ShowDialogBox("Superficie incorrecta", " Por favor verifique que la suma total de las hectáreas por cultivo sea igual a la superficie del predio", "Ok", "", false, "");
				return false;
			}
		}else{
			ShowDialogBox("Ha ocurrido un error", "Para continuar es necesario guardar al menos un cultivo", "Ok", "", false, "");
		}*/
		$("#frm-predio").submit();	
	});	
/*
	$('#btn-save-dg', "#frm-1").once('click', function(){
		//alert(sup_predio);
		if($("#gen-type").val()==1){
			if(validaFrm1()){
					var preId = $("#gen-id").val();
					var frm = new FormData(document.getElementById("frm-1"));
					frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
					$.ajax({
						url: "/backend/diagnosticos/guardardatogeneral/",
						type: "POST",
						data: frm,
						processData: false,  
						contentType: false
					}).done(function(respuesta){
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
							if(preId == 0)
							{
								$("#gen-id").val(respuesta.ok); 
								ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
								congelarFrm1();
								//window.location = ";
							}else
							{
								congelarFrm1();
								ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
							}
						}
						else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
					});
			}
		}else{
			var faltante = false;
			var suma = 0; 
			//comprobamos que al menos haya un registro de cultivos
			if($('#tabla_cultivo tr').length>=3){
				//validamos que la superficie del predio sea igual que la suma de hectareas de los cultivos
				for (var i = 2; i <= formularios.frm1.cultivo; i++) {
					if($("#dgCultivo_despues_" + i, "#frm-1").length > 0)
					{
						if($("#select_cultivo_dg_" + i, "#frm-1").val().trim() == '0' || $("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim() == ''|| $("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim() == 0)
						{
							faltante = true;
							break;
						}
						suma += parseFloat($("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim());
					}
				}
				if(faltante){
					ShowDialogBox("Datos incompletos", "Por favor verifique que todos los campos esten llenados correctamente", "Ok", "", false, "");
				}else if(suma < parseFloat(sup_predio) || suma > parseFloat(sup_predio)){
					ShowDialogBox("Superficie incorrecta", " Por favor verifique que la suma total de las hectáreas por cultivo sea igual a la superficie del predio", "Ok", "", false, "");
				}else{
					var preId = $("#gen-id").val();
					var frm = new FormData(document.getElementById("frm-1"));
					frm.append("id", $("#gen-id").val()); 
					frm.append("tipo", $("#gen-type").val());
					$.ajax({
						url: "/backend/diagnosticos/guardarcultivodatogeneral/",
						type: "POST",
						data: frm,
						processData: false,  
						contentType: false
					}).done(function(respuesta){
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
							congelarFrm1();
							ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
						}else 
							ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
					});
				}
			}else{
				ShowDialogBox("Ha ocurrido un error", "Para continuar es necesario guardar al menos un cultivo", "Ok", "", false, "");
			}
			
		}
	    
	});
*/
	$('#btn-clean-dg', "#frm-1").once('click', function(){
		limpiarproductor();
		$("#rfc_productor", "#frm-1").val('');
		$("#anio_apoyo").val('');
		$("#id_agroconsultor").val('0');
	});

	$.ajax({
        url: "/backend/agroconsultor/obteneragroconsultores/",
        type: "POST",
        async: false,
        processData: false,  
        contentType: false
    }).done(function(data){
        $("#id_agroconsultor",  "#frm-1").html(data);
		if($('#agroconsultor').val()==$("#id_agroconsultor").val()){
			$('#OcultarAgro').hide();
		}else{
			$('#OcultarAgro').show();
		}
    });

    $.ajax({
        url: "/backend/cultivo/obtenercultivos/",
        type: "POST",
		data:{id:$("#gen-id").val(),tipo:$("#gen-type").val()},
        /*async: false,
        processData: false,  
        contentType: false*/
    }).done(function(data){
        $("#select_cultivo_dg_1",  "#frm-1").html(data);
    });

	$.ajax({
        url: "/backend/diagnosticos/obtenerdatosdatogeneral/",
        type: "POST",
        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val(), predio_id:$("#predio_id").val()  }
    }).done(function(respuesta){
    	respuesta = jQuery.parseJSON(respuesta);
    	if(respuesta.error !== undefined) 
    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, intentelo nuevamente", "Ok", "", false, "");
    	else if(respuesta.nodata === undefined)
    	{
    		var res = respuesta;
    		$("#id_productor").val(res.id);
    		$("#curp_productor").val(res.curp).attr("disabled",false);
    		$("#apellido_paterno").val(res.apellido_paterno).attr("disabled",false);
            $("#apellido_materno").val(res.apellido_materno).attr("disabled",false);
            $("#nombre").val(res.nombre).attr("disabled",false);
            $("#genero").val(res.genero).attr("disabled",false);
            $("#estado_id").val(res.estado_id).attr("disabled",false);
            $.ajax({
                type: 'POST',
                url:"/backend/index/municipios/id/"+$('#estado_id').val(),
                success:
                function respuesta(resM)
                {
                   $('#municipio_id').html(resM);
                   console.log(res.estado_id+"|"+res.municipio_id);
                   $("#municipio_id").val(res.estado_id+"|"+res.municipio_id);
                    $.ajax({
                        type: 'POST',
                        url:"/backend/index/localidades/id/"+$('#municipio_id').val(),
                        success:
                        function respuesta(resL)
                        {
                           $("#municipio_id").attr("disabled",false);
                           $('#localidad_id').html(resL);
                           $("#localidad_id").val(res.localidad_id).attr("disabled",false);
                        }
                    });
                }
            });
            $("#pertenece_etnia").val(res.pertenece_etnia).attr("disabled",false).change();
            
            if(res.etnia_id == 1){
            	$("#campo_etnia").show();
            	$("#etnia_id").attr("required","required");
            }

            $("#etnia_id").val(res.etnia_id).attr("disabled",false);
            $("#etnia_id").change();
            
            $("#innovador").val(res.innovador).attr("disabled",false);
            $("#innovador").change();
            
            if($("#predio_id").val() && res.predio){

            	$("#predio_estado_id").val(res.predio.estado);

            	$.ajax({
	                type: 'POST',
	                url:"/backend/index/municipios/id/"+$("#predio_estado_id").val(),
	                success:
	                function respuesta(resM)
	                {
	                   $('#municipio').html(resM);                   
	                   console.log($("#predio_estado_id").val()+"|"+res.predio.municipio);
	                   $("#municipio").val($("#predio_estado_id").val()+"|"+res.predio.municipio);
	                    $.ajax({
	                        type: 'POST',
	                        url:"/backend/index/localidades/id/"+$('#municipio').val(),
	                        success:
	                        function respuesta(resL)
	                        {
	                           console.log(res.predio.localidad);		
	                           $('#localidad').html(resL);
	                           $("#localidad").val(res.predio.localidad);
	                        }
	                    });
	                }
	            });

            	$("#region").val(respuesta.predio.region);
            	$("#tipo_predio").val(respuesta.predio.tipo_predio);
            	listaTipoPredio($("#tipo_predio").val());
            	$("#clave").val(respuesta.predio.clave);
            	$("#folio_predio").val(respuesta.predio.folio_predio);
            	$("#nombre_predio").val(respuesta.predio.nombre_predio);
            	$("#numero_ciclos").val(respuesta.predio.numero_ciclos);
            	$("#manejo_actual").val(respuesta.predio.manejo_actual);
            	$("#superficie").val(respuesta.predio.superficie);
            	$("#altitud").val(respuesta.predio.altitud);
            	$("#regimen_propiedad").val(respuesta.predio.regimen_propiedad);
            	$("#regimen_agrario").val(respuesta.predio.regimen_agrario);
            	$("#regimen_hidrico").val(respuesta.predio.regimen_hidrico);
            	$("#nombre_tecnico").val(respuesta.predio.nombre_tecnico);
            	$("#numero_modulo").val(respuesta.predio.numero_modulo);  
            	if(respuesta.predio.kml!='')
            	{
            		$("#archivo_kml_descargar").attr('href',respuesta.predio.kml).show();
            	}          	

            }
            
            if(parseInt(res.sociedad_agricola) == 1){
                //$("#sociedad_agricola").prop("checked", "checked");  
                //$("#sociedad_agricola").click();
    			$("#ocultaragro-2").show();
            }
            if(res.sociedad_social  == 1)
                $("#sociedad_social").prop("checked", true);
            if(res.sociedad_religiosa  == 1)
                $("#sociedad_religiosa").prop("checked", true);

            $("#sociedad_agricola").attr("disabled",false);
            $("#sociedad_social").attr("disabled",false);
            $("#sociedad_religiosa").attr("disabled",false);
                                
            $("#persona_juridica_id").val(res.persona_juridica_id).attr("disabled",false);   

    		sup_predio = respuesta.superficie;			
    		

    		// Cultivos
			//alert(respuesta.cultivos.length);
    		/*if(respuesta.cultivos.length >= 1)
    		{
				if($("#gen-type").val()==1){
					//alert(respuesta.cultivos[0].cultivos.toSource());
					$("#select_cultivo_dg_1", "#frm-1").html(respuesta.cultivos[0].cultivos);
					$("#hectareas_cultivo_dg_1", "#frm-1").val(respuesta.cultivos[0].hectareas);
					$("#cultivos_id_1", "#frm-1").val(respuesta.cultivos[0].id);
					//alert(respuesta.cultivos[0].id+"<->"+respuesta.cultivos[1].id);
					for (var i = 1; i < respuesta.cultivos.length; i++)
					{
						//alert(respuesta.cultivos[i].id);
						agregarFilaFrm1Cultivo(respuesta.cultivos[i].id, respuesta.cultivos[i].hectareas, respuesta.cultivos[i].cultivos);
					};
				}else{
					//ocultamos la fila generica
					$("#dgCultivo_despues_1", "#frm-1").remove();//.addClass("hide");
					//si estamos en programado o real se pueden eliminar los cultivos
					for (var i = 0; i < respuesta.cultivos.length; i++)
					{
						//alert(respuesta.cultivos[i].id);
						agregarFilaFrm1Cultivo(respuesta.cultivos[i].id, respuesta.cultivos[i].hectareas, respuesta.cultivos[i].cultivos);
					};
				}
				
    		}*/

    		// Vertices
    		/*if(respuesta.vertices.length >= 1)
    		{
    			$("#lat_dgVertice_1", "#frm-1").val(respuesta.vertices[0].latitud);
				$("#long_dgVertice_1", "#frm-1").val(respuesta.vertices[0].longitud);
				$("#vertices_id_1", "#frm-1").val(respuesta.vertices[0].id);

				for (var i = 1; i < respuesta.vertices.length; i++)
	    		{
	    			agregarFilaFrm1Vertice(respuesta.vertices[i].id, respuesta.vertices[i].latitud, respuesta.vertices[i].longitud);
	    		};
    		}*/
    		return false; ////se cambia por productores

    		if(respuesta.congelado == 2)
			{
				if($('#gen-type').val()==1){
					$('#frm-1 *').attr("disabled", "disabled").unbind();
					$('#tablaAgregarCultivo').attr("onclick", "");
					
					dg_congelado = true;
				}else{
					//alert(respuesta.cultivos[0].congelado);
					if(respuesta.cultivos[0].congelado==2){
						$('#frm-1 *').attr("disabled", "disabled").unbind();
						dg_congelado = true;
					}else{
						dg_congelado = false;
						$('#frm-1 *').not('#frm-1 #tabla_cultivo *').not('#frm-1 #contenedor_terminar_proceso *').attr("disabled", "disabled").unbind();
					}
					//$('#btn-save-dg').removeAttr("disabled");
				}
				
			}
			//verificamos si en la url viene alguna variable para activar formulario
			if(!redireccionPagina){
				activarFormulario();
			}
    	}
    });

	/*if($("#verticesdemapa").val().trim() != '')
	{
		var num_vertices = $("#verticesdemapa").val().trim().split('|');
		for (var i = 0; i < num_vertices.length; i++) 
		{
			var coordenada = num_vertices[i].split("::");
			if(i == 0)
			{
				$("#lat_dgVertice_1").val(coordenada[0]);
				$("#long_dgVertice_1").val(coordenada[1]);
			}
			else
			{
				agregarFilaFrm1Vertice(null, coordenada[0], coordenada[1]);
			}
		};
	}*/
	//validamos si estamos en programado o real y la tabla de cultivos la deshabilitamos para que los pueda modificar
	/*if($('#gen-type').val()!=0){
		$('#tabla_cultivo *').each(function() {
            $(this).attr("disabled", "").unbind();//.removeAttr("disabled");
			$(this).removeAttr("readonly");

        });
		//le quitamos el atributo para que el usuario pueda guardar
		$('#btn-save-dg').removeAttr("disabled");
	}*/
	
	$('#estado_id').change(function()
    {
        $("#search_select_localidad").html("<option value='0'>Seleccione una localidad...</option>");
        $.ajax({
            type: 'POST',
            url:"/backend/index/municipios/id/"+$('#estado_id').val(),
            success:
            function respuesta(res)
            {
               $('#municipio_id').html(res);
            }
        });
    });

	$('#municipio_id').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/backend/index/localidades/id/"+$('#municipio_id').val(),
            success:
            function respuesta(res)
            {
               $('#localidad_id').html(res);
            }
        });
    });

    $('#pertenece_etnia').change(function()
    {
        if($('#pertenece_etnia').val()==1)
        {
            $("#campo_etnia").show();
            $("#etnia_id").attr("required","required");            
        }
        else
        {
            $("#campo_etnia").hide();
            $("#etnia_id").attr("required","required");
        }
    });

}

function activaSociedad(checkbox)
{
    if($(checkbox).is(":checked"))
    {
        $("#ocultaragro-2").show();
        $("#persona_juridica_id").attr("required","required");
    }
    else
    {
        $("#ocultaragro-2").hide();
        $("#persona_juridica_id").removeAttr("required");
    }
}

function agregarFilaFrm1Vertice(id = null, latitud = null, longitud = null){
	var pre_count_vertice = formularios.frm1.vertice;
	formularios.frm1.vertice = formularios.frm1.vertice + 1;
	var div = "dgVertice_despues_" + formularios.frm1.vertice;

	$("#frm-1 #tabla_vertice").append(
		$("#frm-1 #tabla_vertice tbody #fila_generica_dgVertice").clone().removeClass("hide").attr("id", div).addClass("dgVertice")
	);

	if(id == null && (latitud == null || longitud == null))
	{
	    $("#frm-1 #" + div + " #lat_gen").attr("id", "lat_dgVertice_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][latitud]").attr("onkeypress", "return numeros(event)").attr("readonly","readonly");
		$("#frm-1 #" + div + " #long_gen").attr("id", "long_dgVertice_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][longitud]").attr("onkeypress", "return numeros(event)").attr("readonly","readonly");
		$("#frm-1 #" + div + " #eliminarFilaVertice_dg_").attr("id", "eliminarFilaVertice_dg_" + formularios.frm1.vertice).attr("onClick", "eliminarFilaFrm1('" + div + "', false)");
		$("#frm-1 #" + div + " #vertices_id_").attr("id", "vertices_id_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][id]");
	}
	else if(id == null && latitud != null && longitud != null)
	{
	    $("#frm-1 #" + div + " #lat_gen").attr("id", "lat_dgVertice_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][latitud]").attr("onkeypress", "return numeros(event)").val(latitud).attr("readonly","readonly");
		$("#frm-1 #" + div + " #long_gen").attr("id", "long_dgVertice_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][longitud]").attr("onkeypress", "return numeros(event)").val(longitud).attr("readonly","readonly");
		$("#frm-1 #" + div + " #eliminarFilaVertice_dg_").attr("id", "eliminarFilaVertice_dg_" + formularios.frm1.vertice).attr("onClick", "eliminarFilaFrm1('" + div + "', false)");
		$("#frm-1 #" + div + " #vertices_id_").attr("id", "vertices_id_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][id]");
	}
	else
	{
		$("#frm-1 #" + div + " #lat_gen").attr("id", "lat_dgVertice_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][latitud]").val(latitud).attr("readonly","readonly");
		$("#frm-1 #" + div + " #long_gen").attr("id", "long_dgVertice_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][longitud]").val(longitud).attr("readonly","readonly");
		$("#frm-1 #" + div + " #eliminarFilaVertice_dg_").attr("id", "eliminarFilaVertice_dg_" + formularios.frm1.vertice).attr("onClick", "eliminarFilaFrm1('" + div + "', true, " + id + ")");
		$("#frm-1 #" + div + " #vertices_id_").attr("id", "vertices_id_" + formularios.frm1.vertice).attr("name", "vertices[" + (formularios.frm1.vertice-1) + "][id]").val(id);
	}
}

function agregarFilaFrm1Cultivo(id = null, hectareas = null, cultivos = null){
	// FORM 1
	var pre_count_cultivo = formularios.frm1.cultivo;
	formularios.frm1.cultivo = formularios.frm1.cultivo + 1;
	var div = "dgCultivo_despues_" + formularios.frm1.cultivo;
	
	$("#frm-1 #tabla_cultivo").append(
		$("#frm-1 #tabla_cultivo tbody #fila_generica_dgCultivo").clone().removeClass("hide").attr("id", div).addClass("dgCultivo")
	);

	if(id == null)
	{
		$.ajax({
	        url: "/backend/cultivo/obtenercultivos/",
	        type: "POST",
	        async: false,
	        processData: false,  
	        contentType: false
	    }).done(function(data){
	        cultivos = data;
	    });
	    $("#frm-1 #" + div + " #select_cultivo_dg_").attr("id", "select_cultivo_dg_" + formularios.frm1.cultivo).attr("name", "cultivos[" + (formularios.frm1.cultivo-1) + "][id_cultivo]").html(cultivos);
		$("#frm-1 #" + div + " #hect_cultivo_dg_").attr("id", "hectareas_cultivo_dg_" + formularios.frm1.cultivo).attr("name", "cultivos[" + (formularios.frm1.cultivo-1) + "][hectareas]").attr("onkeypress", "return numeros(event)");
		$("#frm-1 #" + div + " #eliminarFila_dg_").attr("id", "eliminarFila_dg_" + formularios.frm1.cultivo).attr("onClick", "eliminarFilaFrm1('" + div + "', false)");
		$("#frm-1 #" + div + " #cultivos_id_").attr("id", "cultivos_id_" + formularios.frm1.cultivo).attr("name", "cultivos[" + (formularios.frm1.cultivo-1) + "][id]");
	}
	else
	{
		$("#frm-1 #" + div + " #select_cultivo_dg_").attr("id", "select_cultivo_dg_" + formularios.frm1.cultivo).attr("name", "cultivos[" + (formularios.frm1.cultivo-1) + "][id_cultivo]").html(cultivos);
		$("#frm-1 #" + div + " #hect_cultivo_dg_").attr("id", "hectareas_cultivo_dg_" + formularios.frm1.cultivo).attr("name", "cultivos[" + (formularios.frm1.cultivo-1) + "][hectareas]").val(hectareas);
		$("#frm-1 #" + div + " #eliminarFila_dg_").attr("id", "eliminarFila_dg_" + formularios.frm1.cultivo).attr("onClick", "eliminarFilaFrm1('" + div + "', true, " + id + ")");
		$("#frm-1 #" + div + " #cultivos_id_").attr("id", "cultivos_id_" + formularios.frm1.cultivo).attr("name", "cultivos[" + (formularios.frm1.cultivo-1) + "][id]").val(id);
	}
	
}

function eliminarFilaFrm1(div, guardado, id = null){
	if(guardado){
		ShowConfirmBox("Registro almacenado", "Este registro ya se encuentra almacenado, si lo elimina no podrá revertise, ¿Desea continuar?", eliminarFilaFrm1_2, id, div);
		//$("#" + div).remove();
	}
	else{
		$("#" + div).remove();
	}
	
}

function eliminarFilaFrm1_2(registro, div){
	var url;
	if(div.indexOf('Cultivo') > 0) url = "/backend/diagnosticos/eliminarcultivogeneral/";
	else url = "/backend/diagnosticos/eliminarverticegeneral/";
	$.ajax({
        url: url,
        type: "POST",
        data: { id: registro },
        async: false
    }).done(function(respuesta){
        respuesta = jQuery.parseJSON(respuesta);
        if(respuesta.ok === undefined)
        	ShowDialogBox("Ha ocurrido un error", "No se han podido eliminar el registro, intentelo nuevamente", "Ok", "", false, "");
        else
        {
        	ShowDialogBox("Registro eliminado", "Se elimino correctamente el registro", "Ok", "", false, "");
        	$("#" + div).remove();
        }
    });
}

function validaFrm1(){
	return true;
	var faltante = false;
	var duplicados = false;
	var suma = 0;
	var misCultivos = [];
	var lista = [];
	misCultivos.length = 0;
	var apariciones=0;
	
	if($("#gen-type").val()=='1'){//si es de tipo : diagnostico necesita crear un nuevo predio
		if($("#frm-1 #dependencia").val()==''){
			faltante = true;
		}
		if($("#frm-1 #integradora").val()==''){
			faltante = true;
		}
		if($("#frm-1 #organizacion").val()==''){
			faltante = true;
		}
		if($("#frm-1 #estado").val()=='0'){
			faltante = true;
		}
		if($("#frm-1 #municipio").val()=='0'){
			faltante = true;
		}
		if($("#frm-1 #localidad").val()=='0'){
			faltante = true;
		}
		if($("#frm-1 #potrero").val()==''){
			faltante = true;
		}
		if($("#frm-1 #superficie").val()==''){
			faltante = true;
		}

	}
	
	////Datos de productor
	/*
	//alert($("#anio_apoyo", "#frm-1").val().trim());
	if( $("#gen-type").val()==1){
			if($("#anio_apoyo", "#frm-1").val().trim() == '' || $("#id_agroconsultor", "#frm-1").val().trim() == 0) 
				faltante = true;
	}else{
			if($("#id_predio", "#frm-1").val().trim() == 0  || $("#anio_apoyo", "#frm-1").val().trim() == '' || $("#id_agroconsultor", "#frm-1").val().trim() == 0) 
				faltante = true;
	}
	//obtener todos los values del select de cultivo
	$('#dgCultivo_despues_1 option').not(' option:first').each(function() {
        lista.push($(this).val());
    });

	for (var i = 1; i <= formularios.frm1.cultivo; i++) {
		if($("#dgCultivo_despues_" + i, "#frm-1").length > 0)
		{
			
			var cultivoActual = $("#select_cultivo_dg_" + i, "#frm-1").val();
			
			
			if($("#select_cultivo_dg_" + i, "#frm-1").val().trim() == '0' || $("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim() == '')
			{
				faltante = true;
				break;
			}
			misCultivos[i-1]=cultivoActual;
			suma += parseFloat($("#hectareas_cultivo_dg_" + i, "#frm-1").val().trim());
		}
	}
	
	for(var x=0; x<lista.length; x++){
		for(var y=0; y<misCultivos.length; y++){
			if(misCultivos[y]==lista[x]){
				apariciones++;
				if(apariciones>1){
					duplicados = true;
					break;
				}
			}
		}
		apariciones=0;
		
	}

	for (var i = 1; i <= formularios.frm1.vertice; i++) {
		if($("#dgVertice_despues_" + i, "#frm-1").length > 0)
		{
			if($("#lat_dgVertice_" + i, "#frm-1").val().trim() == '' || $("#long_dgVertice_" + i, "#frm-1").val().trim() == '')
			{
				faltante = true;
				break;
			}
		}
	}
	
	if(faltante)
	{
		ShowDialogBox("Datos incompletos", "Por favor verifique que todos los campos esten llenados correctamente", "Ok", "", false, "");
		return false;
	}
	if(duplicados)
	{
		ShowDialogBox("Cultivos duplicados", "No es posible agregar cultivos duplicados.", "Ok", "", false, "");
		return false;
	}
	if(suma < parseFloat(sup_predio) || suma > parseFloat(sup_predio))
	{
		ShowDialogBox("Superficie incorrecta", "Por favor verifique que la suma total de las hectareas por cultivo sea igual a la superficie del predio", "Ok", "", false, "");
		return false;
	}
	if(formularios.frm1.vertice<3){
		ShowDialogBox("Datos incompletos", "Por favor verifique que por lo menos haya agregado 3 vertices del poligono", "Ok", "", false, "");
		return false;
	}*/
	return true;
}

function congelarFrm1(){
	$.ajax({
        url: "/backend/diagnosticos/congelardatosgenerales/",
        type: "POST",
		data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
        respuesta = jQuery.parseJSON(respuesta);
        if(respuesta.ok !== undefined)
        {
        	//ShowDialogBox("Congelacion exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
        	dg_congelado = true;
        	$('#frm-1 *').attr("disabled", "disabled").unbind();
        }
        else
        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor intentelo nuevamente", "Ok", "", false, "");
    });
}

function agregarProductor(productor_rfc, param2 = null){
	$.ajax({
        url: "/backend/diagnosticos/productorform/",
        type: "POST",
        data: { rfc: productor_rfc },
        success:function result(data){
            $(".dialog-form").html(data);
            $("#accordion").accordion();

            $("#btnSiguiente", "#productor_div").click(function(e){
		        if($("#nombre").val().trim() == '' || $("#rfc").val().trim() == '' || $("#curp").val().trim() == '' || $("#domicilio").val().trim() == '')
		            ShowDialogBox("Datos incompletos", "Por favor verifique que todos los campos esten llenados correctamente", "Ok", "", false, "");
		        else
		        {
		        	var frm = new FormData(document.getElementById("frm-productor"));
		    	    $.ajax({
		    	    	url: "/backend/productor/guardar/",
		    	      	type: "POST",
		    	      	data: frm,
		    	      	processData: false,  
		    	      	contentType: false
		    	    }).done(function(data){
		    	    	if(data > 0){
							ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
			                $("#rfc_productor", "#frm-1").val($("#rfc", "#frm-productor").val());
			                $(".dialog-form").dialog("close");
							$(".dialog-form").dialog("destroy");
							$("#search_rfc", "#frm-1").trigger("click");
						}else if(data == -1){
		    	    		ShowDialogBox("No se ha podido guardar", "El productor ya existe, compruebe que algunos de los campos capturados no existan en otro registro", "Ok", "", false, "");
		    	    		$("#btnAnterior", "#predio_div").trigger("click");
		    	    	}else{
		    	    		ShowDialogBox("Ha ocurrido un error al agregar el productor", data, "Ok", "", false, "");
		    	    		$("#btnAnterior", "#predio_div").trigger("click");
		    	    	}
					});
				}
					/*$("#id_productor_view", "#frm-predio").html('<option value="0" selected>' + $("#nombre", "#frm-productor").val().trim() + '</option>');
		        	$("#productor_div").hide("slow", function() {
					    $("#predio_div").show("slow", function() {
						    // Animation complete.
						});
					});*/
					
		 	});

		 	/*$("#btnAnterior", "#predio_div").click(function(e){
	        	$("#predio_div").hide("slow", function() {
				    $("#productor_div").show("slow", function() {
					    // Animation complete.
					});
				});
		 	});*/

		 	$("#btnLimpiar", "#productor_div").click(function(e){
				$("#nombre, #curp, #domicilio", "#frm-productor").val('');
			});

			/*$("#btnLimpiar", "#predio_div").click(function(e){
				$("#nombre", "#frm-predio").val('');
			    $("#id_dependencia", "#frm-predio").val('0');
			    $("#id_integradora", "#frm-predio").val('0');
			    $("#id_organizacion", "#frm-predio").val('0');
			    $("#id_estado", "#frm-predio").val('0');
			    $("#potrero", "#frm-predio").val('');
			    $("#superficie", "#frm-predio").val('');
			    $("#id_productor", "#frm-predio").val('0');
			    $("#id_municipio", "#frm-predio").html('<option value="0">Seleccione un municipio</option>');
			    $("#id_localidad", "#frm-predio").html('<option value="0">Seleccione una localidad</option>');
			});*/

		 	/*$("#btnGuardar", "#predio_div").click(function(e){
		        if($("#nombre", "#frm-predio").val().trim() == '' || $("#id_dependencia", "#frm-predio").val() == 0 || $("#id_integradora", "#frm-predio").val() == 0 || $("#id_organizacion", "#frm-predio").val() == 0 || $("#id_estado", "#frm-predio").val() == 0 || $("#id_municipio", "#frm-predio").val() == 0 || $("#id_localidad", "#frm-predio").val() == 0 || $("#id_productor", "#frm-predio").val() == 0 || $("#potrero", "#frm-predio").val().trim() == '' || $("#superficie", "#frm-predio").val().trim() == '')
		            ShowDialogBox("Datos incompletos", "Por favor verifique que todos los campos esten llenados correctamente", "Ok", "", false, "");
		        else
		        {
		        	var frm = new FormData(document.getElementById("frm-productor"));
		    	    $.ajax({
		    	    	url: "/backend/productor/guardar/",
		    	      	type: "POST",
		    	      	data: frm,
		    	      	processData: false,  
		    	      	contentType: false
		    	    }).done(function(data){
		    	    	if(data > 0) 
		    	    	{
		    	    		var frm = new FormData(document.getElementById("frm-predio"));
		    	    		frm.append("id_productor", data.trim());
				    	    $.ajax({
				    	    	url: "/backend/predio/guardar/",
				    	      	type: "POST",
				    	      	data: frm,
				    	      	processData: false,  
				    	      	contentType: false
				    	    }).done(function(data){
				    	    	if(data > 0) 
				    	    	{
				    	    		ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
			                        $("#rfc_productor", "#frm-1").val($("#rfc", "#frm-productor").val());
			                        $(".dialog-form").dialog("close");
									$(".dialog-form").dialog("destroy");
									$("#search_rfc", "#frm-1").trigger("click");
				    	    	}
				    	    	else if(data == -1) 
				    	    		ShowDialogBox("No se ha podido guardar", "El predio ya existe", "Ok", "", false, "");
				    	    	else 
				    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
				    	    });
		    	    	}
		    	    	else if(data == -1) 
		    	    	{
		    	    		ShowDialogBox("No se ha podido guardar", "El productor ya existe, compruebe que algunos de los campos capturados no existan en otro registro", "Ok", "", false, "");
		    	    		$("#btnAnterior", "#predio_div").trigger("click");
		    	    	}
		    	    	else 
		    	    	{
		    	    		ShowDialogBox("Ha ocurrido un error al agregar el productor", data, "Ok", "", false, "");
		    	    		$("#btnAnterior", "#predio_div").trigger("click");
		    	    	}
		    	    });
		        }
		 	});*/
/*
			$("#id_estado", "#frm-predio").change(function(e){
		        if($("#id_estado", "#frm-predio").val() > 0)
		        {
		            $.ajax({
		                url: "/backend/predio/obtenermunicipios/estado/" + $("#id_estado", "#frm-predio").val(),
		                type: "POST",
		                processData: false,  
		                contentType: false
		            }).done(function(data){
		                $("#id_municipio", "#frm-predio").html(data);
		            });
		        }
		        else
		        {
		            $("#id_municipio", "#frm-predio").html('<option value="0">Seleccione un municipio</option>');
		            $("#id_localidad", "#frm-predio").html('<option value="0">Seleccione una localidad</option>');
		        }
		    });

		    $("#id_municipio", "#frm-predio").change(function(e){
		        if($("#id_municipio", "#frm-predio").val() > 0)
		        {
		            $.ajax({
		                url: "/backend/predio/obtenerlocalidades/municipio/" + $("#id_municipio", "#frm-predio").val(),
		                type: "POST",
		                processData: false,  
		                contentType: false
		            }).done(function(data){
		                $("#id_localidad", "#frm-predio").html(data);
		            });
		        }
		        else
		        {
		            $("#id_localidad", "#frm-predio").html('<option value="0">Seleccione una localidad</option>');
		        }
		    });*/
        
            $(".dialog-form").dialog({              
                height:'auto',
                width: 'auto',
                resizable: false,
                title: "Agregar productor",
                position: "center",
                modal: true
            }) //dialog
        } //success
    }) //$.ajax
}

function filtrargrigAvanzado()
{
    var filtro = "";

    if($("#filtro_nombre", "#frm-1-filtro").val().trim() != "") filtro += "/nombre/" + $("#filtro_nombre", "#frm-1-filtro").val().trim();
    if($("#filtro_curp", "#frm-1-filtro").val().trim() != "") filtro += "/curp/" + $("#filtro_curp", "#frm-1-filtro").val().trim();
    if($("#filtro_rfc", "#frm-1-filtro").val().trim() != "") filtro += "/rfc/" + $("#filtro_rfc", "#frm-1-filtro").val().trim();
	if($("#filtro_domicilio", "#frm-1-filtro").val().trim() != "") filtro += "/domicilio/" + $("#filtro_domicilio", "#frm-1-filtro").val().trim();

	$('#flexigrid-filtro').flexOptions({url: "/backend/productor/griddiagnostico" + filtro}).flexReload();
}

function limpiarFiltroAvanzado()
{
	$("#filtro_nombre, #filtro_curp, #filtro_rfc, #filtro_domicilio", "#frm-1-filtro").val('');
	filtrargrigAvanzado();
}

function seleccionar_productor(rfc)
{
	$("#rfc_productor", "#frm-1").val(rfc);
    $("#filtro-regresar", "#frm-1-filtro").trigger("click");
	$("#search_rfc", "#frm-1").trigger("click");
}

$(document).ready(function(){

	$( "#predio_estado_id").change(function() 
	{
		$.ajax({
            type: 'POST',
            url:"/backend/index/municipios/id/"+$("#predio_estado_id").val(),
            success:
            function respuesta(resM)
            {
               $('#municipio').html(resM);
               $("#municipio").val('');                  
            }
        });

	});

	$( "#municipio").change(function() 
	{
		$.ajax({
            type: 'POST',
            url:"/backend/index/localidades/id/"+$('#municipio').val(),
            success:
            function respuesta(resL)
            {	
               $('#localidad').html(resL);
               $("#localidad").val('');
            }
        });

	});

	

});


function listaTipoPredio(value)
{
	var listHtml='<option value="">Elija una opción</option>';
	if(value=='PREDIO NORMAL')
	{
		for(var i=1; i<=20; i++)
		{
			listHtml+='<option value="PREDIO-'+i+'">PREDIO-'+i+'</option>';
		}
	}
	else if(value=='ECA')
	{
		for(var i=1; i<=10; i++)
		{
			listHtml+='<option value="ECA-'+i+'">ECA-'+i+'</option>';
		}
	}
	else
	{
		listHtml='<option value="">Elija primero un tipo de predio</option>';
	}

	$("#clave").html(listHtml);

}


function guardarProductorGeneral()
{        
    $("#frm-1").submit();

}