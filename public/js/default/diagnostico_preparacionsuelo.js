$(document).ready(function(){
	$("#subsoleo_precio, #subsoleo_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('subsoleo');
	});
	$("#cinceleo_precio, #cinceleo_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('cinceleo');
	});
	$("#barbecho_precio, #barbecho_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('barbecho');
	});
	$("#rastra_precio, #rastra_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('rastra');
	});
	$("#cruza_precio, #cruza_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('cruza');
	});
	$("#laser_precio, #laser_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('laser');
	});
	$("#landplane_precio, #landplane_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('landplane');
	});
	$("#yunta_precio, #yunta_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('yunta');
	});
	$("#preparacion_manual_precio, #preparacion_manual_cantidad", "#frm-5").once('keyup', function(){
		calculaImporteFrm5('preparacion_manual');
	});
});

function cargarPreparacionSuelo(){
	
	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
		$('.preparacion tr').each(function(index, element) {
				if(index=='0'){
					$(this).find('th:nth-child(8)').remove();
				}else{
					$(this).find('td:nth-child(8)').remove();
				}
        });
	}
	
	var conceptos = ["subsoleo", "cinceleo", "barbecho", "rastra", "cruza", "laser", "landplane", "yunta", "preparacion_manual"];
	if($("#gen-type").val() == 1) for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_innovacion", "#frm-5").attr("disabled","disabled"); }
	else for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_innovacion", "#frm-5").removeAttr('disabled'); }
	
	$('#btn-save-pds', "#frm-5").once('click', function(){
		if(validaFrm5())
		{
			for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-5").removeAttr('disabled'); }
			var preId = $("#gen-id").val();
			var frm = new FormData(document.getElementById("frm-5"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarpreparaciondelsuelo/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	respuesta = jQuery.parseJSON(respuesta);
				try
				{
		        	for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-5").attr("disabled","disabled"); }
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data+"/preparacion_suelo/1";
		        	}
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });
	    }
	});
	
	$("#btn-congelar-pds", "#frm-5").once('click', function(){
		if(validaFrm5())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm5);
	});

	$('#btn-clean-pds', "#frm-5").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			for (var i = 0; i < conceptos.length; i++) {
				$("#" + conceptos[i] + "_Si", "#frm-5").prop('checked', false);
				$("#" + conceptos[i] + "_No", "#frm-5").prop('checked', false);
				$("#" + conceptos[i] + "_id_unidad",  "#frm-5").val('0');
				$("#" + conceptos[i] + "_cantidad",  "#frm-5").val('');
				$("#" + conceptos[i] + "_precio",  "#frm-5").val('');
				$("#" + conceptos[i] + "_importe",  "#frm-5").val('');
				$("#" + conceptos[i] + "_fecha",  "#frm-5").val('');
				$("#" + conceptos[i] + "_innovacion",  "#frm-5").val('');
			};
		});
	});

	function selectNo(concepto)
	{
		//alert(concepto);
		$("#" + concepto + "_id_unidad",  "#frm-5").val('0');
		$("#" + concepto + "_cantidad",  "#frm-5").val('');
		$("#" + concepto + "_precio",  "#frm-5").val('');
		$("#" + concepto + "_importe",  "#frm-5").val('');
		$("#" + concepto + "_fecha",  "#frm-5").val('');
		$("#" + concepto + "_innovacion",  "#frm-5").val('');
	}

	$('#subsoleo_No', "#frm-5").once('click', function(){ selectNo("subsoleo"); });
	$('#cinceleo_No', "#frm-5").once('click', function(){ selectNo("cinceleo"); });
	$('#barbecho_No', "#frm-5").once('click', function(){ selectNo("barbecho"); });
	$('#rastra_No', "#frm-5").once('click', function(){ selectNo("rastra"); });
	$('#cruza_No', "#frm-5").once('click', function(){ selectNo("cruza"); });
	$('#laser_No', "#frm-5").once('click', function(){ selectNo("laser"); });
	$('#landplane_No', "#frm-5").once('click', function(){ selectNo("landplane"); });

	//asignamos las unidades a los conceptos dependiendo la configuracion general
	obtenerUnidadesPorFormularioConceptoFrm5(conceptos);
	/*$.ajax({
        url: "/backend/diagnosticos/obtenerunidades/",
        type: "POST",
        async: false,
        processData: false,  
        contentType: false
    }).done(function(data){
        $("#subsoleo_id_unidad",  "#frm-5").html(data);
        $("#cinceleo_id_unidad",  "#frm-5").html(data);
        $("#barbecho_id_unidad",  "#frm-5").html(data);
        $("#rastra_id_unidad",  "#frm-5").html(data);
        $("#cruza_id_unidad",  "#frm-5").html(data);
        $("#laser_id_unidad",  "#frm-5").html(data);
        $("#landplane_id_unidad",  "#frm-5").html(data);
    });*/

    //[2020-06-04]Agregado por Christian López para resover el problema de asincronía 
    //con respecto a las unidades de medida
    setTimeout(function()
    { 

		$.ajax({
	        url: "/backend/diagnosticos/obtenerdatospreparaciondelsuelo/",
	        type: "POST",
	        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
	    }).done(function(data){
	    	var registro = jQuery.parseJSON(data);
			
			try {
	    	if(registro.error !== undefined) 
	    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
	    	else if(registro.nodata === undefined)
	    	{
				for (var i = 0; i < conceptos.length; i++) {
					//alert(registro[conceptos[i] + "_cantidad"]);
	        		if(registro[conceptos[i] + "_realizado"] == 1)
	        		{
	        			$("#" + conceptos[i] + "_Si", "#frm-5").prop('checked', true);
	        			$("#" + conceptos[i] + "_Si", "#frm-5").trigger( "click" );
	        		}
	        		else
	        		{
						$("#" + conceptos[i] + "_No", "#frm-5").prop('checked', true);
	        			$("#" + conceptos[i] + "_No", "#frm-5").trigger( "click" );
					}
					if(registro[conceptos[i] + "_id_unidad"] == null) $("#" + conceptos[i] + "_id_unidad",  "#frm-5").val('0');
					else $("#" + conceptos[i] + "_id_unidad", "#frm-5").val(registro[conceptos[i] + "_id_unidad"]);
					$("#" + conceptos[i] + "_cantidad", "#frm-5").val(registro[conceptos[i] + "_cantidad"]);
					$("#" + conceptos[i] + "_precio", "#frm-5").val(registro[conceptos[i] + "_precio"]);
					$("#" + conceptos[i] + "_importe", "#frm-5").val(registro[conceptos[i] + "_importe"]);
					$("#" + conceptos[i] + "_fecha", "#frm-5").val(registro[conceptos[i] + "_fecha"]);
					$("#" + conceptos[i] + "_innovacion", "#frm-5").val(registro[conceptos[i] + "_innovacion"]);
				}
				if(registro.congelado == 2) $('#frm-5 *').attr("disabled", "disabled").unbind();
	    	}
			}
			catch(err) {
				ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
			}
			
			
			
	    });
	}, 2000);

}
function obtenerUnidadesPorFormularioConceptoFrm5(conceptos){
	console.log(conceptos);
	for (var i = 0; i < conceptos.length; i++) {
			$.ajax({
				url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
				type: "POST",
				data:{formulario:"PreparacionDelTerreno",concepto:conceptos[i]}
			}).done(function(data){
				concepto = data.split("|")[0];
				datos = data.split("|")[1]
				$('#'+concepto+'_id_unidad', '#frm-5').html(datos);
			});
	}
	
}
function congelarFrm5(){
		var conceptos = ["subsoleo", "cinceleo", "barbecho", "rastra", "cruza", "laser", "landplane"];
			for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-5").removeAttr('disabled'); }
			var preId = $("#gen-id").val();
			var frm = new FormData(document.getElementById("frm-5"));
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardarpreparaciondelsuelo/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	respuesta = jQuery.parseJSON(respuesta);
				try
				{
		        	for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-5").attr("disabled","disabled"); }
		        	if(respuesta.ok !== undefined)
		        	{
							$.ajax({
							url: "/backend/diagnosticos/congelarpreparacionsuelo/",
							type: "POST",
							data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
							async: false
							}).done(function(respuesta){
								respuesta = jQuery.parseJSON(respuesta);
								
								try {
								if(respuesta.ok !== undefined)
								{
									ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true,"/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/"+$("#gen-id").val()+"/mejoramiento/1");
									$('#frm-5 *').attr("disabled", "disabled").unbind();
								}
								else
									ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
								catch(err) {
									ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
									
							});

		        		//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data+"/preparacion_suelo/1";
		        	}
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });

	
}

function validaFrm5()
{
	return true;
	if($("#subsoleo_Si").is(':checked')) {  

			if($("#subsoleo_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad del subsoleo es obligatoria", "Ok", "", false, "");
				$("#subsoleo_id_unidad").focus();		
				return false;
			}
			
			if($("#subsoleo_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad del subsoleo es obligatoria", "Ok", "", false, "");
				$("#subsoleo_cantidad").focus();
				return false;
			}
			
			if($("#subsoleo_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario del subsoleo es obligatorio", "Ok", "", false, "");
				$("#subsoleo_precio").focus();
				return false;
			}
			
			if($("#subsoleo_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total del subsoleo es obligatorio", "Ok", "", false, "");
				$("#subsoleo_importe").focus();
				return false;
			}
			
			if($("#subsoleo_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
		
		if($("#cinceleo_Si").is(':checked')) {  

			if($("#cinceleo_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad del cinceleo es obligatoria", "Ok", "", false, "");
				$("#cinceleo_id_unidad").focus();		
				return false;
			}
			
			if($("#cinceleo_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad del cinceleo es obligatoria", "Ok", "", false, "");
				$("#cinceleos_cantidad").focus();
				return false;
			}
			
			if($("#cinceleo_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario del cinceleo es obligatorio", "Ok", "", false, "");
				$("#cinceleo_precio").focus();
				return false;
			}
			
			if($("#cinceleo_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total del cinceleo es obligatorio", "Ok", "", false, "");
				$("#cinceleo_importe").focus();
				return false;
			}
			
			if($("#cinceleo_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización del cinceleo es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
		
		if($("#barbecho_Si").is(':checked')) {  

			if($("#barbecho_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad del barbecho con arado de discos es obligatoria", "Ok", "", false, "");
				$("#barbecho_id_unidad").focus();		
				return false;
			}
			
			if($("#barbecho_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad del barbecho con arado de discos es obligatoria", "Ok", "", false, "");
				$("#barbecho_cantidad").focus();
				return false;
			}
			
			if($("#barbecho_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario del barbecho con arado de discos es obligatorio", "Ok", "", false, "");
				$("#barbecho_precio").focus();
				return false;
			}
			
			if($("#barbecho_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total del barbecho con arado de discos es obligatorio", "Ok", "", false, "");
				$("#barbecho_importe").focus();
				return false;
			}
			
			if($("#barbecho_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización del barbecho con arado de discos es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
		
		if($("#rastra_Si").is(':checked')) {  

			if($("#rastra_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de rastra es obligatoria", "Ok", "", false, "");
				$("#rastra_id_unidad").focus();		
				return false;
			}
			
			if($("#rastra_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad de rastra es obligatoria", "Ok", "", false, "");
				$("#rastra_cantidad").focus();
				return false;
			}
			
			if($("#rastra_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de rastra es obligatorio", "Ok", "", false, "");
				$("#rastra_precio").focus();
				return false;
			}
			
			if($("#rastra_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de rastra es obligatorio", "Ok", "", false, "");
				$("#rastra_importe").focus();
				return false;
			}
			
			if($("#rastra_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de rastra es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
		
		
		if($("#cruza_Si").is(':checked')) {  

			if($("#cruza_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de Rastra (cruza) es obligatoria", "Ok", "", false, "");
				$("#cruza_id_unidad").focus();		
				return false;
			}
			
			if($("#cruza_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad de Rastra (cruza) es obligatoria", "Ok", "", false, "");
				$("#cruza_cantidad").focus();
				return false;
			}
			
			if($("#cruza_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de Rastra (cruza) es obligatorio", "Ok", "", false, "");
				$("#cruza_precio").focus();
				return false;
			}
			
			if($("#cruza_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de Rastra (cruza) es obligatorio", "Ok", "", false, "");
				$("#cruza_importe").focus();
				return false;
			}
			
			if($("#cruza_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de Rastra (cruza) es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
			
		if($("#laser_Si").is(':checked')) {  
			if($("#laser_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de nivelación con equipo laser es obligatoria", "Ok", "", false, "");
				$("#laser_id_unidad").focus();		
				return false;
			}
			
			if($("#laser_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad de nivelación con equipo laser es obligatoria", "Ok", "", false, "");
				$("#laser_cantidad").focus();
				return false;
			}
			
			if($("#laser_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de nivelación con equipo laser es obligatorio", "Ok", "", false, "");
				$("#laser_precio").focus();
				return false;
			}
			
			if($("#laser_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de nivelación con equipo laser es obligatorio", "Ok", "", false, "");
				$("#laser_importe").focus();
				return false;
			}
			
			if($("#laser_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de nivelación con equipo laser es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
		
		
		if($("#landplane_Si").is(':checked')) {  
			if($("#landplane_id_unidad").val() == 0){
				ShowDialogBox("Validación", "La Unidad de nivelación con landplane u otro es obligatoria", "Ok", "", false, "");
				$("#landplane_id_unidad").focus();		
				return false;
			}
			
			if($("#landplane_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad de nivelación con landplane u otro es obligatoria", "Ok", "", false, "");
				$("#landplane_cantidad").focus();
				return false;
			}
			
			if($("#landplane_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario de nivelación con landplane u otro es obligatorio", "Ok", "", false, "");
				$("#landplane_precio").focus();
				return false;
			}
			
			if($("#landplane_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total de nivelación con landplane u otro es obligatorio", "Ok", "", false, "");
				$("#landplane_importe").focus();
				return false;
			}
			
			if($("#landplane_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización de nivelación con landplane u otro es obligatoria", "Ok", "", false, "");		
				return false;
			}
			
			/*if($("#subsoleo_innovacion").val() == ""){
				ShowDialogBox("Validación", "La Innovación del subsoleo es obligatoria", "Ok", "", false, "");		
				return false;
			}*/
        } 
    return true;
}

function calculaImporteFrm5(concepto){
	if(parseFloat($("#" + concepto + "_cantidad", "#frm-5").val()) >= 0 && parseFloat($("#" + concepto + "_precio", "#frm-5").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-5").val(parseFloat($("#" + concepto + "_cantidad", "#frm-5").val()) * parseFloat($("#" + concepto + "_precio", "#frm-5").val()));
	else
		$("#" + concepto + "_importe", "#frm-5").val('');
}