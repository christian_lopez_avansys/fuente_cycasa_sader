var dg_congelado = false;
var MisDatos = new Array();

$(document).ready(function(){
	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
			$('.tabfol tr:eq(0)').each(function(index, element) {
					//$(this).find('th:eq(7)').remove();
					$(this).find('th:eq(7)').remove();
			});
			$('.tabfol tr').not('.tabfol tr:eq(0)').each(function(index, element) {
				if($(this).find('th').length==2){
					//$(this).find('th:eq(7)').remove();
					$(this).find('th:eq(0)').attr("colspan","7");
				}else{
					//$(this).find('th:eq(7)').remove();
					$(this).find('td:eq(6)').remove();
				}
					
			});
			$('#tabla-generica-fol tr:eq(0)').each(function(index, element) {
					$(this).find('th:eq(7)').remove();
			});
			$('#tabla-generica-fol tr').not('#tabla-generica-fol tr:eq(0)').each(function(index, element) {
					if($(this).find('th').length==2){
					//$(this).find('th:eq(7)').remove();
					$(this).find('th:eq(0)').attr("colspan","7");
				}else{
					//$(this).find('th:eq(7)').remove();
					$(this).find('td:eq(6)').remove();
				}
			});
	}
	
	
	if($("#frm-14 #congelado").val() == 2){
		$('#frm-14 *').attr("disabled", "disabled").unbind();	
	}
	
	$('#btnGuardarControlPlagasFoliares').once('click', function()
	{
		if(validarFrm14())
		{
			$.ajax({
				url: "/backend/diagnosticos/guardar-control-plagas-foliares/",
				type: "POST",
				data: $("#frm-14").serialize()
			}).done(function(respuesta)
			{
				$('#frm-14 #id').val(respuesta);
				ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", false, "");
			});
		}
	});


	$('#btnCongelarControlPlagasFoliares').once('click', function()
	{
		if(validarFrm14())
		{
			$.ajax({
			url: "/backend/diagnosticos/guardar-control-plagas-foliares/",
			type: "POST",
			data: $("#frm-14").serialize()
			}).done(function(respuesta)
			{
				$('#frm-14 #id').val(respuesta);
				$.ajax({
				url: "/backend/diagnosticos/congelar-control-plagas-foliares/",
				type: "POST",
				data: $("#frm-14").serialize()
				}).done(function(respuesta)
				{
					if(respuesta==1){
						$('#frm-14 *').attr("disabled", "disabled").unbind();	
						
						ShowDialogBox("Guardado", "La información se ha congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/plagas_foliares/1");
					}else{
						ShowDialogBox("Guardado", "Primero es necesario guardar la información", "Ok", "", false, "", false);
					}			});
					//ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
				});
		
		}else{
			ShowDialogBox("Validación", "El formulario está incompleto; favor de completarlo.", "Ok", "", false, "");
		}
	});
	
	$('#btnLimpiarControlPlagasFoliares').once('click', function()
	{
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$('input','#frm-14').val('');
			$('#frm-14 select').not("[id^='tipo_ap_select_fol_']").each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
			$('#frm-14 [id^="tipo_ap_select_fol_"]').each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).val('2').change();
			});
		});
	});



});

function validarFrm14()
{
	return true;
	var bandera=true;
	//console.log("entra: ");
	$("#frm-14 table").not(".hide").each(function(i){
		console.log("Tabla: "+this.id);	
		$(this).find(" tr").not("#fila_generica_coad, #fila_generica_marca").each(function(i){	
		console.log("Renglon: "+this.id);
			$(this).find(".required").each(function(index, element) {
				if($(this).val()=="" || $(this).val()<=0)
				{
					console.log("campo: "+this.id);
					$(this).focus();	
					bandera=false;
				}
			});
		});
	});

	return bandera;
/*	var bandera=true;
	$("#frm-14 .required").each(function(i){

		//console.log($(this).val());
		if($(this).val()=="" || $(this).val()=="0")
		{
			$(this).focus();	
			bandera=false;
		}

	});

	return bandera;*/
 
}

function calculaImporteFrm14(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
			var cantidad = $(parent2).find(' td:eq(1) input').attr("value");
			var precio = $(parent2).find(' td:eq(2) input').attr("value");
			var importe = $(parent2).find(' td:eq(3) input');

		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		
		if(cantidad>0 && precio>0){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}