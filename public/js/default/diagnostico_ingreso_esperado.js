$(document).ready(function(){
	
});

function cargarIngresoEsperado()
{
	$('#btn-save-ies', "#frm-18").once('click', function(){
		if(validaFrm18())
		{
			arreglo =new Array();
			$("#frm-18 input[type='radio']").each(function(index, element) {
					id=this.id;
					trozos = id.split("-");
					concepto = trozos[0];
					/*if(concepto=='proagroproductivo')
						concepto='Produccion para el bienestar';*/
					input = trozos[1];
					if(input=="Si" && $('#'+id).is(':checked')){
						arreglo.push({concepto:concepto,toneladas_por_hectarea:$('#'+concepto+'-tonelada').val(),ingreso_por_tonelada:$('#'+concepto+'-ingreso_tonelada').val(),ingreso_total:$('#'+concepto).val()});
					}
					
				});
			//alert(JSON.stringify(arreglo).toSource());
			/*var frm = new FormData(document.getElementById("frm-18"));	
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());*/
			$.ajax({
	            url: "/backend/diagnosticos/guardaringresoesperado/",
	            type: "POST",
	            data: {datos:JSON.stringify(arreglo),tipo:$("#gen-type").val(),id:$("#gen-id").val()},
	        }).done(function(respuesta){
	        	try
	        	{
	        		respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
		        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        	}
		        	else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });
		}
	});
	
	$("#btn-congelar-ies", "#frm-18").once('click', function(){
		if(validaFrm18()){
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm18);
		}
	});

	$('#btn-clean-ies', "#frm-18").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$("#frm-18").get(0).reset();
		});
	});

	$.ajax({
        url: "/backend/diagnosticos/obtenerdatosingresoesperado/",
        type: "POST",
        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
    }).done(function(data){
    	try
    	{
	    	var registro = jQuery.parseJSON(data);
			//alert(registro.toSource());
	    	if(registro.error !== undefined) 
	    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, intentelo nuevamente", "Ok", "", false, "");
	    	else if(registro.nodata === undefined)
	    	{
				
				if(registro.concepto!=undefined){
					var cuantos = registro.concepto.length;
					$.each(registro.concepto, function(i) {
						if(i!=null){
							concepto = registro.concepto[i].uso;
							toneladas_por_hectarea = registro.concepto[i].toneladas_por_hectarea;
							ingreso_por_tonelada = registro.concepto[i].ingreso_por_tonelada;
							ingreso_total = registro.concepto[i].ingreso_total;
							

							//alert(concepto+": "+ toneladas_por_hectarea+" "+ingreso_por_tonelada+" "+ingreso_total);
							if(concepto=='Produccion para el bienestar')
							{

								$('#proagroproductivo').val(ingreso_total);
								$('#proagroproductivo'+'-tonelada').val(toneladas_por_hectarea);
								$('#proagroproductivo'+'-ingreso_tonelada').val(ingreso_por_tonelada);
							}
							else
							{
								$('#'+concepto).val(ingreso_total);
								$('#'+concepto+'-tonelada').val(toneladas_por_hectarea);
								$('#'+concepto+'-ingreso_tonelada').val(ingreso_por_tonelada);
							}
							if(concepto=='proagroproductivo')
								concepto='Producción para el bienestar';
						}
	
					});
					$("#frm-18 input[type='radio']").each(function(index, element) {
							id=this.id;
							trozos = id.split("-");
							concepto = trozos[0];
							input = trozos[1];
							if(input=="Si"){
								habilitaIE(this);
							}
							
					});
				
				
					
				
					if(registro.congelado == 2)
						$('#frm-18 *').attr("disabled", "disabled").unbind();
	
					//$("#valor_ingreso_por_tonelada").html("$"+(parseFloat(registro.grano)/parseFloat(registro.toneladas_por_hectarea)).toFixed(2));
				}

	    	}
	    	else
			{
				alert('No hay conceptos');
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente: " + err.message, "Ok", "", false, "");
		}
    });

}

function congelarFrm18(){
	
			var frm = new FormData(document.getElementById("frm-18"));	
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
			$.ajax({
	            url: "/backend/diagnosticos/guardaringresoesperado/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(respuesta){
	        	try
	        	{
	        		respuesta = jQuery.parseJSON(respuesta);
		        	if(respuesta.ok !== undefined)
		        	{
							$.ajax({
							url: "/backend/diagnosticos/congelaringresoesperado/",
							type: "POST",
							data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
							async: false
							}).done(function(respuesta){
								try
								{
									respuesta = jQuery.parseJSON(respuesta);
									if(respuesta.ok !== undefined){
										ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", false, "");
										$('#frm-18 *').attr("disabled", "disabled").unbind();	
									}
									else
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
								catch(err)
								{
									ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
								}
							});

		        		//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
		        	}
		        	else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");
				}
	        });

}

function validaFrm18()
{
	return true;
/*	var val = true;
	$("#frm-18 :input").each(function(){ 
		if($(this).val().trim() === "")
		{
			ShowDialogBox("Datos incompletos", "Hay datos faltantes, por favor revise los datos que este completos", "Ok", "", false, "");
			val = false;
		}
	});
	return val;
*/
	var bandera = true;
	//recorremos los radios y los seteamos
	$("#frm-18 input[type='radio']").each(function(index, element) {
        id= this.id;
		trozos = id.split("-");
		concepto = trozos[0];
		
		if($("#"+id).is(':checked') && ($("#"+concepto+'-ingreso_tonelada').val()<=0 || $("#"+concepto+'-tonelada').val()<=0 || $("#"+concepto).val()<=0) && $("#"+id).val()=='1'){
			ShowDialogBox("Datos incompletos", "Hay datos faltantes, por favor revise los datos que este completos", "Ok", "", false, "");
			bandera= false;
		}
    });
	return bandera;


}

function habilitaIE(selector){
	id= selector.id;
	trozos = selector.id.split("-");
	concepto = trozos[0];
	input = trozos[1];
	//alert(concepto);
	//comprobamos el valor del selector para habilitar o deshabilitar
	if($("#"+concepto+'-Si').is(':checked')){
		//habilitamos el campo
		$('#'+concepto+'-Si').attr('checked', true);
		$("#"+concepto+'-No').attr('checked', false);
		//quitamos el attr readonly del elemento
		//$('#'+concepto).removeAttr("readonly");
		$('#'+concepto+'-tonelada').removeAttr("readonly");
		$('#'+concepto+'-ingreso_tonelada').removeAttr("readonly");
	}else if($("#"+concepto+'-No').is(':checked')){
		//deshabilitamos el campo
		$('#'+concepto+'-Si').attr('checked', false);
		$("#"+concepto+'-No').attr('checked', true);
		//deshabilitamos el attr readonly del elemento
		//$('#'+concepto).attr("readonly");
		$('#'+concepto+'-tonelada').attr("readonly");
		$('#'+concepto+'-ingreso_tonelada').attr("readonly");
		//seteamos a cero
		$('#'+concepto+'-tonelada').val('');
		$('#'+concepto+'-ingreso_tonelada').val('');
		$('#'+concepto).val('');
	}else{
		if($('#'+concepto).val()>0){
			//habilitamos el campo
			$('#'+concepto+'-Si').attr('checked', true);
			$("#"+concepto+'-No').attr('checked', false);
			//quitamos el attr readonly del elemento
			//$('#'+concepto).removeAttr("readonly");
			$('#'+concepto+'-tonelada').removeAttr("readonly");
			$('#'+concepto+'-ingreso_tonelada').removeAttr("readonly");
		}else{
			//deshabilitamos el campo
			$('#'+concepto+'-Si').attr('checked', false);
			$("#"+concepto+'-No').attr('checked', true);
			//deshabilitamos el attr readonly del elemento
			//$('#'+concepto).attr("readonly");
			$('#'+concepto+'-tonelada').attr("readonly");
			$('#'+concepto+'-ingreso_tonelada').attr("readonly");
			//seteamos a cero
			$('#'+concepto+'-tonelada').val('');
			$('#'+concepto+'-ingreso_tonelada').val('');
			$('#'+concepto).val('');
		}
		
	}
	
}
function calculaIngresoFrm18(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
	//var parent3 = $(obj).parents().get(2);
	//$(parent).css("background-color","#F00");
	
			var toneladas = $(parent).find(' td:eq(2) input').attr("value");
			var ingreso_x_ton = $(parent).find(' td:eq(3) input').attr("value");
			var ingreso_total = $(parent).find(' td:eq(4) input');

		toneladas = parseFloat(toneladas);
		ingreso_x_ton = parseFloat(ingreso_x_ton);

		//alert(toneladas+" "+ingreso_x_ton+" "+ingreso_total);
		if(toneladas>0 && ingreso_x_ton>0){
			ingreso_total.val(toneladas*ingreso_x_ton);
		}else{
			ingreso_total.val('');
		}
}
