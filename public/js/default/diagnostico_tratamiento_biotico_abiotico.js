var dg_congelado = false;
var MisDatos = new Array();
var cargado = 0;



$(document).ready(function(){
	if($("#frm-16 #congelado").val() == 2){
		$('#frm-16 *').attr("disabled", "disabled").unbind();	
	}

	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
			$('#frm-16 #tabla tr:eq(0)').each(function(index, element) {
					$(this).find('th:eq(7)').remove();
			});
			$('#frm-16 #tabla tr').not('#frm-16 #tabla tr:eq(0)').each(function(index, element) {
					$(this).find('td:eq(6)').remove();	
			});
	}
	
	
	$('#btnGuardarTratamientoBioticoAbiotico').once('click', function()
	{
		if(validarFrm16())
		{
			$.ajax({
				url: "/backend/diagnosticos/guardar-tratamiento-de-estres-biotico-y-abiotico/",
				type: "POST",
				data: $("#frm-16").serialize()
			}).done(function(respuesta)
			{
				$('#frm-16 #id').val(respuesta);
				ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", false, "");//"/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"");
			});
		}else
		{
			ShowDialogBox("Validación", "El formulario está incompleto; favor de completarlo.", "Ok", "", false, "");
		}
	});


	$('#btnCongelarTratamientoBioticoAbiotico').once('click', function()
	{
		if(validarFrm16())
		{
		$.ajax({
			url: "/backend/diagnosticos/guardar-tratamiento-de-estres-biotico-y-abiotico/",
			type: "POST",
			data: $("#frm-16").serialize()
		}).done(function(respuesta)
		{
				$('#frm-16 #id').val(respuesta);
				$.ajax({
				url: "/backend/diagnosticos/congelar-tratamiento-de-estres-biotico-y-abiotico/",
				type: "POST",
				data: $("#frm-16").serialize()
				}).done(function(respuesta)
				{
					if(respuesta==1){
						$('#frm-16 *').attr("disabled", "disabled").unbind();	
						ShowDialogBox("Guardado", "La información se ha congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/tratamiento_estres/1");
					}else{
						ShowDialogBox("Error al congelar", "Hubo un error al congelar el formulario, inténtelo nuevamente", "Ok", "", false, "", false);
					}
				});

			//ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", false, "");//"/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"");
		});

		}
		else
		{
			ShowDialogBox("Validación", "El formulario está incompleto; favor de completarlo.", "Ok", "", false, "");
		}
	});
	
	$('#btnLimpiarTratamientoBioticoAbiotico').once('click', function()
	{
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$('input','#frm-16').val('');
			$('#frm-16 select').not("[id^='tipo_ap_select_gran2_1']").not("[id^='tipo_ap_select_trat2_1']").not("[id^='tipo_ap_select_rec2_1']").not("[id^='tipo_ap_select_ctrl2_1']").not("[id^='tipo_ap_select_otr2_1']").each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
			$('#frm-16 [id^="tipo_ap_select_gran2_1"], #frm-16 [id^="tipo_ap_select_trat2_1"], #frm-16 [id^="tipo_ap_select_rec2_1"], #frm-16 [id^="tipo_ap_select_ctrl2_1"], #frm-16 [id^="tipo_ap_select_otr2_1"]').each(function() {
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).val('2').change();
			});
		});
	});


});

function validarFrm16()
{
	return true;
	var bandera=true;
	//console.log("entra: ");
	$("#frm-16 #tabla tr").not("#frm-16 #tabla #fila-general-gran2, #frm-16 #tabla #fila-general-trat2, #frm-16 #tabla #fila-general-rec2").each(function(i){	
		$(this).find(".required").each(function(index, element) {
			
			if($(this).val()=="" || $(this).val()=="0")
			{
				console.log(this.id);
				$(this).focus();	
				bandera=false;
			}
		});

	});

	return bandera;

}

function evaluaCambioAplicacionFrm16(elemento){
	
	$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").removeClass("required");
	$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").removeClass("required");
	$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").removeClass("required");
	
	$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").removeClass("required");
	$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").removeClass("required");
	
	
	if($("#frm-16 #despues_"+elemento+"_apli_1  #tipo_ap_select_" + elemento+"_1").val() == 1 || $("#frm-16 #despues_"+elemento+"_apli_1  #tipo_ap_select_" + elemento+"_1").val() == 3){
		//mecanizada
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_"+elemento+"_1").val('1').trigger("change");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").addClass("hide");
		
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_"+elemento+"2_1 option:eq(0)").prop('selected', true);
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').addClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').addClass("required");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1").removeClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1").removeClass("required");
		
	}else{
		//manual
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1").removeClass("hide").addClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1").addClass("hide").removeClass("required");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").val('').addClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").val('').addClass("required");
	
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').removeClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').removeClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo"+elemento+"_1").removeClass("required");
	}
	
}
function evaluaCambioAplicacion2Frm16(elemento){
	$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').removeClass("required");
	$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').removeClass("required");
	$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").removeClass("required");
	
	if($("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_"+elemento+"_1").val() == 1){
		//propia
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").val('').removeClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").val('').removeClass("required");
		
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').addClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').addClass("required");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").addClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").val('').removeClass("required");

	}else{
		//rentada
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_"+elemento+"_1").addClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('').removeClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('').removeClass("required");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_no_jornales_"+elemento+"_1").val('').removeClass("required");
		$("#frm-16 #despues_"+elemento+"_apli_1 #manual_div_"+elemento+"_1 #manual_div_costo_jornal_"+elemento+"_1").val('').removeClass("required");
		
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").val('').addClass("required");
		
		
	}
}
/*function evaluaCambioAplicacion2Frm16(elemento){
	if($("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_"+elemento+"_1").val() == 1){
		//mecanizada
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");

		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_combustible_"+elemento+"_1").val('');
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_operador_"+elemento+"_1").val('');

	}else{
		//manual
		$("#frm-16 #despues_"+elemento+"_apli_1  #mecanizada_div_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1  #manual_div_"+elemento+"_1").addClass("hide");
		
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_propia_"+elemento+"_1").addClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1").removeClass("hide");
		$("#frm-16 #despues_"+elemento+"_apli_1 #mecanizada_div_"+elemento+"_1 #mecanizada_div_select_div_rentada_"+elemento+"_1 #mecanizada_div_select_div_rentada_costo_"+elemento+"_1").val('');
		
	}
}
*/

/*function calculaImporteFrm16(obj,id,hermano,receptor){
	
	var parent = $(obj).parents().get(1);
		//$(parent).css("background-color","#F00");
		if(id==undefined){
			var cantidad = $(parent).find(' td:eq(1) input').attr("value");
			var precio = $(parent).find(' td:eq(2) input').attr("value");
			var importe = $(parent).find(' td:eq(3) input');

		}else{
			var cantidad = $("#"+id).val();
			var precio = $("#"+hermano).val();
			var importe = $("#"+receptor);
		}
		
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		
		if(cantidad>0 && precio>0){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}*/
function calculaImporteFrm16(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
	
			if($(parent).attr("id")!=undefined && $(parent).attr("id").valueOf()=='despues_gran2_marca_1'){
				var cantidad = $(parent).find(' td:eq(1) input').attr("value");
				var precio = $(parent).find(' td:eq(2) input').attr("value");
				var importe = $(parent).find(' td:eq(3) input');
			}else{
				var cantidad = $(parent2).find(' td:eq(1) input').attr("value");
				var precio = $(parent2).find(' td:eq(2) input').attr("value");
				var importe = $(parent2).find(' td:eq(3) input');
			}
/*			var cantidad = $(parent2).find(' td:eq(1) input').attr("value");
			var precio = $(parent2).find(' td:eq(2) input').attr("value");
			var importe = $(parent2).find(' td:eq(3) input');*/
			//var hectareas = $('.semilla').attr("value");
			//alert(hectareas);
		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		//hectareas = parseFloat(hectareas);
		
		if(cantidad>0 && precio>0){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}