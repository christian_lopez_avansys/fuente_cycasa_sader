	  google.load("visualization", "1", {packages:["corechart"]});
      /*google.setOnLoadCallback(drawChart);
      function drawChart() {
        
      }*/
var total_costos_indirectos =0;
var total_indirectos_cultivos=0;
function cargarReporteCostos()
{
	
/*	$('#frm-20 table td').each(function() {
		$(this).find('*:contains("(+hectareas+)")').css("color","#00F");
		$(this).find("(+hectareas+)").css("background-color","#00F");
	});*/
	/*$("tr td:contains('(+hectareas+)'), tr th:contains('(+hectareas+)')").each(function(){
		var texto = $(this).text();
		texto =texto.split('(+hectareas+)')[0];
	  	$(this).html("<strong>"+texto+" ("+$("#superficie").val()+")"+"</strong>");
	});*/

	$.ajax({
			url: "/backend/diagnosticos/obtener-indice-de-renta/",
			type: "POST",
			data: {id_dato_general: $("#gen-id").val(),tipo: $("#gen-type").val(),funcion:"reporte_costos"}
	}).done(function(respuesta)
	{
		respuesta = jQuery.parseJSON(respuesta);
		
		(respuesta.renta_tierra==null)?$('#renta_tierra').val('0'):$('#renta_tierra').val(respuesta.renta_tierra);
		(respuesta.acondicionamiento_predio==null)?$('#acondicionamiento_predio').val('0'):$('#acondicionamiento_predio').val(respuesta.acondicionamiento_predio);
		(respuesta.mejoramiento_fertilidad==null)?$('#mejoramiento_fertilidad').val('0'):$('#mejoramiento_fertilidad').val(respuesta.mejoramiento_fertilidad);
		(respuesta.preparacion_suelo==null)?$('#preparacion_suelo').val('0'):$('#preparacion_suelo').val(respuesta.preparacion_suelo);
		(respuesta.riego==null)?$('#riego').val('0'):$('#riego').val(respuesta.riego);
		(respuesta.siembra==null)?$('#siembra').val('0'):$('#siembra').val(respuesta.siembra);
		(respuesta.practica_siembra==null)?$('#practica_siembra').val('0'):$('#practica_siembra').val(respuesta.practica_siembra);
		(respuesta.resiembra==null)?$('#resiembra').val('0'):$('#resiembra').val(respuesta.resiembra);
		(respuesta.practica_resiembra==null)?$('#practica_resiembra').val('0'):$('#practica_resiembra').val(respuesta.practica_resiembra);
		(respuesta.fertilizacion==null)?$('#fertilizacion').val('0'):$('#fertilizacion').val(respuesta.fertilizacion);
		(respuesta.control_malezas==null)?$('#control_malezas').val('0'):$('#control_malezas').val(respuesta.control_malezas);
		(respuesta.control_plagas_suelo==null)?$('#control_plagas_suelo').val('0'):$('#control_plagas_suelo').val(respuesta.control_plagas_suelo);
		(respuesta.control_plagas_foliares==null)?$('#control_plagas_foliares').val('0'):$('#control_plagas_foliares').val(respuesta.control_plagas_foliares);
		(respuesta.control_enfermedades==null)?$('#control_enfermedades').val('0'):$('#control_enfermedades').val(respuesta.control_enfermedades);
		(respuesta.tratamiento_estres==null)?$('#tratamiento_estres').val('0'):$('#tratamiento_estres').val(respuesta.tratamiento_estres);
		total = parseFloat((respuesta.renta_tierra==null)?0:respuesta.renta_tierra) + parseFloat((respuesta.acondicionamiento_predio==null)?0:respuesta.acondicionamiento_predio) + parseFloat((respuesta.mejoramiento_fertilidad==null)?0:respuesta.mejoramiento_fertilidad) + parseFloat((respuesta.preparacion_suelo==null)?0:respuesta.preparacion_suelo) + parseFloat((respuesta.riego==null)?0:respuesta.riego) + parseFloat((respuesta.siembra==null)?0:respuesta.siembra) + parseFloat((respuesta.practica_siembra==null)?0:respuesta.practica_siembra) + parseFloat((respuesta.resiembra==null)?0:respuesta.resiembra) + parseFloat((respuesta.practica_resiembra==null)?0:respuesta.practica_resiembra) + parseFloat((respuesta.fertilizacion==null)?0:respuesta.fertilizacion) + parseFloat((respuesta.control_malezas==null)?0:respuesta.control_malezas) + parseFloat((respuesta.control_plagas_suelo==null)?0:respuesta.control_plagas_suelo) + parseFloat((respuesta.control_plagas_foliares==null)?0:respuesta.control_plagas_foliares) + parseFloat((respuesta.control_enfermedades==null)?0:respuesta.control_enfermedades) + parseFloat((respuesta.tratamiento_estres==null)?0:respuesta.tratamiento_estres);
		//alert( parseFloat((respuesta.mejoramiento_fertilidad==null)?0:respuesta.mejoramiento_fertilidad));
		$('#total_costos').html("$ "+total.toFixed(2));
		var costo_cultivo_hectarea=parseFloat(total)/parseFloat($("#superficie").val())
		//$('#costo_cultivo_hectarea').html("$ "+costo_cultivo_hectarea.toFixed(2));
		console.log("toneladas_por_hectarea: "+respuesta.toneladas_por_hectarea);
		$("#costo_por_tonelada").html("$"+(costo_cultivo_hectarea/parseFloat(respuesta.toneladas_por_hectarea)).toFixed(2));
		
		
		var renta_tierra = (respuesta.renta_tierra==null)?0:respuesta.renta_tierra;
		var acondicionamiento = (respuesta.acondicionamiento_predio==null)?0: respuesta.acondicionamiento_predio;
		var mejoramiento = (respuesta.mejoramiento_fertilidad==null)?0:respuesta.mejoramiento_fertilidad;
		var preparacion_suelo = (respuesta.preparacion_suelo==null)?0:respuesta.preparacion_suelo;
		var riego = (respuesta.riego==null)?0:respuesta.riego;
		var siembra= (respuesta.siembra==null)?0:respuesta.siembra;
		var practica_siembra = (respuesta.practica_siembra==null)?0:respuesta.practica_siembra;
		var resiembra = (respuesta.resiembra==null)?0:respuesta.resiembra;
		var practica_resiembra = (respuesta.practica_resiembra==null)?0:respuesta.practica_resiembra;
		var fertilizacion = (respuesta.fertilizacion==null)?0:respuesta.fertilizacion;
		var control_malezas = (respuesta.control_malezas==null)?0:respuesta.control_malezas;
		var control_plagas = (respuesta.control_plagas_suelo==null)?0:respuesta.control_plagas_suelo;
		var plagas_foliares = (respuesta.control_plagas_foliares==null)?0:respuesta.control_plagas_foliares;
		var control_enfermedades = (respuesta.control_enfermedades==null)?0:respuesta.control_enfermedades;
		var tratamiento_estres = (respuesta.tratamiento_estres==null)?0:respuesta.tratamiento_estres;

		console.log(respuesta);
		
			var data = google.visualization.arrayToDataTable([
				['Concepto', 'Costo'],
				['Renta de la tierra',  parseFloat(renta_tierra)  ],
				['Acondicionamiento del predio',   parseFloat(acondicionamiento) ],
				['Mejoramiento de fertilidad del suelo',  parseFloat(mejoramiento)],
				['Preparación del suelo', parseFloat(preparacion_suelo)],
				['Riego',    parseFloat(riego)],
				['Siembra',    parseFloat(siembra)],
				['Practica de siembra',   parseFloat(practica_siembra)],
				//['Resiembra',   parseFloat(resiembra)],
				//['Practica de resiembra',   parseFloat(practica_resiembra)],
				['Nutrición',    parseFloat(fertilizacion)],
				['Manejo de plantas no deseadas',   parseFloat(control_malezas)],
				['Manejo Fitosanitario (plagas de suelo)',    parseFloat(control_plagas)],
				['Manejo Fitosanitario (plagas foliares)',    parseFloat(plagas_foliares)],
				['Manejo Fitosanitario (control de enfermedades)',    parseFloat(control_enfermedades)],
				['Inductores de resistencia vegetal, crecimiento y productividad',   parseFloat(tratamiento_estres)]
			]);


			
			var options = {
			  title: 'Reporte de costo de cultivo',
			  is3D: true,
			};
			
			var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        	chart.draw(data, options);


			//nos traemos los datos de los costos indirectos
			$.ajax({
			url: "/backend/diagnosticos/obtenerdatoscostoindirecto/",
			type: "POST",
			data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
			}).done(function(data){
				try
				{
					var registro = jQuery.parseJSON(data);
					if(registro.error !== undefined) 
						ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, intentelo nuevamente", "Ok", "", false, "");
					else if(registro.nodata === undefined)
					{
						//formateamos la cadena recibida para poder manipular las cantidades
						var trilla_corte = (registro.trilla_corte==null)?0:parseFloat(registro.trilla_corte);
						var flete = (registro.flete==null)?0:parseFloat(registro.flete);
						var seguro_agricola = (registro.seguro_agricola==null)?0:parseFloat(registro.seguro_agricola);
						var asistencia_tecnica = (registro.asistencia_tecnica==null)?0:parseFloat(registro.asistencia_tecnica);
						var gasto_almacen = (registro.gasto_almacen==null)?0:parseFloat(registro.gasto_almacen);
						var cobertura_precio = (registro.cobertura_precio==null)?0:parseFloat(registro.cobertura_precio);
						var garantia_liquida = (registro.garantia_liquida==null)?0:parseFloat(registro.garantia_liquida);
						
						total_costos_indirectos = trilla_corte+flete+seguro_agricola+asistencia_tecnica+gasto_almacen+cobertura_precio+garantia_liquida;
						

						$("#trilla_o_corte", "#frm-20").val(trilla_corte);
						$("#flete", "#frm-20").val(flete);
						$("#seguro_agricola", "#frm-20").val(seguro_agricola);
						$("#asistencia_tecnica", "#frm-20").val(asistencia_tecnica);
						$("#gastos_de_almacen", "#frm-20").val(gasto_almacen);
						$("#cobertura_precios", "#frm-20").val(cobertura_precio);
						$("#garantia_liquida", "#frm-20").val(garantia_liquida);
						//asignamos el total de costos de cultivos obtenido arriba en esta seccion
						total_indirectos_cultivos=parseFloat(total)+parseFloat(total_costos_indirectos);
						$('#frm-20 #costo_de_cultivo_indirecto').val("$ "+total_indirectos_cultivos.toFixed(2));
						$('#frm-20 #costos_indirectos').val("$ "+total_costos_indirectos.toFixed(2));

						var costos_indirectos_hectarea=parseFloat(total_costos_indirectos)/parseFloat($("#superficie").val())
						//$('#frm-20 #costos_indirectos_hectarea').val("$ "+costos_indirectos_hectarea.toFixed(2));
						//$('#frm-20 #costo_de_cultivo_indirecto_por_hectarea').val("$ "+ (parseFloat(total_costos_indirectos) + parseFloat(total_indirectos_cultivos)) / parseFloat($("#superficie").val()));
						
						//dibujamos el grafico
						
						var dataCc = google.visualization.arrayToDataTable([
							['Concepto', 'Costo'],
							['Trilla o corte',  parseFloat(trilla_corte)  ],
							['Flete',   parseFloat(flete) ],
							['Seguro agrícola',  parseFloat(seguro_agricola)  ],
							['Asistencia técnica',  parseFloat(asistencia_tecnica)  ],
							['Gastos de almacén',  parseFloat(gasto_almacen)  ],
							['Cobertura de precios',  parseFloat(cobertura_precio)  ],
							['Intereses financieros',  parseFloat(garantia_liquida)  ],
						]);

						var options = {
						  title: 'Reporte de Costos Indirectos',
						  is3D: true,
						};
						
						var chartCc = new google.visualization.PieChart(document.getElementById('piechartCc_3d'));
						chartCc.draw(dataCc, options);
						
						
						//dibujamos el grafico
						
						var dataCI = google.visualization.arrayToDataTable([
							['Concepto', 'Costo'],
							['Costos de cultivos',  parseFloat(total)  ],
							['Costos indirectos',   parseFloat(total_costos_indirectos) ],
						]);

						var options = {
						  title: 'Reporte de Costos de Cultivo / Costos Indirectos',
						  is3D: true,
						};
						
						var chartCI = new google.visualization.PieChart(document.getElementById('piechartCI_3d'));
						chartCI.draw(dataCI, options);
						obtenerDatosIngresoEsperado();
					}
				}
				catch(err)
				{
					ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos de costos indirectos. " + err.message, "Ok", "", false, "");
				}
			});
		
		
		function obtenerDatosIngresoEsperado(){
		
				//nos traemos los datos de los ingresos esperados
				$.ajax({
				url: "/backend/diagnosticos/obtenerdatosingresoesperado/",
				type: "POST",
				data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
				}).done(function(data){
					try
					{
						var registro = jQuery.parseJSON(data);
						if(registro.error !== undefined) 
							ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, intentelo nuevamente", "Ok", "", false, "");
						else if(registro.nodata === undefined)
						{
							/*//formateamos la cadena recibida para poder manipular las cantidades
							var grano = (registro.grano==null)?0:parseFloat(registro.grano);
							var elote = (registro.elote==null)?0:parseFloat(registro.elote);
							var forraje_rastrojo = (registro.forraje_rastrojo==null)?0:parseFloat(registro.forraje_rastrojo);
							var forraje_verde = (registro.forraje_verde==null)?0:parseFloat(registro.forraje_verde);
							var hoja = (registro.hoja==null)?0:parseFloat(registro.hoja);
							var indemnizacion_seguro = (registro.indemnizacion_seguro==null)?0:parseFloat(registro.indemnizacion_seguro);
							var procampo = (registro.procampo==null)?0:parseFloat(registro.procampo);
							var incentivo = (registro.incentivo==null)?0:parseFloat(registro.incentivo);
							
							$("#grano", "#frm-20").val(grano);
							$("#elote", "#frm-20").val(elote);
							$("#forraje_rastrojo", "#frm-20").val(forraje_rastrojo);
							$("#forraje_verde", "#frm-20").val(forraje_verde);
							$("#hojas", "#frm-20").val(hoja);
							$("#indemnizacion_seguro", "#frm-20").val(registro.indemnizacion_seguro);
							$("#procampo", "#frm-20").val(registro.procampo);
							$("#incentivo", "#frm-20").val(registro.incentivo);*/
							//var cuantos = registro.concepto.length;
							if(registro.concepto!=undefined){
									var ingresos_esperados=0;
									var ingresos_por_tonelada_total=0;
									
									var dataIE = new  google.visualization.DataTable();
									// in this case the first column is of type 'string'.
									dataIE.addColumn('string', "Concepto");
									dataIE.addColumn('number', "Ingreso total");
									
									$.each(registro.concepto, function(i) {
										if(i!=null){
											concepto = registro.concepto[i].uso;

											toneladas_por_hectarea = registro.concepto[i].toneladas_por_hectarea;
											ingreso_por_tonelada = registro.concepto[i].ingreso_por_tonelada;
											ingreso_total = registro.concepto[i].ingreso_total;
											ingresos_esperados = ingresos_esperados+parseFloat(ingreso_total);
											ingresos_por_tonelada_total = ingresos_por_tonelada_total+parseFloat(ingreso_por_tonelada);
											
											//data.addRow([concepto,parseFloat(ingreso_total)]);
											if(concepto=='proagroproductivo')
												dataIE.addRow(['Produccion para el bienestar',parseFloat(ingreso_total)]);
											else
												dataIE.addRow([''+concepto,parseFloat(ingreso_total)]);
											
											//alert(concepto+": "+ toneladas_por_hectarea+" "+ingreso_por_tonelada+" "+ingreso_total);		
											$('#'+concepto,"#frm-20").val(ingreso_total);
											$('#'+concepto+'-tonelada',"#frm-20").val(toneladas_por_hectarea);
											$('#'+concepto+'-ingreso_tonelada',"#frm-20").val(ingreso_por_tonelada);
										}
					
									});
									//alert(ingresos_esperados);
									$('#frm-20 #total_ingresos_esperados').val("$ "+ingresos_esperados.toFixed(2));
									//$('#frm-20 #ingresos_esperados_hectarea').val("$ "+(ingresos_esperados/$("#superficie").val()).toFixed(2));
									$('#frm-20 #total_costo_indirecto').val("$ "+total_indirectos_cultivos.toFixed(2));
									$('#frm-20 #utilidad').val("$ "+(ingresos_esperados-total_indirectos_cultivos).toFixed(2));
									console.log(total_indirectos_cultivos);
									$("#total_ingresos_utilidad").val("$ "+ingresos_esperados.toFixed(2));
									$("#total_egresos_utilidad").val("$ "+total_indirectos_cultivos.toFixed(2));
									
									$("#total_ingresos_utilidad_por_hectarea").val("$ "+parseFloat(ingresos_esperados).toFixed(2));
									$("#total_egresos_utilidad_por_hectarea").val("$ "+parseFloat(total_indirectos_cultivos).toFixed(2));
									$('#frm-20 #utilidad_por_hectarea').val("$ "+parseFloat(ingresos_esperados-total_indirectos_cultivos).toFixed(2));
									
									
									$("#beneficio_costo").val(""+(ingresos_esperados/total_indirectos_cultivos).toFixed(2));
									//colores a los numeros
									if((ingresos_esperados-total_indirectos_cultivos)>0){
										$("#utilidad").css("color","#090");
										$('#frm-20 #utilidad_por_hectarea').css("color","#090");
									}else{
										$("#utilidad").css("color","#F00");
										$('#frm-20 #utilidad_por_hectarea').css("color","#F00");
									}
									
									if((ingresos_esperados/total_indirectos_cultivos)>0){
										$("#beneficio_costo").css("color","#090");
									}else{
										$("#beneficio_costo").css("color","#F00");
									}
									
									//dibujamos el grafico
									//dataTable = dataTable.replace(/'/g, '"');
									
		
			
									var options = {
									  title: 'Reporte de ingresos',
									  is3D: true,
									};
									
									var chartIE = new google.visualization.PieChart(document.getElementById('piechartIE_3d'));
									chartIE.draw(dataIE, options);
							}//if registro != undefined
							
						}
					}
					catch(err)
					{
						ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente: " + err.message, "Ok", "", false, "");
					}
				});
		}
		
		
		
		
		
		
	});
}