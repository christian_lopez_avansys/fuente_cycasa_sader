function cargarPracticaResiembra(){

   	$('#btn-save-rdlt', "#frm-10").once('click', function(){
			if(validaFrm10()){
			var preId = $("#gen-id").val();
			var frm = new FormData(document.getElementById("frm-10"));
			
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());	
			$.ajax({
	            url: "/backend/diagnosticos/guardarpracticaderesiembra/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(data){
	        	try
	        	{
					data = jQuery.parseJSON(data);
	        		if(data.ok !== undefined)
	        			ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
	        		else
	        			ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");				
				}
				catch(err) {
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");				
				}
	        });
			}
		});


		$("#btn-congelar-prs", "#frm-10").once('click', function(){
			if(validaFrm10())
				ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm10);
	});
	
		$('#btn-clean-rdlt', "#frm-10").once('click', function(){
			ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
				$('input','#frm-10').val('');
				$('#frm-10 select').each(function() {
					//$(this).css("background-color","#F00");
					 $(this).find('option:selected').removeAttr("selected");
					 $(this).val(2).change();
				});
			});
			//limpiarFrm10();
		});

		$.ajax({
            url: "/backend/diagnosticos/obtenerdatospracticaderesiembra/id/" + $("#gen-id").val() + "/tipo/" + $("#gen-type").val(),
            type: "POST",
            data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
        }).done(function(data){
        	try
        	{
        		limpiarFrm10();
	        	var registro = jQuery.parseJSON(data);
				if(registro.error !== undefined) 
	        		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");	
	        	else if(registro.noexiste === undefined)
	        	{
	        		$("#tipo_ap_select_praresie_1", "#frm-10").val(registro.aplicacion_tipo);
					if(registro.aplicacion_tipo==1){
						$("#manual_div_praresie_1", "#frm-10").addClass("hide");
						$("#mecanizada_div_praresie_1", "#frm-10").removeClass("hide");
						if(registro.mecanizada_tipo==1){
							$("#mecanizada_div_select_div_propia_praresie_1", "#frm-10").removeClass("hide");
							$("#mecanizada_div_select_div_rentada_praresie_1", "#frm-10").addClass("hide");
						}else{
							$("#mecanizada_div_select_div_propia_praresie_1", "#frm-10").addClass("hide");
							$("#mecanizada_div_select_div_rentada_praresie_1", "#frm-10").removeClass("hide");
						}
					}else{
						$("#manual_div_praresie_1", "#frm-10").removeClass("hide");
						$("#mecanizada_div_praresie_1", "#frm-10").addClass("hide");
						
					}
	        		//$("#tipo_ap_select_praresie_1", "#frm-10").change();
					$("#manual_div_no_jornales_praresie_1", "#frm-10").val(registro.manual_no_jornales);
					$("#manual_div_costo_jornal_praresie_1", "#frm-10").val(registro.manual_costo_jornal);
					$("#mecanizada_div_select_praresie_1", "#frm-10").val(registro.mecanizada_tipo);
					//$("#mecanizada_div_select_praresie_1", "#frm-10").change();
					$("#mecanizada_div_select_div_propia_combustible_praresie_1", "#frm-10").val(registro.mecanizada_propia_costo_combustible);
					$("#mecanizada_div_select_div_propia_operador_praresie_1", "#frm-10").val(registro.mecanizada_propia_costo_operador);
					$("#mecanizada_div_select_div_rentada_costo_praresie_1", "#frm-10").val(registro.mecanizada_rentada_monto);
	        	}
				if(registro.congelado == 2)
					$('#frm-10 *').attr("disabled", "disabled").unbind();
			}
			catch(err)
			{
				ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
			}
        });
	
}


function congelarFrm10(){
			var preId = $("#gen-id").val();
			var frm = new FormData(document.getElementById("frm-10"));
			
			frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());	
			$.ajax({
	            url: "/backend/diagnosticos/guardarpracticaderesiembra/",
	            type: "POST",
	            data: frm,
	            processData: false,  
	            contentType: false
	        }).done(function(data){
	        	try
	        	{
					data = jQuery.parseJSON(data);
	        		if(data.ok !== undefined){
							$.ajax({
								url: "/backend/diagnosticos/congelarpracticaresiembra/",
								type: "POST",
								data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
								async: false
								}).done(function(respuesta){
									respuesta = jQuery.parseJSON(respuesta);
									
									try {
									if(respuesta.ok !== undefined){
										ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", false, "");
										$('#frm-10 *').attr("disabled", "disabled").unbind();
									}
									else
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
									}
									catch(err) {
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
									}
								});
	        			//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
					}else
	        			ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");				
				}
				catch(err) {
					ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro, inténtelo nuevamente", "Ok", "", false, "");				
				}
	        });
}

function validaFrm10(){
	
	return true;
	if($("#tipo_ap_select_praresie_1").val() == 1 ){  //Meanizada
			
			if($("#mecanizada_div_select_praresie_1").val() == 1 ){//propia
				
				if($("#mecanizada_div_select_div_propia_combustible_praresie_1").val() == ""){
					ShowDialogBox("Validación", "El costo del combustible es obligatorio", "Ok", "", false, "");
					$("#mecanizada_div_select_div_propia_combustible_praresie_1").focus();
				
					return false;
				}
			
				if($("#mecanizada_div_select_div_propia_operador_praresie_1").val() == ""){
					ShowDialogBox("Validación", "El costo del operador es obligatorio", "Ok", "", false, "");
					$("#mecanizada_div_select_div_propia_operador_praresie_1").focus();
				
					return false;
				}
			}
			
				
			if($("#mecanizada_div_select_praresie_1").val() == 2 ){//rentada
				
				if($("#mecanizada_div_select_div_rentada_costo_praresie_1").val() == ""){
					ShowDialogBox("Validación", "El monto de la renta es obligatorio", "Ok", "", false, "");
					$("#mecanizada_div_select_div_rentada_costo_praresie_1").focus();
				
					return false;
				}
			}
		}      
		
		if($("#tipo_ap_select_praresie_1").val() == 2){  //manual
			
			if($("#manual_div_no_jornales_praresie_1").val() == ""){
				ShowDialogBox("Validación", "El numero de jornales es obligatorio", "Ok", "", false, "");
				$("#manual_div_no_jornales_praresie_1").focus();
				
				return false;
			}
			
			if($("#manual_div_costo_jornal_praresie_1").val() == ""){
				ShowDialogBox("Validación", "El Costo por jornal es obligatorio", "Ok", "", false, "");
				$("#manual_div_costo_jornal_praresie_1").focus();
				
				return false;
			}		
		}
		return true;
}

function limpiarFrm10(){
	$('#frm-10 *').removeAttr("disabled");
	$("#tipo_ap_select_prasie_1", "#frm-10").val('2');
	$("#manual_div_no_jornales_praresie_1", "#frm-10").val('');
	$("#manual_div_costo_jornal_praresie_1", "#frm-10").val('');
	$("#mecanizada_div_select_praresie_1", "#frm-10").val('1');
	$("#mecanizada_div_select_div_propia_combustible_praresie_1", "#frm-10").val('');
	$("#mecanizada_div_select_div_propia_operador_praresie_1", "#frm-10").val('');
	$("#mecanizada_div_select_div_rentada_costo_praresie_1", "#frm-10").val('');
	$("#tipo_ap_select_praresie_1", "#frm-10").change();
	$("#mecanizada_div_select_praresie_1", "#frm-10").change();
}