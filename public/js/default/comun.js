








function mostrarMensaje($idDialogo, $titulo, $mensaje, $ancho, $callBack)
{
    $($idDialogo).empty().append($mensaje);

    $($idDialogo).dialog({
        close: $callBack,
        modal: true,
        title: $titulo,
        width: $ancho,
		draggable:false,
        buttons: [
            {
                text: 'Aceptar',
                click: function()
                {
                    $(this).dialog('close');
                }
            }
        ]
    }).height('auto');
}