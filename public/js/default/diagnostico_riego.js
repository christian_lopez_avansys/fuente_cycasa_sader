$(document).ready(function(){
	$("#acondicionamiento_precio, #acondicionamiento_cantidad", "#frm-6").once('keyup', function(){
		calculaImporteFrm6('acondicionamiento');
	});
	$("#apertura_desague_precio, #apertura_desague_cantidad", "#frm-6").once('keyup', function(){
		calculaImporteFrm6('apertura_desague');
	});
	$("#apertura_zanjas_precio, #apertura_zanjas_cantidad", "#frm-6").once('keyup', function(){
		calculaImporteFrm6('apertura_zanjas');
	});
	$("#cuota_precio, #cuota_cantidad", "#frm-6").once('keyup', function(){
		calculaImporteFrm6('cuota');
	});
	$("#nacencia_precio, #nacencia_cantidad", "#frm-6").once('keyup', function(){
		calculaImporteFrm6('nacencia');
	});
	$("#auxilio_precio, #auxilio_cantidad", "#frm-6").once('keyup', function(){
		calculaImporteFrm6('auxilio');
	});
});

function cargarRiego(){
	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1){
		$('#tb_riego tr').each(function(index, element) {
				if(index=='0'){
					$(this).find('th:nth-child(8)').remove();
				}else{
					$(this).find('td:nth-child(8)').remove();
				}
			
        });
	}

	var conceptos = ["acondicionamiento", "apertura_desague", "apertura_zanjas", "cuota", "nacencia", "auxilio"];
	if($("#gen-type").val() == 1) for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_innovacion", "#frm-6").attr("disabled","disabled"); }
	else for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_innovacion", "#frm-6").removeAttr('disabled'); }
	
	
	
	$('#btn-save-riego', "#frm-6").once('click', function(){
		if(validaFrm6()){
		
		for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-6").removeAttr('disabled'); }
		var preId = $("#gen-id").val();
		var frm = new FormData(document.getElementById("frm-6"));	
		
		frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
		$.ajax({
            url: "/backend/diagnosticos/guardarriego/",
            type: "POST",
            data: frm,
            processData: false,  
            contentType: false
        }).done(function(respuesta){
        	respuesta = jQuery.parseJSON(respuesta);
			
			try {
        	for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-6").attr("disabled","disabled"); }
        	if(respuesta.ok !== undefined)
        	{
        		ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data+"/riego/1";
        	}
        	else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
			}
			catch(err) {
			ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
			}
			
			
			
        });
		}
	});
	
	
	
	$("#btn-congelar-r", "#frm-6").once('click', function(){
		if(validaFrm6()){
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm6);
		}
	});
	

	$('#btn-clean-riego', "#frm-6").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			for (var i = 0; i < conceptos.length; i++) {
				$("#" + conceptos[i] + "_Si", "#frm-6").prop('checked', false);
				$("#" + conceptos[i] + "_No", "#frm-6").prop('checked', false);
				$("#" + conceptos[i] + "_id_unidad",  "#frm-6").val('0');
				$("#" + conceptos[i] + "_cantidad",  "#frm-6").val('');
				$("#" + conceptos[i] + "_precio",  "#frm-6").val('');
				$("#" + conceptos[i] + "_importe",  "#frm-6").val('');
				$("#" + conceptos[i] + "_fecha",  "#frm-6").val('');
				$("#" + conceptos[i] + "_innovacion",  "#frm-6").val('');
			};
		});
	});

	function selectNo(concepto)
	{
		//alert(concepto);
		$("#" + concepto + "_id_unidad",  "#frm-6").val('0');
		$("#" + concepto + "_cantidad",  "#frm-6").val('');
		$("#" + concepto + "_precio",  "#frm-6").val('');
		$("#" + concepto + "_importe",  "#frm-6").val('');
		$("#" + concepto + "_fecha",  "#frm-6").val('');
		$("#" + concepto + "_innovacion",  "#frm-6").val('');
	}

	$('#acondicionamiento_No', "#frm-6").once('click', function(){ selectNo("acondicionamiento"); });
	$('#apertura_desague_No', "#frm-6").once('click', function(){ selectNo("apertura_desague"); });
	$('#apertura_zanjas_No', "#frm-6").once('click', function(){ selectNo("apertura_zanjas"); });
	$('#cuota_No', "#frm-6").once('click', function(){ selectNo("cuota"); });
	$('#nacencia_No', "#frm-6").once('click', function(){ selectNo("nacencia"); });
	$('#auxilio_No', "#frm-6").once('click', function(){ selectNo("auxilio"); });

	$.ajax({
        url: "/backend/diagnosticos/obtenerunidadesporformularioconceptoriego",
        type: "POST",
        async: false,
        processData: false,  
        contentType: false
    }).done(function(data){
        $("#acondicionamiento_id_unidad",  "#frm-6").html(data);
        $("#apertura_desague_id_unidad",  "#frm-6").html(data);
        $("#apertura_zanjas_id_unidad",  "#frm-6").html(data);
        $("#cuota_id_unidad",  "#frm-6").html(data);
        $("#nacencia_id_unidad",  "#frm-6").html(data);
        $("#auxilio_id_unidad",  "#frm-6").html(data);
    });

    setTimeout(function()
    { 
		$.ajax({
	        url: "/backend/diagnosticos/obtenerdatosriego/",
	        type: "POST",
	        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
	    }).done(function(data){
	    	var registro = jQuery.parseJSON(data);
			
			
			try {
	    	if(registro.error !== undefined) 
	    		ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
	    	else if(registro.nodata === undefined)
	    	{
				for (var i = 0; i < conceptos.length; i++) {
					//alert(registro[conceptos[i] + "_cantidad"]);
	        		if(registro[conceptos[i] + "_realizado"] == 1)
	        		{
	        			$("#" + conceptos[i] + "_Si", "#frm-6").prop('checked', true);
	        			$("#" + conceptos[i] + "_Si", "#frm-6").trigger( "click" );
	        		}
	        		else
	        		{
						$("#" + conceptos[i] + "_No", "#frm-6").prop('checked', true);
	        			$("#" + conceptos[i] + "_No", "#frm-6").trigger( "click" );
					}
					if(registro[conceptos[i] + "_id_unidad"] == null) $("#" + conceptos[i] + "_id_unidad",  "#frm-6").val('0');
					else $("#" + conceptos[i] + "_id_unidad", "#frm-6").val(registro[conceptos[i] + "_id_unidad"]);
					$("#" + conceptos[i] + "_cantidad", "#frm-6").val(registro[conceptos[i] + "_cantidad"]);
					$("#" + conceptos[i] + "_precio", "#frm-6").val(registro[conceptos[i] + "_precio"]);
					$("#" + conceptos[i] + "_importe", "#frm-6").val(registro[conceptos[i] + "_importe"]);
					$("#" + conceptos[i] + "_fecha", "#frm-6").val(registro[conceptos[i] + "_fecha"]);
					//$("#" + conceptos[i] + "_innovacion", "#frm-6").val(registro[conceptos[i] + "_innovacion"]);
				}
	    		}
				
				if(registro.congelado == 2)
				{
					$('#frm-6 *').attr("disabled", "disabled").unbind();
					dg_congelado = true;
				}
				
				
			}
			
			
				catch(err) {
				ShowDialogBox("Ha ocurrido un error", "No se han podido obtener los datos, inténtelo nuevamente", "Ok", "", false, "");
				}
			
	    });
	}, 2000);

}

function congelarFrm6(){
	
		var conceptos = ["acondicionamiento", "apertura_desague", "apertura_zanjas", "cuota", "nacencia", "auxilio"];
		for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-6").removeAttr('disabled'); }
		var preId = $("#gen-id").val();
		var frm = new FormData(document.getElementById("frm-6"));	
		
		frm.append("id", $("#gen-id").val()); frm.append("tipo", $("#gen-type").val());
		$.ajax({
            url: "/backend/diagnosticos/guardarriego/",
            type: "POST",
            data: frm,
            processData: false,  
            contentType: false
        }).done(function(respuesta){
        	respuesta = jQuery.parseJSON(respuesta);
			
			try {
        	for (var i = 0; i < conceptos.length; i++) { $("#" + conceptos[i] + "_fecha", "#frm-6").attr("disabled","disabled"); }
        	if(respuesta.ok !== undefined)
        	{
					$.ajax({
					url: "/backend/diagnosticos/congelarriego/",
					type: "POST",
					data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
					async: false
					}).done(function(respuesta){
						respuesta = jQuery.parseJSON(respuesta);
						
						try {
						if(respuesta.ok !== undefined){
							ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true,"/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/"+$("#gen-id").val()+"/riego/1");
							$('#frm-6 *').attr("disabled", "disabled").unbind();	
						}
						else
							ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
						}
						catch(err) {
							ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
						}
							
					});

        		//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
        		if(preId == 0) window.location = "/backend/diagnosticos/agregar/tipo/" + $("#gen-type").val() + "/id/" + data;
        	}
        	else ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
			}
			catch(err) {
			ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente los registros, inténtelo nuevamente", "Ok", "", false, "");
			}

        });

}

function validaFrm6(){
	return true;
	if($("#acondicionamiento_Si").is(':checked')) {  
		if($("#acondicionamiento_id_unidad").val() == 0){
			ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
			$("#acondicionamiento_id_unidad").focus();
				
			return false;
		}
			
			if($("#acondicionamiento_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#acondicionamiento_cantidad").focus();
				
				return false;
			}
			
			if($("#acondicionamiento_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#acondicionamiento_precio").focus();
				
				return false;
			}
			
			if($("#acondicionamiento_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatoria  es obligatorio", "Ok", "", false, "");
				$("#acondicionamiento_importe").focus();
				
				return false;
			}
			
			if($("#acondicionamiento_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#acondicionamiento_fecha').attr('readonly', 'readonly');
			}
        } 
		
		if($("#apertura_desague_Si").is(':checked')) {  
		if($("#apertura_desague_id_unidad").val() == 0){
			ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
			$("#apertura_desague_id_unidad").focus();
				
			return false;
		}
			
			if($("#apertura_desague_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#apertura_desague_cantidad").focus();
				
				return false;
			}
			
			if($("#apertura_desague_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#apertura_desague_precio").focus();
				
				return false;
			}
			
			if($("#apertura_desague_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatorio", "Ok", "", false, "");
				$("#apertura_desague_importe").focus();
				
				return false;
			}
			
			if($("#apertura_desague_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#apertura_desague_fecha').attr('readonly', 'readonly');
			}
        } 
		
		
		if($("#apertura_zanjas_Si").is(':checked')) {  
		if($("#apertura_zanjas_id_unidad").val() == 0){
			ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
			$("#apertura_zanjas_id_unidad").focus();
				
			return false;
		}
			
			if($("#apertura_zanjas_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#apertura_zanjas_cantidad").focus();
				
				return false;
			}
			
			if($("#apertura_zanjas_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#apertura_zanjas_precio").focus();
				
				return false;
			}
			
			if($("#apertura_zanjas_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatorio", "Ok", "", false, "");
				$("#apertura_zanjas_importe").focus();
				
				return false;
			}
			
			if($("#apertura_zanjas_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#apertura_zanjas_fecha').attr('readonly', 'readonly');
			}
        } 
		
		if($("#cuota_Si").is(':checked')) {  
		if($("#cuota_id_unidad").val() == 0){
			ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
			$("#cuota_id_unidad").focus();
				
			return false;
		}
			
			if($("#cuota_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#cuota_cantidad").focus();
				
				return false;
			}
			
			if($("#cuota_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#cuota_precio").focus();
				
				return false;
			}
			
			if($("#cuota_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatorio", "Ok", "", false, "");
				$("#cuota_importe").focus();
				
				return false;
			}
			
			if($("#cuota_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#cuota_fecha').attr('readonly', 'readonly');
			}
        } 
		
		
		if($("#nacencia_Si").is(':checked')) {  
		if($("#nacencia_id_unidad").val() == 0){
			ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
			$("#nacencia_id_unidad").focus();
				
			return false;
		}
			
			if($("#nacencia_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#nacencia_cantidad").focus();
				
				return false;
			}
			
			if($("#nacencia_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#nacencia_precio").focus();
				
				return false;
			}
			
			if($("#nacencia_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatorio", "Ok", "", false, "");
				$("#nacencia_importe").focus();
				
				return false;
			}
			
			if($("#nacencia_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#nacencia_fecha').attr('readonly', 'readonly');
			}
        } 
		
	if($("#auxilio_Si").is(':checked')) {  
		if($("#auxilio_id_unidad").val() == 0){
			ShowDialogBox("Validación", "La Unidad es obligatoria", "Ok", "", false, "");
			$("#auxilio_id_unidad").focus();
				
			return false;
		}
			
			if($("#auxilio_cantidad").val() == ""){
				ShowDialogBox("Validación", "La Cantidad es obligatoria", "Ok", "", false, "");
				$("#auxilio_cantidad").focus();
				
				return false;
			}
			
			if($("#auxilio_precio").val() == ""){
				ShowDialogBox("Validación", "El Costo unitario es obligatorio", "Ok", "", false, "");
				$("#auxilio_precio").focus();
				
				return false;
			}
			
			if($("#auxilio_importe").val() == ""){
				ShowDialogBox("Validación", "El Costo total es obligatorio", "Ok", "", false, "");
				$("#auxilio_importe").focus();
				
				return false;
			}
			
			if($("#auxilio_fecha").val() == ""){
				ShowDialogBox("Validación", "La Fecha de realización es obligatoria", "Ok", "", false, "");				
				return false;
				$('#auxilio_fecha').attr('readonly', 'readonly');
			}
        } 
		
		
		return true;
}

function calculaImporteFrm6(concepto){
	if(parseFloat($("#" + concepto + "_cantidad", "#frm-6").val()) >= 0 && parseFloat($("#" + concepto + "_precio", "#frm-6").val()) >= 0)
		$("#" + concepto + "_importe", "#frm-6").val(parseFloat($("#" + concepto + "_cantidad", "#frm-6").val()) * parseFloat($("#" + concepto + "_precio", "#frm-6").val()));
	else
		$("#" + concepto + "_importe", "#frm-6").val('');
}