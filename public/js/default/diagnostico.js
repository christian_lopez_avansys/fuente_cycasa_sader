var dg_congelado = false;
var redireccionPagina=false;
var num_cultivos = 1;
var act = false;
var arr = new Array();

var formularios = { 
	frm1: { cultivo: 1, vertice: 1 },
	frm2: null,
	frm3: null,
	frm4: null,
	frm5: null,
	frm6: null,
	frm7: { cultivos: [  ] },
	frm8: null,
	frm9: { cultivos: [  ] },
	frm10: { num: 1, tb1: { fuente: 1, quimica: 1, organica: 1, mineral: 1, acondicionador: 1 } },
	frm11: { pre: 1, post: 1, des: 1 },
	frm12: { granulado: 1, semilla: 1, recarga: 1 },
	frm13: { num: 1, tb1: { marca: 1 } },
	frm14: null,
	frm15: { granulado: 1, semilla: 1, recarga: 1 },
	frm16: null,
	frm17: null,
	frm18: null
}
function activarFormulario(){
	if(window.location.toString().indexOf("renta_tierra/1")!=-1){
		$('#btn-rdlt-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("acondicionamiento/1")!=-1){
		$('#btn-adp-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("mejoramiento/1")!=-1){
		$('#btn-mdfds-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("preparacion_suelo/1")!=-1){
		$('#btn-pds-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("riego/1")!=-1){
		$('#btn-r-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("siembra/1")!=-1){
		$('#btn-s-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("practica_s/1")!=-1){
		$('#btn-pdsi-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("resie/1")!=-1){
		$('#btn-rs-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("practica_r/1")!=-1){
		$('#btn-pdrsi-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("fertilizacion/1")!=-1){
		$('#btn-f-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("control_maleza/1")!=-1){
		$('#btn-cdm-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("plagas_suelo/1")!=-1){
		$('#btn-cdpds-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("plagas_foliares/1")!=-1){
		$('#btn-cdpf-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("control_enfermedades/1")!=-1){
		$('#btn-cde-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("tratamiento_estres/1")!=-1){
		$('#btn-tdebya-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("costos_indirectos/1")!=-1){
		$('#btn-ci-mn').trigger("click");
		redireccionPagina=true;
	}else if(window.location.toString().indexOf("ingresos_esperados/1")!=-1){
		$('#btn-ie-mn').trigger("click");
		redireccionPagina=true;
	}
}


$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function()
{
	$.fn.once = function(a, b) {
	    return this.each(function() {
	        $(this).off(a).on(a,b);
	    });
	};

	$("#btn-ch-t3").click(function(){
		$.ajax({
	        url: "/backend/diagnosticos/verificartipocompleto/",
	        type: "POST",
	        async: false,
	        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
	    }).done(function(respuesta){
	    	respuesta = jQuery.parseJSON(respuesta);
	    	if(respuesta.ok !== undefined)
	        	window.location = "/backend/diagnosticos/agregar/tipo/3/id/" + $("#gen-id").val();
	        else
	        	ShowDialogBox("Programado no completado", "No es posible cambiar de pestaña hasta que se congelen todos los formularios de tipo Programado", "Ok", "", false, "");
	    });
	});

	$("#btn-ch-t2").click(function(){
		$.ajax({
	        url: "/backend/diagnosticos/verificartipocompleto/",
	        type: "POST",
	        async: false,
	        data: { id: $("#gen-id").val(), tipo: $("#gen-type").val() }
	    }).done(function(respuesta){
	    	respuesta = jQuery.parseJSON(respuesta);
	    	if(respuesta.ok !== undefined)
	        	window.location = "/backend/diagnosticos/agregar/tipo/2/id/" + $("#gen-id").val();
	        else
	        	ShowDialogBox("Diagnostico no completado", "No es posible cambiar de pestaña hasta que se congelen todos los formularios de tipo Diagnóstico", "Ok", "", false, "");
	    });
	});

	$("#btn-ch-t1").click(function(){
		window.location = "/backend/diagnosticos/agregar/tipo/1/id/" + $("#gen-id").val();
	});

	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        /*alert(label);
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log )
            {
            	$("#span_documento", "#frm-4").html('&nbsp;' + log + ' <input type="file" id="analisis_doc" name="analisis_doc" accept=".doc, .docx, .pdf">');
            }
        }*/
        $("#span_documento", "#frm-4").html('&nbsp;' + label);
		doc=null;
        //$("#div_up1", "#frm-4").removeClass('col-xs-7').addClass('col-xs-3');
        //$("#div_up2, #div_up3", "#frm-4").show();
    });

    $("#btn-dg-mn").trigger("click");
	
	//if($('#utipo').val()==1){
		$("#flexigrid").flexigrid({
			url: "/backend/diagnosticos/grid"
			,dataType: "xml"
			,colModel: [
				{display: "Editar", name: "editar", width : 40, sortable: false, align: "center"},
				{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"},
				{display: "Estado", name: "productor.estado_id", width: 100, sortable: true, align: "center"},
				{display: "Municipio", name: "productor.municipio_id", width: 100, sortable: true, align: "center"},
				{display: "Localidad", name: "productor.localidad_id", width: 130, sortable: true, align: "center"},
				{display: "Región", name: "region", width: 130, sortable: true, align: "center"},
				{display: "Territorio funcional", name: "territorio_funcional", width: 130, sortable: true, align: "center"},
				{display: "Ténico agroecológico", name: "agroconsultor", width: 130, sortable: true, align: "center"},
				{display: "Productor", name: "datos_personales", width: 120, sortable: true, align: "center"},
				{display: "Tipo predio", name: "tipo_predio", width: 120, sortable: true, align: "center"},
				{display: "Clave predio", name: "clave", width: 120, sortable: true, align: "center"},
				{display: "Folio predio", name: "folio_predio", width: 100, sortable: true, align: "center"},
				{display: "Nombre predio", name: "nombre", width: 110, sortable: true, align: "center"},
				//{display: "Apellido m. productor", name: "productor.apellido_materno", width: 120, sortnameble: true, align: "center"},
				//{display: "CURP", name: "productor.curp", width: 130, sortable: true, align: "center"},
				//{display: "Género", name: "productor.genero", width: 100, sortable: true, align: "center"},
				//{display: "Ejido", name: "productor.ejido", width: 140, sortable: true, align: "center"},
			]
			,sortname: "id"
			,sortorder: "asc"
			,usepager: true
			,useRp: false
			,singleSelect: true
			,resizable: false
			,showToggleBtn: false
			,rp: 10
			,width: '100%'
			,height: 350
			,onSuccess: function(){
	
			}
			,onError: function(respuesta){
	
				_mensaje("#_mensaje-1", "Ocurri&oacute; un error al tratar de seleccionar los contactos, int&eacute;ntelo de nuevo");
			}
    	});
		
	/*}else{
		$("#flexigrid").flexigrid({
			url: "/backend/diagnosticos/grid"
			,dataType: "xml"
			,colModel: [
				{display: "Editar", name: "editar", width : 40, sortable: false, align: "center"},
				{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"},
				{display: "Dependencia", name: "integradora", width: 120, sortable: true, align: "center"},
				{display: "Estado", name: "estado", width: 80, sortable: true, align: "center"},
				{display: "Integradora", name: "integradora", width: 120, sortable: true, align: "center"},
				{display: "Organización", name: "orgnizadora", width: 130, sortable: true, align: "center"},
				{display: "Productor", name: "productor", width: 120, sortable: true, align: "center"},
				{display: "Municipio", name: "municipio", width: 80, sortable: true, align: "center"},
				{display: "Localidad", name: "localidad", width: 130, sortable: true, align: "center"},
				{display: "Potrero", name: "potrero", width: 100, sortable: true, align: "center"},
				{display: "Año de apoyo", name: "ano", width: 80, sortable: true, align: "center"}
			]
			,sortname: "id"
			,sortorder: "asc"
			,usepager: true
			,useRp: false
			,singleSelect: true
			,resizable: false
			,showToggleBtn: false
			,rp: 10
			,width: '100%'
			,height: 350
			,onSuccess: function(){
	
			}
			,onError: function(respuesta){
	
				_mensaje("#_mensaje-1", "Ocurri&oacute; un error al tratar de seleccionar los contactos, int&eacute;ntelo de nuevo");
			}
		});
	}*/

	
	$.fn.datepicker.defaults.language="es";
	$('.date').datepicker({ format: 'yyyy-mm-dd' });
	
	$(document).on("click","#eliminarFila",function(){
		var parent = $(this).parents().get(2);
		$(parent).remove();
	});

	//HABILITAR RENGLON SI CLICKEAN EN EL RADIO BUTTON
	$(document).on("click", "input[type='radio']", function(){
		var parent = $(this).parents().get(2);
		var label = $(this).parents().get(0);
		
		if(parent.getAttribute("id")==null){//verificamos si existe el id tempporal
			idTemp="temp_"+Math.floor((Math.random() * 1000) + 1);
			parent.setAttribute("id",""+idTemp);			
		}
		else
		{
			idTemp=parent.getAttribute("id");
		}

		if(label.outerHTML.indexOf('Si')>=0){
			var formulario = $(this).closest('form').get(0).getAttribute('id');
			if((formulario == 'frm-4' || formulario == 'frm-5' || formulario == 'frm-6') && $('#gen-type').val() == 1)
			{
				$('#'+idTemp+' :input').not('input[type="radio"], #'+idTemp+' .date :input, #'+idTemp+' .inno, #'+idTemp+' .import').removeAttr("disabled");
			}
			else
				$('#'+idTemp+' :input').not('input[type="radio"], #'+idTemp+' .date :input, #'+idTemp+' .import').removeAttr("disabled");
			$('#'+idTemp+' .input-group-addon').removeClass("hide");
		}
		else
		{
			$('#'+idTemp+' :input').not('input[type="radio"]').attr("disabled","disabled");
			$('#'+idTemp+' .input-group-addon').addClass("hide");
		}		
	});
	
	$('#tb_preparacion :input, #tb_renta :input, #tb_riego :input').not('input[type="radio"]').attr("disabled","disabled");

	$("#despues2").attr("disabled", "disabled");
	$("#despues2").hide();

	$("#hubo_resiembra").change(function()
	{
		if($("#hubo_resiembra").val() == 1)
		{
			$("#despues2").removeAttr("disabled");
			$("#despues2").show();
		}
		else
		{
			$(".row2").remove();
			$("#despues2").attr("disabled", "disabled");
			$("#despues2").hide();
		}
	});

	$("#tabla_resiembra").attr("disabled", "disabled");
	$("#tabla_resiembra").hide();

	$("#hubo_resiembra_resiembra").change(function()
	{
		if($("#hubo_resiembra_resiembra").val() == 1)
		{
			$("#tabla_resiembra").removeAttr("disabled");
			$("#tabla_resiembra").show();
		}
		else
		{
			$("#tabla_resiembra").attr("disabled", "disabled");
			$("#tabla_resiembra").hide();
		}
	});
	

	$('#tabla-costos-indirectos :input, #tabla-ingresos-esperados :input').removeAttr("disabled");
	$('#tabla-rentabilidad :input').attr("disabled", "disabled");
	
	
	
	    $("#filtroEstado").change(function(e){
        if($("#filtroEstado").val() > 0)
        {
            $.ajax({
                url: "/backend/predio/obtenermunicipiosyclave/estado/" + $("#filtroEstado").val(),
                type: "POST",
                processData: false,  
                contentType: false
            }).done(function(data){
                $("#filtroMunicipio").html(data);
            });
        }
        else
        {
            $("#filtroMunicipio").html('<option value="0">Seleccione un municipio</option>');
            $("#filtroLocalidad").html('<option value="0">Seleccione una localidad</option>');
        }
    });

    $("#filtroMunicipio").change(function(e){
		var municipio = $("#filtroMunicipio").val().split("|");
		
        if(municipio[0] > 0)
        {
            $.ajax({
                url: "/backend/predio/obtenerlocalidades/municipio/" + municipio[0],
                type: "POST",
                processData: false,  
                contentType: false
            }).done(function(data){
                $("#filtroLocalidad").html(data);
            });
        }
        else
        {
            $("#filtroLocalidad").html('<option value="0">Seleccione una localidad</option>');
        }
    });
	
	$("#filtroTerritorio").change(function(e){
		var territorio = $("#filtroTerritorio").val();
		
        $.ajax({
            url: "/backend/diagnosticos/obteneragroconsultores/id_territorio/" + territorio,
            type: "POST",
            processData: false,  
            contentType: false
        }).done(function(data){
            $("#filtroAgroconsultor").html(data);
        });
    });
	
	
    $("#filtroTipoPredio").change(function(e){
    	mostrarPredios($("#filtroTipoPredio").val());
    });
});


function filtrargrig(){

    var filtro = "/backend/diagnosticos/grid";
    var filtro_ = "/backend/diagnosticos/grid";
    var filtro__ = "";

    if($("#filtroEstado").val()!="")   filtro__+="/estado/"+$("#filtroEstado").val();
    if($("#filtroMunicipio").val()!=0)   filtro__+="/municipio/"+$("#filtroMunicipio").val();
    if($("#filtroLocalidad").val() != 0)  filtro__ += "/localidad/"+$("#filtroLocalidad").val();
    if($("#filtroRegion").val() != 0)  filtro__ += "/region/"+$("#filtroRegion").val();
    if($("#filtroTerritorio").val() != 0)  filtro__ += "/territorio/"+$("#filtroTerritorio").val();
    if($("#filtroAgroconsultor").val() != 0)  filtro__ += "/agroconsultor/"+$("#filtroAgroconsultor").val();
	//if($("#filtroProductor").val() != "")  filtro__ += "/productor/"+$("#filtroProductor").val();
    if($("#filtroTipoPredio").val() != 0)  filtro__ += "/tipo_predio/"+$("#filtroTipoPredio").val();
	if($("#filtroClave").val() != 0)  filtro__ += "/clave/"+$("#filtroClave").val();
	/*
    if($("#filtroDependencia").val() != "") filtro__ += "/dependencia/"+$("#filtroDependencia").val(); 
    if($("#filtroIntegradora").val() != "")  filtro__ += "/integradora/"+$("#filtroIntegradora").val();
	if($("#filtroOrganizacion").val() != "") filtro__ += "/organizacion/"+$("#filtroOrganizacion").val(); 
    if($("#filtroPotrero").val()!="")   filtro__+="/potrero/"+$("#filtroPotrero").val();
    if($("#filtroAno").val() != "")  filtro__ += "/ano/"+$("#filtroAno").val();
	*/
	/*if($('#utipo').val()==1){
		if($("#filtroAgroconsultor").val() != "0")  filtro__ += "/agroconsultor/"+$("#filtroAgroconsultor").val();
	}*/
    
    filtro += filtro__;
    filtro_ += filtro__;
    
    $("#flexigrid").flexOptions({
        url: filtro_
        ,onSuccess: function(){

            $("#flexigrid").flexOptions({
                url: filtro
                
            });
        }
    }).flexReload();
}



function agregarTabla(tipo, form)
{

	var count = 0;
	$('.' + tipo).each(function(){ count ++; });
	var div = "aderido_" + tipo + "_" + (count + 1);

	$("#" + form + " #tabla_div_" + tipo + " #despues_" + tipo + "_1").after( 
		$("#" + form + " #generico").clone().removeClass("hide").attr("id", div).addClass(tipo)
	);

	count += 1;

	$("#" + div + " #nombre_").attr("id", tipo + "_nombre_" + count);
	$("#" + div + " #eliminar-tabla").attr("id", tipo + "eliminar_tabla" + count).attr("onClick", "eliminarExtra('" + div + "')");
	$("#" + div + " #primera_nombre_").attr("id", tipo + "_primera_nombre_" + count);
	$("#" + div + " #tipo_unidad_").attr("id", tipo + "_tipo_unidad_" + count);
	$("#" + div + " #despues__interno_").attr("id", "despues_" + tipo + "_interno_" + count);
	$("#" + div + " #semilla_alto_cantidad").attr("id", tipo + "_semilla_alto_cantidad_" + count);
	$("#" + div + " #semilla_alto_pu").attr("id", tipo + "_semilla_alto_pu_" + count);
	$("#" + div + " #semilla_alto_importe").attr("id", tipo + "_semilla_alto_importe_" + count);
	$("#" + div + " #semilla_alto_frealizacion").attr("id", tipo + "_semilla_alto_frealizacion_" + count);
	$("#" + div + " #semilla_alto_innovacion").attr("id", tipo + "_semilla_alto_innovacion_" + count);
	$("#" + div + " #agrega_fila_").attr("id", tipo + "_agrega_fila_" + count).attr("onClick", "agregarFilaExtra('" + tipo + "','frm-8', " + count + ")");
	$("#" + div + " #mezclado_cantidad").attr("id", tipo + "_mezclado_cantidad_" + count);
	$("#" + div + " #mezclado_pu").attr("id", tipo + "_mezclado_pu_" + count);
	$("#" + div + " #mezclado_importe").attr("id", tipo + "_mezclado_importe_" + count);
	$("#" + div + " #mezclado_frealizacion").attr("id", tipo + "_mezclado_frealizacion_" + count);
	$("#" + div + " #mezclado_innovacion").attr("id", tipo + "_mezclado_innovacion_" + count);
	$("#" + div + " #tipo_ap_select").attr("id", tipo + "_tipo_ap_select_" + count).attr("onChange", "evaluaCambio('" + tipo + "_tipo_ap_select_" + count + "', '" + tipo + "', " + count + ")");
	$("#" + div + " #manual_div_").attr("id", "manual_div_" + tipo + "_" + count);
	$("#" + div + " #manual_div_no_jornales_").attr("id", "manual_div_no_jornales_" + tipo + "_" + count);
	$("#" + div + " #manual_div_costo_jornal_").attr("id", "manual_div_costo_jornal_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_").attr("id", "mecanizada_div_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_").attr("id", "mecanizada_div_select_" + tipo + "_" + count).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_" + tipo + "_" + count + "', '" + tipo + "', " + count + ")");
	$("#" + div + " #mecanizada_div_select_div_propia_").attr("id", "mecanizada_div_select_div_propia_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_propia_combustible_").attr("id", "mecanizada_div_select_div_propia_combustible_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_propia_operador_").attr("id", "mecanizada_div_select_div_propia_operador_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_rentada_").attr("id", "mecanizada_div_select_div_rentada_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_rentada_costo_").attr("id", "mecanizada_div_select_div_rentada_costo_" + tipo + "_" + count);

}

function agregarFilaExtra(tipo, form, num)
{
	var ini = 1;
	if(tipo == 'frm-11') ini = 0;
	var count = 0;
	$('.' + tipo + "_interno_" + num).each(function(){ count ++; });

	$("#" + form + " #tabla_div_" + tipo + " #despues_" + tipo + "_interno_" + num).after( 
		$("#" + form + " #generico-tabla tbody tr:eq(1)").clone().removeClass("hide").addClass(tipo + "_interno_" + num).attr("id", "despues_" + tipo + "_interno_aderido_" + (num+1))
	);

	$("#despues_" + tipo + "_interno_aderido_" + (num+1) + " #eliminar-fila").attr("id", tipo + "_eliminar_fila_" + (num+1)).attr("onClick", "eliminarExtra('" + "despues_" + tipo + "_interno_aderido_" + (num+1) + "')");
}


function agregarFilaExtra2(tipo, subtipo, num, subnum)
{
	
	if(subtipo == '')
	{
		var div = "despues_" + tipo + "_interno_" + (subnum + 1) + "_" + num;

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #despues_" + tipo + "_interno_" + subnum + "_" + num).after( 
			$("#frm-11 #generico-campo-fuente tbody tr:eq(0)").clone().removeClass("hide").addClass(tipo).attr("id", div)
		);

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #" + div + " #eliminar-fila").attr("id", subtipo + "_eliminar_fila_" + (subnum+1)).attr("onClick", "eliminarExtra('" + div + "')");
	}
	else
	{
		var div = "despues_" + subtipo + "_interno_" + (subnum + 1) + "_" + num;
		var ind = 0;
		if(subtipo == 'quimica') ind = 1;
		else if(subtipo == 'organica') ind = 2;
		else if(subtipo == 'mineral') ind = 3;
		else ind = 4;

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #despues_" + subtipo + "_interno_" + subnum + "_" + num).after( 
			$("#frm-11 #generico-foliar #tabla-foliar tbody tr:eq(" + ind + ")").clone().removeClass("hide").addClass(tipo + "_interno_" + (subnum+1)).attr("id", div)
		);

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #" + div + " #agregar").attr("id", subtipo + "_eliminar_fila_" + (subnum+1)).attr("onClick", "eliminarExtra('" + div + "')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger");
	}
}


// Evento que selecciona la fila y la elimina 
function eliminarExtra(id, form = null, count = null)
{
	$("#" + id).remove();
	if(form == 'frm-1') $("#sem_despues_" + count).remove();
}

function eliminarExtra2(p1, p2, p3)
{
	if(p1 != null) $("#" + p1).remove();
	if(p2 != null) $("#" + p2).remove();
	if(p3 != null) $("#" + p3).remove();
}

function eliminarExtra3(p1, p2)
{
	if(p1.indexOf("|")>-1){
		
		p1=p1.split("|");
		id= p1[1];
		p1=p1[0];
		$.ajax({
			url: "/backend/diagnosticos/eliminarhijoplagafoliar/",
			type: "POST",
			data: { id: id },
			async: false
		}).done(function(respuesta){ 
			
			
		});
	}else if(p1.indexOf("*")>-1){
		p1=p1.split("*");
		id= p1[1];
		p1=p1[0];
		$.ajax({
			url: "/backend/diagnosticos/eliminartablaplagafoliar/",
			type: "POST",
			data: { id: id },
			async: false
		}).done(function(respuesta){
			
			
		});
	}
	if(p1 != null) $("#" + p1).remove();
	if(p2 != null) $("#" + p2).remove();
}

// Evento que selecciona la fila y la elimina 
function eliminarFila(event, form = null, count = null)
{
	var parent = $(this).parents().get(2);
	$(parent).remove();
}

function agregarFila(tipo, form, id, clase = null)
{
	//si presionamos el boton de agregar fila
	if(id==''){
		if(tipo == '')
			equal = 1;
		else if(tipo == 1)
			equal = 1;
		else if(tipo == 2)
			equal=2;
		else if(tipo == 3)
			equal=2;
		else
			equal=3;
	}
	else
		equal=id;	
	

	if(form == 'frm-1')
	{
		

	}
	else if(form == 'frm-12')
	{
		var count = 0;
		$('.cdmpre-' + clase).each(function(){ count ++; });
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_coad_" + (count + 1);
		var div3 = "despues_" + clase + "_apli_" + (count + 1);

		$("#" + form + " #tabla tbody #despues_" + clase + "_apli_" + count).after(
			$("#" + form + " #tabla tbody #fila_generica_marca").clone().removeClass("hide").attr("id", div1).addClass("cdmpre-" + clase)
		);

		$("#" + form + " #tabla tbody #" + div1).after(
			$("#" + form + " #tabla tbody #fila_generica_coad").clone().removeClass("hide").attr("id", div2)
		);

		$("#" + form + " #tabla tbody #" + div2).after(
			$("#" + form + " #tabla tbody #fila_generica_apli").clone().removeClass("hide").attr("id", div3)
		);

		count += 1;

		arr[0] = div1; arr[1] = div2; arr[2] = div3;
		
		$("#" + div1 + " #eliminarFila_").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra2('" + div1 + "', '" + div2 + "', '" + div3 + "')");

		$("#" + div3 + " #tipo_ap_select_gen").attr("id", "tipo_ap_select_" + clase + "_" + count).attr("onChange", "evaluaCambio('tipo_ap_select_" + clase + "_" + count + "', '" + clase + "', " + count + ")");
		$("#" + div3 + " #manual_div_gen").attr("id", "manual_div_" + clase + "_" + count);
		$("#" + div3 + " #manual_div_no_jornales_gen").attr("id", "manual_div_no_jornales_" + clase + "_" + count);
		$("#" + div3 + " #manual_div_costo_jornal_gen").attr("id", "manual_div_costo_jornal_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_gen").attr("id", "mecanizada_div_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_gen").attr("id", "mecanizada_div_select_" + clase + "_" + count).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_" + clase + "_" + count + "', '" + clase + "', " + count + ")");
		$("#" + div3 + " #mecanizada_div_select_div_propia_gen").attr("id", "mecanizada_div_select_div_propia_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_propia_combustible_gen").attr("id", "mecanizada_div_select_div_propia_combustible_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_propia_operador_gen").attr("id", "mecanizada_div_select_div_propia_operador_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_rentada_gen").attr("id", "mecanizada_div_select_div_rentada_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_rentada_costo_gen").attr("id", "mecanizada_div_select_div_rentada_costo_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_gen").attr("id", "aerea_div_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_gen").attr("id", "aerea_div_select_" + clase + "_" + count).attr("onChange", "evaluaCambioAplicacion('aerea_div_select_" + clase + "_" + count + "', '" + clase + "', " + count + ")");
		$("#" + div3 + " #aerea_div_select_div_propia_gen").attr("id", "aerea_div_select_div_propia_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_propia_combustible_gen").attr("id", "aerea_div_select_div_propia_combustible_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_propia_operador_gen").attr("id", "aerea_div_select_div_propia_operador_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_rentada_gen").attr("id", "aerea_div_select_div_rentada_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_rentada_costo_gen").attr("id", "aerea_div_select_div_rentada_costo_" + clase + "_" + count);
	}
	else if(form == 'frm-13')
	{
		
		var count = 0;
		$('.cdpds-' + clase).each(function(){ count ++; });
		
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_apli_" + (count + 1);

		
		$("#" + form + " #tabla tbody #despues_" + clase + "_marca_" + count).after(
			$("#" + form + " #tabla tbody #fila-general-" + clase).clone().removeClass("hide").attr("id", div1).addClass("cdpds-" + clase)
		);
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(1) input').attr("id",clase+"_insecticida_cantidad_"+(count + 1));/*.attr("onkeyup","calculaImporteFrm16(this,'insecticida_cantidad_"+(count + 1)+"','insecticida_precio_"+(count + 1)+"','insecticida_importe_"+(count + 1)+"')");*/
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(2) input').attr("id",clase+"_insecticida_precio_"+(count + 1));/*.attr("onkeyup","calculaImporteFrm16(this,'insecticida_precio_"+(count + 1)+"','insecticida_cantidad_"+(count + 1)+"','insecticida_importe_"+(count + 1)+"')");*/
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(3) input').attr("id",clase+"_insecticida_importe_"+(count + 1));

		count += 1;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else if(form == 'frm-13_enfermedades')
	{
		
		var count = 0;
		$('.cdpds-' + clase+"-enf").each(function(){ count ++; });
		
		
		var div1 = "despues_" + clase + "_marca_enf_" + (count + 1);
		var div2 = "despues_" + clase + "_apli_enf_" + (count + 1);

		
		$("#" + form + " #tabla tbody #despues_enf_" + clase + "_marca_" + count).after(
			$("#" + form + " #tabla tbody #fila-general-" + clase).clone().removeClass("hide").attr("id", div1).addClass("cdpds-" + clase)
		);
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(1) input').attr("id",clase+"_insecticida_cantidad_"+(count + 1));/*.attr("onkeyup","calculaImporteFrm16(this,'insecticida_cantidad_"+(count + 1)+"','insecticida_precio_"+(count + 1)+"','insecticida_importe_"+(count + 1)+"')");*/
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(2) input').attr("id",clase+"_insecticida_precio_"+(count + 1));/*.attr("onkeyup","calculaImporteFrm16(this,'insecticida_precio_"+(count + 1)+"','insecticida_cantidad_"+(count + 1)+"','insecticida_importe_"+(count + 1)+"')");*/
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(3) input').attr("id",clase+"_insecticida_importe_"+(count + 1));

		count += 1;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else if(form == 'frm-16')
	{
		var count = 0;
		$('.tdebya-' + clase).each(function(){ count ++; });		
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_apli_" + (count + 1); 
		var div3 = "despues_" + clase + "_empresa_" + (count + 1); //liz

		$("#" + form + " #tabla tbody #despues_" + clase + "_marca_" + count).after(
			$("#" + form + " #tabla tbody #fila-general-" + clase).clone().removeClass("hide").attr("id", div1).addClass("tdebya-" + clase)
		);
		
		$("#" + form + " #tabla tbody #despues_" + clase + "_empresa_" + count).after(
			$("#" + form + " #tabla tbody #fila-general-" + clase).clone().removeClass("hide").attr("id", div3).addClass("tdebya-" + clase)
		);
		
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(1) input').attr("id",clase+"_insecticida_cantidad_"+(count + 1));/*.attr("onkeyup","calculaImporteFrm16(this,'insecticida_cantidad_"+(count + 1)+"','insecticida_precio_"+(count + 1)+"','insecticida_importe_"+(count + 1)+"')");*/
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(2) input').attr("id",clase+"_insecticida_precio_"+(count + 1));/*.attr("onkeyup","calculaImporteFrm16(this,'insecticida_precio_"+(count + 1)+"','insecticida_cantidad_"+(count + 1)+"','insecticida_importe_"+(count + 1)+"')");*/
		$("#" + form + " #tabla tbody #"+div1).find(' td:eq(3) input').attr("id",clase+"_insecticida_importe_"+(count + 1));
		
		count += 1;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else if(form == 'frm-14')
	{
		var count = 0;
		$('.cdpf-' + clase).each(function(){ count ++; });
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_coad_" + (count + 1);
		var div3 = "despues_" + clase + "_apli_" + (count + 1);

		$("#" + form + " #tabla tbody #despues_" + clase + "_coad_" + count).after(
			$("#" + form + " #tabla tbody #fila_generica_marca").clone().removeClass("hide").attr("id", div1).addClass("cdpf-" + clase)
		);

		$("#" + form + " #tabla tbody #" + div1).after(
			$("#" + form + " #tabla tbody #fila_generica_coad").clone().removeClass("hide").attr("id", div2)
		);

		count += 1;

		arr[0] = div1; arr[1] = div2;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else
	{
		if(tipo == '')
		{
			$("#"+form+" #tabla tbody #despues"+tipo+id).after( 
				$("#"+form+" #tabla tbody tr:eq("+equal+")").clone().removeAttr("id class")
			);
		}
		else
		{
			$("#"+form+" #tabla tbody #despues"+tipo+id).after( 
				$("#"+form+" #tabla tbody tr:eq("+equal+")").clone().removeAttr("id class").addClass("row"+tipo)
			);
		}
	}
	$('.date').datepicker({ format: 'dd/mm/yyyy' });
}


function agregarTabla3(tb)
{
	var count = 0;
	$('.tabfol').each(function(){ count ++; });

	$("#frm-14 #tablafol_" + count).after(
		$("#frm-14 #tabla-generica-fol").clone().removeClass("hide").attr("id", "tablafol_" + (count+1)).addClass("tabfol")
	);

	tb = count;	
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_").attr("id", "despues_fol_marca_" + (tb+1) + "_1").addClass("fol-" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_").attr("id", "despues_fol_coad_" + (tb+1) + "_1");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_" + (tb+1) + "_1 #agre").attr("id", "agre" + (tb+1) + "_1").attr("onClick", "agregarFila3(" + (tb+1) + ", 1)");
	$("#frm-14 #tablafol_" + (tb+1) + " #eliminarFila").attr("id", "eliminartab_" + (tb+1)).attr("onClick", "eliminarExtra3('tablafol_" + (tb+1) + "', null)");
	
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #marca").attr("name", "marca_marca["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #activo").attr("name", "marca_activo["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #unidad").attr("name", "marca_unidad["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #cantidad").attr("name", "marca_cantidad["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #precio").attr("name", "marca_precio["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #importe").attr("name", "marca_importe["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #frealizacion").attr("name", "marca_frealizacion["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #etapa_fenologica").attr("name", "marca_etapa_fenologica["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_"+(tb+1)+"_1 #innovacion").attr("name", "marca_innovacion["+(tb+1)+"][]");

	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #unidad").attr("name", "coadyudante_unidad["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #cantidad").attr("name", "coadyudante_cantidad["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #precio").attr("name", "coadyudante_precio["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #importe").attr("name", "coadyudante_importe["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #frealizacion").attr("name", "coadyudante_frealizacion["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #etapa_fenologica").attr("name", "coadyudante_etapa_fenologica["+(tb+1)+"][]");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_"+(tb+1)+"_1 #innovacion").attr("name", "coadyudante_innovacion["+(tb+1)+"][]");


	$("#frm-14 #tablafol_" + (tb+1) + " #tipo_ap_select_gen").attr("id", "tipo_ap_select_fol_" + (tb+1)).attr("onChange", "evaluaCambio('tipo_ap_select_fol_" + (tb+1) + "', 'fol', " + (tb+1) + ")").attr("name", "aplicacion[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #manual_div_gen").attr("id", "manual_div_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #manual_div_no_jornales_gen").attr("id", "manual_div_no_jornales_fol_" + (tb+1)).attr("name", "numero_jornales[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #manual_div_costo_jornal_gen").attr("id", "manual_div_costo_jornal_fol_" + (tb+1)).attr("name", "costo_jornal[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_gen").attr("id", "mecanizada_div_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_gen").attr("id", "mecanizada_div_select_fol_" + (tb+1)).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_fol_" + (tb+1) + "', 'fol', " + (tb+1) + ")").attr("name", "tipo_mecanizacion[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_propia_gen").attr("id", "mecanizada_div_select_div_propia_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_propia_combustible_gen").attr("id", "mecanizada_div_select_div_propia_combustible_fol_" + (tb+1)).attr("name", "costo_combustible[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_propia_operador_gen").attr("id", "mecanizada_div_select_div_propia_operador_fol_" + (tb+1)).attr("name", "costo_operador[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_rentada_gen").attr("id", "mecanizada_div_select_div_rentada_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_rentada_costo_gen").attr("id", "mecanizada_div_select_div_rentada_costo_fol_" + (tb+1)).attr("name", "monto_renta[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_gen").attr("id", "aerea_div_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_gen").attr("id", "aerea_div_select_fol_" + (tb+1)).attr("onChange", "evaluaCambioAplicacion('aerea_div_select_fol_" + (tb+1) + "', 'fol', " + (tb+1) + ")").attr("name", "tipo_mecanizacion_aerea[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_propia_gen").attr("id", "aerea_div_select_div_propia_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_propia_combustible_gen").attr("id", "aerea_div_select_div_propia_combustible_fol_" + (tb+1)).attr("name", "costo_combustible_aerea[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_propia_operador_gen").attr("id", "aerea_div_select_div_propia_operador_fol_" + (tb+1)).attr("name", "costo_operador_aerea[]");
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_rentada_gen").attr("id", "aerea_div_select_div_rentada_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_rentada_costo_gen").attr("id", "aerea_div_select_div_rentada_costo_fol_" + (tb+1)).attr("name", "monto_renta_aerea[]");

	$('.date').datepicker({ format: 'yyyy/mm/dd' });
}

function agregarFila3(tb, fil)
{
	
	var count = 0;
	$('.fol-' + tb).each(function(){ count ++; });
	count++;

	$("#frm-14 #tablafol_" + tb + " tbody #despues_fol_coad_" + tb + "_1").after(
		$("#frm-14 #tablafol_1 tbody #fila_generica_marca").clone().removeClass("hide").attr("id", "despues_fol_marca_" + tb + "_" + count).addClass("fol-" + tb)
	);

	$("#frm-14 #tablafol_" + tb + " tbody #despues_fol_marca_" + tb + "_" + count).after(
		$("#frm-14 #tablafol_1 tbody #fila_generica_coad").clone().removeClass("hide").attr("id", "despues_fol_coad_" + tb + "_" + count)
	);

	$("#frm-14 #tablafol_" + tb + " #despues_fol_marca_" + tb + "_" + count + " #eliminarFila").attr("id", "elimina_" + tb + "_" + count).attr("onClick", "eliminarExtra3('" + "despues_fol_marca_" + tb + "_" + count + "','" + "despues_fol_coad_" + tb + "_" + count + "')");
	
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #marca").attr("name", "marca_marca["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #activo").attr("name", "marca_activo["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #unidad").attr("name", "marca_unidad["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #cantidad").attr("name", "marca_cantidad["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #precio").attr("name", "marca_precio["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #importe").attr("name", "marca_importe["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #frealizacion").attr("name", "marca_frealizacion["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #etapa_fenologica").attr("name", "marca_etapa_fenologica["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_marca_"+(tb)+"_" + count + " #innovacion").attr("name", "marca_innovacion["+tb+"][]");

	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #unidad").attr("name", "coadyudante_unidad["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #cantidad").attr("name", "coadyudante_cantidad["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #precio").attr("name", "coadyudante_precio["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #importe").attr("name", "coadyudante_importe["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #frealizacion").attr("name", "coadyudante_frealizacion["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #etapa_fenologica").attr("name", "coadyudante_etapa_fenologica["+tb+"][]");
	$("#frm-14 #tablafol_" + (tb) + " #despues_fol_coad_"+(tb)+"_" + count + " #innovacion").attr("name", "coadyudante_innovacion["+tb+"][]");
	$('.date').datepicker({ format: 'yyyy/mm/dd' });


}

function matriz(fmr, opc)
{
	var matriz, suelo;
	if(fmr == 1) { matriz = "matriz"; suelo = "analisis_suelo"; }
	else { matriz = "matriz_mdfds"; suelo = "analisis_suelo_mdfds"; }

	if(opc==1)
	{
		$('#' + matriz).fadeOut(1200,function(){});
		$('#' + matriz + ' :input').attr("disabled","disabled");
		$('#' + suelo).show();
	}
	else if(opc==0)
	{
		$('#' + matriz).fadeOut(1200,function(){});
		$('#' + matriz + ' :input').attr("disabled","disabled");
		$('#' + suelo).hide();
	}
	else if(opc==3)
	{
		$('#' + matriz + ' :input').removeAttr("disabled");
		$('#' + matriz).fadeIn(1200,function(){});
	}
	else
	{
		$('#' + matriz).fadeOut(1200,function(){});
		$('#' + matriz + ' :input').attr("disabled","disabled");
	}
}

function guardar(num)
{
	if(num == 6)
	{
		if($("#hubo_siembra_precision").val() == 1)
		{
			if($("#propia_cantidad").val() == '' && $("#rentada_cantidad").val() == '' && $("#manual_cantidad").val() == '')
			{
				alert("Al menos una cantidad de hectareas es requerida");
			}
			else
			{
				var val_propia = ($("#propia_cantidad").val() != '') ? $("#propia_cantidad").val() : 0;
				var val_rentada = ($("#rentada_cantidad").val() != '') ? $("#rentada_cantidad").val() : 0;
				var val_manual = ($("#manual_cantidad").val() != '') ? $("#manual_cantidad").val() : 0;
				var sum = parseFloat(val_propia) + parseFloat(val_rentada) + parseFloat(val_manual);
				
				if(sum > parseFloat($("#num_hectareas").val()))
					alert("La suma de la cantidad de hectareas no debe superar el Número de hectareas del predio");
				else if(sum < parseFloat($("#num_hectareas").val()))
					alert("La suma de la cantidad de hectareas no debe ser menor al Número de hectareas del predio");
			}
		}
	}
	else if(num == 7)
	{
		if($("#hubo_resiembra_resiembra").val() == 1)
		{
			if($("#propia_cantidad_resiembra").val() == '' && $("#rentada_cantidad_resiembra").val() == '' && $("#manual_cantidad_resiembra").val() == '')
			{
				alert("Al menos una cantidad de hectareas es requerida");
			}
			else
			{
				var val_propia = ($("#propia_cantidad_resiembra").val() != '') ? $("#propia_cantidad_resiembra").val() : 0;
				var val_rentada = ($("#rentada_cantidad_resiembra").val() != '') ? $("#rentada_cantidad_resiembra").val() : 0;
				var val_manual = ($("#manual_cantidad_resiembra").val() != '') ? $("#manual_cantidad_resiembra").val() : 0;
				var sum = parseFloat(val_propia) + parseFloat(val_rentada) + parseFloat(val_manual);
				
				if(sum > parseFloat($("#num_hectareas").val()))
					alert("La suma de la cantidad de hectareas no debe superar el Número de hectareas del predio");
				else if(sum < parseFloat($("#num_hectareas_resiembra").val()))
					alert("La suma de la cantidad de hectareas no debe ser menor al Número de hectareas del predio");
			}
		}
	}
}

function evaluaCambio(id, tipo, num)
{
	$("#aerea_div_select_div_propia_combustible_" + tipo + "_" + num).val('').removeClass("required");
	$("#aerea_div_select_div_propia_operador_" + tipo + "_" + num).val('').removeClass("required");
	$("#aerea_div_select_div_rentada_costo_" + tipo + "_" + num).val('').removeClass("required");
	
	$("#mecanizada_div_select_div_propia_combustible_" + tipo + "_" + num).val('').removeClass("required");
	$("#mecanizada_div_select_div_propia_operador_" + tipo + "_" + num).val('').removeClass("required");
	$("#mecanizada_div_select_div_rentada_costo_" + tipo + "_" + num).val('').removeClass("required");
	
	$("#manual_div_no_jornales_" + tipo + "_" + num).val('').removeClass("required");
	$("#manual_div_costo_jornal_" + tipo + "_" + num).val('').removeClass("required");
	//alert("cambia1: " + id + " -- " + tipo + " -- " + num);
	if($("#" + id).val() == 1)
	{
		$("#mecanizada_div_" + tipo + "_" + num).removeClass("hide");
		$("#manual_div_" + tipo + "_" + num).addClass("hide");
		$("#aerea_div_" + tipo + "_" + num).addClass("hide");

		$("#manual_div_no_jornales_" + tipo + "_" + num).val('');
		$("#manual_div_costo_jornal_" + tipo + "_" + num).val('');
		
		$("#mecanizada_div_select_div_propia_" + tipo + "_" + num).removeClass("hide");
		$("#mecanizada_div_select_div_rentada_" + tipo + "_" + num).addClass("hide");
		
		$("#mecanizada_div_select_div_propia_combustible_" + tipo + "_" + num).addClass("required");
		$("#mecanizada_div_select_div_propia_operador_" + tipo + "_" + num).addClass("required");
		
		$("#aerea_div_select_" + tipo + "_" + num).val('1');
		$("#aerea_div_select_div_propia_combustible_" + tipo + "_" + num).val('');
		$("#aerea_div_select_div_propia_operador_" + tipo + "_" + num).val('');
		$("#aerea_div_select_div_rentada_costo_" + tipo + "_" + num).val('');
		
	}
	else if($("#" + id).val() == 2)
	{
		$("#manual_div_" + tipo + "_" + num).removeClass("hide");
		$("#mecanizada_div_" + tipo + "_" + num).addClass("hide");
		
		$("#manual_div_no_jornales_" + tipo + "_" + num).addClass("required");
		$("#manual_div_costo_jornal_" + tipo + "_" + num).addClass("required");
	
		$("#aerea_div_" + tipo + "_" + num).addClass("hide");

		$("#aerea_div_select_" + tipo + "_" + num).val('1');
		$("#aerea_div_select_div_propia_combustible_" + tipo + "_" + num).val('');
		$("#aerea_div_select_div_propia_operador_" + tipo + "_" + num).val('');
		$("#aerea_div_select_div_rentada_costo_" + tipo + "_" + num).val('');

		$("#mecanizada_div_select_" + tipo + "_" + num).val('1');
		$("#mecanizada_div_select_div_propia_combustible_" + tipo + "_" + num).val('');
		$("#mecanizada_div_select_div_propia_operador_" + tipo + "_" + num).val('');
		$("#mecanizada_div_select_div_rentada_costo_" + tipo + "_" + num).val('');
	}
	else if($("#" + id).val() == 3)
	{
		$("#manual_div_" + tipo + "_" + num).addClass("hide");
		$("#mecanizada_div_" + tipo + "_" + num).addClass("hide");
		$("#aerea_div_" + tipo + "_" + num).removeClass("hide");

		$("#manual_div_no_jornales_" + tipo + "_" + num).val('');
		$("#manual_div_costo_jornal_" + tipo + "_" + num).val('');
		
		$("#aerea_div_select_div_propia_" + tipo + "_" + num).removeClass("hide");
		$("#aerea_div_select_div_rentada_" + tipo + "_" + num).addClass("hide");
		
		$("#aerea_div_select_div_propia_combustible_" + tipo + "_" + num).val('').addClass("required");
		$("#aerea_div_select_div_propia_operador_" + tipo + "_" + num).val('').addClass("required");
		
		$("#mecanizada_div_select_" + tipo + "_" + num).val('1');
		$("#mecanizada_div_select_div_propia_combustible_" + tipo + "_" + num).val('');
		$("#mecanizada_div_select_div_propia_operador_" + tipo + "_" + num).val('');
		$("#mecanizada_div_select_div_rentada_costo_" + tipo + "_" + num).val('');
	}
}

function evaluaCambioAplicacion(id, tipo, num)
{
	
	
	$("#aerea_div_select_div_propia_combustible_" + tipo + "_" + num).val('').removeClass("required");
	$("#aerea_div_select_div_propia_operador_" + tipo + "_" + num).val('').removeClass("required");
	$("#aerea_div_select_div_rentada_costo_" + tipo + "_" + num).val('').removeClass("required");
	
	
	$("#mecanizada_div_select_div_propia_combustible_" + tipo + "_" + num).val('').removeClass("required");
	$("#mecanizada_div_select_div_propia_operador_" + tipo + "_" + num).val('').removeClass("required");
	$("#mecanizada_div_select_div_rentada_costo_" + tipo + "_" + num).val('').removeClass("required");


	if($("#" + id).val() == 1)
	{
		if(id.indexOf("aerea") > -1)
		{
			$("#aerea_div_select_div_propia_" + tipo + "_" + num).removeClass("hide");
			$("#aerea_div_select_div_rentada_" + tipo + "_" + num).addClass("hide");
			$("#aerea_div_select_div_rentada_costo_" + tipo + "_" + num).val('').removeClass("required");
			$("#aerea_div_select_div_propia_combustible_" + tipo + "_" + num).val('').addClass("required");
			$("#aerea_div_select_div_propia_operador_" + tipo + "_" + num).val('').addClass("required");
		}
		else
		{
			$("#mecanizada_div_select_div_propia_" + tipo + "_" + num).removeClass("hide");
			$("#mecanizada_div_select_div_rentada_" + tipo + "_" + num).addClass("hide");
			$("#mecanizada_div_select_div_rentada_costo_" + tipo + "_" + num).val('').removeClass("required");
			$("#mecanizada_div_select_div_propia_combustible_" + tipo + "_" + num).val('').addClass("required");
			$("#mecanizada_div_select_div_propia_operador_" + tipo + "_" + num).val('').addClass("required");
		}
	}
	else
	{
		if(id.indexOf("aerea") > -1)
		{
			$("#aerea_div_select_div_propia_" + tipo + "_" + num).addClass("hide");
			$("#aerea_div_select_div_rentada_" + tipo + "_" + num).removeClass("hide");

			$("#aerea_div_select_div_rentada_costo_" + tipo + "_" + num).val('').addClass("required");

		}
		else
		{
			$("#mecanizada_div_select_div_propia_" + tipo + "_" + num).addClass("hide");
			$("#mecanizada_div_select_div_rentada_" + tipo + "_" + num).removeClass("hide");

			$("#mecanizada_div_select_div_rentada_costo_" + tipo + "_" + num).val('').addClass("required");
		}
	}
	
}

function evaluaCambioFert(id, tipo, num)
{
	if($("#" + id).val() == 2)
	{
		$("#" + tipo + "_tipo_ap_div_" + num).removeClass("hide");
		$("#" + tipo + "_tipo_ap_cont_div_" + num).removeClass("hide");
	}
	else
	{
		$("#" + tipo + "_tipo_ap_div_" + num).addClass("hide");
		$("#" + tipo + "_tipo_ap_cont_div_" + num).addClass("hide");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function AparecerFormulario(parcial, btn, excepcion=false)
{

	//if(dg_congelado == true || (dg_congelado == false && parcial == 'DatosGenerales') || excepcion==true)
	//{
		limpiarSelectMenu();

		$('#' + btn).css({'background-color':color_check,'color':'#666'});
		
		//aparecemos los formularios
		if(parcial == 'DatosGenerales')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarDatosGenerales();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'RentaDeLaTierra')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarRentaTierra();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'AcondicionamientoDelPredio')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarAcondicionamientoDelPredio();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'MejoramientoDeFertilidadDelSuelo')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarMejoramientoDeFertilidadDelSuelo();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'PreparacionDelTerreno')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarPreparacionSuelo();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'Riego')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarRiego();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'SiembraDePrecision')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarPracticaSiembra();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'PracticaDeResiembra'){
			$.ajax({
			url: "/backend/diagnosticos/tieneresiembra/",
			type: "POST",
			data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
			async: false
			}).done(function(respuesta){
					respuesta = jQuery.parseJSON(respuesta);
					if(respuesta.ok=="ok"){
						cargarPracticaResiembra();
					}else{
						/*ShowExecuteBox("Datos de Resiembra no establecidos", "Para poder acceder a este formulario, es necesario primero guardar datos en resiembra.", function(){
							$('#btn-rs-mn').trigger("click");
						});	*/					
					}
			});
			
		}
		else if(parcial == 'Semilla'){
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarSiembra();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}else if(parcial == 'Resiembra'){
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarResiembra();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}else if(parcial == 'Fertilizacion')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarFertilizacion();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'CostosIndirectos')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarCostoIndirecto();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'IngresosEsperados')
		{
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarIngresoEsperado();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		else if(parcial == 'ControlDeMalezas'){
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarControlMalezas();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}else if(parcial=='ControlDeEnfermedades'){
			//cargarControlEnfermedades();
		}else if(parcial=='IndiceDeRentabilidad'){
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarIndiceDeRentabilidad();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}else if(parcial=='ReporteCostos'){
			ShowLoadingBox("Cargando...","Espere un momento por favor...");
			cargarReporteCostos();
			setTimeout(function()
			{ 
				$("#dialog").dialog('close');
				$("#dialog").dialog('destroy');
			},3000);
		}
		
		$("#parcial > div").each(function ()
		{
			$(this).addClass("hide");
		});

		$('#'+parcial).removeClass("hide");
		$('#encabezadoManejo').removeClass("hide");

		

		$("#contenidoFormulariosCaracterizacion > div").each(function ()
		{
			$(this).addClass("hide");
		});
		$('#'+parcial).removeClass("hide");


		

	/*}
	else
		ShowDialogBox("Datos generales no establecidos", "Para poder acceder a cualquier formulario, es necesario congelar los datos generales.", "Ok", "", false, "", true);*/
}



function agregar(id=0, predio_id = 0)
{
	window.location = "/backend/diagnosticos/agregar/tipo/1/id/" + id + '/predio/' + predio_id;
}

function numeros(e){
    var tecla;
	
    if ( document.all ) {
        tecla    = e.keyCode;
    } else {
        tecla    = e.which;
    }

    if ( tecla < 10 ) {
        return true;
    }
	if( tecla== 99 || tecla == 67 || tecla ==86 || tecla == 118 ){
		//alert("ctrl");
	}

    if ((tecla < 48 || tecla > 57) && tecla !=46 ) {
        return false;
    } else {//si ingresa solo numeros regreso true para que asigne valor al campo
        	return true;
    }

}

function deshabilitar(id)
{
	ShowConfirmBox('Eliminar', '¿Realmente desea eliminar el registro? Una vez borrado la información no se puede recuperar', function(){
		$.ajax({
	    	url: "/backend/diagnosticos/deshabilitar/id/" + id,
	      	type: "POST",
	      	processData: false,  
	      	contentType: false
	    }).done(function(data){
	        if(data == 'true')
	        {
	            var filtro = $("#filtroNombre").val();
	            $('#flexigrid').flexOptions({url: "/backend/diagnosticos/grid/filtro/" + filtro}).flexReload(); 
	        }
	        else
	        {
	            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
	        }
	    });
	});
}

function habilitar(id)
{
    $.ajax({
        url: "/backend/diagnosticos/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/diagnosticos/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}

function limpiarSelectMenu(){
    $('.btn-menu').each(function(){ 
    	$(this).css({'background-color': color_uncheck, 'color':'#fff'});
    });
}
function limpiarFiltro(){


	$("#filtroEstado").val('').trigger("change");
	$("#filtroMunicipio").val('').trigger("change");
	$("#filtroLocalidad").val('').trigger("change");
	$("#filtroRegion").val('');
	$("#filtroTerritorio").val('');
	$("#filtroAgroconsultor").val('');
	$("#filtroProductor").val('');
	$("#filtroTipoPredio").val('');
	$("#filtroClave").val('');
	 var filtro_ = "/backend/diagnosticos/grid";
	 $('#flexigrid').flexOptions({url: filtro_}).flexReload(); 
}


function loadEmpresas(tipo, padre)
{        	 
	 //alert($(padre).attr('value'));
	
	 $.ajax({
	        url: "/backend/diagnosticos/obtenerempresas/",
	        type: "POST",
			data: {tipo:tipo, id_empresa: $(padre).attr('value') }, /*  id_marca: $("#empresa_herbicida_" + padre).val(),  tipo: $("#thov_" + padre).val(),   */
	        async: false
	    }).done(function(respuesta){
	    	try
	    	{	
				
				//alert(respuesta); 
				if(respuesta.indexOf('value="0"') > -1){
		        	//$(padre).siblings('select').css("background-color","#F00");
					$(padre).siblings('select').html(respuesta);
				}
				
			}catch(err)
			{
				ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
			}
	    });
}

function cargarSubcatalogo(padre, id)
{        	 
	 //alert($(padre).attr('value'));
	
	$.ajax({
	        url: "/backend/diagnosticos/obtenersubcatalogo/",
	        type: "POST",
			data: {padre_id:$(padre).attr('value'), id: id}, /*  id_marca: $("#empresa_herbicida_" + padre).val(),  tipo: $("#thov_" + padre).val(),   */
	        async: false
	    }).done(function(respuesta){
	    	try
	    	{
	    		$(padre).siblings('select').html(respuesta);
			}catch(err)
			{
				ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el selector, por favor inténtelo nuevamente", "Ok", "", false, "");
			}
		});
}

function mostrarPredios(value)
{
	var listHtml='<option value="">Elija una opción</option>';
	if(value=='PREDIO NORMAL')
	{
		for(var i=1; i<=20; i++)
		{
			listHtml+='<option value="PREDIO-'+i+'">PREDIO-'+i+'</option>';
		}
	}
	else if(value=='ECA')
	{
		for(var i=1; i<=10; i++)
		{
			listHtml+='<option value="ECA-'+i+'">ECA-'+i+'</option>';
		}
	}
	else
	{
		listHtml='<option value="">Elija primero un tipo de predio</option>';
	}

	$("#filtroClave").html(listHtml);

}

function activaPlanTransicion()
{
	$("#gen-type").val(2);
	$("#btn-cultivos-mn").show();
}

