var doc = null;
var registrofrm4 = null;
var MisDatos = new Array();
var cargado = 0;



$(document).ready(function(){



});

function cargarFertilizacion()
{
	var conceptos = ["concepto", "mezclado", "quimica", "organica", "mineral", "acondicionador"];
	//asignamos las unidades a los conceptos dependiendo la configuracion general
	if(cargado!=1)
		obtenerUnidadesPorFormularioConceptoFrm11(conceptos);
	
	
		
	
	
	
	if($("#gen-type").val() == 1) 
		$(".inno", "#frm-11").attr("disabled", "disabled");
	else 
		$(".inno", "#frm-11").removeAttr("disabled");
	
	//desaparecemos la columna de innovaacion para cuando sea diagnostico
	if($("#gen-type").val()==1 && cargado ==0){
		$('.encabezado1').each(function(index, element) {
				$(this).find('th:eq(6)').remove();
		});
		$('.encabezado2').each(function(index, element) {
				$(this).find('th:eq(7)').remove();
		});
		$('#antes .rmezclado, #rantes .rmezclado, #antes .rconcepto, #rantes .rconcepto, #enla .rmezclado, #renla .rmezclado, #enla .rconcepto, #renla .rconcepto').each(function(index, element) {
				$(this).find('td:eq(5)').remove();
		});
		$('#durante .rmezclado, #durante .rconcepto, #durante .rquimica, #durante .rorganica, #durante .rmineral, #durante .racondicionador, #rdurante .rmezclado, #rdurante .rconcepto, #rdurante .rquimica, #rdurante .rorganica, #rdurante .rmineral, #rdurante .racondicionador').each(function(index, element) {
				$(this).find('th:eq(7)').remove();
				$(this).find('td:eq(6)').remove();
		});
	}
	
	//verificamos si hay resiembraa para aparecer las 3 tablas adicionales para resiembra
		$.ajax({
			url: "/backend/diagnosticos/tieneresiembra/",
			type: "POST",
			data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
			async: false
			}).done(function(respuesta){
					respuesta = jQuery.parseJSON(respuesta);
					if(respuesta.ok=="ok"){
						FertilizacionEnLaResiembra(true);
					}else{
						FertilizacionEnLaResiembra(false);					
					}
			});
	
	
	
	$.ajax({
        url: "/backend/diagnosticos/obtenerdatosfertilizacion/",
        type: "POST",
        data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
        async: false
    }).done(function(respuesta){
		var info;
		setTimeout(function()
		{
	    	try
	    	{
		        respuesta = jQuery.parseJSON(respuesta);
		        //cargo los catalogos en los campos
				
		        if(respuesta.ok !== undefined)
		        {
					if(cargado != 1){//validacion para no cargar 2 veces el form
						if(respuesta.antes!=undefined){
							if(respuesta.antes.id_fertilizacion>0){
								$('#realizo_antes').val('1').trigger("change");
							}else{
								$('#realizo_antes').val('2').trigger("change");
							}
							
							/*for (var i = 0; i < respuesta.antes.length; i++)
							{*/
								cargado =1;
								//count = (i+1);
								//seteamos lo que no es dinamico de cada tabla
								idFertilizacion = respuesta.antes.id_fertilizacion;
								nombre =respuesta.antes.nombre;
								mezclado_unidad = respuesta.antes.mezclado_unidad;
								mezclado_cantidad = respuesta.antes.mezclado_cantidad;
								mezclado_precio = respuesta.antes.mezclado_precio;
								mezclado_importe = respuesta.antes.mezclado_importe;
								mezclado_frealizacion = respuesta.antes.mezclado_frealizacion;
								mezclado_etapa_fenologica = respuesta.antes.mezclado_etapa_fenologica;
								mezclado_innovacion = respuesta.antes.mezclado_innovacion;
								id_fertilizacion = respuesta.antes.id_fertilizacion;
								es_fertilizacion = respuesta.antes.es_fertilizacion;
								tipo_mecanizacion = respuesta.antes.tipo_mecanizacion;
								costo_combustible = respuesta.antes.costo_combustible;
								costo_operador = respuesta.antes.costo_operador;
								tipo_aplicacion = respuesta.antes.tipo_aplicacion;
								numero_jornales = respuesta.antes.numero_jornales;
								costo_jornal = respuesta.antes.costo_jornal;
								monto_renta = respuesta.antes.monto_renta;
								$("#frm-11  #antes #fertilizacion_antes_id").val(idFertilizacion);
								$("#frm-11  #antes #fertilizacion_antes_nombre").val(nombre);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_unidad").val(mezclado_unidad);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_cantidad").val(mezclado_cantidad);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_pu").val(mezclado_precio);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_importe").val(mezclado_importe);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_frealizacion").val(mezclado_frealizacion);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_etapa_fenologica").html(mezclado_etapa_fenologica);
								$("#frm-11  #antes #fertilizacion_antes_mezclado_innovacion").val(mezclado_innovacion);
								
								$("#frm-11 #antes #fertilizacion_antes_tipo_ap_select").val(tipo_aplicacion).trigger("change");
								$("#frm-11 #antes #mecanizada_div #mecanizada_div_select").val(tipo_mecanizacion).trigger("change");//("selected", "selected");
								
								$("#frm-11 #antes #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(costo_combustible);
								$("#frm-11 #antes #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(costo_operador);
								$("#frm-11 #antes #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(monto_renta);
								
								$("#frm-11 #antes #manual_div #manual_div_no_jornales").val(numero_jornales);//.css("background-color","#F00");
								$("#frm-11 #antes #manual_div #manual_div_costo_jornal").val(costo_jornal);
								//alert(tipo_aplicacion);
								//alert(numero_jornales);
								//alert(costo_jornal);
								if(respuesta.antes.concepto!=undefined)
								{
									for (var j = 0; j < respuesta.antes.concepto.length; j++)
									{
										count = (j+1);
										if((j+1)< respuesta.antes.concepto.length){
											agregarFilaExtraFrm11('antes','antes', ''+respuesta.antes.concepto[j+1].id);
										}
										//$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_fuente").html(respuesta.antes.concepto[j].fuente);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_empresa").html(respuesta.antes.concepto[j].empresa);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_marca").html(respuesta.antes.concepto[j].marca);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_unidad").html(respuesta.antes.concepto[j].unidad);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_cantidad").val(respuesta.antes.concepto[j].cantidad);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_pu").val(respuesta.antes.concepto[j].precio);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_importe").val(respuesta.antes.concepto[j].importe);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_frealizacion").val(respuesta.antes.concepto[j].frealizacion);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_etapa_fenologica").html(respuesta.antes.concepto[j].etapa_fenologica);
										$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_innovacion").val(respuesta.antes.concepto[j].innovacion);
									}
								}
		
							//}
						}else{
							$('#realizo_antes').val('2').trigger("change");
						}
						
						
						if(respuesta.enla!=undefined){
							if(respuesta.enla.id_fertilizacion>0){
								$('#realizo_enla').val('1').trigger("change");
							}else{
								$('#realizo_enla').val('2').trigger("change");
							}
	/*						for (var j = 0; j < respuesta.enla.length; j++)
							{
	*/							cargado =1;
								//count = (j+1);
								//seteamos lo que no es dinamico de cada tabla
								idFertilizacion = respuesta.enla.id_fertilizacion;
								nombre =respuesta.enla.nombre;
								mezclado_unidad = respuesta.enla.mezclado_unidad;
								mezclado_cantidad = respuesta.enla.mezclado_cantidad;
								mezclado_precio = respuesta.enla.mezclado_precio;
								mezclado_importe = respuesta.enla.mezclado_importe;
								mezclado_frealizacion = respuesta.enla.mezclado_frealizacion;
								mezclado_etapa_fenologica = respuesta.enla.mezclado_etapa_fenologica;
								mezclado_innovacion = respuesta.enla.mezclado_innovacion;
								id_fertilizacion = respuesta.enla.id_fertilizacion;
								costo_extra = respuesta.enla.costo_extra;
								tipo_mecanizacion = respuesta.enla.tipo_mecanizacion;
								costo_combustible = respuesta.enla.costo_combustible;
								costo_operador = respuesta.enla.costo_operador;
								tipo_aplicacion = respuesta.enla.tipo_aplicacion;
								numero_jornales = respuesta.enla.numero_jornales;
								costo_jornal = respuesta.enla.costo_jornal;
								monto_renta = respuesta.enla.monto_renta;
								
								$("#frm-11  #enla #fertilizacion_enla_id").val(idFertilizacion);
								$("#frm-11  #enla #fertilizacion_enla_nombre").val(nombre);
								$("#frm-11  #enla #fertilizacion_enla_mezclado_unidad").val(mezclado_unidad).trigger("change");
								$("#frm-11  #enla #fertilizacion_enla_mezclado_cantidad").val(mezclado_cantidad);
								$("#frm-11  #enla #fertilizacion_enla_mezclado_pu").val(mezclado_precio);
								$("#frm-11  #enla #fertilizacion_enla_mezclado_importe").val(mezclado_importe);
								$("#frm-11  #enla #fertilizacion_enla_mezclado_frealizacion").val(mezclado_frealizacion);
								$("#frm-11  #enla #fertilizacion_enla_mezclado_etapa_fenologica").html(mezclado_etapa_fenologica);
								$("#frm-11  #enla #fertilizacion_enla_mezclado_innovacion").val(mezclado_innovacion);
								
											
								//$("#frm-11 #antes #fertilizacion_antes_es_fert_select_1 ").val(es_fertilizacion).trigger("change");//option[value="+es_fertilizacion+"]").attr("selected", "selected");
								$("#frm-11 #enla #fertilizacion_enla_hubo").val(costo_extra).trigger("change");
								if(costo_extra==1)
								{
									$("#frm-11 #enla #fertilizacion_enla_tipo_ap_select").val(tipo_aplicacion).trigger("change");
									$("#frm-11 #enla #mecanizada_div #mecanizada_div_select").val(tipo_mecanizacion).trigger("change");//("selected", "selected");
								}
								$("#frm-11 #enla #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(costo_combustible);
								$("#frm-11 #enla #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(costo_operador);
								$("#frm-11 #enla #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(monto_renta);
								
								//$("#frm-11 #antes #fsiembra_tipo_ap_div_1 #fsiembra_tipo_ap_select_1 option[value="+tipo_aplicacion+"]").attr("selected", "selected");
								$("#frm-11 #enla #manual_div #manual_div_no_jornales").val(numero_jornales);
								$("#frm-11 #enla #manual_div #manual_div_costo_jornal").val(costo_jornal);						
								
								if(respuesta.enla.concepto.length!=undefined && respuesta.enla.concepto.length>0)
								{
									for (var k = 0; k < respuesta.enla.concepto.length; k++){
										count = (k+1);
										if((k+1)< respuesta.enla.concepto.length){
											agregarFilaExtraFrm11('enla','enla', ''+respuesta.enla.concepto[k+1].id);
											//alert('tfsiembra_'+count+'fsiembra-concepto_'+(j+1));
										}
										//alert("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #fsiembra-concepto_"+(j+1)+" #fsiembra_tipo_fu_select_"+(j+1));
										//$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_fuente").html(respuesta.enla[0].concepto[k].fuente);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_empresa").html(respuesta.enla.concepto[k].empresa);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_marca").html(respuesta.enla.concepto[k].marca);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_unidad").html(respuesta.enla.concepto[k].unidad);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_cantidad").val(respuesta.enla.concepto[k].cantidad);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_pu").val(respuesta.enla.concepto[k].precio);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_importe").val(respuesta.enla.concepto[k].importe);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_frealizacion").val(respuesta.enla.concepto[k].frealizacion);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_etapa_fenologica").html(respuesta.enla.concepto[k].etapa_fenologica);
										$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_innovacion").val(respuesta.enla.concepto[k].innovacion);
									}
								}
							//}
						}else{
							$('#realizo_enla').val('2').trigger("change");
							$('#fertilizacion_enla_hubo').val('2').trigger("change");
							
						}

						if(respuesta.durante!=undefined){
							if(respuesta.durante.id_fertilizacion>0){
								$('#realizo_durante').val('1').trigger("change");
							}else{
								$('#realizo_durante').val('2').trigger("change");
							}
							//alert("antes del for durante: "+respuesta.durante.concepto.length);
							//for (var l = 0; l < respuesta.durante.length; l++)
							/*if(respuesta.durante!=undefined)
							{*/
								cargado =1;
								//fsiembra ="tfsiembra_";
								//foliar="tfoliar_";
								
								
								//seteamos lo que no es dinamico de cada tabla
								idFertilizacion = respuesta.durante.id_fertilizacion;
								nombre =respuesta.durante.nombre;
								mezclado_unidad = respuesta.durante.mezclado_unidad;
								mezclado_cantidad = respuesta.durante.mezclado_cantidad;
								mezclado_precio = respuesta.durante.mezclado_precio;
								mezclado_importe = respuesta.durante.mezclado_importe;
								mezclado_frealizacion = respuesta.durante.mezclado_frealizacion;
								mezclado_etapa_fenologica = respuesta.durante.mezclado_etapa_fenologica;
								mezclado_innovacion = respuesta.durante.mezclado_innovacion;
								id_fertilizacion = respuesta.durante.id_fertilizacion;
								costo_extra = respuesta.durante.costo_extra;
								tipo_mecanizacion = respuesta.durante.tipo_mecanizacion;
								costo_combustible = respuesta.durante.costo_combustible;
								costo_operador = respuesta.durante.costo_operador;
								tipo_aplicacion = respuesta.durante.tipo_aplicacion;
								numero_jornales = respuesta.durante.numero_jornales;
								costo_jornal = respuesta.durante.costo_jornal;
								monto_renta = respuesta.durante.monto_renta;
								//alert("mezclado_etapa_fenologica: "+mezclado_etapa_fenologica);
								if(idFertilizacion>0){
									$('#realizo_durante').val('1').trigger("change");
								}
								//alert(mezclado_etapa_fenologica);
								$("#frm-11  #durante #fertilizacion_durante_id").val(idFertilizacion);
								$("#frm-11  #durante #fertilizacion_durante_nombre").val(nombre);
								$("#frm-11  #durante #fertilizacion_durante_mezclado_unidad").val(mezclado_unidad).trigger("change");
								$("#frm-11  #durante #fertilizacion_durante_mezclado_cantidad").val(mezclado_cantidad);
								$("#frm-11  #durante #fertilizacion_durante_mezclado_pu").val(mezclado_precio);
								$("#frm-11  #durante #fertilizacion_durante_mezclado_importe").val(mezclado_importe);
								$("#frm-11  #durante #fertilizacion_durante_mezclado_frealizacion").val(mezclado_frealizacion);
								$("#frm-11  #durante #fertilizacion_durante_mezclado_etapa_fenologica").html(mezclado_etapa_fenologica);
								$("#frm-11  #durante #fertilizacion_durante_mezclado_innovacion").val(mezclado_innovacion);
								//				
								$("#frm-11 #durante #fertilizacion_durante_hubo").val(costo_extra).trigger("change");
								if(costo_extra==1)
								{
									$("#frm-11 #durante #fertilizacion_durante_tipo_ap_select").val(tipo_aplicacion).trigger("change");
									$("#frm-11 #durante #mecanizada_div #mecanizada_div_select").val(tipo_mecanizacion).trigger("change");//("selected", "selected");
								}
								$("#frm-11 #durante #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(costo_combustible);
								$("#frm-11 #durante #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(costo_operador);
								$("#frm-11 #durante #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(monto_renta);
								//$("#frm-11 #antes #fsiembra_tipo_ap_div_1 #fsiembra_tipo_ap_select_1 option[value="+tipo_aplicacion+"]").attr("selected", "selected");
								$("#frm-11 #durante #manual_div #manual_div_no_jornales").val(numero_jornales);
								$("#frm-11 #durante #manual_div #manual_div_costo_jornal").val(costo_jornal);						
								
								
								
								if(respuesta.durante.concepto.length!=undefined && respuesta.durante.concepto.length>0)
								{
									for (var m = 0; m < respuesta.durante.concepto.length; m++){
										count = (m+1);
										if((m+1)< respuesta.durante.concepto.length){
											agregarFilaExtraFrm11('durante','durante', ''+respuesta.durante.concepto[m+1].id);
											//alert('tfsiembra_'+count+'fsiembra-concepto_'+(j+1));
										}
										//alert("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #fsiembra-concepto_"+(j+1)+" #fsiembra_tipo_fu_select_"+(j+1));
										//$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_fuente").html(respuesta.durante[0].concepto[m].fuente);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_empresa").html(respuesta.durante.concepto[m].empresa);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_marca").html(respuesta.durante.concepto[m].marca);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_unidad").html(respuesta.durante.concepto[m].unidad);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_cantidad").val(respuesta.durante.concepto[m].cantidad);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_pu").val(respuesta.durante.concepto[m].precio);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_importe").val(respuesta.durante.concepto[m].importe);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_frealizacion").val(respuesta.durante.concepto[m].frealizacion);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_etapa_fenologica").html(respuesta.durante.concepto[m].etapa_fenologica);
										$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_innovacion").val(respuesta.durante.concepto[m].innovacion);
									}
								}
								
								if(respuesta.durante.quimica!=undefined){
									
									if(respuesta.durante.quimica.length>0)
									{
										for (var q = 0; q < respuesta.durante.quimica.length; q++){
											if((q+1)< respuesta.durante.quimica.length){
												agregarFilaExtraFrm11('durante','quimica', ''+respuesta.durante.quimica[q+1].id);
												//alert('tfsiembra_'+count+'fsiembra-concepto_'+(j+1));
											}
											//alert(respuesta.durante[0].quimica[q].empresa);
											//alert("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #fsiembra-concepto_"+(j+1)+" #fsiembra_tipo_fu_select_"+(j+1));
											//$("#frm-11 #durante #fertilizacion_durante-cquimica_"+count+" #fertilizacion_durante-quimica_fuente").html(respuesta.durante[0].quimica[q].fuente);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_empresa").html(respuesta.durante.quimica[q].empresa);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_marca").html(respuesta.durante.quimica[q].marca);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_unidad").html(respuesta.durante.quimica[q].unidad);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_cantidad").val(respuesta.durante.quimica[q].cantidad);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_pu").val(respuesta.durante.quimica[q].precio);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_importe").val(respuesta.durante.quimica[q].importe);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_frealizacion").val(respuesta.durante.quimica[q].frealizacion);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_etapa_fenologica").html(respuesta.durante.quimica[q].etapa_fenologica);
											$("#frm-11 #durante #fertilizacion_durante-cquimica_"+(q+1)+" #fertilizacion_durante-quimica_innovacion").val(respuesta.durante.quimica[q].innovacion);
										}
									}
								}
								
							//}
						}else{
							$('#realizo_durante').val('2').trigger("change");
							$('#fertilizacion_durante_hubo').val('2').trigger("change");
						}
						
						
						
						//si  hay resiembra	
						if(respuesta.rantes!=undefined){
							if(respuesta.rantes.id_fertilizacion>0){
								$('#realizo_rantes').val('1').trigger("change");
							}else{
								$('#realizo_rantes').val('2').trigger("change");
							}
							/*for (var i = 0; i < respuesta.rantes.length; i++)
							{*/
								cargado =1;
								//count = (i+1);
								//seteamos lo que no es dinamico de cada tabla
								idFertilizacion = respuesta.rantes.id_fertilizacion;
								nombre =respuesta.rantes.nombre;
								mezclado_unidad = respuesta.rantes.mezclado_unidad;
								mezclado_cantidad = respuesta.rantes.mezclado_cantidad;
								mezclado_precio = respuesta.rantes.mezclado_precio;
								mezclado_importe = respuesta.rantes.mezclado_importe;
								mezclado_frealizacion = respuesta.rantes.mezclado_frealizacion;
								mezclado_etapa_fenologica = respuesta.rantes.mezclado_etapa_fenologica;
								mezclado_innovacion = respuesta.rantes.mezclado_innovacion;
								id_fertilizacion = respuesta.rantes.id_fertilizacion;
								es_fertilizacion = respuesta.rantes.es_fertilizacion;
								tipo_mecanizacion = respuesta.rantes.tipo_mecanizacion;
								costo_combustible = respuesta.rantes.costo_combustible;
								costo_operador = respuesta.rantes.costo_operador;
								tipo_aplicacion = respuesta.rantes.tipo_aplicacion;
								numero_jornales = respuesta.rantes.numero_jornales;
								costo_jornal = respuesta.rantes.costo_jornal;
								monto_renta = respuesta.rantes.monto_renta;
								if(idFertilizacion>0){
									$('#realizo_rantes').val('1').trigger("change");
								}
								$("#frm-11  #rantes #fertilizacion_rantes_id").val(idFertilizacion);
								$("#frm-11  #rantes #fertilizacion_rantes_nombre").val(nombre);
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_unidad").val(mezclado_unidad).trigger("change");
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_cantidad").val(mezclado_cantidad);
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_pu").val(mezclado_precio);
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_importe").val(mezclado_importe);
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_frealizacion").val(mezclado_frealizacion);
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_etapa_fenologica").html(mezclado_etapa_fenologica);
								$("#frm-11  #rantes #fertilizacion_rantes_mezclado_innovacion").val(mezclado_innovacion);
													
								$("#frm-11 #rantes #fertilizacion_rantes_tipo_ap_select").val(tipo_aplicacion).trigger("change");
								$("#frm-11 #rantes #mecanizada_div #mecanizada_div_select").val(tipo_mecanizacion).trigger("change");//("selected", "selected");
								$("#frm-11 #rantes #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(costo_combustible);
								$("#frm-11 #rantes #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(costo_operador);
								$("#frm-11 #rantes #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(monto_renta);
								$("#frm-11 #rantes #manual_div #manual_div_no_jornales").val(numero_jornales);
								$("#frm-11 #rantes #manual_div #manual_div_costo_jornal").val(costo_jornal);						
								
								if(respuesta.rantes.concepto!=undefined)
								{
									for (var j = 0; j < respuesta.rantes.concepto.length; j++){
										count = (j+1);
										if((j+1)< respuesta.rantes.concepto.length){
											agregarFilaExtraFrm11('rantes','rantes', ''+respuesta.antes.concepto[j+1].id);
										}
										//$("#frm-11 #antes #fertilizacion_antes-cantes_"+count+" #fertilizacion_antes_fuente").html(respuesta.antes[0].concepto[j].fuente);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_empresa").html(respuesta.rantes.concepto[j].empresa);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_marca").html(respuesta.rantes.concepto[j].marca);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_unidad").html(respuesta.rantes.concepto[j].unidad);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_cantidad").val(respuesta.rantes.concepto[j].cantidad);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_pu").val(respuesta.rantes.concepto[j].precio);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_importe").val(respuesta.rantes.concepto[j].importe);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_frealizacion").val(respuesta.rantes.concepto[j].frealizacion);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_etapa_fenologica").html(respuesta.rantes.concepto[j].etapa_fenologica);
										$("#frm-11 #rantes #fertilizacion_rantes-crantes_"+count+" #fertilizacion_rantes_innovacion").val(respuesta.rantes.concepto[j].innovacion);
									}
								}
							//}
						}else{
							$('#realizo_rantes').val('2').trigger("change");
						}
						
						
						if(respuesta.renla!=undefined){
							if(respuesta.renla.id_fertilizacion>0){
								$('#realizo_renla').val('1').trigger("change");
							}else{
								$('#realizo_renla').val('2').trigger("change");
							}
							/*for (var j = 0; j < respuesta.renla.length; j++)
							{*/
								cargado =1;
								//count = (j+1);
								//seteamos lo que no es dinamico de cada tabla
								idFertilizacion = respuesta.renla.id_fertilizacion;
								nombre =respuesta.renla.nombre;
								mezclado_unidad = respuesta.renla.mezclado_unidad;
								mezclado_cantidad = respuesta.renla.mezclado_cantidad;
								mezclado_precio = respuesta.renla.mezclado_precio;
								mezclado_importe = respuesta.renla.mezclado_importe;
								mezclado_frealizacion = respuesta.renla.mezclado_frealizacion;
								mezclado_etapa_fenologica = respuesta.renla.mezclado_etapa_fenologica;
								mezclado_innovacion = respuesta.renla.mezclado_innovacion;
								id_fertilizacion = respuesta.renla.id_fertilizacion;
								costo_extra = respuesta.renla.costo_extra;
								tipo_mecanizacion = respuesta.renla.tipo_mecanizacion;
								costo_combustible = respuesta.renla.costo_combustible;
								costo_operador = respuesta.renla.costo_operador;
								tipo_aplicacion = respuesta.renla.tipo_aplicacion;
								numero_jornales = respuesta.renla.numero_jornales;
								costo_jornal = respuesta.renla.costo_jornal;
								monto_renta = respuesta.renla.monto_renta;
								
								$("#frm-11  #renla #fertilizacion_renla_id").val(idFertilizacion);
								$("#frm-11  #renla #fertilizacion_renla_nombre").val(nombre);
								$("#frm-11  #renla #fertilizacion_renla_mezclado_unidad").val(mezclado_unidad).trigger("change");
								$("#frm-11  #renla #fertilizacion_renla_mezclado_cantidad").val(mezclado_cantidad);
								$("#frm-11  #renla #fertilizacion_renla_mezclado_pu").val(mezclado_precio);
								$("#frm-11  #renla #fertilizacion_renla_mezclado_importe").val(mezclado_importe);
								$("#frm-11  #renla #fertilizacion_renla_mezclado_frealizacion").val(mezclado_frealizacion);
								$("#frm-11  #renla #fertilizacion_renla_mezclado_etapa_fenologica").html(mezclado_etapa_fenologica);
								$("#frm-11  #renla #fertilizacion_renla_mezclado_innovacion").val(mezclado_innovacion);
								
											
								//$("#frm-11 #antes #fertilizacion_antes_es_fert_select_1 ").val(es_fertilizacion).trigger("change");//option[value="+es_fertilizacion+"]").attr("selected", "selected");
								$("#frm-11 #renla #fertilizacion_enla_hubo").val(costo_extra).trigger("change");
								$("#frm-11 #renla #fertilizacion_renla_tipo_ap_select").val(tipo_aplicacion).trigger("change");
								$("#frm-11 #renla #mecanizada_div #mecanizada_div_select").val(tipo_mecanizacion).trigger("change");//("selected", "selected");
								$("#frm-11 #renla #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(costo_combustible);
								$("#frm-11 #renla #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(costo_operador);
								$("#frm-11 #renla #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(monto_renta);
								
								//$("#frm-11 #antes #fsiembra_tipo_ap_div_1 #fsiembra_tipo_ap_select_1 option[value="+tipo_aplicacion+"]").attr("selected", "selected");
								$("#frm-11 #renla #manual_div #manual_div_no_jornales").val(numero_jornales);
								$("#frm-11 #renla #manual_div #manual_div_costo_jornal").val(costo_jornal);						
								
								if(respuesta.renla.concepto!=undefined)
								{
									for (var k = 0; k < respuesta.renla.concepto.length; k++){
										count = (k+1);
										/*if((k+1)< respuesta.enla.concepto.length){
											agregarFilaExtraFrm11('renla','renla', ''+respuesta.enla.concepto[k+1].id);
											//alert('tfsiembra_'+count+'fsiembra-concepto_'+(j+1));
										}*/
										//alert("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #fsiembra-concepto_"+(j+1)+" #fsiembra_tipo_fu_select_"+(j+1));
										//$("#frm-11 #enla #fertilizacion_enla-cenla_"+count+" #fertilizacion_enla_fuente").html(respuesta.enla[0].concepto[k].fuente);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_empresa").html(respuesta.renla.concepto[k].empresa);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_marca").html(respuesta.renla.concepto[k].marca);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_unidad").html(respuesta.renla.concepto[k].unidad);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_cantidad").val(respuesta.renla.concepto[k].cantidad);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_pu").val(respuesta.renla.concepto[k].precio);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_importe").val(respuesta.renla.concepto[k].importe);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_frealizacion").val(respuesta.renla.concepto[k].frealizacion);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_etapa_fenologica").html(respuesta.renla.concepto[k].etapa_fenologica);
										$("#frm-11 #renla #fertilizacion_renla-crenla_"+count+" #fertilizacion_renla_innovacion").val(respuesta.renla.concepto[k].innovacion);
									}
								}
							//}
						}else{
							$('#realizo_renla').val('2').trigger("change");
							$('#fertilizacion_renla_hubo').val('2').trigger("change");
							
						}

						if(respuesta.rdurante!=undefined){
							if(respuesta.rdurante.id_fertilizacion>0){
								$('#realizo_rdurante').val('1').trigger("change");
							}else{
								$('#realizo_rdurante').val('2').trigger("change");
							}
							//alert("antes del for: "+respuesta.rdurante.length);
							/*for (var l = 0; l < respuesta.rdurante.length; l++)
							{*/
								cargado =1;
								//count = (l+1);
								//alert(respuesta.rdurante.mezclado_cantidad);
								idFertilizacion = respuesta.rdurante.id_fertilizacion;
								nombre =respuesta.rdurante.nombre;
								mezclado_unidad = respuesta.rdurante.mezclado_unidad;
								mezclado_cantidad = respuesta.rdurante.mezclado_cantidad;
								mezclado_precio = respuesta.rdurante.mezclado_precio;
								mezclado_importe = respuesta.rdurante.mezclado_importe;
								mezclado_frealizacion = respuesta.rdurante.mezclado_frealizacion;
								mezclado_etapa_fenologica = respuesta.rdurante.mezclado_etapa_fenologica;
								mezclado_innovacion = respuesta.rdurante.mezclado_innovacion;
								id_fertilizacion = respuesta.rdurante.id_fertilizacion;
								costo_extra = respuesta.rdurante.costo_extra;
								tipo_mecanizacion = respuesta.rdurante.tipo_mecanizacion;
								costo_combustible = respuesta.rdurante.costo_combustible;
								costo_operador = respuesta.rdurante.costo_operador;
								tipo_aplicacion = respuesta.rdurante.tipo_aplicacion;
								numero_jornales = respuesta.rdurante.numero_jornales;
								costo_jornal = respuesta.rdurante.costo_jornal;
								monto_renta = respuesta.rdurante.monto_renta;
								if(idFertilizacion>0){
									$('#realizo_rdurante').val('1').trigger("change");
								}
								//alert(mezclado_etapa_fenologica);
								$("#frm-11  #rdurante #fertilizacion_rdurante_id").val(idFertilizacion);
								$("#frm-11  #rdurante #fertilizacion_rdurante_nombre").val(nombre);
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_unidad").val(mezclado_unidad).trigger("change");
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_cantidad").val(mezclado_cantidad);
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_pu").val(mezclado_precio);
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_importe").val(mezclado_importe);
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_frealizacion").val(mezclado_frealizacion);
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_etapa_fenologica").html(mezclado_etapa_fenologica);
								$("#frm-11  #rdurante #fertilizacion_rdurante_mezclado_innovacion").val(mezclado_innovacion);
								//				
								//alert(tipo_aplicacion);
								$("#frm-11 #rdurante #fertilizacion_rdurante_hubo").val(costo_extra).trigger("change"); 
								$("#frm-11 #rdurante #fertilizacion_rdurante_tipo_ap_select").val(tipo_aplicacion).trigger("change");
								$("#frm-11 #rdurante #mecanizada_div #mecanizada_div_select").val(tipo_mecanizacion).trigger("change");//("selected", "selected");
								$("#frm-11 #rdurante #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").val(costo_combustible);
								$("#frm-11 #rdurante #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").val(costo_operador);
								$("#frm-11 #rdurante #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val(monto_renta);
								//$("#frm-11 #rantes #fsiembra_tipo_ap_div_1 #fsiembra_tipo_ap_select_1 option[value="+tipo_aplicacion+"]").attr("selected", "selected");
								$("#frm-11 #rdurante #manual_div #manual_div_no_jornales").val(numero_jornales);
								$("#frm-11 #rdurante #manual_div #manual_div_costo_jornal").val(costo_jornal);						
								
								if(respuesta.rdurante.concepto!=undefined)
								{
									for (var m = 0; m < respuesta.rdurante.concepto.length; m++){
										count = (m+1);
										if((m+1)< respuesta.rdurante.concepto.length){
											agregarFilaExtraFrm11('rdurante','rdurante', ''+respuesta.rdurante.concepto[m+1].id);
											//alert('tfsiembra_'+count+'fsiembra-concepto_'+(j+1));
										}
										//alert("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #fsiembra-concepto_"+(j+1)+" #fsiembra_tipo_fu_select_"+(j+1));
										//$("#frm-11 #durante #fertilizacion_durante-cdurante_"+count+" #fertilizacion_durante_fuente").html(respuesta.durante[0].concepto[m].fuente);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_empresa").html(respuesta.rdurante.concepto[m].empresa);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_marca").html(respuesta.rdurante.concepto[m].marca);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_unidad").html(respuesta.rdurante.concepto[m].unidad);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_cantidad").val(respuesta.rdurante.concepto[m].cantidad);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_pu").val(respuesta.rdurante.concepto[m].precio);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_importe").val(respuesta.rdurante.concepto[m].importe);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_frealizacion").val(respuesta.rdurante.concepto[m].frealizacion);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_etapa_fenologica").html(respuesta.rdurante.concepto[m].etapa_fenologica);
										$("#frm-11 #rdurante #fertilizacion_rdurante-crdurante_"+count+" #fertilizacion_rdurante_innovacion").val(respuesta.rdurante.concepto[m].innovacion);
									}
								}
								
								if(respuesta.rdurante.quimica!=undefined){
									
									for (var q = 0; q < respuesta.rdurante.quimica.length; q++){
										if((q+1)< respuesta.rdurante.quimica.length){
											agregarFilaExtraFrm11('rdurante','quimica', ''+respuesta.rdurante.quimica[q+1].id);
											//alert('tfsiembra_'+count+'fsiembra-concepto_'+(j+1));
										}
										
										//alert("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #fsiembra-concepto_"+(j+1)+" #fsiembra_tipo_fu_select_"+(j+1));
										//$("#frm-11 #durante #fertilizacion_durante-cquimica_"+count+" #fertilizacion_durante-quimica_fuente").html(respuesta.durante[0].quimica[q].fuente);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_empresa").html(respuesta.rdurante.quimica[q].empresa);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_marca").html(respuesta.rdurante.quimica[q].marca);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_unidad").html(respuesta.rdurante.quimica[q].unidad);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_cantidad").val(respuesta.rdurante.quimica[q].cantidad);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_pu").val(respuesta.rdurante.quimica[q].precio);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_importe").val(respuesta.rdurante.quimica[q].importe);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_frealizacion").val(respuesta.rdurante.quimica[q].frealizacion);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_etapa_fenologica").html(respuesta.rdurante.quimica[q].etapa_fenologica);
										$("#frm-11 #rdurante #fertilizacion_rdurante-cquimica_"+(q+1)+" #fertilizacion_rdurante-quimica_innovacion").val(respuesta.rdurante.quimica[q].innovacion);
									}
								}
								
							//}
						}else{
							$('#realizo_rdurante').val('2').trigger("change");
							$('#fertilizacion_rdurante_hubo').val('2').trigger("change");
						}					
						
					}
					        	
					if(respuesta.congelado == 2) 
						$('#frm-11 *').attr("disabled", "disabled").unbind();
				}
		        else if(respuesta.noexiste === undefined){
		        	ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener el formulario, por favor intentelo nuevamente", "Ok", "", false, "");
				}else{//formulario nuevo, cargo la informacion de los catalogos en los select
					$('#realizo_antes').val('2').trigger("change");
					$('#realizo_enla').val('2').trigger("change");
					$('#realizo_durante').val('2').trigger("change");
					$('#fertilizacion_enla_hubo').val('2').trigger("change");
					$('#fertilizacion_durante_hubo').val('2').trigger("change");

									
				}
			}
			catch(err)
			{
				console.log(err);
				ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente: " + err.message, "Ok", "", false, "");
			}
		},3000);
    });

    $('#btn-save-fert', "#frm-11").once('click', function(){
		//if(validaFrm11()){
			var id_generico = $("#gen-id").val();
			$(".date", "#frm-11").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-11"));
			frm.append("id", $("#gen-id").val()); 
			frm.append("tipo", $("#gen-type").val());
				$.ajax({
					url: "/backend/diagnosticos/guardarfertilizacion/",
					type: "POST",
					data: frm,
					processData: false,  
					contentType: false,
				}).done(function(respuesta){
					
					try
					{
						$(".date", "#frm-11").attr('disabled', 'disabled');
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
							ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
						}
						else 
							ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
					}
					catch(error)
					{
						ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
					}
				});
	    /*}else{
			ShowDialogBox("Campos faltantes", "Hay campos incorrectos o faltantes. Revise los campos marcados y velva a intentarlo.", "Ok", "", false, "");
		}*/
	});
	
	
	$("#btn-congelar-fert", "#frm-11").once('click', function(){
		if(validaFrm11())
			ShowConfirmBox("Confirmación de congelación", "¿Realmente desea congelar este formulario?, si lo congela no podrá revertise, ¿Desea continuar?", congelarFrm11);
	});
	$("#btn-clean-fert", "#frm-11").once('click', function(){
		ShowConfirmBox("Confirmación para Limpiar el formulario", "¿Estás seguro de querer borrar la información de este formulario?", function(){
			$('input','#frm-11').val('');
			$('#frm-11 select').not("#fsiembra_es_fert_select_1").not("#fsiembra_tipo_ap_select_1").each(function() {
				//$(this).css("background-color","#F00");
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).eq(0).prop('selected', 'selected').change();
			});
			$('#frm-11 #fsiembra_tipo_ap_select_1, #frm-11 #fsiembra_es_fert_select_1').each(function() {
				//$(this).css("background-color","#F00");
				 $(this).find('option:selected').removeAttr("selected");
				 $(this).val('2').change();
			});
		}); 
	});

}
function FertilizacionEnLaResiembra(habilitar){
	
	if(habilitar){
		$("#rantes, #renla, #rdurante").removeClass("hide");
	}else{
		$("#rantes, #renla, #rdurante").addClass("hide");
	}
	
}
function CargarCatalogos(fuente,ef){
	$('#generico-antes, #generico-enla, #generico-durante, #generico-quimica, #generico-organica, #generico-mineral, #generico-acondicionador').find('th:eq(0) select').html(fuente);
	$('#fertilizacion_antes-cantes_1, #fertilizacion_enla-cenla_1, #fertilizacion_durante-cdurante_1, #fertilizacion_durante-cquimica_1, #fertilizacion_durante-corganica_1, #fertilizacion_durante-cmineral_1, #fertilizacion_durante-cacondicionador_1').find('th:eq(0) select').html(fuente);
	$('#generico-durante, #generico-quimica, #generico-organica, #generico-mineral, #generico-acondicionador, #fertilizacion_durante-cdurante_1, #fertilizacion_durante-cquimica_1, #fertilizacion_durante-corganica_1, #fertilizacion_durante-cmineral_1, #fertilizacion_durante-cacondicionador_1, #durante .rmezclado').find('td:eq(5) select').html(ef);
	$('#rdurante #generico-durante, #rdurante #generico-quimica, #rdurante #generico-organica, #rdurante #generico-mineral, #rdurante #generico-acondicionador, #rdurante #fertilizacion_rdurante-crdurante_1, #rdurante #fertilizacion_rdurante-cquimica_1, #rdurante #fertilizacion_rdurante-corganica_1, #rdurante #fertilizacion_rdurante-cmineral_1, #rdurante #fertilizacion_rdurante-cacondicionador_1, #rdurante .rmezclado').find('td:eq(5) select').html(ef);
}
function obtenerUnidadesPorFormularioConceptoFrm11(conceptos){
	for (var i = 0; i < conceptos.length; i++) {
			$.ajax({
				url: "/backend/diagnosticos/obtenerunidadesporformularioconcepto/",
				type: "POST",
				data:{formulario:"Fertilizacion",concepto:conceptos[i]}
			}).done(function(data){
				concepto = data.split("|")[0];
				datos = data.split("|")[1];
				if(concepto=="concepto"){
					$('.rconcepto').each(function(index, element) {
                        $(this).find(' td:eq(0) select').html(datos);
                    });
				}
				if(concepto=="mezclado"){
					$('.rmezclado').each(function(index, element) {
                        $(this).find(' td:eq(0) select').html(datos);
                    });
				}
				/*if(concepto=="quimica"){
					$('.rquimica').each(function(index, element) {
                        $(this).find(' td:eq(0) select').html(datos);
                    });
				}
				if(concepto=="organica"){
					$('.rorganica').each(function(index, element) {
                        $(this).find(' td:eq(0) select').html(datos);
                    });
				}
				if(concepto=="mineral"){
					$('.rmineral').each(function(index, element) {
                        $(this).find(' td:eq(0) select').html(datos);
                    });
				}
				if(concepto=="acondicionador"){
					$('.racondicionador').each(function(index, element) {
                        $(this).find(' td:eq(0) select').html(datos);
                    });
				}*/
			});
	}
	
}
function calculaImporteFrm11(obj){
	
	var parent = $(obj).parents().get(1);
	var parent2 = $(obj).parents().get(2);
	//$(parent).css("background-color","#f00");
			
		var cantidad = $(parent).find(' td:eq(1) input').attr("value");
		var precio = $(parent).find(' td:eq(2) input').attr("value");
		var importe = $(parent).find(' td:eq(3) input');

		cantidad = parseFloat(cantidad);
		precio = parseFloat(precio);
		//hectareas = parseFloat(hectareas);
		
		if(cantidad>0 && precio>0){
			importe.val(cantidad*precio);
		}else{
			importe.val('');
		}
}
function cargaSelectClones(respuesta){
	//renglon generico
	//alert(respuesta.toSource());
				$("#frm-11 #tfsiembra_1 #fsiembra-concepto_1 #fsiembra_tipo_fu_select_1").html(respuesta.select_fuente);
				$("#frm-11 #tfsiembra_1 #fsiembra-concepto_1 #fsiembra_tipo_unidad_1").html(respuesta.select_unidad);
				$("#frm-11 #tfsiembra_1 #fsiembra-concepto_1 #fsiembra_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				//renglon de costo por mezclado
				$("#frm-11 #tfsiembra_1 #fsiembra_mezclado_tipo_unidad_1").html(respuesta.select_unidad);
				$("#frm-11 #tfsiembra_1 #fsiembra_mezclado_etapa_fenologica_1").html(respuesta.select_etapa_fenologica);
				//renglon generico clon
				$("#frm-11 #tfsiembra_1  #generico-fsiembra #fsiembra_tipo_fu_select_").html(respuesta.select_fuente);
				$("#frm-11 #generico-fsiembra #fsiembra_tipo_unidad_").html(respuesta.select_unidad);
				$("#frm-11 #generico-fsiembra #fsiembra_etapa_fenologica_").html(respuesta.select_etapa_fenologica);
				
				//cargamos los select en la tabla de foliar aunque no existe para cuando se clone la fila ya tenga los valores
				$("#frm-11 #generico-foliar #generico-quimica_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-11 #generico-foliar #generico-quimica_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-11 #generico-foliar #generico-quimica_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-11 #generico-foliar #generico-organica_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-11 #generico-foliar #generico-organica_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-11 #generico-foliar #generico-organica_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-11 #generico-foliar #generico-mineral_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-11 #generico-foliar #generico-mineral_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-11 #generico-foliar #generico-mineral_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
				
				$("#frm-11 #generico-foliar #generico-acondicionador_1 #fnutricion_tipo_ap").html(respuesta.select_fuente);
				$("#frm-11 #generico-foliar #generico-acondicionador_1 #fnutricion_tipo_unidad").html(respuesta.select_unidad);
				$("#frm-11 #generico-foliar #generico-acondicionador_1 #fnutricion_etapa_fenologica").html(respuesta.select_etapa_fenologica);
	
}

function congelarFrm11(){
			var id_generico = $("#gen-id").val();
			$(".date", "#frm-11").removeAttr('disabled');
			var frm = new FormData(document.getElementById("frm-11"));
			frm.append("id", $("#gen-id").val()); 
			frm.append("tipo", $("#gen-type").val());
				$.ajax({
					url: "/backend/diagnosticos/guardarfertilizacion/",
					type: "POST",
					data: frm,
					processData: false,  
					contentType: false,
				}).done(function(respuesta){
					
					try
					{
						$(".date", "#frm-11").attr('disabled', 'disabled');
						respuesta = jQuery.parseJSON(respuesta);
						if(respuesta.ok !== undefined)
						{
							
								$.ajax({
								url: "/backend/diagnosticos/congelarfertilizacion/",
								type: "POST",
								data: { id: $("#gen-id").val() ,  tipo:$("#gen-type").val() },
								async: false
								}).done(function(respuesta){
									try
									{
										respuesta = jQuery.parseJSON(respuesta);
										if(respuesta.ok !== undefined){
											ShowDialogBox("Congelación exitosa", "El formulario ha sido congelado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/fertilizacion/1");
											$('#frm-11 *').attr("disabled", "disabled").unbind();	
										}
										else
											ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
									}
									catch(err)
									{
										ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de congelar el formulario, por favor inténtelo nuevamente", "Ok", "", false, "");
									}
								});
							//ShowDialogBox("Guardado", "La informacion se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val());
						}
						else 
							ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
					}
					catch(error)
					{
						ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor intentelo nuevamente: " + error.message, "Ok", "", false, "");
					}
				});
}
function realizo_f(elemento){
	if($('#realizo_'+elemento).val()=='2'){//no
		//deshabilitamos los dates
		$("#"+elemento).find('.input-group-addon').addClass("hide");
		//deshabilitamos los demas inputs
		$("#"+elemento).find('select, input, i').not('#realizo_'+elemento).each(function(index, element){
			$(this).attr("disabled","disabled").not('.input-group-addon');
			//quitamos la clase requerida
			$(this).removeClass("required").not('.input-group-addon');
			//borramos la informacion de los inputs o select
			if($(this).is('select')){
				if($(this).find('option[value="0"]').length>0){
					$(this).val('0');//.css("background-color","#F00");
				}else{
					$(this).val('2').trigger("change");//.css("background-color","#FF0");
				}
			}else if($(this).is('input')){
				$(this).val('');
			}
		});
		
	}else{//si
		//habilitamos los dates
		$("#"+elemento).find('.input-group-addon').removeClass("hide");
		//habilitamos los demas inputs
		$("#"+elemento).find('select, input, i').not('#realizo_'+elemento+', #fertilizacion_agrega_fila_1, #fertilizacion_'+elemento+'_id, #mecanizada_div_select, #mecanizada_div_select_div_propia_combustible, #mecanizada_div_select_div_propia_operador, #mecanizada_div_select_div_rentada_costo').each(function(index, element){
			$(this).removeAttr("disabled").not('.input-group-addon');
			//habilitamos la clase requerida
			$(this).addClass("required").not('.input-group-addon');
		});
		$("#"+elemento+" #fertilizacion_agrega_fila_1").removeAttr("disabled");
	}
	$('#fertilizacion_'+elemento+'_hubo').trigger("change");
	
	
}

function validaFrm11()
{
	return true;
	var bandera=true;
	$("#frm-11 #antes tr").not("#frm-11 #antes #generico-antes").each(function(i){
		$(this).find(".required").each(function(index, element) {
			if($(this).val()=="" || $(this).val()=="0")
			{
				$(this).focus();	
				bandera=false;
			}
		});
	});
	$("#frm-11 #enla tr").not("#frm-11 #enla #generico-enla").each(function(i){
		$(this).find(".required").each(function(index, element) {
			if($(this).val()=="" || $(this).val()=="0")
			{
				$(this).focus();	
				bandera=false;
			}
		});
	});
	$("#frm-11 #durante tr").not("#frm-11 #durante #generico-durante, #frm-11 #durante #generico-quimica, #frm-11 #durante #generico-mineral, #frm-11 #durante #generico-organica, #frm-11 #durante #generico-acondicionador").each(function(i){
		$(this).find(".required").not('#agregar').each(function(index, element) {
			
			if($(this).val()=="" || $(this).val()=="0")
			{
				console.log(this.id);
				$(this).focus();	
				bandera=false;
			}
		});
	});

	return bandera;
}

function changeOcultaMuestra(tabla){
	
	$("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").removeClass("required");
	$("#frm-11 #" + tabla  + " #manual_div #manual_div_costo_jornal").removeClass("required");
	$("#frm-11 #" + tabla  + " #fertilizacion_"+tabla+"_tipo_ap_select").removeClass("required");
	$("#frm-11 #" + tabla  + " #fertilizacion_"+tabla+"_hubo").removeClass("required");
	
	
	
	$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select_div_propia_combustible").removeClass("required");
	$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select_div_propia_operador").removeClass("required");
	$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").removeClass("required");
		
	if($("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"_hubo").val()==1){
			$("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"_tipo_ap").removeClass("hide");
			$("#frm-11 #"+tabla+" #manual_div").removeClass("hide");
			$("#frm-11 #"+tabla+" #mecanizada_div").addClass("hide");
			
			$("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").addClass("required");
			$("#frm-11 #" + tabla  + " #manual_div #manual_div_costo_jornal").addClass("required");
	}else{
		
		$("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"_tipo_ap").addClass("hide");
		$("#frm-11 #"+tabla+" #manual_div").addClass("hide");
		$("#frm-11 #"+tabla+" #mecanizada_div").addClass("hide");
	}

}
function evaluaCambioAplicacionFrm11(tabla,elemento){
	//alert("entra a evaluaCambioAplicacionFrm11: "+tabla+" "+elemento);
	$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select_div_propia_combustible").removeClass("required").removeAttr("disabled");
	$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select_div_propia_operador").removeClass("required").removeAttr("disabled");
	
	$("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").removeClass("required").removeAttr("disabled");
	$("#frm-11 #" + tabla  + " #manual_div #manual_div_costo_jornal").removeClass("required").removeAttr("disabled");
	$("#frm-11 #" + tabla  + " #mecanizada_div  #mecanizada_div_select").removeClass("required").removeAttr("disabled");
	//$("#frm-11 #" + tabla+" #"+elemento).css("background-color","#F00");
	if($("#frm-11 #" + tabla+" #"+elemento).val() == 1){
		//mecanizada
		$("#frm-11 #" + tabla  + " #mecanizada_div").removeClass("hide");
		$("#frm-11 #" + tabla + " #manual_div").addClass("hide");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia").removeClass("hide");
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada").addClass("hide");

		$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select option:eq(0)").prop('selected', true);
		$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('').addClass("required");
		$("#frm-11 #" + tabla  + " #mecanizada_div #mecanizada_div_select_div_propia_operador").val('').addClass("required");

	}else{
		//manual
		$("#frm-11 #" + tabla  + " #manual_div ").removeClass("hide");
		$("#frm-11 #" + tabla + " #mecanizada_div").addClass("hide");
		//$("#aerea_div_" + tipo + "_" + num).addClass("hide");
/*		if(($("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").val()!='')){
			$("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").val('').addClass("required");
		}else{
			$("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").addClass("required");
		}*/
		$("#frm-11 #" + tabla  + " #manual_div #manual_div_no_jornales").val('').addClass("required");
		$("#frm-11 #" + tabla  + " #manual_div #manual_div_costo_jornal").val('').addClass("required");

	}
}
function evaluaCambioAplicacion2Frm11(tabla){
	$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('').removeClass("required").removeAttr("disabled");
	$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador").val('').removeClass("required").removeAttr("disabled");
	
	$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").removeClass("required").removeAttr("disabled");
	
	if($("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select").val() == 1){

		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia").removeClass("hide");
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada").addClass("hide");

		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select option:eq(0)").prop('selected', true);
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('').addClass("required");
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador").val('').addClass("required");
		
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val('');

	}else{

		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia").addClass("hide");
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada").removeClass("hide");
		
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_combustible").val('');
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_propia_operador").val('');
		
		$("#frm-11 #" + tabla+" #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").val('').addClass("required");
		

	}
}
/*function calculaImporteFrm11(tabla,idPadre,id,hermano,receptor)
{
	//obtenemos el valor del campo
	if(idPadre==''){
		//es un valor de costo por mezclado
		selector = "#frm-11 #"+tabla+" #"+id;
		selector2 = "#frm-11 #"+tabla+" #"+hermano;
		selector3 = "#frm-11 #"+tabla+" #"+receptor;
	}else{
		//es un valor de la fila de concepto
		selector = "#frm-11 #"+tabla+" #"+idPadre+" #"+id;
		selector2 = "#frm-11 #"+tabla+" #"+idPadre+" #"+hermano;
		selector3 = "#frm-11 #"+tabla+" #"+idPadre+" #"+receptor;
		
	}
	var valor = parseFloat($(selector).val());
	var hermano = parseFloat($(selector2).val());
	if(valor>0 && hermano>0){
		$(selector3).val(valor*hermano);
	}else{
		$(selector3).val('');
	}
}*/

function agregarFilaExtraFrm11(tabla, elemento,idd)
{
	var ultimo_id = $("#frm-11 #"+tabla+" [id^='fertilizacion_"+tabla+"-c"+elemento+"_']").not("").last().attr("id").valueOf();
	
	ultimo_id = ultimo_id.split("-")[1];
	actual = ultimo_id.split("_")[1];
	//actual = parseInt(ultimo_id[1]);
	var contador = (parseInt(actual)+1);
	//alert(ultimo_id);
	//if(elemento!="quimica" && elemento!="organica" && elemento!="mineral" && elemento!="acondicionador" ){
		console.log($("#frm-11 #"+tabla+" #generico-"+elemento).html());
		$("#frm-11 #fertilizacion_"+tabla+"-c"+elemento+"_"+actual).after
		(

				$("#frm-11 #"+tabla+" #generico-"+elemento).clone().removeClass("hide").attr("id", "fertilizacion_"+tabla+"-c"+elemento+"_"+contador)
		);
		if(elemento=='quimica' || elemento=='organica' || elemento=='mineral' || elemento=='acondicionador'){
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #eliminar-fila").attr("id",elemento+"_eliminar_"+ contador ).attr("onClick", "eliminarConceptoPorId('" + tabla + "', 'c"+elemento+"_"+contador+"', '"+idd+"')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger").removeClass("required");
			//$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_fuente").attr("name", "fertilizacion_"+tabla+"[concepto]["+contador+"][fuente]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_empresa").attr("name", "fertilizacion_"+tabla+"["+elemento+"]["+contador+"][empresa]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_marca").attr("name", "fertilizacion_"+tabla+"["+elemento+"]["+contador+"][marca]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_unidad").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][unidad]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_cantidad").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm11(this)");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_pu").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm11(this)");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_importe").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][importe]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_frealizacion").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][frealizacion]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_etapa_fenologica").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][etapa_fenologica]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"-"+elemento+"_innovacion").attr("name","fertilizacion_"+tabla+"["+elemento+"]["+contador+"][innovacion]");
			
		}else{
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #eliminar-fila").attr("id",elemento+"_eliminar_"+ contador ).attr("onClick", "eliminarConceptoPorId('" + tabla + "', 'c"+elemento+"_"+contador+"', '"+idd+"')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger");
			//$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_fuente").attr("name", "fertilizacion_"+tabla+"[concepto]["+contador+"][fuente]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_empresa").attr("name", "fertilizacion_"+tabla+"[concepto]["+contador+"][empresa]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_marca").attr("name", "fertilizacion_"+tabla+"[concepto]["+contador+"][marca]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_unidad").attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][unidad]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_cantidad")/*.attr("id","fsiembra_semilla_alto_cantidad_"+contador)*/.attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][cantidad]").attr("onkeyup", "calculaImporteFrm11(this)");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_pu")/*.attr("id","fsiembra_semilla_alto_pu_"+contador)*/.attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][precio]").attr("onkeyup", "calculaImporteFrm11(this)");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_importe")/*.attr("id","fsiembra_semilla_alto_importe_"+contador)*/.attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][importe]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_frealizacion")/*.attr("id","fsiembra_semilla_alto_frealizacion_"+contador)*/.attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][frealizacion]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" .date").datepicker({ format: 'yyyy-mm-dd' });
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_etapa_fenologica")/*.attr("id","fsiembra_etapa_fenologica_"+contador)*/.attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][etapa_fenologica]");
			$("#frm-11 #" + tabla + " #fertilizacion_"+tabla+"-c"+elemento+"_"+contador+" #fertilizacion_"+tabla+"_innovacion")/*.attr("id","fsiembra_semilla_alto_innovacion_"+contador)*/.attr("name","fertilizacion_"+tabla+"[concepto]["+contador+"][innovacion]");
		}
}

// Evento que selecciona la fila y la elimina 
function eliminarConceptoPorId(tabla, elemento,idd)
{	
	if(idd=='undefined'){
		//alert("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"-"+elemento);
		$("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"-"+elemento).remove();
	}else{
		var direccion;
		if(idd.indexOf("f")<0){
			direccion  = "/backend/diagnosticos/eliminarconceptofertilizacion/";
		}else{
			idd=idd.substr(1);
			direccion  = "/backend/diagnosticos/eliminarconceptoffertilizacion/";
		}

		$.ajax({
			url: direccion,
			type: "POST",
			data: { id: idd, tipo:$("#gen-type").val() },
			async: false
		}).done(function(respuesta){
			respuesta = jQuery.parseJSON(respuesta);
			if(respuesta !== undefined){
				$("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"-"+elemento).remove();
			}else{
				$("#frm-11 #"+tabla+" #fertilizacion_"+tabla+"-"+elemento).remove();
				ShowDialogBox("Ha ocurrido un error", "No se han podido eliminar los conceptos, intentelo nuevamente", "Ok", "", false, "");	
			}
		});
	}
	
}
/*function eliminarTablasFrm11(count){
	tabla1 = "tfsiembra_"+count;
	tabla2 = "tfoliar_"+count;
	
	id = $("#frm-11 #tabla_div_siembra #tfsiembra_"+count+" #tfsiembra_"+count+"_id").val();
	if(parseInt(id)>0){
		$.ajax({
			url: "/backend/diagnosticos/eliminarfertilizacion/",
			type: "POST",
			data: { id: id, tipo:$("#gen-type").val() },
			async: false
		}).done(function(respuesta){
			respuesta = jQuery.parseJSON(respuesta);
			if(respuesta !== undefined){
				$("#"+tabla1).remove();
				$("#"+tabla2).remove();
			}else{
				ShowDialogBox("Ha ocurrido un error", "No se han podido eliminar las tablas, intentelo nuevamente", "Ok", "", false, "");	
			}
		});

	}else{
		$("#"+tabla1).remove();
		$("#"+tabla2).remove();	
	}
	
	
}

function agregarTablaFrm11()
{
	//obtenemos el ultimo conjunto de tablas y apartir de ahi obtenemos el contador	
	var ultimo_id = $("#frm-11 #tabla_div_siembra [id^='tfsiembra']").last().attr("id").valueOf();
	ultimo_id = ultimo_id.split("_");
	actual = parseInt(ultimo_id[1]);
	var count = (actual+1);
	
	if(count==2){
		$("#frm-11 #tabla_div_siembra #tfsiembra_" + actual).after( 
			$("#frm-11 #generico-siembra").clone().removeClass("hide").attr("id", "tfsiembra_" + count).addClass("fsiembra")
		);
		$("#frm-11 #tabla_div_siembra #tfsiembra_" + count).after( 
			$("#frm-11 #generico-foliar").clone().removeClass("hide").attr("id", "tfoliar_" + count).addClass("foliar")
		);
	}else{
		$("#frm-11 #tabla_div_siembra #tfoliar_" + actual).after( 
			$("#frm-11 #generico-siembra").clone().removeClass("hide").attr("id", "tfsiembra_" + count).addClass("fsiembra")
		);
		$("#frm-11 #tabla_div_siembra #tfsiembra_" + count).after( 
			$("#frm-11 #generico-foliar").clone().removeClass("hide").attr("id","tfoliar_" + count).addClass("foliar")
		);
	}

	var tipo = "fsiembra";
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_agrega_fila_1").attr("onClick", "agregarFilaExtraFrm11('tfsiembra_"+count+"', 'fsiembra-concepto')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #eliminar-tabla").attr("id", "eliminar_" + count).attr("onClick", "eliminarTablasFrm11('"+count+"')");

	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_tipo_ap_select_1").attr("onChange", "evaluaCambioAplicacionFrm11('tfsiembra_"+count+"', 'fsiembra_tipo_ap_select_1')");
	
	//asignamos el onkeyup en cantidad y precio para que se calcule el importe
	//fsiembra
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_pu_1").attr("onkeyup", "calculaImporteFrm11('tfsiembra_"+count+"','fsiembra-concepto_1', 'fsiembra_semilla_alto_pu_1','fsiembra_semilla_alto_cantidad_1','fsiembra_semilla_alto_importe_1')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_cantidad_1").attr("onkeyup", "calculaImporteFrm11('tfsiembra_"+count+"','fsiembra-concepto_1', 'fsiembra_semilla_alto_cantidad_1','fsiembra_semilla_alto_pu_1','fsiembra_semilla_alto_importe_1')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_cantidad_1").attr("onkeyup", "calculaImporteFrm11('tfsiembra_"+count+"','', 'fsiembra_mezclado_cantidad_1','fsiembra_mezclado_pu_1','fsiembra_mezclado_importe_1')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_pu_1").attr("onkeyup", "calculaImporteFrm11('tfsiembra_"+count+"','', 'fsiembra_mezclado_pu_1','fsiembra_mezclado_cantidad_1','fsiembra_mezclado_importe_1')");
	//controlamos los id ocultos
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #oculto").attr("id", "tfsiembra_" + (count) + "_id").attr("name", "tfsiembra_" + (count) + "[id]");
	//modificamos los names
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_nombre_1").attr("name", "tfsiembra_" + (count) + "[nombre]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_tipo_fu_select_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][fuente]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_tipo_unidad_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][unidad]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_cantidad_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][cantidad]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_pu_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][precio]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_importe_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][importe]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_frealizacion_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][frealizacion]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 .date").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_etapa_fenologica_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][etapa_fenologica]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra-concepto_1 #fsiembra_semilla_alto_innovacion_1").attr("name", "tfsiembra_" + (count) + "[concepto][1][innovacion]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_tipo_unidad_1").attr("name", "tfsiembra_" + (count) + "[mezclado][unidad]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_cantidad_1").attr("name", "tfsiembra_" + (count) + "[mezclado][cantidad]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_pu_1").attr("name", "tfsiembra_" + (count) + "[mezclado][precio]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_importe_1").attr("name", "tfsiembra_" + (count) + "[mezclado][importe]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_frealizacion_1").attr("name", "tfsiembra_" + (count) + "[mezclado][frealizacion]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " .date").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_etapa_fenologica_1").attr("name", "tfsiembra_" + (count) + "[mezclado][etapa_fenologica]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_mezclado_innovacion_1").attr("name", "tfsiembra_" + (count) + "[mezclado][innovacion]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_es_fert_select_1").attr("name", "tfsiembra_" + (count) + "[mezclado][es_fertilizacion]").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #fsiembra_tipo_ap_div_1 #fsiembra_tipo_ap_select_1").attr("name", "tfsiembra_" + (count) + "[mezclado][aplicacion]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #manual_div #manual_div_no_jornales").attr("name", "tfsiembra_" + (count) + "[mezclado][numero_jornales]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #manual_div #manual_div_costo_jornal").attr("name", "tfsiembra_" + (count) + "[mezclado][costo_jornal]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #mecanizada_div #mecanizada_div_select").attr("name", "tfsiembra_" + (count) + "[mezclado][tipo_mecanizacion]").attr("onchange","evaluaCambioAplicacion2Frm11('tfsiembra_" + (count) +"')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_combustible").attr("name", "tfsiembra_" + (count) + "[mezclado][costo_combustible]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #mecanizada_div #mecanizada_div_select_div_propia #mecanizada_div_select_div_propia_operador").attr("name", "tfsiembra_" + (count) + "[mezclado][costo_operador]");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #mecanizada_div #mecanizada_div_select_div_rentada #mecanizada_div_select_div_rentada_costo").attr("name", "tfsiembra_" + (count) + "[mezclado][monto_renta]");

	
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #agregar").attr("onClick", "agregarFilaExtraFrm11('tfoliar_"+count+"', 'generico-quimica')");
	//asgnamos los names
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_tipo_ap").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][fuente]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_tipo_unidad").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][unidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_cantidad").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][cantidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_pu").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][precio]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_importe").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][importe]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_frealizacion").attr("id","tfoliar_"+count+"_quimica_frealizacion_1").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][frealizacion]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 .date").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_etapa_fenologica").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][etapa_fenologica]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_innovacion").attr("name", "tfsiembra_"+count+"[foliar_quimica][1][innovacion]");

	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_tipo_ap").attr("name", "tfsiembra_"+count+"[foliar_organica][1][fuente]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_tipo_unidad").attr("name", "tfsiembra_"+count+"[foliar_organica][1][unidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_cantidad").attr("name", "tfsiembra_"+count+"[foliar_organica][1][cantidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_pu").attr("name", "tfsiembra_"+count+"[foliar_organica][1][precio]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_importe").attr("name", "tfsiembra_"+count+"[foliar_organica][1][importe]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_frealizacion").attr("id","tfoliar_"+count+"_organica_frealizacion_1").attr("name", "tfsiembra_"+count+"[foliar_organica][1][frealizacion]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 .date").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_etapa_fenologica").attr("name", "tfsiembra_"+count+"[foliar_organica][1][etapa_fenologica]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_innovacion").attr("name", "tfsiembra_"+count+"[foliar_organica][1][innovacion]");

	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_tipo_ap").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][fuente]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_tipo_unidad").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][unidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_cantidad").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][cantidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_pu").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][precio]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_importe").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][importe]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_frealizacion").attr("id","tfoliar_"+count+"_mineral_frealizacion_1").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][frealizacion]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 .date").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_etapa_fenologica").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][etapa_fenologica]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_innovacion").attr("name", "tfsiembra_"+count+"[foliar_mineral][1][innovacion]");

	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_tipo_ap").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][fuente]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_tipo_unidad").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][unidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_cantidad").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][cantidad]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_pu").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][precio]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_importe").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][importe]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_frealizacion").attr("id","tfoliar_"+count+"_acondicionador_frealizacion_1").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][frealizacion]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 .date").datepicker({ format: 'yyyy-mm-dd' });
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_etapa_fenologica").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][etapa_fenologica]");
	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_innovacion").attr("name", "tfsiembra_"+count+"[foliar_acondicionador][1][innovacion]");
	
	//asignamos el onkeyup en cantidad y precio para que se calcule el importe
	//quimica
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_cantidad").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-quimica_1', 'fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_pu','fnutricion_semilla_alto_importe')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-quimica_1 #fnutricion_semilla_alto_pu").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-quimica_1', 'fnutricion_semilla_alto_pu','fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_importe')");

	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-organica_1 #agregar").attr("onClick", "agregarFilaExtraFrm11('tfoliar_"+count+"', 'generico-organica')");
	//organica
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_cantidad").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-organica_1', 'fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_pu','fnutricion_semilla_alto_importe')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-organica_1 #fnutricion_semilla_alto_pu").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-organica_1', 'fnutricion_semilla_alto_pu','fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_importe')");

	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-mineral_1 #agregar").attr("onClick", "agregarFilaExtraFrm11('tfoliar_"+count+"', 'generico-mineral')");
	//mineral
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_cantidad").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-mineral_1', 'fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_pu','fnutricion_semilla_alto_importe')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-mineral_1 #fnutricion_semilla_alto_pu").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-mineral_1', 'fnutricion_semilla_alto_pu','fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_importe')");

	$("#frm-11 #tabla_div_siembra #tfoliar_" + (count) + " #generico-acondicionador_1 #agregar").attr("onClick", "agregarFilaExtraFrm11('tfoliar_"+count+"', 'generico-acondicionador')");
	//acondicionador
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_cantidad").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-acondicionador_1', 'fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_pu','fnutricion_semilla_alto_importe')");
	$("#frm-11 #tabla_div_siembra #tfsiembra_" + (count) + " #generico-acondicionador_1 #fnutricion_semilla_alto_pu").attr("onkeyup", "calculaImporteFrm11('tfoliar_"+count+"','generico-acondicionador_1', 'fnutricion_semilla_alto_pu','fnutricion_semilla_alto_cantidad','fnutricion_semilla_alto_importe')");

}
*/
