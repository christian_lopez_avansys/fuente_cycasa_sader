function validar()
{
    nombre = document.getElementById("nombre").value;
    comentarios = document.getElementById("comentarios").value;
    telefono = document.getElementById("telefono").value;
    email = document.getElementById("email").value;
    empresa = document.getElementById("empresa").value;

	if( nombre == null || nombre.length == 0 || /^\s+$/.test(nombre) ) {
		$("#msgErrorContacto").html("Nombre es requerido").show();
	 	return false;
	}

	if( (telefono == null || telefono.length == 0 || /^\s+$/.test(telefono)) && (email == null || email.length == 0 || /^\s+$/.test(email))) {
		$("#msgErrorContacto").html("Teléfono o email son requeridos").show();
	 	return false;
	}
	else
	{
		if( email != null && email.length != 0 ) {
			var patron=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			//if( !(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(email)) ) {
			if( email.search(patron)!=0 ) {
				$("#msgErrorContacto").html("Ingrese una direccion de email valida").show();
			 	return false;
			}
		}
	}

	if( comentarios == null || comentarios.length == 0 || /^\s+$/.test(comentarios) ) {
		$("#msgErrorContacto").html("Comentarios son requeridos").show();
	 	return false;
	}

	$("#msgErrorContacto").hide();

	return true;
}


                    



