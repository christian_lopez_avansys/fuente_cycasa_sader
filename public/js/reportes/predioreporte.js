google.load("visualization", "1", {packages:["corechart"]});
function filtrarreportepredio(){
    var filtros = {
        estado: $('#filtroEstado').val(),
        municipio: $('#filtroMunicipio').val(),
        localidad: $('#filtroLocalidad').val(),
        territorio: $('#filtroTerritorio').val(),
        agroconsultor: $('#filtroAgroconsultor').val(),
        tipo_predio: $('#filtroTipoPredio').val(),
        clave: $('#filtroClave').val(),
        region: $('#filtroRegion').val()
    };

    $.ajax({
        url: "/backend/reportes/reportequery",
        type: "POST",
        data: filtros,
        async: false
    }).done(function(response){
        var res = JSON.parse(response);
        $('#tabla-reporte-predio').html(res.tabla_html);
        var data = google.visualization.arrayToDataTable(
            res.tabla_js
          );

          var options = {
            title: 'Tipos de insumos utilizados',
            width: 600,
            height: 400,
            legend: { position: 'top', maxLines: 6 },
            bar: { groupWidth: '75%' },
            isStacked: true
          };

        var chart = new google.visualization.BarChart(document.getElementById('grafico-reporte-predio'));
        chart.draw(data, options);

        var dataTot = google.visualization.arrayToDataTable(
            res.tabla_totales_js
        );

        var options = {
          title: 'Totales',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('grafico-reporte-predio-totales'));
        chart.draw(dataTot, options);
    });
}