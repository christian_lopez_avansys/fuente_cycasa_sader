/* Declaración de variables */
function iniciaTablas(){
    tablas = {
        arcilla:        { tabla: "ARC", archivos: 4, parametro: "ARCILLA", resultados: new Array(), html: null, tipo: 1 },
        arena:          { tabla: "ARE", archivos: 4, parametro: "ARENA", resultados: new Array(), html: null, tipo: 1 },
        cah:            { tabla: "CAH", archivos: 4, parametro: "CAH", resultados: new Array(), html: null, tipo: 2 },
        cic:            { tabla: "CIC", archivos: 4, parametro: "CIC", resultados: new Array(), html: null, tipo: 2 },
        iht:            { tabla: "IHT", archivos: 4, parametro: "IH", resultados: new Array(), html: null, tipo: 2 },
        ip:             { tabla: "IP", archivos: 4, parametro: "IND_PROD", resultados: new Array(), html: null, tipo: 2 },
        limo:           { tabla: "LIM", archivos: 4, parametro: "LIMO", resultados: new Array(), html: null, tipo: 2 },
        mo:             { tabla: "MO", archivos: 4, parametro: "MO", resultados: new Array(), html: null, tipo: 2 },
        ph:             { tabla: "PH", archivos: 4, parametro: "pH", resultados: new Array(), html: null, tipo: 2 },
        profundidad:    { tabla: "PRO", archivos: 4, parametro: "PROF", resultados: new Array(), html: null, tipo: 1 },
        sb:             { tabla: "SB", archivos: 4, parametro: "SB", resultados: new Array(), html: null, tipo: 2 },
        textura:        { tabla: "TEX", archivos: 4, parametro: "CLASE_TEXT", resultados: new Array(), html: null, tipo: 1 },
        fc:             { tabla: "FC", archivos: 4, parametro: "VAR_LIM", resultados: new Array(), html: null, tipo: 2 },
        qb:             { tabla: "QB", archivos: 4, parametro: "VAR_LIM", resultados: new Array(), html: null, tipo: 2 },
        azufre:             { tabla: "AZ", archivos: 4, parametro: "AZUFRE", resultados: new Array(), html: null, tipo: 2 },
        fosforo:             { tabla: "FS", archivos: 4, parametro: "FOSFORO", resultados: new Array(), html: null, tipo: 2 },
        aluminio:       { tabla: "AL", archivos: 3, parametro: "ALUMINIO", resultados: new Array(), html: null, tipo: 2 },
        boro:             { tabla: "BO", archivos: 4, parametro: "BORO", resultados: new Array(), html: null, tipo: 2 },
        sodio:             { tabla: "SD", archivos: 4, parametro: "SODIO", resultados: new Array(), html: null, tipo: 2 },
        potasio:             { tabla: "PT", archivos: 4, parametro: "POTASIO", resultados: new Array(), html: null, tipo: 2 },
        magnesio:             { tabla: "MG", archivos: 4, parametro: "MAGNESIO", resultados: new Array(), html: null, tipo: 2 },
        manganeso:            { tabla: "MAN", archivos:4, parametro: "MANGANESO", resultados: new Array(), html: null, tipo: 2 },
        cobre:             { tabla: "CB", archivos: 4, parametro: "COBRE", resultados: new Array(), html: null, tipo: 2 },
        calcio:             { tabla: "CA", archivos: 4, parametro: "CALCIO", resultados: new Array(), html: null, tipo: 2 },
        ce:             { tabla: "CE", archivos: 4, parametro: "CE", resultados: new Array(), html: null, tipo: 2 },
        hr:             { tabla: "HR", archivos: 4, parametro: "HIERRO", resultados: new Array(), html: null, tipo: 2 },
        zc:             { tabla: "ZC", archivos: 4, parametro: "ZINC", resultados: new Array(), html: null, tipo: 2 }
    };

    //mostrarZonaAgricola();
}//function
iniciaTablas();

var marker = null;
var bubble = null;
var lat = null;
var lng = null;
var variable = "";
var dispo = true;
var pre = null;
var counter = null;
var coords = null;
var changing = false;
var type_change = null;
$("#wrapper").css("height", ($(document).height()));
$("#main").css("height", ($("#wrapper").height() - $("#header").height()));
$("#map-canvas").css("height", ($("#main").height()));
// Initialize the platform object:
var platform = new H.service.Platform({
'apikey': '7pGCYoE3M6KwXDBBq4aD0I9GDQr893rk8uRTsPEiZy8'
});
// Obtain the default map types from the platform object
var defaultLayers = platform.createDefaultLayers();

// Instantiate (and display) a map object:
var map = new H.Map(
    document.getElementById('map-canvas'),
    defaultLayers.raster.satellite.map,
    {
        zoom: 5,
        center: { lng:-103.0161594, lat:22.8140445 }
    }
);
var mapEvents = new H.mapevents.MapEvents(map);
map.addEventListener('tap', clicMapaValor);
var behavior = new H.mapevents.Behavior(mapEvents);
var ui = new H.ui.UI.createDefault(map, defaultLayers);
var pixelProy=new H.geo.PixelProjection();
//var contenedor = new nokia.maps.map.Container();
var contenedor = new H.map.Group();
var poligono = null;
var trazo_activo = false;
var punteros = new Array();
var clicpoligono = false;
var idpoligono = null;
var poligonos_productor = new Array();

/**
###########################################################################################
############################ CARGA DEL DOCUMENTO ##########################################
###########################################################################################
**/

$(document).ready(function() 
{   
    try
    {

    }
    catch(error)
    {
        window.location = window.location;
    }

    /* Limpiamos la información de los filtros */
    limpiarInfo();

    /* Cerrar la informacion de las variables */
    $('.closeVariables').click(function(){
        $('.tableVariables, .closeVariables').hide();
    });

    /* Cerrar la informacion de las busquedas */
    $('.btnCloseBusqueda').click(function(){
        $('.tablaBusqueda, .btnCloseBusqueda').hide();
        $(".btnLeft2").show();
    });

    $('.btnCloseValores').click(function(){
        $('.tableValores, .btnCloseValores, .trzPoligono, #tablaVariables').hide();
        $('.btnMI').show();
    });

    /* Loader */
    //mostrarLoading("Cargando configuraciones. Espere un momento");

    $('#sbnvid').click(function(){
        //alert("aqui");
        if($("#hsbnvid").hasClass("fa-caret-up"))
        {
            $("#hsbmpid").removeClass("fa-caret-down").addClass("fa-caret-up");
            $("#hsbnvid").removeClass("fa-caret-up").addClass("fa-caret-down");
            $(".tbnvsb").hide("slow", function() {
                $(".tbmpsb").show("slow", function() {
                    // Animation complete.
                })
            });
        }
        else
            $('#sbmpid').trigger("click");
    });

    $('#sbmpid').click(function(){
        if($("#hsbmpid").hasClass("fa-caret-up"))
        {
            $("#hsbnvid").removeClass("fa-caret-down").addClass("fa-caret-up");
            $("#hsbmpid").removeClass("fa-caret-up").addClass("fa-caret-down");
            $(".tbmpsb").hide("slow", function() {
                $(".tbnvsb").show("slow", function() {
                    // Animation complete.
                })
            });
        }
        else
            $('#sbnvid').trigger("click");
    });

    /* Indicamos que se busquen los municipios del estado elegido */
    $('#search_select_estado').change(function()
    {
        $("#search_select_localidad").html("<option value='0'>Seleccione una localidad...</option>");
        $.ajax({
            type: 'POST',
            url:"/backend/index/municipios/id/"+$('#search_select_estado').val(),
            success:
            function respuesta(res)
            {
               $('#search_select_municipio').html(res);
            }
        });

        if($("#search_select_estado").val() != 0)
        {
            deshabilitaCoordenadas();
            deshabilitaProductores();
        }
        else 
        {
            habilitaCoordenadas();
            habilitaProductores();
        }
    });

    $('#estado_id').change(function()
    {
        $("#search_select_localidad").html("<option value='0'>Seleccione una localidad...</option>");
        $.ajax({
            type: 'POST',
            url:"/backend/index/municipios/id/"+$('#estado_id').val(),
            success:
            function respuesta(res)
            {
               $('#municipio_id').html(res);
            }
        });
    });

    /* Indicamos que se busquen las localidades del municipio elegido */
    $('#search_select_municipio').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/backend/index/localidades/id/"+$('#search_select_municipio').val(),
            success:
            function respuesta(res)
            {
               $('#search_select_localidad').html(res);
            }
        });
    });

    $('#municipio_id').change(function()
    {
        $.ajax({
            type: 'POST',
            url:"/backend/index/localidades/id/"+$('#municipio_id').val(),
            success:
            function respuesta(res)
            {
               $('#localidad_id').html(res);
            }
        });
    });

    $('#pertenece_etnia').change(function()
    {
        if($('#pertenece_etnia').val()==1)
        {
            $("#campo_etnia").show();
            $("#etnia_id").attr("required","required");            
        }
        else
        {
            $("#campo_etnia").hide();
            $("#etnia_id").attr("required","required");
        }
    });


    /* Indicamos que se busquen por productor */
    $('#search_select_productor').change(function()
    {
        if($("#search_select_productor").val() != 0)
        {
            deshabilitaCoordenadas();
            deshabilitaFiltros();
        }
        else 
        {
            habilitaCoordenadas();
            habilitaFiltros();
        }   
    });

    

    /* Indicamos que se deshabilite el filtro por localidad si se usa el de coordinadas */
    $('#search_input_lat').keyup(function()
    {
        var la = $('#search_input_lat').val();
        var lo = $('#search_input_lng').val();
        if((la == '' || la == null) && (lo == '' || lo == null))
        {
            habilitaFiltros();
            habilitaProductores();
        }
        else
        {
            deshabilitaFiltros();
            deshabilitaProductores();
        }
    });

    /* Indicamos que se deshabilite el filtro por localidad si se usa el de coordinadas */
    $('#search_input_lng').keyup(function()
    {
        var la = $('#search_input_lat').val();
        var lo = $('#search_input_lng').val();
        if((la == '' || la == null) && (lo == '' || lo == null))
        {
            habilitaFiltros();
            habilitaProductores();
        }
        else
        {
            deshabilitaFiltros();
            deshabilitaProductores();
        }
    });

    /* 
        Indicamos el nivel de zoom que se hara dependiendo el tipo de filtro realizado.
        Tipo 1: Filtro por localidades.
        Tipo 2: Filtro por coordenadas.
    */
    /*map.addListener("mapviewchangeend", function () {
        if(changing)
        {
            if(type_change == 1) map.setZoomLevel(16);
            else map.setZoomLevel(26);
            type_change = null;
            changing = false;
            ocultarLoading();   
        }
    });

    /* Indicamos un loader al inicio de la pagina y hasta terminar de cargar el mapa */
    /*map.addListener("displayready", function () {
        map.objects.add(contenedor);
        dibujarCultivos();
        ocultarLoading();
    });*/
});

function activaSociedad(val)
{
    if(val=='1')
    {
        $("#campo_persona_juridica").show();
        $("#persona_juridica_id").attr("required","required");
    }
    else
    {
        $("#campo_persona_juridica").hide();
        $("#persona_juridica_id").removeAttr("required");
    }
}

function buscaCurp(e)
{
    console.log(e);
    if(e.keyCode==13 || e.target.name=='btnBuscarCurp')
    {
        $.ajax({
            type: 'POST',
            url:"/backend/index/curp/curp/"+$('#curp').val(),
            success:
            function respuesta(res)
            {
               if(res.data==false)
               {
                    $(".capturar").show();
                    $("#divCurpNoEncontrada").show();
                    $("#divCurpEncontrada").hide();

                    $("#apellido_paterno").val('').attr("readonly",false);
                    $("#apellido_materno").val('').attr("readonly",false);
                    $("#nombre").val('').attr("readonly",false);
                    $("#genero").val('').attr("readonly",false);
                    $("#ejido").val('').attr("readonly",false);
                    $("#estado_id").val('').attr("readonly",false).change();
                    $("#localidad_id").val('').attr("readonly",false);
                    $("#municipio_id").val('').attr("readonly",false).change();
                    $("#pertenece_etnia").val('').attr("readonly",false).change();
                    $("#etnia_id").val('').attr("readonly",false);
                    $("#sociedad_agricola").attr("disabled",false).prop("checked",false);
                    $("#sociedad_social").attr("disabled",false).prop("checked",false);
                    $("#sociedad_religiosa").attr("disabled",false).prop("checked",false);
                    $("#persona_juridica_id").val('').attr("readonly",false);
               }
               else
               {
                    $(".capturar").show();
                    $("#divCurpNoEncontrada").hide();
                    $("#divCurpEncontrada").show();
                    $("#apellido_paterno").val(res.apellido_paterno).attr("readonly","readonly");
                    $("#apellido_materno").val(res.apellido_materno).attr("readonly","readonly");
                    $("#nombre").val(res.nombre).attr("readonly","readonly");
                    $("#genero").val(res.genero).attr("readonly","readonly");
                    $("#ejido").val(res.ejido).attr("readonly","readonly");
                    $("#estado_id").val(res.estado_id).attr("readonly","readonly");
                    $.ajax({
                        type: 'POST',
                        url:"/backend/index/municipios/id/"+$('#estado_id').val(),
                        success:
                        function respuesta(resM)
                        {
                           $('#municipio_id').html(resM);
                           console.log(res.estado_id+"|"+res.municipio_id);
                           $("#municipio_id").val(res.estado_id+"|"+res.municipio_id).attr("readonly","readonly");
                            $.ajax({
                                type: 'POST',
                                url:"/backend/index/localidades/id/"+$('#municipio_id').val(),
                                success:
                                function respuesta(resL)
                                {
                                   $('#localidad_id').html(resL);
                                   $("#localidad_id").val(res.localidad_id).attr("readonly","readonly");
                                }
                            });
                        }
                    });
                    $("#pertenece_etnia").val(res.pertenece_etnia).attr("readonly","readonly").change();
                    $("#etnia_id").val(res.etnia_id).attr("readonly","readonly");
                    $("#etnia_id").change();
                    
                    if(res.sociedad_agricola == 1){
                        $("#sociedad_agricola").prop("checked", true);  
                        $("#sociedad_agricola").click();
                    }
                    if(res.sociedad_social  == 1)
                        $("#sociedad_social").prop("checked", true);
                    if(res.sociedad_religiosa  == 1)
                        $("#sociedad_religiosa").prop("checked", true);

                    $("#sociedad_agricola").attr("disabled","disabled");
                    $("#sociedad_social").attr("disabled","disabled");
                    $("#sociedad_religiosa").attr("disabled","disabled");
                                        
                    $("#persona_juridica_id").val(res.persona_juridica_id).attr("readonly","readonly");                    
               }
            }
        });
    }
}

$("#productor_form").validate(
{
   submitHandler: function(form) 
   {    
    $("#btnCerrarDatosGenerales,#btnGuardarDatosGenerales").hide();
      
     $(form).ajaxSubmit(
     {

        success:    function(response) 
        { 
            var datosResp=response.split("-");
            if(response!='')
            {                
                var datos = {};
                var i=0;                
                for(var marcador in punteros){
                    var puntero = punteros[marcador];
                    datos['lat_' + i] = puntero.b.lat;
                    datos['lng_' + i] = puntero.b.lng
                    i++;
                }
                var json = JSON.stringify(datos);
                alert("Guardado correcto");
                $('#modalProductor').modal('toggle');
                if(punteros.length>0)
                    window.location = '/backend/diagnosticos/agregar/tipo/1/id/'+datosResp[0]+'/predio/'+datosResp[1]+'/vertices/' + json;
                else
                    window.location = '/backend/diagnosticos/agregar/tipo/1/id/'+datosResp[0]+'/predio/' + datosResp[1];
            }
            else
            {   
                $("#btnCerrarDatosGenerales,#btnGuardarDatosGenerales").show();
                alert("Error al guardar");
            }
        }
     });
   }
});




function guardarProductor()
{    
    if(!$("#apellido_paterno").val()){
        return false;
    }
    $("#productor_form").submit();

}



$("#sociedad_agricola").click(function(){
    if($(this).is(':checked'))
        $(".capturar").show();
    else
        $(".capturar_juridica").hide();
});



/**
###########################################################################################
############################ FILTROS DE UBICACIONES #######################################
###########################################################################################
**/

function clicMapaValor(evt)
{
    if(dispo)
    {
        $(".closeVariables").trigger("click");
        $(".btnCloseBusqueda").trigger("click");
        $(".btnMI").hide();
        //mostrarLoading("Cargando información sobre el punto. Espere un momento");
        $('#notifications').jGrowl('close');
        console.log(evt);
        var coord = map.screenToGeo(evt.currentPointer.viewportX,evt.currentPointer.viewportY);
        //pixelProy.xyToGeo(evt.pointers[0].viewportX, evt.pointers[0].viewportY);
        console.log(coord);
        coords = coord;
        dispo = false;
        if(bubble) bubbleContainer.closeBubble(bubble);
        if(marker) map.removeObject(marker);
        lat = coord.lat.toFixed(6);
        lng = coord.lng.toFixed(6);
        if(!clicpoligono) shapesEnPunto(lng, lat, null);
        else shapesEnPunto(lng, lat, idpoligono);
    }
    else
    {
        $('#notifications').jGrowl("Se esta realizando una busqueda", { glue: 'before' });
    }
}

function buscarPredio()
{
    $(".btnLeft2").hide();
    $(".btnCloseBusqueda").show();
    $(".tablaBusqueda").show();
}

/* Identifica que tipo de filtro se realizara y lo ejecuta **/
function buscarInfo()
{
    map.removeObjects(map.getObjects ())
    var filtros = validaPorFiltros(); // 1 : Filtro completo | 2 : Filtro no usado | 3 : Filtro incompleto
    var coord = validaPorCoor(); // 1 : Filtro completo | 2 : Filtro no usado | 3 : Filtro incompleto
    var produ = validaPorProductor(); // 1 : Filtro completo | 2 : Filtro no usado | 3 : Filtro incompleto

    if(filtros == 1 && coord == 2 && produ == 2)
    {
        mostrarLoading("Buscando localidad. Espere un momento");
        type_change = 1;
        var estado = $("#search_select_estado option:selected").html();
        var municipio = $("#search_select_municipio option:selected").html();
        var localidad = $("#search_select_localidad option:selected").html();
        var pais = 'México';

        var geocoder = platform.getGeocodingService(),
        geocodingParameters = {
          searchText: localidad + ", " + municipio + ", " + estado + ", " + pais,
          jsonattributes : 1
        };

        geocoder.geocode(
            geocodingParameters,
            resultadoBusqueda,
            errorResultadoBusqueda
        );
    }
    else if(coord == 1 && filtros == 2 && produ == 2)
    {
        mostrarLoading("Buscando coordenadas. Espere un momento");
        /*type_change = 2;
        var coordinada_a_buscar = new nokia.maps.geo.Coordinate(
            parseFloat($("#search_input_lat").val().trim()),
            parseFloat($("#search_input_lng").val().trim())
        );

        searchManager.reverseGeoCode({
            latitude: coordinada_a_buscar.latitude,
            longitude: coordinada_a_buscar.longitude,
            onComplete: resultadoBusqueda
        });*/
        var parisMarker = new H.map.Marker({lat:$("#search_input_lat").val().trim(), lng:$("#search_input_lng").val().trim()});
        map.addObject(parisMarker);
        map.setCenter({lat:$("#search_input_lat").val().trim(), lng:$("#search_input_lng").val().trim()});
        map.setZoom(12);

        ocultarLoading();
    }
    else if(produ == 1 && filtros == 2 && coord == 2)
    {
        dibujarCultivos($("#search_select_productor").val());
    }
    else if(filtros == 2 && coord == 2 && produ == 2)
    {
        mostrarValidacion("Filtros no establecidos");
    }
}

/** Gestiona el resultado de la busqueda de los filtros **/
var resultadoBusqueda = function (result) 
{
    var locations = result.response.view[0].result;
    addLocationsToMap(locations);
    ocultarLoading();

    /*
    var i, len, locations, marker;

    if (requestStatus == "OK") 
    {
        locations = data.results ? data.results.items : [data.location];

        /*if (locations.length > 0) /* Hay resultados de la busqueda */
        //{
            /* Eliminamos el resultado de la busqueda anterior si existe */
            /*if (resultSet) map.objects.remove(resultSet);
            resultSet = new nokia.maps.map.Container();
            /* Creamos markers para indicar los resultados de la busqueda, pero no se dibujan en el mapa */
            /*for (i = 0, len = locations.length; i < len; i++) {
                marker = new nokia.maps.map.StandardMarker(locations[i].position, { text: i+1 });
                resultSet.objects.add(marker);
            }
            /* Nos dirigimos en el mapa al (los) punto (s) encontrado (s), e indicamos que se esta produciendo un cambio de posicion */
            /*map.zoomTo(resultSet.getBoundingBox(), false);
            changing = true;
        }
        else /* No hay resultados de la busqueda */
        /*{
            ocultarLoading();
            mostrarValidacion("No se han encontrado resultados en la búsqueda realizada");
        }
    }
    else /* Busqueda con errores */
    /*{
        ocultarLoading();
        mostrarValidacion("Ha surgido un inconveniente, por favor inténtelo nuevamente");
    }*/
};

var errorResultadoBusqueda = function ()
{
    alert('No se han encontrado resultados');
}


function addLocationsToMap(locations)
{
    var group = new  H.map.Group(),position,i;

    // Add a marker for each location found
    /*for (i = 0;  i < locations.length; i += 1) 
    {*/
        position = 
        {
            lat: locations[0].location.displayPosition.latitude,
            lng: locations[0].location.displayPosition.longitude
        };
        marker = new H.map.Marker(position);
        marker.label = locations[0].location.address.label;
        group.addObject(marker);
    //}

    group.addEventListener('tap', function (evt) 
    {
        map.setCenter(evt.target.getGeometry());
        openBubble(
            evt.target.getGeometry(), 
            evt.target.label
        );
    }, false);

    // Add the locations group to the map
    map.addObject(group);
    map.setCenter(group.getBoundingBox().getCenter());
    map.setZoom(12);
}

/** 
    Valida si se esta usando el filtro por localidades.
    1 : Filtro completo | 2 : Filtro no usado | 3 : Filtro incompleto
**/
function validaPorFiltros()
{
    if($("#search_select_estado").val() != 0)
    {
        if($("#search_select_municipio").val() == 0)
        {
            mostrarValidacion("Seleccione un municipio");
            return 3;
        }
        else if($("#search_select_localidad").val() == 0)
        {
            mostrarValidacion("Seleccione una localidad");
            return 3;
        }
        ocultarValidacion();
        return 1;
    }
    return 2;
}

/** 
    Valida si se esta usando el filtro por productores.
    1 : Filtro completo | 2 : Filtro no usado | 3 : Filtro incompleto
**/
function validaPorProductor()
{
    if($("#search_select_productor").val() != 0)
    {
        ocultarValidacion();
        return 1;
    }
    return 2;
}

/** 
    Valida si se esta usando el filtro por coordinadas.
    1 : Filtro completo | 2 : Filtro no usado | 3 : Filtro incompleto
**/
function validaPorCoor()
{
    if(($("#search_input_lat").val().trim() != '' && $("#search_input_lng").val().trim() == '') || ($("#search_input_lat").val().trim() == '' && $("#search_input_lng").val().trim() != ''))
    {
        if($("#search_input_lat").val().trim() == '') mostrarValidacion("Digite la latitud");
        else mostrarValidacion("Digite la longitud");
        return 3;
    }
    else
    {
        if($("#search_input_lat").val().trim() != '' && $("#search_input_lng").val().trim() != '')
        {
            if(validaLatitud($("#search_input_lat").val().trim()) && validaLongitud($("#search_input_lng").val().trim()))
            {
                ocultarValidacion();
                return 1;
            }
            else
            {
                if(!validaLatitud($("#search_input_lat").val().trim())) mostrarValidacion("Latitud no es una coordenada");
                else if(!validaLongitud($("#search_input_lng").val().trim())) mostrarValidacion("Longitud no es una coordenada");
                return 3;
            }
        }
    }
    return 2;
}

/** Se limpian los filtros establecidos por el cliente **/
function limpiarInfo()
{
    for (var i = 0; i < poligonos_productor.length; i++) {
        map.removeObject(poligonos_productor[i]);
    };
    $("#search_input_lat").val("");
    $("#search_input_lng").val("");
    $("#search_select_estado").val("0");
    $("#search_select_municipio").html("<option value='0'>Seleccione un municipio...</option>");
    $("#search_select_localidad").html("<option value='0'>Seleccione una localidad...</option>");
    $("#search_select_productor").val("0");
    $("#verror").hide();
    habilitaCoordenadas();
    habilitaFiltros();
    habilitaProductores();
}

/** Se valida si el parametro dado es una latitud **/
function validaLatitud(lat)
{
    if (!isNaN(lat) && lat <= 90 && lat >= -90) return true;
    else return false;
}

/** Se valida si el parametro dado es una longitud **/
function validaLongitud(lng)
{
    if (!isNaN(lng) && lng <= 180 && lng >= -180) return true;
    else return false;
}

/**
###########################################################################################
################################## SHAPES & KMLS ##########################################
###########################################################################################
**/

/** Mostrar u ocultar el KML del par valor-variable seleccionado **/
/*function kml(name, file, subname = null)
{
    if($("#" + name + "_chbx_" + file).is(':checked'))
    {
        mostrarLoading("Cargando capa. Espere un momento");
        pre = name;
        counter = file;
        window["layer_" + name + "_" + file] = new nokia.maps.kml.Manager();
        window["layer_" + name + "_" + file].addObserver("state", kmlProcesado);
       // alert(name + " :: " + subname);
        if(subname != null)
            window["layer_" + name + "_" + file].parseKML('/kmls/' + subname + '-' + file + '.kml');
        else
            window["layer_" + name + "_" + file].parseKML('/kmls/' + name + '-' + file + '.kml');
        counter = file;  
    }
    else
    {
       map.objects.remove(window["container_" + name + "_" + file]);
    }
}*/

function mostrarZonaAgricola()
{    
    window["layer_ip_1"] = new nokia.maps.kml.Manager();
    window["layer_ip_1"].addObserver("state", kmlProcesado);
    window["layer_ip_1"].parseKML('/kmls/ip-1.kml');

    window["layer_ip_2"] = new nokia.maps.kml.Manager();
    window["layer_ip_2"].addObserver("state", kmlProcesado);
    window["layer_ip_2"].parseKML('/kmls/ip-2.kml');

    window["layer_ip_3"] = new nokia.maps.kml.Manager();
    window["layer_ip_3"].addObserver("state", kmlProcesado);
    window["layer_ip_3"].parseKML('/kmls/ip-3.kml');

    window["layer_ip_4"] = new nokia.maps.kml.Manager();
    window["layer_ip_4"].addObserver("state", kmlProcesado);
    window["layer_ip_4"].parseKML('/kmls/ip-4.kml');
}

/** Gestiona el resultado de leer el kml solicitado **/
function kmlProcesado(kmlManager)
{
    var resultSet;
    if (kmlManager.state == "finished") 
    {
        resultSet = new nokia.maps.kml.component.KMLResultSet(kmlManager.kmlDocument, map);
        resultSet.addObserver("state", function (resultSet) {
                if (resultSet.state == "finished")
                {
                    window["container_" + pre + "_" + counter] = resultSet.container;
                    map.objects.add(window["container_" + pre + "_" + counter]);
                    ocultarLoading();
                }
        });
        resultSet.create();
    }
}

/** Realiza la busqueda de la informacion que se encuentra en el punto cliceado por el usuario **/
function buscaPunto(_variable)
{
    variable = _variable;
    
    var query ="SELECT '" + tablas[variable].parametro + "', 'color' FROM " + tablas[variable].tabla + " WHERE ST_INTERSECTS(\'geometry\', CIRCLE(LATLNG(" + lat + "," + lng + "),0.5));";
    query = encodeURIComponent(query);
    
    var query = new google.visualization.Query('http://www.google.com/fusiontables/gvizdata?tq=' + query);
    query.send(resultadoBuscaPunto);
}

/** Gestiona el resultado de la busqueda de la informacion del punto */
function resultadoBuscaPunto(response)
{
    if (!response)
    {
        bubbleContainer.closeBubble(bubble); 
        bubble = bubbleContainer.openBubble("<b>No se ha encontrado una respuesta del servidor:</br>CODE 404</b>", coords);
        return;
    }
    else if (response.isError())
    {
        bubbleContainer.closeBubble(bubble); 
        bubble = bubbleContainer.openBubble("<b>Se ha detectado un error en query: </b></br>" + response.getMessage() + " " + response.getDetailedMessage(), coords);
        return;
    }
    
    tablas[variable].resultados.length = 0;
    
    for (var i=0; i < response.getDataTable().getNumberOfRows(); i++){
        tablas[variable].resultados.push(new Array(response.getDataTable().getValue(i,0), response.getDataTable().getValue(i,1)));
    }//for
    
    if(variable == 'arcilla') buscaPunto('arena');
    else if(variable == 'arena') buscaPunto('cah');
    else if(variable == 'cah') buscaPunto('cic');
    else if(variable == 'cic') buscaPunto('iht');
    else if(variable == 'iht') buscaPunto('ip');
    else if(variable == 'ip') buscaPunto('limo');
    else if(variable == 'limo') buscaPunto('mo');
    else if(variable == 'mo') buscaPunto('ph');
    else if(variable == 'ph') buscaPunto('profundidad');
    else if(variable == 'profundidad') buscaPunto('sb');
    else if(variable == 'sb') buscaPunto('textura');
    else if(variable == 'textura') buscaPunto('fc');
    else if(variable == 'fc') buscaPunto('qb');
    else if(variable == 'aluminio') buscaPunto('al');
    else if(variable == 'zinc') buscaPunto('zc');
    else if(variable == 'azufre') buscaPunto('az');
    else if(variable == 'boro') buscaPunto('bo');
    else if(variable == 'calcio') buscaPunto('ca');
    else if(variable == 'ce') buscaPunto('ce');
    else if(variable == 'cb') buscaPunto('cb');
    else if(variable == 'fs') buscaPunto('fs');
    else if(variable == 'hr') buscaPunto('hr');
    else if(variable == 'mg') buscaPunto('mg');
    else if(variable == 'pt') buscaPunto('pt');
    else if(variable == 'sd') buscaPunto('sd');
    else if(variable == 'manganeso') buscaPunto('man');
    else if(variable == 'zinc') buscaPunto('zc');
    else muestraResultado();
}

function shapesEnPunto(coorX, coorY, poligono){
    $("#tablaVariables").hide();
    dispo=true;
    muestraResultado();
}//function

/** Mustra el (los) resultado (s) de la busqueda (s) del punto **/
function muestraResultado()
{
    var info = new String();
    var found = false;
    
    ocultarLoading();
    //bubbleContainer.closeBubble(bubble);
    //$(".btnCloseValores").trigger('click');
    cargarValores();

    var svgMarkup = '<svg width="24" height="24" ' +
    'xmlns="http://www.w3.org/2000/svg">' +
    '<rect stroke="white" fill="#1b468d" x="1" y="1" width="22" ' +
    'height="22" /><text x="12" y="18" font-size="12pt" ' +
    'font-family="Arial" font-weight="bold" text-anchor="middle" ' +
    'fill="white"></text></svg>';

    // Create an icon, an object holding the latitude and longitude, and a marker:
    var icon = new H.map.Icon(svgMarkup);
    c = {lat: lat, lng: lng};
    console.log(c);
    marker = new H.map.Marker(c, {icon: icon});

    // Add the marker to the map and center the map at the location of the marker:
    map.addObject(marker);
    map.setCenter(coords);
    //map.setZoomLevel(15);
    
    dispo = true;
} 

/** Gestiona el resultado de la busqueda de la coordenada a partir de la busqueda de informacion de un punto **/
var resultadoBusquedaCoord = function (data, requestStatus, requestId) { 
    var location = data.location;
    /* Cerramos notificaciones si existen */
    $('#notifications').jGrowl('close');
    ocultarLoading();
    /* Tenemos un resultado valido */
    cargarValores();
    marker = new nokia.maps.map.StandardMarker([parseFloat(lat), parseFloat(lng)], {
        text: 'P',
        draggable: false
    });
    map.objects.add(marker);
    map.setZoomLevel(15, "default");
    map.setCenter(coords,"default");
};



function cargarValores()
{
    if(!clicpoligono) $(".tablaSup3").hide();
    /*var keys = ['arcilla', 'arena', 'limo', 'profundidad', 'textura', 'cah', 'cic', 'iht', 'mo', 'ph', 'sb', 'ip','man','sd','pt','mg','cb','ce','ca','bo','az','zc','al','qb','fc'];*/
    var keys = ['arcilla', 'arena', 'cah', 'cic', 'mo', 'ph', 'sb', 'azufre', 'aluminio', 'boro', 'sodio', 'potasio','manganeso','cobre','calcio','ce', 'ip'];
    var al_menos_un_valor = false;
    for (var i = 0; i < keys.length; i++) {
        if(tablas[keys[i]].resultados.length > 0)
        {
            $("#txt_" + keys[i]).html(tablas[keys[i]].resultados[0][0]);
            //$("#clr_" + keys[i]).css("background-color", "#" + tablas[keys[i]].resultados[0][1]);
            if(keys[i]!='ip'){
                var semaforo = obtenerSemaforo( tablas[keys[i]].resultados[0][1].toLowerCase());
                $("#clr_" + keys[i]).html(semaforo);
            }else{
                $("#clr_" + keys[i]).css("background-color", "#" + tablas[keys[i]].resultados[0][1]);
            }
            al_menos_un_valor = true;
        }
        else
        {
            $("#txt_" + keys[i]).html('No disponible');
            $("#clr_" + keys[i]).css("background-color", "transparent");
        }
    };
    if(al_menos_un_valor)
    {
        $(".tablaSup1").hide();
        //$(".tablaSup2").show();
    }
    else
    {
        $(".tablaSup2").hide();
        //$(".tablaSup1").show();
    }
    $(".btnCloseValores, .tableValores, .trzPoligono").show();
    if(!clicpoligono) {
        $(".tablaSup3").hide();
        $("#datosPredio").hide();
        $(".trzPoligono").show();
    }else{
        $(".tablaSup3").show();
        $("#datosPredio").show();
        $(".trzPoligono").hide();
    }
    clicpoligono = false;
    idpoligono = null;
}

function obtenerSemaforo(color){
    //seteamos los colores en un array
    var colores = Array();
    colores.verde = "40ff00";
    colores.amarillo = "ffff00";
    colores.naranja = "fe9a2e";
    colores.rojo = "fa5858";
    
    var html="";
    //console.log(colores.rojo+"-"+color);
    //generamos el html dependiendo del color del semaforo
        if(colores.verde == color){//si es verde
            html='<div id="semaforo-seleccion">'+
                        '<span id="seleccion-color-semaforo">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>'+
                '<div id="semaforo-color">'+
                        '<span style=" background-color:#'+colores.verde+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.amarillo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.naranja+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.rojo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>';
        }else if(colores.amarillo == color){//si es amarillo
                        html='<div id="semaforo-seleccion">'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span id="seleccion-color-semaforo">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>'+
                '<div id="semaforo-color">'+
                        '<span style=" background-color:#'+colores.verde+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.amarillo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.naranja+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.rojo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>';
        }else if(colores.naranja == color){//si es naranja
                        html='<div id="semaforo-seleccion">'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span id="seleccion-color-semaforo">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>'+
                '<div id="semaforo-color">'+
                        '<span style=" background-color:#'+colores.verde+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.amarillo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.naranja+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.rojo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>';
        }else if(colores.rojo == color){//si es rojo
                        html='<div id="semaforo-seleccion">'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span id="seleccion-color-semaforo">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>'+
                '<div id="semaforo-color">'+
                        '<span style=" background-color:#'+colores.verde+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.amarillo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.naranja+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                        '<span style=" background-color:#'+colores.rojo+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>'+
                '</div>';
        }
        
        return html;
}
/**
###########################################################################################
###################################### GENERALES ##########################################
###########################################################################################
**/

function mostrarLoading(texto)
{
    $("#loading").css("height", $( document ).height()).html('');
    //$("#loading").css("height", $( document ).height()).html('</br></br></br>' + texto);
    $("#loading").show();
}

function ocultarLoading()
{
    $("#loading").hide();
}

function mostrarValidacion(texto)
{
    $("#verrorc").html(texto);
    $("#verror").show();
}

function ocultarValidacion()
{
    $("#verror").hide();
}

function deshabilitaCoordenadas()
{
    $("#search_input_lat").val("");
    $("#search_input_lng").val("");
    $("#search_input_lat").prop("readonly", true);
    $("#search_input_lng").prop("readonly", true);
}

function habilitaCoordenadas()
{
    $("#search_input_lat").prop("readonly", false);
    $("#search_input_lng").prop("readonly", false);
}

function deshabilitaFiltros()
{
    $("#search_select_estado").prop('disabled', true);
    $("#search_select_municipio").prop('disabled', true);
    $("#search_select_localidad").prop('disabled', true);
}

function habilitaFiltros()
{
    $("#search_select_estado").prop('disabled', false);
    $("#search_select_municipio").prop('disabled', false);
    $("#search_select_localidad").prop('disabled', false);
}

function deshabilitaProductores()
{
    $("#search_select_productor").prop('disabled', true);
}

function habilitaProductores()
{
    $("#search_select_productor").prop('disabled', false);
}

function verMuestreo()
{
    $.ajax({
        type: 'POST',
        url:"/backend/servicios/excel",
        success:
        function respuesta(res)
        {
            var jsn = jQuery.parseJSON(res);
            for (var i = 0; i < jsn.length; i++)
            {
                var marker = new nokia.maps.map.StandardMarker([jsn[i].lat, jsn[i].lng], {
                    text: jsn[i].identificador,
                    draggable: false
                });

                map.objects.add(marker);
            }
        }
    });
}


/**
###########################################################################################
##################################### NUEVAS MODIFICACIONES ###############################
###########################################################################################
**/

function verQuimicas ()
{
   $('#vq').css("display","block");
   $('#eq').css("display","block");
    mostrarLoading("Cargando información. Espere un momento");
    var array = ['cic', 'mo', 'ph', 'sb','calcio','manganeso','boro','aluminio','azufre','ce','cobre','sodio','potasio'];
    for (var i = 0; i < array.length; i++) {
        $.ajax({
            type: "POST"
            ,url: "/backend/servicios/obtenersubvariables"
            ,data: { variable : tablas[array[i]].tabla }
            ,success: function(respuesta){  
                respuesta = jQuery.parseJSON(respuesta);        
                if(respuesta.subvariable)
                {
                    for (var j = 0; j < respuesta.subvariable.length; j++) {
                        $("#" + respuesta.subvariable[j].variable + "_chbx_" + (j+1)).attr("onClick", "kml(\"" + respuesta.subvariable[j].variable + "\", " + respuesta.subvariable[j].archivo + ")");
                        $("#" + respuesta.subvariable[j].variable + "_txt_" + (j+1)).html("&nbsp;" + respuesta.subvariable[j].valor);
                        $("#" + respuesta.subvariable[j].variable + "_clr_" + (j+1)).css("background-color", "#" + respuesta.subvariable[j].color);
                        
                    };
                    ocultarLoading();
                }
                else
                {
                    ocultarLoading();
                    $('#notifications').jGrowl(respuesta.error, { glue: 'before' });
                }
            }
        });
    };
    $("#variables_ip, #btnCerrarIp").hide();
    $("#variables_fisicas, #btnCerrarFisicas").hide();
    $("#variables_quimicas, #btnCerrarQuimicas").show();
    $("#tabsQumicoBilogico").tabs();
}

function verFisicas ()
{
    mostrarLoading("Cargando información. Espere un momento");
    /*var array = ['cah', 'iht', 'arcilla', 'arena', 'limo', 'profundidad', 'textura'];*/
    var array = ['cah', 'arcilla', 'arena'];
    for (var i = 0; i < array.length; i++) {
        $.ajax({
            type: "POST"
            ,url: "/backend/servicios/obtenersubvariables"
            ,data: { variable : tablas[array[i]].tabla, nombre: array[i] }
            ,success: function(respuesta){  
                respuesta = jQuery.parseJSON(respuesta);        
                if(respuesta.subvariable)
                {
                    for (var j = 0; j < respuesta.subvariable.length; j++) {
                        if(respuesta.subvariable[j].subvariable != null)
                            $("#" + respuesta.subvariable[j].variable + "_chbx_" + (j+1)).attr("onClick", "kml(\"" + respuesta.subvariable[j].variable + "\", " + respuesta.subvariable[j].archivo + ",\"" + respuesta.subvariable[j].subvariable + "\")");
                        else
                            $("#" + respuesta.subvariable[j].variable + "_chbx_" + (j+1)).attr("onClick", "kml(\"" + respuesta.subvariable[j].variable + "\", " + respuesta.subvariable[j].archivo + ")");
                        $("#" + respuesta.subvariable[j].variable + "_txt_" + (j+1)).html("&nbsp;" + respuesta.subvariable[j].valor);
                        $("#" + respuesta.subvariable[j].variable + "_clr_" + (j+1)).css("background-color", "#" + respuesta.subvariable[j].color);
                    };
                    ocultarLoading();
                }
                else
                {
                    $('#notifications').jGrowl(respuesta.error, { glue: 'before' });
                    ocultarLoading();
                }
            }
        });
    };
    $("#variables_quimicas, #btnCerrarQuimicas").hide();
    $("#variables_ip, #btnCerrarIp").hide();
    $("#variables_fisicas, #btnCerrarFisicas").show();
}

function verIP ()
{
    mostrarLoading("Cargando información. Espere un momento");
    var array = ['ip'];
    for (var i = 0; i < array.length; i++) {
        $.ajax({
            type: "POST"
            ,url: "/backend/servicios/obtenersubvariables"
            ,data: { variable : tablas[array[i]].tabla }
            ,success: function(respuesta){  
                respuesta = jQuery.parseJSON(respuesta);        
                if(respuesta.subvariable)
                {
                    for (var j = 0; j < respuesta.subvariable.length; j++) {
                        $("#" + respuesta.subvariable[j].variable + "_chbx_" + (j+1)).attr("onClick", "kml(\"" + respuesta.subvariable[j].variable + "\", " + respuesta.subvariable[j].archivo + ")");
                        $("#" + respuesta.subvariable[j].variable + "_txt_" + (j+1)).html("&nbsp;" + respuesta.subvariable[j].valor);
                        $("#" + respuesta.subvariable[j].variable + "_clr_" + (j+1)).css("background-color", "#" + respuesta.subvariable[j].color);
                    };
                    ocultarLoading();
                }
                else
                {
                    ocultarLoading();
                    $('#notifications').jGrowl(respuesta.error, { glue: 'before' });
                }
            }
        });
    };
    $("#variables_quimicas, #btnCerrarQuimicas").hide();
    $("#variables_fisicas, #btnCerrarFisicas").hide();
    $("#variables_ip, #btnCerrarIp").show();
}

/**
###########################################################################################
################################### DIBUJAR POLIGONO EN MAPA ##############################
###########################################################################################
**/

function trazarPoligono()
{
    if(!trazo_activo)
    {
        $('#notifications').jGrowl("Funcion trazabilidad activada", { glue: 'before' });
        punteros.length = 0;
        trazo_activo = true;
        map.removeEventListener('tap', clicMapaValor, false);
        map.addEventListener('tap', clicMapaMarker);

        $("#lblDP").html('NO TRAZAR POLIGONO');
        $("#btnDP").attr("onClick", "quitarPoligono()");
        //deshabilitamos drag and drop y zoom del mapa cuando la trazabilidad esta activada
        deshabilitarComponentesMapa();
    }
    else
    {
        $('#notifications').jGrowl("Esta funcionalidad ya esta activa", { glue: 'before' });
    }
}

function quitarPoligono()
{
    //alert(punteros.length);
    while(punteros.length > 0)
    { 
        eliminarPoligono();
    }
    trazo_activo = false;
    map.removeEventListener('tap', clicMapaMarker, false);
    map.addEventListener('tap', clicMapaValor);
    $("#lblDP").html('TRAZAR POLIGONO');
    $("#btnDP").attr("onClick", "trazarPoligono()");
    $('#notifications').jGrowl("Funcion trazabilidad desactivada", { glue: 'before' });
    habilitarComponentesMapa();

}

function clicMapaMarker(evt)
{
    var pixelGeo =  map.screenToGeo(evt.currentPointer.viewportX,evt.currentPointer.viewportY);
    //var coordinate = new nokia.maps.geo.Coordinate(pixelGeo.latitude, pixelGeo.longitude);


    var svgMarkup = '<svg width="24" height="24" ' +
    'xmlns="http://www.w3.org/2000/svg">' +
    '<rect stroke="white" fill="#1b468d" x="1" y="1" width="22" ' +
    'height="22" /><text x="12" y="18" font-size="12pt" ' +
    'font-family="Arial" font-weight="bold" text-anchor="middle" ' +
    'fill="white">'+(punteros.length + 1)+'</text></svg>';

    // Create an icon, an object holding the latitude and longitude, and a marker:
    var icon = new H.map.Icon(svgMarkup);
    coords = {lat: pixelGeo.lat, lng: pixelGeo.lng};
    var puntero = new H.map.Marker(coords, {icon: icon});

    // Add the marker to the map and center the map at the location of the marker:
    map.addObject(puntero);
    //map.setCenter(coords);


    punteros.push(puntero);

    /*contenedor.objects.add(puntero);*/
    dibujarPoligono();
}

function dibujarPoligono()
{
    
    //var aMarkers = contenedor.objects.asArray();
    var aCoords = [];
    lineString = new H.geo.LineString();


    for(var marcador in punteros){
        var puntero = punteros[marcador];
        lineString.pushPoint({lat:puntero.b.lat, lng:puntero.b.lng});
        aCoords.push(puntero.b);
    }

    if(poligono) map.removeObject(poligono);
    poligono=map.addObject(
        new H.map.Polygon(lineString, {
          style: {
            fillColor: '#a779e8',
            strokeColor: '#5706c9',
            lineWidth: 3
          }
        })
    );
}

function eliminarPoligono()
{
    
    if(punteros.length>0)
    {
        map.removeObject(punteros[punteros.length-1]);
        punteros.pop();
        if(punteros.length>0)
            dibujarPoligono();
    }
    else
    {
        punteros = new Array();
        $('#notifications').jGrowl("No hay vertices en mapa", { glue: 'before' });
    }
}

function nuevoDiagnostico(predio=false)
{
    if(punteros.length < 3 && predio==false)
    {
        $('#notifications').jGrowl('Indique por lo menos 3 puntos del poligono', { glue: 'before' });
    }
    else
    {
        if(predio==false)
        {
            var datos = {};
            var i=0;


            for(var marcador in punteros){
                var puntero = punteros[marcador];
                datos['lat_' + i] = puntero.b.lat;
                datos['lng_' + i] = puntero.b.lng
                i++;
            }

            var json = JSON.stringify(datos);
        }
        $('#modalProductor').modal('toggle')
        //window.location = '/backend/diagnosticos/agregar/tipo/1/id/0/vertices/' + json;
    }
}

function clicPoligonoValor(id)
{
    clicpoligono = true;
    idpoligono = id;
    $.ajax({
        type: "POST"
        ,url: "/backend/cultivo/obtenercultivosestado"
        ,data: {id: id}
        ,success: function(respuesta){  
            respuesta = jQuery.parseJSON(respuesta);        
            $("#LOCALIDAD").html(respuesta.localidad);
            $("#MUNICIPIO").html(respuesta.municipio);
            $("#ESTADO").html(respuesta.Estado);
            $("#PRODUCTO").html(respuesta.productor);
            $("#datosPredio").show();
            $("#idEstadoCultivo").html("Estado: " + respuesta.estado);
            var e = 0;
            if(respuesta.estado == "Programado") e = 1;
            else if(respuesta.estado == "Real") e = 2;
            $("#btnEstadoCultivo").attr("onClick", "window.location = '/backend/diagnosticos/agregar/tipo/1/id/" + id + "'");
            //$(".tablaSup3, .tableValores, .tableVariables").show();
        }
    });
}

function dibujarCultivos(productor = null)
{
    for (var i = 0; i < poligonos_productor.length; i++) {

        map.removeObject(poligonos_productor[i]);
        map.objects.remove(poligonos_productor[i]);
    };

    $.ajax({
        type: "POST"
        ,url: "/backend/cultivo/obtenercultivosshape"
        ,data: { productor:  productor }
        ,success: function(respuesta){  
            respuesta = jQuery.parseJSON(respuesta);      
            if(respuesta.cultivo !== undefined)
            {  
                for (var i = 0; i < respuesta.cultivo.length; i++) 
                {
                    var aCoords = [];
                    lineString = new H.geo.LineString();
                    for (var j = 0; j < respuesta.cultivo[i].punto.length; j++) 
                    {
                        //var coordinate = new nokia.maps.geo.Coordinate(parseFloat(respuesta.cultivo[i].punto[j].lat), parseFloat(respuesta.cultivo[i].punto[j].lon));
                        lineString.pushPoint({lat:parseFloat(respuesta.cultivo[i].punto[j].lat), lng:parseFloat(respuesta.cultivo[i].punto[j].lon)});
                        aCoords.push({lat:parseFloat(respuesta.cultivo[i].punto[j].lat), lng:parseFloat(respuesta.cultivo[i].punto[j].lon)});
                    };

                    
                    
                    if(productor == null)
                        poligono=map.addObject(
                            new H.map.Polygon(lineString, {
                              style: {
                                strokeColor: '#00F8',
                                lineWidth: 3
                              }
                            })
                        );
                    else
                    {
                        poligono=map.addObject(
                            new H.map.Polygon(lineString, {
                              style: {
                                fillColor: '#A52A2A',
                                strokeColor: '#FFFFFF',
                                lineWidth: 3
                              }
                            })
                        );
                        poligonos_productor.push(poligono);
                    }

                    map.setCenter(aCoords[0]);
                    map.setZoom(14);

                    poligono.addEventListener('tap', function(id){
                        return function(){
                            clicPoligonoValor(id);
                        }
                    }(respuesta.cultivo[i].id));
                };
            }
        }
    });

    
}
function deshabilitarComponentesMapa()
{
    /*map.removeComponent( map.getComponentById("Behavior") );
    map.removeComponent( map.getComponentById("ZoomBar") );
    map.removeComponent( map.getComponentById("ZoomRectangle") );*/
}

function habilitarComponentesMapa()
{
    console.log("test");
    /*map.addComponent( new nokia.maps.map.component.Behavior() );
    map.addComponent( new nokia.maps.map.component.ZoomBar() );
    map.addComponent( new nokia.maps.map.component.ZoomRectangle() );*/
}

function guardarPoligonoPredio(predio)
{
    if(punteros.length<3)
    {
        alert("El polígono no puede tener menos de 3 puntos");
    }
    else
    {
        var datos = {};
        var i=0;                
        for(var marcador in punteros){
            var puntero = punteros[marcador];
            datos[i] = {lat: puntero.b.lat, lng: puntero.b.lng};
            i++;
        }
        var json = JSON.stringify(datos);
        console.log(json);
        $.ajax({
            type: "POST",
            url: "/backend/index/guardarpoligono",
            data: { predio:  predio, vertices:json },
            success: function(respuesta){
                location.href='/backend/diagnosticos/agregar/tipo/1/id/'+respuesta.id_productor+'/predio/'+predio;
            }
        });
    }
}