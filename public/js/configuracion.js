 
$(document).ready(function(){
	//inicializamos los multiselect
  	$('#parcial select').each(function() {
        if($(this).attr('multiple').valueOf()=='multiple'){
			$(this).multiselect();
		}
    });

	//ocultmaos la clase
	//alert($('.ui-multiselect-menu').length);
	$('.ui-multiselect-menu').each(function(index, element) {
        $(this).css("display","none");
    });
	
	//asignamos una leyenda al multiselect
	$("#parcial select").each(function(index, element) {
		if($(this).attr('multiple').valueOf()=='multiple'){
			$(this).multiselect({noneSelectedText:'Unidades de medida'});
		}
	});
	

});

function AparecerFormulario(parcial)
{
		$("#parcial > div").not("#"+parcial).each(function ()
		{
			$(this).addClass("hide");
		});
		//aparecemos los formularios
		$('#'+parcial).removeClass("hide");
		obtenerUnidades(parcial);
		/*if(parcial == 'RentaDeLaTierra'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'AcondicionamientoDelPredio'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'MejoramientoDeFertilidadDelSuelo'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'PreparacionDelTerreno'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'Riego'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'SiembraDePrecision'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'PracticaDeResiembra'){$('#'+parcial).removeClass("hide")}
		else if(parcial == 'Semilla'){}
		else if(parcial == 'Resiembra'){}
		else if(parcial == 'Fertilizacion'){}
		else if(parcial == 'CostosIndirectos'){}
		else if(parcial == 'IngresosEsperados'){}
		else if(parcial == 'ControlDeMalezas'){}
		else if(parcial=='ControlDeEnfermedades'){
		}else if(parcial=='IndiceDeRentabilidad'){
		}else if(parcial=='ReporteCostos'){}*/

}

function guardarUnidades(prefijo){
	arreglo =new Array();
	
	$('#'+prefijo+' select').each(function(index, element) {
         if($(this).attr('multiple').valueOf()=='multiple'){
			 concepto = $(this).attr("id").split("-")[1];
			// alert(concepto);
			//arreglo.formulario= prefijo;
			 //asignamos los valores de cada select al formulario y creamos el array de datos
			arreglo.push({formulario:prefijo, concepto: concepto,unidades: $(this).val(),estatus:1});
			
		}
    });
	//alert(arreglo.toSource());
	//mandamos el array para guardarlo
	$.ajax({
        url: "/backend/configuracion/guardarpestana/",
        type: "POST",
		data: { datos: JSON.stringify(arreglo),prefijo:prefijo },
        async: false
    }).done(function(respuesta){
    	try
    	{
	        if(respuesta==''){
				ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "", false);
			}else{
				ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar las unidades, por favor inténtelo nuevamente", "Ok", "", false, "");
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar las unidades, por favor inténtelo nuevamente", "Ok", "", false, "");
		}
    });
	
	
}

function deselect(prefijo){
	
}

function obtenerUnidades(prefijo){
	$.ajax({
        url: "/backend/configuracion/obtenerunidades/",
        type: "POST",
		data: { formulario:prefijo },
        async: false
    }).done(function(respuesta){
    	try
    	{
			respuesta = jQuery.parseJSON(respuesta);
	        if(respuesta!=null){
				$.each(respuesta, function(i, item) {
					if(item!=null){
						var dataarray=item.split(",");
						$('#'+prefijo+'-'+i).val(dataarray);
						//alert('#'+prefijo+'-'+i+' : '+item);
					}
					$('#'+prefijo+'-'+i).multiselect("refresh");
				});
				
				//ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "", false);
			}else{
				//ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener las unidades, por favor inténtelo nuevamente", "Ok", "", false, "");
			}
		}
		catch(err)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de obtener las unidades, por favor inténtelo nuevamente "+err, "Ok", "", false, "");
		}
    });
}