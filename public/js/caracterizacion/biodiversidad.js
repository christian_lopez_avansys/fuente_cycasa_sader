$(document).ready(function(){

	$('.date').datepicker({ format: 'yyyy-mm-dd' });

});



function agregarBiodiversidadPlantas()
{
	if($("#biodiversidad_plantas_especie").val()=='' || $("#biodiversidad_plantas_comun").val()=='' || $("#biodiversidad_plantas_cientifico").val()=='' || $("#biodiversidad_plantas_cultivada").val()=='' || $("#biodiversidad_plantas_forma_biologica").val()=='' || $("#biodiversidad_plantas_usos").val()=='' || $("#biodiversidad_plantas_conserve").val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "No pueden haber datos vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarbiodiversidadplantas/",
			type: "POST",
			data: {
				predio_id:$("#predio_id").val(), 
				especie:$("#biodiversidad_plantas_especie").val(), 
				nombre_comun:$("#biodiversidad_plantas_comun").val(), 
				nombre_cientifico:$("#biodiversidad_plantas_cientifico").val(), 
				es_cultivada:$("#biodiversidad_plantas_cultivada").val(), 
				forma_biologica:$("#biodiversidad_plantas_forma_biologica").val(), 
				usos:$("#biodiversidad_plantas_usos").val(), 
				interesa_conserve:$("#biodiversidad_plantas_conserve").val(), 
				comentarios:$("#biodiversidad_plantas_comentarios").val()
			},
		}).done(function(respuesta)
		{
			var respuesta = jQuery.parseJSON(respuesta);
			var es_cultivada='No';
			var interesa_conserve='No';
			if(respuesta.es_cultivada==1)
				es_cultivada='Si';
			if(respuesta.interesa_conserve==1)
				interesa_conserve='Si';

			var s='<tr id="fila_biodiversidad_plantas_'+respuesta.id+'">'+
					'<td>'+respuesta.especie+'</td>'+
					'<td>'+respuesta.nombre_comun+'</td>'+
					'<td>'+respuesta.nombre_cientifico+'</td>'+
					'<td>'+es_cultivada+'</td>'+
					'<td>'+respuesta.forma_biologica+'</td>'+
					'<td>'+respuesta.usos+'</td>'+
					'<td>'+interesa_conserve+'</td>'+
					'<td>'+respuesta.comentarios+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarBiodiversidadPlantas('+respuesta.id+');"></td>'+
				'</tr>';
			$("#tbl_biodiversidad_plantas").append(s);

			$("#biodiversidad_plantas_especie").val('');
			$("#biodiversidad_plantas_comun").val('');
			$("#biodiversidad_plantas_cientifico").val('');
			$("#biodiversidad_plantas_cultivada").val('');
			$("#biodiversidad_plantas_forma_biologica").val('');
			$("#biodiversidad_plantas_usos").val('');
			$("#biodiversidad_plantas_conserve").val('');
			$("#biodiversidad_plantas_comentarios").val('');
			
		});
	}
}

function eliminarBiodiversidadPlantas(id)
{
	$.ajax({
		url: "/backend/diagnosticos/eliminarbiodiversidadplantas/",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		$("#fila_biodiversidad_plantas_"+id).remove();
	});
}


function agregarBiodiversidadAnimales()
{
	if($("#biodiversidad_animales_especie").val()=='' || $("#biodiversidad_animales_comun").val()=='' || $("#biodiversidad_animales_cientifico").val()=='' || $("#biodiversidad_animales_clasificacion").val()=='' || $("#biodiversidad_animales_usos").val()=='' || $("#biodiversidad_animales_conserve").val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "No pueden haber datos vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarbiodiversidadanimales/",
			type: "POST",
			data: {
				predio_id:$("#predio_id").val(), 
				especie:$("#biodiversidad_animales_especie").val(), 
				nombre_comun:$("#biodiversidad_animales_comun").val(), 
				nombre_cientifico:$("#biodiversidad_animales_cientifico").val(),  
				clasificacion:$("#biodiversidad_animales_clasificacion").val(), 
				usos:$("#biodiversidad_animales_usos").val(), 
				interesa_conserve:$("#biodiversidad_animales_conserve").val(), 
				comentarios:$("#biodiversidad_animales_comentarios").val()
			},
		}).done(function(respuesta)
		{
			var respuesta = jQuery.parseJSON(respuesta);			
			var s='<tr id="fila_biodiversidad_animales_'+respuesta.id+'">'+
					'<td>'+respuesta.especie+'</td>'+
					'<td>'+respuesta.nombre_comun+'</td>'+
					'<td>'+respuesta.nombre_cientifico+'</td>'+
					'<td>'+respuesta.clasificacion+'</td>'+
					'<td>'+respuesta.usos+'</td>'+
					'<td>'+respuesta.interesa_conserve+'</td>'+
					'<td>'+respuesta.comentarios+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarBiodiversidadAnimales('+respuesta.id+');"></td>'+
				'</tr>';
			$("#tbl_biodiversidad_animales").append(s);

			$("#biodiversidad_animales_especie").val('');
			$("#biodiversidad_animales_comun").val('');
			$("#biodiversidad_animales_cientifico").val('');
			$("#biodiversidad_animales_clasificacion").val('');
			$("#biodiversidad_animales_usos").val('');
			$("#biodiversidad_animales_conserve").val('');
			$("#biodiversidad_animales_comentarios").val('');

			
			
		});
	}
}

function eliminarBiodiversidadAnimales(id)
{
	$.ajax({
		url: "/backend/diagnosticos/eliminarbiodiversidadanimales",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		$("#fila_biodiversidad_animales_"+id).remove();
	});
}


function agregarBiodiversidadActividades()
{
	if($("#biodiversidad_actividades_actividad").val()=='' || $("#biodiversidad_actividades_comun").val()=='' || $("#biodiversidad_actividades_descripcion").val()=='' || $("#biodiversidad_actividades_nombre_local").val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "No pueden haber datos vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarbiodiversidadactividades/",
			type: "POST",
			data: {
				predio_id:$("#predio_id").val(), 
				actividad:$("#biodiversidad_actividades_actividad").val(), 
				nombre_comun:$("#biodiversidad_actividades_comun").val(), 
				descripcion:$("#biodiversidad_actividades_descripcion").val(),  
				fase_lunar:$("#biodiversidad_actividades_fase_lunar").val(), 
				nombre_local_fase:$("#biodiversidad_actividades_nombre_local").val(), 
			},
		}).done(function(respuesta)
		{
			var respuesta = jQuery.parseJSON(respuesta);
			var s='<tr id="fila_biodiversidad_actividades_'+respuesta.id+'">'+
					'<td>'+respuesta.actividad+'</td>'+
					'<td>'+respuesta.nombre_comun+'</td>'+
					'<td>'+respuesta.descripcion+'</td>'+
					'<td>'+respuesta.fase_lunar+'</td>'+
					'<td>'+respuesta.nombre_local+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarBiodiversidadActividades('+respuesta.id+');"></td>'+
				'</tr>';
			$("#tbl_biodiversidad_actividades").append(s);

			$("#biodiversidad_actividades_actividad").val('');
			$("#biodiversidad_actividades_comun").val('');
			$("#biodiversidad_actividades_descripcion").val('');
			$("#biodiversidad_actividades_fase_lunar").val('');
			$("#biodiversidad_actividades_nombre_local").val('');
			
		});
	}
}

function eliminarBiodiversidadActividades(id)
{
	$.ajax({
		url: "/backend/diagnosticos/eliminarbiodiversidadactividades",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		$("#fila_biodiversidad_actividades_"+id).remove();
	});
}