


function guardarCaracterizacionCosecha()
{
	var frm = new FormData(document.getElementById("frmCaracterizacionCosecha"));

	$.ajax({
		url: "/backend/diagnosticos/guardarcaracterizacioncosecha/",
		type: "POST",
		data: frm,
		processData: false,  
		contentType: false,
	}).done(function(respuesta)
	{
		try
		{
			respuesta = jQuery.parseJSON(respuesta);
			if(respuesta.ok !== undefined)
			{
				ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
			}
			else 
				ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(error)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
		}
	});
}

