function guardarCaracterizacionFinanciamiento()
{
	var frm = new FormData(document.getElementById("frmCaracterizacionFinanciamiento"));
	console.log(frm);

	$.ajax({
		url: "/backend/diagnosticos/guardarcaracterizacionfinanciamiento/",
		type: "POST",
		data: {
				predio_id:$("#predio_id").val(), 
				acceso_financiamiento:$("#acceso_financiamiento").val(), 
				formal_informal:$("#formal_informal").val(), 
				tipo_financiamiento:$("#tipo_financiamiento").val(), 
				cartera_vencida:$("#cartera_vencida").val(), 
				desde_cuando:$("#desde_cuando").val(), 
				seguro_agricola:$("#seguro_agricola").val(), 
				aseguradora:$("#aseguradora").val()
		},
	}).done(function(respuesta)
	{
		try
		{
			respuesta = jQuery.parseJSON(respuesta);
			if(respuesta.ok !== undefined)
			{
				ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", false, "");
			}
			else 
				ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(error)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
		}
	});
}

function cargarTipoFinanciamiento(valor='')
{
	$.ajax({
		url: "/backend/diagnosticos/cagartipofinanciamiento/",
		type: "POST",
		data: {tipo:$("#formal_informal").val()}
	}).done(function(respuesta)
	{
		$("#tipo_financiamiento").html(respuesta);
		if(valor!='')
			$("#tipo_financiamiento").val(valor);

	});

}

$(document).ready(function(){
	cargarTipoFinanciamiento($("#tipo_financiamiento_temporal").val());
});
