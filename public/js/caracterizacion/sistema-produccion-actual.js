$(document).ready(function(){

	$('.date').datepicker({ format: 'yyyy-mm-dd' });

});

function muestraTabla(valor, valorSi, aMostrar)
{
	if(valor==valorSi)
	{
		$("#"+aMostrar).removeClass('hide');
	}
	else
	{
		$("#"+aMostrar).addClass('hide');
	}

}



function guardarSistemaProduccionActual()
{
	var frm = new FormData(document.getElementById("frmSistemaProduccionActual"));

	$.ajax({
		url: "/backend/diagnosticos/guardarsistemaproduccionactual/",
		type: "POST",
		data: frm,
		processData: false,  
		contentType: false,
	}).done(function(respuesta)
	{
		try
		{
			$(".date", "#frmSistemaProduccionActual").attr('disabled', 'disabled');
			respuesta = jQuery.parseJSON(respuesta);
			if(respuesta.ok !== undefined)
			{
				ShowDialogBox("Guardado", "La información se ha guardado correctamente", "Ok", "", true, "/backend/diagnosticos/agregar/tipo/"+$("#gen-type").val()+"/id/"+$("#gen-id").val()+"/predio/"+$("#predio_id").val());
			}
			else 
				ShowDialogBox("Ha ocurrido un error", "No se han podido guardar correctamente el registro: " + respuesta.error + ", inténtelo nuevamente", "Ok", "", false, "");
		}
		catch(error)
		{
			ShowDialogBox("Ha ocurrido un error", "Ha ocurrido un error al tratar de guardar el formulario, por favor inténtelo nuevamente: " + error.message, "Ok", "", false, "");
		}
	});
}

//Tablas: maquinaria agrícola, equipo agricola, equipo tecnológico e infraestructura
function agregarEquipoSistemaProduccion(tipo)
{
	if($("#nombre_"+tipo).val()=='' || $("#cantidad_"+tipo).val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "Los campos nombre y cantidad no deben estar vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarsistemaequipo/",
			type: "POST",
			data: {predio_id:$("#predio_id").val(),nombre:$("#nombre_"+tipo).val(), cantidad:$("#cantidad_"+tipo).val(), tipo:tipo},
		}).done(function(respuesta)
		{
			var s='<tr id="fila_sistema_equipo_'+respuesta.ok+'">'+
					'<td>'+$("#nombre_"+tipo).val()+'</td>'+
					'<td>'+$("#cantidad_"+tipo).val()+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarEquipoSistemaProduccion('+respuesta.ok+');"></td>'+
				'</tr>';
			$("#tbl_"+tipo).append(s);

			$("#nombre_"+tipo).val('');
			$("#cantidad_"+tipo).val('');
		});
	}

}

function eliminarEquipoSistemaProduccion(id)
{
	
	$.ajax({
		url: "/backend/diagnosticos/eliminarsistemaequipo/",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		
		$("#fila_sistema_equipo_"+id).remove();

	});

}

//Tabla práctica de conservación
function agregarConservacionSistemaProduccion(tipo)
{
	if($("#nombre_tecnico_conservacion").val()=='' || $("#nombre_local_conservacion").val()=='' || $("#practica_vigente_conservacion").val()=='' || $("#de_quien_aprendio_conservacion").val()=='' || $("#tiempo_implementando_conservacion").val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "No pueden haber datos vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarsistemaconservacion/",
			type: "POST",
			data: {predio_id:$("#predio_id").val(), nombre_tecnico:$("#nombre_tecnico_conservacion").val(),nombre_local:$("#nombre_local_conservacion").val(), practica_vigente:$("#practica_vigente_conservacion").val(), de_quien_aprendio:$("#de_quien_aprendio_conservacion").val(), tiempo_implementando:$("#tiempo_implementando_conservacion").val()},
		}).done(function(respuesta)
		{
			var s='<tr id="fila_sistema_conservacion_'+respuesta.ok+'">'+
					'<td>'+$("#nombre_tecnico_conservacion").val()+'</td>'+
					'<td>'+$("#nombre_local_conservacion").val()+'</td>'+
					'<td>'+$("#practica_vigente_conservacion").val()+'</td>'+
					'<td>'+$("#de_quien_aprendio_conservacion").val()+'</td>'+
					'<td>'+$("#tiempo_implementando_conservacion").val()+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarConservacionSistemaProduccion('+respuesta.ok+');"></td>'+
				'</tr>';
			$("#tbl_conservacion_sistema_produccion").append(s);

			$("#nombre_tecnico_conservacion").val('');
			$("#nombre_local_conservacion").val('');
			$("#practica_vigente_conservacion").val('');
			$("#de_quien_aprendio_conservacion").val('');
			$("#tiempo_implementando_conservacion").val('');
			
		});
	}

}


function eliminarConservacionSistemaProduccion(id)
{
	
	$.ajax({
		url: "/backend/diagnosticos/eliminarsistemaconservacion/",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		
		$("#fila_sistema_conservacion_"+id).remove();

	});

}


function calcularPromedioClima(concepto)
{
	var suma=0;
	$('input.'+concepto).each(function()
	{
		if(parseFloat($(this).val()))
    		suma+=parseFloat($(this).val());
    	else
    		suma+=0;
	})

	$("#"+concepto).html(parseFloat(suma/12).toFixed(2));
}


function habilitaOtroCiclo(val)
{
	if(val!=4)
	{
		$("#otro_ciclo_relacion_cultivos").attr('disabled',true);
		$("#otro_ciclo_relacion_cultivos").val('No aplica');
	}
	else
	{
		$("#otro_ciclo_relacion_cultivos").attr('disabled',false);
		$("#otro_ciclo_relacion_cultivos").val('');
	}
}


function agregarCultivoSistemaProduccion(tipo='')
{
	if(tipo>1)
		tipo=tipo-1;
	if($("#cultivo_relacion_cultivos"+tipo).val()=='' || $("#ciclo_relacion_cultivos"+tipo).val()=='' || $("#superficie_relacion_cultivos"+tipo).val()=='' || $("#disposicion_relacion_cultivos"+tipo).val()=='' || $("#obtiene_semilla_relacion_cultivos"+tipo).val()=='' || $("#genetico_relacion_cultivos"+tipo).val()=='' || $("#variedad_relacion_cultivos"+tipo).val()=='' || $("#numero_relacion_cultivos"+tipo).val()=='' || $("#cosecha_relacion_cultivos"+tipo).val()=='' || $("#fecha_relacion_cultivos"+tipo).val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "No pueden haber datos vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarsistemacultivo/",
			type: "POST",
			data: {
				predio_id:$("#predio_id").val(), 
				id_cultivo:$("#cultivo_relacion_cultivos"+tipo).val(), 
				ciclo:$("#ciclo_relacion_cultivos"+tipo).val(), 
				otro_ciclo:$("#otro_ciclo_relacion_cultivos"+tipo).val(), 
				disposicion:$("#disposicion_relacion_cultivos"+tipo).val(), 
				obtiene_semilla:$("#obtiene_semilla_relacion_cultivos"+tipo).val(), 
				material_genetico:$("#genetico_relacion_cultivos"+tipo).val(), 
				variedad_semilla:$("#variedad_relacion_cultivos"+tipo).val(), 
				seleccion_semilla_propia:$("#seleccion_relacion_cultivos"+tipo).val(), 
				numero_semillas:$("#numero_relacion_cultivos"+tipo).val(), 
				cosecha:$("#cosecha_relacion_cultivos"+tipo).val(),
				fecha_cosecha:$("#fecha_relacion_cultivos"+tipo).val(),
				numero_hectareas:$("#superficie_relacion_cultivos"+tipo).val(),
				tipo:tipo
			},
		}).done(function(respuesta)
		{
			var respuesta = jQuery.parseJSON(respuesta);
			var s='<tr id="fila_relacion_cultivos_'+respuesta.id+'">'+
					'<td>'+respuesta.cultivo+'</td>'+
					'<td>'+respuesta.ciclo+'</td>'+
					'<td>'+respuesta.otro_ciclo+'</td>'+
					'<td>'+respuesta.numero_hectareas+'</td>'+
					'<td>'+respuesta.disposicion+'</td>'+
					'<td>'+respuesta.obtiene_semilla+'</td>'+
					'<td>'+respuesta.material_genetico+'</td>'+
					'<td>'+respuesta.variedad_semilla+'</td>'+
					'<td>'+respuesta.seleccion_semilla_propia+'</td>'+
					'<td>'+respuesta.numero_semillas+'</td>'+
					'<td>'+respuesta.cosecha+'</td>'+
					'<td>'+respuesta.fecha_cosecha+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarCultivoSistemaProduccion('+respuesta.id+');"></td>'+
				'</tr>';
			$("#tbl_relacion_cultivos"+tipo).append(s);

			if(tipo>1)
				$('#cultivo_plagas, #cultivo_enfermedades, #cultivo_abiotico, #cultivo_otros').append('<option value="'+respuesta.id+'">'+respuesta.cultivo+'</option>');

			$("#cultivo_relacion_cultivos"+tipo).val('');
			$("#ciclo_relacion_cultivos"+tipo).val('');
			$("#otro_ciclo_relacion_cultivos"+tipo).val('');
			$("#disposicion_relacion_cultivos"+tipo).val('');
			$("#genetico_relacion_cultivos"+tipo).val('');
			$("#variedad_relacion_cultivos"+tipo).val('');
			$("#seleccion_relacion_cultivos"+tipo).val('');
			$("#numero_relacion_cultivos"+tipo).val('');
			$("#cosecha_relacion_cultivos"+tipo).val('');
			$("#fecha_relacion_cultivos"+tipo).val('');
			$("#superficie_relacion_cultivos"+tipo).val('');

			//Integramos una fila a la cosecha
			if(tipo!=1)
				$("#tblCosecha").append(respuesta.fila_cosecha);
		});
	}
}

function eliminarCultivoSistemaProduccion(id)
{
	$.ajax({
		url: "/backend/diagnosticos/eliminarsistemacultivo/",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		if(respuesta.error)
			ShowDialogBox("Error", "Ocurrió un error el eliminar el registro", "Ok", "", false, "");
		else{
			$("#fila_relacion_cultivos_"+id).remove();
			$("#cultivo_plagas option[value='"+id+"']").remove();
			$("#cultivo_enfermedades option[value='"+id+"']").remove();
			$("#cultivo_abiotico option[value='"+id+"']").remove();
			$("#cultivo_otros option[value='"+id+"']").remove();
		}
	});
}



//Tablas: maquinaria agrícola, equipo agricola, equipo tecnológico e infraestructura
function agregarEventoSistemaProduccion(tipo)
{
	if($("#nombre_"+tipo).val()=='' || $("#cultivo_"+tipo).val()=='' || $("#periodo_"+tipo).val()==''|| $("#etapa_"+tipo).val()==''||  $("#producto_"+tipo).val()=='')
	{
		ShowDialogBox("Imposible agregar el registro", "Los campos nombre y cantidad no deben estar vacíos", "Ok", "", false, "");
	}
	else
	{
		$.ajax({
			url: "/backend/diagnosticos/guardarsistemaevento/",
			type: "POST",
			data: {predio_id:$("#predio_id").val(),nombre:$("#nombre_"+tipo).val(),cultivo_id:$("#cultivo_"+tipo).val(),periodo:$("#periodo_"+tipo).val(),etapa:$("#etapa_"+tipo).val(),nombre_local_etapa:$("#local_etapa_"+tipo).val(),producto_aplico:$("#producto_"+tipo).val(), tipo:tipo},
		}).done(function(respuesta)
		{
			var s='<tr id="fila_sistema_equipo_'+respuesta.ok+'">'+
					'<td>'+$("#nombre_"+tipo).val()+'</td>'+
					'<td>'+$("#cultivo_"+tipo+" option[value='"+$("#cultivo_"+tipo).val()+"']").text()+'</td>'+
					'<td>'+$("#periodo_"+tipo).val()+'</td>'+
					'<td>'+$("#etapa_"+tipo).val()+'</td>'+
					'<td>'+$("#local_etapa_"+tipo).val()+'</td>'+
					'<td>'+$("#producto_"+tipo).val()+'</td>'+
					'<td><input type="button" class="btn btn-danger" value="-" onclick="eliminarEquipoSistemaProduccion('+respuesta.ok+');"></td>'+
				'</tr>';
			$("#tbl_"+tipo+"_sistema_produccion").append(s);

			$("#nombre_"+tipo).val('');
			$("#cultivo_"+tipo).val('');
			$("#periodo_"+tipo).val('');
			$("#etapa_"+tipo).val('');
			$("#local_etapa_"+tipo).val('');
			$("#producto_"+tipo).val('');
			
		});
	}

}

function eliminarEventoSistemaProduccion(id)
{
	$.ajax({
		url: "/backend/diagnosticos/eliminarsistemaevento/",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		$("#fila_sistema_evento_"+id).remove();
	});
}

function eliminarEquipoSistemaProduccion(id)
{
	$.ajax({
		url: "/backend/diagnosticos/eliminarsistemaequipo/",
		type: "POST",
		data: {id:id},
	}).done(function(respuesta)
	{
		
		$("#fila_sistema_equipo_"+id).remove();

	});
}