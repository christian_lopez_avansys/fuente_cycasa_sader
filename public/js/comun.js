function openSubMenu() 
{
    $(this).find('ul').css('visibility', 'visible');	
}

function closeSubMenu() 
{
    $(this).find('ul').css('visibility', 'hidden');	
}



$(document).ready(function() 
{
    $('#menu > div').bind('mouseover', openSubMenu);
    $('#menu > div').bind('mouseout', closeSubMenu);

    $(".ui-icon-closethick").hide();

});

function ShowDialogBox(title, content, btn1text, btn2text, isEdit, sendTo) {
    var btn1css;
    var btn2css;

    if (btn1text == '')
    {
        btn1css = "hidecss";
    }
    else
    {
        btn1css = "showcss";
    }

    if (btn2text == '')
    {
        btn2css = "hidecss";
    }
    else
    {
        btn2css = "showcss";
    }
	$("#lblMessage").html('');
    $("#_dialogo-1").html(content);
	//antes el id donde se cargaba el dialogo era #dialog. se modifico porque no se actualizaban los mensajes de error
    $("#_dialogo-1").dialog({
        resizable: false,
        title: title,
        modal: true,
        width: '400px',
        height: 'auto',
		open:function(event, ui) {
        	/*$(event.target).dialog('widget')
            .css({ zIndex:"99999"})
            .position({ my: 'center', at: 'center', of: window });*/
    	},
		close:function(event, ui){
			$("#_dialogo-1").dialog('close');
			$("#_dialogo-1").dialog('destroy');
		},
        bgiframe: false,
        hide: { effect: 'scale', duration: 0 },
        buttons: [
            {
                text: btn1text,
                "class": btn1css,
                click: function () {                    
                    $("#_dialogo-1").dialog('close');
                    if(isEdit) window.location = sendTo;
                }
            },
            {
                text: btn2text,
                "class": btn2css,
                click: function () {
                    $("#_dialogo-1").dialog('close');
                    if(isEdit) window.location = sendTo;
                }
            }
        ]
    });
}


function ShowConfirmBox(title, content, funcion, id, div) {
    var btn1css;
    var btn2css;

    btn1css = "showcss";
    btn2css = "showcss";
	$("#lblMessage").html('');
    $("#lblMessage").html(content);

    $("#dialog").dialog({
        resizable: false,
        title: title,
        modal: true,
		open:function(event, ui) {
        	/*$(event.target).dialog('widget')
            .css({ zIndex:"99999"})
            .position({ my: 'center', at: 'center', of: window });*/
    	},
		close:function(event, ui){
			$("#dialog").dialog('close');
			$("#dialog").dialog('destroy');
		},
        width: '400px',
        height: 'auto',
        bgiframe: false,
        hide: { effect: 'scale', duration: 0 },
        buttons: [
            {
                text: "Aceptar",
                "class": btn1css,
                click: function () {    
                    $("#dialog").dialog('close');
                    funcion(id, div);                
                }
            },
            {
                text: "Cancelar",
                "class": btn2css,
                click: function () {
                    $("#dialog").dialog('close');
                }
            }
        ]
    });
}
function ShowExecuteBox(title, content, funcion, id, div) {

	$("#_mensaje-1").html('');
    $("#_mensaje-1").html(content);

    $("#_mensaje-1").dialog({
        resizable: false,
        title: title,
        modal: true,
		beforeClose:function(event, ui){
			funcion(id, div); 
		},
		open:function(event, ui) {
        	/*$(event.target).dialog('widget')
            .css({ zIndex:"99999"})
            .position({ my: 'center', at: 'center', of: window });*/
    	},
		close:function(event, ui){
			$("#_mensaje-1").dialog('close');
			$("#_mensaje-1").dialog('destroy');
		},
        width: '400px',
        height: 'auto',
        bgiframe: false,
        hide: { effect: 'scale', duration: 0 },
        buttons: [
            {
                text: "OK",
                click: function () {    
                    $("#_mensaje-1").dialog('close');
					$("#_mensaje-1").dialog('destroy');
                    funcion(id, div);                
                }
            },
        ]
    });
}

function ShowLoadingBox(title, content) {
    var btn1css;
    var btn2css;

    btn1css = "showcss";
    btn2css = "showcss";
	$("#lblMessage").html('');
    $("#lblMessage").html(content);

    $("#dialog").dialog({
        resizable: false,
        title: title,
        modal: true,
		open:function(event, ui) {
        	/*$(event.target).dialog('widget')
            .css({ zIndex:"99999"})
            .position({ my: 'center', at: 'center', of: window });*/
    	},
		close:function(event, ui){
			$("#dialog").dialog('close');
			$("#dialog").dialog('destroy');
		},
        width: '400px',
        height: 'auto',
        bgiframe: false,
        hide: { effect: 'scale', duration: 0 },
        buttons: []
    });
}
function ShowWindowBox(title, content, funcion, id, div) {
	$("#dialogWindow").html('');
    $("#dialogWindow").html(content);
	
    $("#dialogWindow").dialog({
        resizable: false,
        title: title,
		close:function(event, ui){
			$("#dialogWindow").dialog('close');
			$("#dialogWindow").dialog('destroy');
		},
		open:function(event, ui) {
        	$(event.target).dialog('widget')
            .css({ position: 'fixed', zIndex:"9999", top:"60" })
            .position({ my: 'center', at: 'center', of: window });
    	},
        modal: true,
        width: '800px',
        height: 'auto',
		draggable:false,
        //hide: { effect: 'scale', duration: 0 },
        buttons: [
        ]
    });
}
function ShowWindow2Box(title, content, funcion, id, div) {
	$("#dialogWindow2").html('');
    $("#dialogWindow2").html(content);

    $("#dialogWindow2").dialog({
        resizable: false,
        title: title,
		close:function(event, ui){
			$("#dialogWindow2").dialog('close');
			$("#dialogWindow2").dialog('destroy');
		},
		open:function(event, ui) {
        	$(event.target).dialog('widget')
            .css({ position: 'fixed', zIndex:"9999", top:"60" })
            .position({ my: 'center', at: 'center', of: window });
    	},
        modal: true,
        width: '800px',
        height: 'auto',
        bgiframe: false,
        hide: { effect: 'scale', duration: 0 },
        buttons: [
        ]
    });
}
//PERMITE NUMEROS FLOTANTES Y NEGATIVOS
function permiteNumerosConDecimalN(evt, obj)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
	
    var dotcontains = value.indexOf(".") != -1;
	var dotcontains2 = value.indexOf("-") >0;

    if (dotcontains)
        if (charCode == 46) return false;
	if(dotcontains2)
		if (charCode == 45) return false;
    if (charCode == 46 && value!='') return true;
	if (charCode == 45 && value=='') return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;

}
function permiteNumerosConDecimal(evt, obj)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
	
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
        if (charCode == 46) return false;
    if (charCode == 46 && value!='') return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;

}
function permiteNumerosSinDecimal(evt, obj)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != 1;
    if (dotcontains)
        if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;

}
