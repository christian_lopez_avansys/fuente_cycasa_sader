var my_skins = ["skin-blue", "skin-black", "skin-red", "skin-yellow", "skin-purple", "skin-green"];
var demo, info, demo_settings, info_settings;
$(function () {
//mostramos en el div
/*	  $("#map-canvas").animate({"height":"68%"});
	  $("#var_canvas").animate({"height":"25%"});
	  $("#stats_canvas").animate({"height":"0%"});
	  $("#var_canvas").addClass("open");
	  $("#stats_canvas").removeClass("open");*/
  /* For demo purposes */
  demo = $("<div id='togg-demo' />").css({
    position: "relative",
    background: "#fff",
    "border-radius": "0px 5px 5px 0px",
    padding: "10px 15px",
    "font-size": "16px",
    "z-index": "99999",
    cursor: "pointer",
    color: "#3c8dbc",
	width: "50px",
	height: "45px",
	margin:"10px 0px",
    "box-shadow": "0 1px 3px rgba(0,0,0,0.1)"
  }).html("<i class='fa fa-gear'></i>").addClass("no-print2");

  demo_settings = $("<div />").css({
    "padding": "10px",
    position: "relative",
    top: "-45px",
    left: "-250%",
    background: "#fff",
    border: "0px solid #ddd",
    "width": "250%",
    "z-index": "99999",
    "box-shadow": "0 1px 3px rgba(0,0,0,0.1)",
	height: "20px",
  }).addClass("no-print2");

  info = $("<div id='togg-info' />").css({
    position: "relative",
    background: "#fff",
    "border-radius": "0px 5px 5px 0px",
    padding: "10px 15px",
    "font-size": "16px",
    "z-index": "99999",
    cursor: "pointer",
    color: "#3c8dbc",
	width: "50px",
	height: "45px",
	margin:"10px 0px",
    "box-shadow": "0 1px 3px rgba(0,0,0,0.1)"
  }).html("<i class='fa fa-warning'></i>").addClass("no-print2");

  info_settings = $("<div />").css({
    "padding": "10px",
    position: "relative",
    left: "-250%",
    background: "#fff",
    border: "0px solid #ddd",
    "width": "250%",
	height: "45px",
    "z-index": "99999",
    "box-shadow": "0 1px 3px rgba(0,0,0,0.1)",
	top: "-45px",

  }).addClass("no-print2");
 
$('#var_canvas').append(
  		"<div id='variables_demo'>"+
			"<div class='col-xs-10'>"+
				"<h4 class='text-light-blue' style='margin: 0; border-bottom: 1px solid #ddd; padding: 3px 0px; text-align:center'>Variables</h4>"+
				"<div class='col-xs-12'>"+
					"<div class='col-xs-2'>"+
						"<div class='form-group'>"
							+ "<div class='radio' style='position: unset !important;'>"
								+ "<label>"
									+ "<input name='variables' value='cah' type='radio' onclick='buscaSubVariables(\"cah\")'/>"
										  + " cah"
								+ "</label>"
							  + "<div id='s_cah' style='padding-left: 1.5em; display:none;'></div>"
							+ "</div>"
						+ "</div>"+
					"</div>"+
					"<div class='col-xs-2'>"+
						"<div class='form-group'>"
							+ "<div class='radio' style='position: unset !important;'>"
								+ "<label>"
									+ "<input name='variables' value='arena' type='radio' onClick='buscaSubVariables(\"arena\")'/>"
									+" Arena"
								+ "</label>"
							  + "<div id='s_arena' style='padding-left: 1.5em; display:none;'></div>"
							+ "</div>"
						  + "</div>"+
					"</div>"+
					"<div class='col-xs-2'></div>"+
					"<div class='col-xs-2'></div>"+
				"</div>"+
			"</div>"+
		"</div>"
);

/*  $('#stats_canvas').append(
  
  		"<div id='variables_demo'>"+
          "<table width='100%' border='0' cellpadding='5'>"+
			  "<tr>"+
				"<th colspan='4'><h4 class='text-light-blue' style='margin: 0 0 5px 0; border-bottom: 1px solid #ddd; padding-bottom: 15px; text-align:center'>Variables</h4></th>"+
			  "</tr>"+
			  "<tr>"+
				"<td>"+
					"<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='arcilla' type='radio' onclick='buscaSubVariables(\"arcilla\")'/>"
									  + " Arcilla"
							+ "</label>"
						  + "<div id='s_arcilla' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					+ "</div>"
				+"</td>"+
				"<td>"+
					"<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='arena' type='radio' onClick='buscaSubVariables(\"arena\")'/>"
								+" Arena"
							+ "</label>"
						  + "<div id='s_arena' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					  + "</div>"
				+"</td>"+
				"<td>"+
					"<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='cah' type='radio' onClick='buscaSubVariables(\"cah\")'/>"
								+" CAH"
							+ "</label>"
					  	+ "<div id='s_cah' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
				  	+ "</div>"
				+"</td>"+
				"<td>"+
				"<div class='form-group'>"
					+ "<div class='radio' style='position: unset !important;'>"
						+ "<label>"
							+ "<input name='variables' value='profundidad' type='radio' onClick='buscaSubVariables(\"profundidad\")'/>"
							+" Profundidad"
						+ "</label>"
					  + "<div id='s_profundidad' style='padding-left: 1.5em; display:none;'></div>"
					+ "</div>"
				  + "</div>"+
				"</td>"+
				"<td>"
					+ "<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='cic' type='radio' onClick='buscaSubVariables(\"cic\")'/>"
								+" CIC"
							+ "</label>"
						  + "<div id='s_cic' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					  + "</div>"+
				"</td>"+
				"</tr>"+
			   "<tr>"+
				"<td>"
					+ "<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='iht' type='radio' onClick='buscaSubVariables(\"iht\")'/>"
								+" IHT"
							+ "</label>"
						  + "<div id='s_iht' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					  + "</div>"+
				"</td>"+
				"<td>"
					+ "<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='ip' type='radio' onClick='buscaSubVariables(\"ip\")'/>"
								+" IP"
							+ "</label>"
						  + "<div id='s_ip' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					  + "</div>"+
				"</td>"+
				"<td>"+
					"<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='sb' type='radio' onClick='buscaSubVariables(\"sb\")'/>"
								+" SB"
							+ "</label>"
						  + "<div id='s_sb' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					  + "</div>"+
				"</td>"+
				"<td>"
					+ "<div class='form-group'>"
						+ "<div class='radio' style='position: unset !important;'>"
							+ "<label>"
								+ "<input name='variables' value='limo' type='radio' onClick='buscaSubVariables(\"limo\")'/>"
								+" LIMO"
							+ "</label>"
						  + "<div id='s_limo' style='padding-left: 1.5em; display:none;'></div>"
						+ "</div>"
					  + "</div>"
				+"</td>"+
			"</tr>"+
			  "<tr>"+
				"<td>"
					+ "<div class='form-group'>"
					+ "<div class='radio' style='position: unset !important;'>"
						+ "<label>"
							+ "<input name='variables' value='mo' type='radio' onClick='buscaSubVariables(\"mo\")'/>"
							+" MO"
						+ "</label>"
					  + "<div id='s_mo' style='padding-left: 1.5em; display:none;'></div>"
					+ "</div>"
				  + "</div>"+
				"</td>"+
				"<td>"
				+ "<div class='form-group'>"
					+ "<div class='radio' style='position: unset !important;'>"
						+ "<label>"
							+ "<input name='variables' value='ph' type='radio' onClick='buscaSubVariables(\"ph\")'/>"
							+" PH"
						+ "</label>"
					  + "<div id='s_ph' style='padding-left: 1.5em; display:none;'></div>"
					+ "</div>"
				  + "</div>"+
				"</td>"+
				"<td>"+
				"<div class='form-group'>"
					+ "<div class='radio' style='position: unset !important;'>"
						+ "<label>"
							+ "<input name='variables' value='textura' type='radio' onClick='buscaSubVariables(\"textura\")'/>"
							+" Textura"
						+ "</label>"
					  + "<div id='s_textura' style='padding-left: 1.5em; display:none;'></div>"
					+ "</div>"
				  + "</div>"+
				"</td>"+
			  "</tr>"+
			"</table>"
		  +"</div>"
  );
*/		
		  
		
		
		
  $("#stats_canvas").append(
    "<div id='table-info-settings' style='text-align: center; width: 84%; margin: 5px; position: absolute;'><label>De clic en alguna parte del mapa para observar su informaci&oacute;n</label></div>"
  );

   demo.click(function(){
    if (!$(this).hasClass("open")) 
    {
      //$(this).animate({"right": "250px"});
	  //demo_settings.animate({"right": "0"});
	  //$(this).animate({"left": "250%"});
	  //demo_settings.animate({"left": "0"});
      $(this).addClass("open");
	  if(info.has("open")){
		 info.removeClass("open") 
	  }
	  //info.removeClass("open");
      //////////////////////////
      //$("#togg-info").hide();
	  //$("#togg-info").css("visibility","hidden");
	  //demo_settings.css("display","block");
	  //demo_settings.css("height","auto");
      //info.animate({"right": "0"});
      //info_settings.animate({"right": "-50%"});
	  //info.animate({"left": "0"});
      //info_settings.animate({"left": "-250%"});
      //$("#var_canvas").removeClass("open");
	  //mostramos en el div
	  $("#map-canvas").animate({"height":"68%"});
	  $("#var_canvas").animate({"height":"0%"});
	  $("#stats_canvas").animate({"height":"25%"});
    }
    else
    {
      //$(this).animate({"right": "0"});
//      demo_settings.animate({"right": "-250px"});
		//$(this).animate({"left": "0"},"fast",function(){
		 //$("#togg-info").css("visibility","visible");
		 //demo_settings.css("display","block");
		 //demo_settings.css("height","20px");	
		//});
		
      	//demo_settings.animate({"left": "-250%"});
        $(this).removeClass("open");
		$("#map-canvas").animate({"height":"93%"});
		$("#var_canvas").animate({"height":"0%"});
		$("#stats_canvas").animate({"height":"0%"});
      	//$("#togg-info").show();
		
		
    }
  });

  info.click(function(){
    if(!$(this).hasClass("open")) 
    {
      //$(this).animate({"right": "50%"});
     // info_settings.animate({"right": "0"});
	 //$(this).animate({"left": "250%"});
     //info_settings.animate({"left": "0"});
      $(this).addClass("open");
	   if(demo.has("open")){
		 demo.removeClass("open") 
	  }
	  //demo.removeClass("open");
      ///////////////////////////
      //demo.animate({"right": "0"});
      //demo_settings.animate({"right": "-250px"});
	  // $("#togg-demo").css("visibility","hidden");
	 	//info_settings.css("display","block");
	  	//info_settings.css("height","auto");
	  //demo.animate({"left": "0"});
      //demo_settings.animate({"left": "-250%"});
      //$("#stats_canvas").removeClass("open");
	  //$("#togg-demo").hide();
      //$("#togg-demo").css("visibility","hidden");
	  
	  //mostramos en el div
	  $("#map-canvas").animate({"height":"68%"});
	  $("#var_canvas").animate({"height":"25%"});
	  $("#stats_canvas").animate({"height":"0%"});
    }
    else
    {
      //$(this).animate({"right": "0"});
      //info_settings.animate({"right": "-50%"});
	  //$(this).animate({"left": "0"},"fast",function(){
		//$("#togg-demo").css("visibility","visible");
		  //info_settings.css("display","block");
		  //info_settings.css("height","20px");  
	  //});
      //info_settings.animate({"left": "-250%"});
      $(this).removeClass("open");
	  //$("#togg-demo").css("visibility","visible");
	  
	  //mostramos en el div
	  $("#map-canvas").animate({"height":"93%"});
	  $("#var_canvas").animate({"height":"0%"});
	  $("#stats_canvas").animate({"height":"0%"});
      //$("#togg-demo").show();
    }
  });

  $("#demo_info").append(demo);
 // $("#stats_canvas").append(demo_settings);
  $("#demo_info").append(info);
 // $("#var_canvas").append(info_settings);

  setup();
});


function change_layout(cls) {
  $("body").toggleClass(cls);
  $.AdminLTE.layout.fixSidebar();  
}

function store(name, val) {
  if (typeof (Storage) !== "undefined") {
    localStorage.setItem(name, val);
  } else {
    alert('Please use a modern browser to properly view this template!');
  }
}

function get(name) {
  if (typeof (Storage) !== "undefined") {
    return localStorage.getItem(name);
  } else {
    alert('Please use a modern browser to properly view this template!');
  }
}

function setup() {
  var tmp = get('skin');
  if (tmp && $.inArray(tmp, my_skins))
    change_skin(tmp);
}
