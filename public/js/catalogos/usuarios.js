var tipo;
function agregarUsuario(id)
{
    $.ajax({
            url:"/catalogos/index/agregar/id/"+id,
            success:function result(data)
            {                    
                    $(".dialog-form").html(data);
                    
                    $("#frmUsuario").validate(
                    {
                       /*rules:
                       {
                            plaza_id: {required:function(){if($("#tipo").val()=='1')return true; else return false;},min:1}
                       },
                       messages:
                       {
                            plaza_id: {min: "Debe elegir una plaza para este tipo de usuario"}
                       },*/
                       submitHandler: function(form) 
                       {
                            $("#Aplicar,#Aceptar").hide(); 
                         $(form).ajaxSubmit(
                         {
                            success:    function(response) 
                            { 
                                if(response=="-1")
                                {
                                    $("#Aplicar,#Aceptar").show(); 
                                    alert("Este nombre de usuario ya existe");
                				}
                				else
                				{
                                    $('#flexgrid').flexReload();
                                    $("#Aplicar,#Aceptar").show(); 
                                    if(tipo==1)
                                    {
                                        $('#frmUsuario').resetForm();	
                                    }
                                    else
                                    {
                                        $(".dialog-form").dialog("destroy");
					                    $(".dialog-form").dialog("close");
                                    }
				                }
                            }
                         });
                       }
                    });
                    
                    
                    $(".dialog-form").dialog({
                            height:'auto',
                            width: 'auto',
                            resizable: false,
                            title: "Agregar usuario",
                            position:"center",
                            modal: true,
                            buttons: [{
                                    id:"Aplicar",
                                    text:"Aplicar",
                                    click:function()
                                    {
                                            tipo = 1;
											
                                            $("#frmUsuario").submit();
                                    }
                                    },
                                    {
                                        id:"Aceptar",
                                        text:"Aceptar",
                                        click:
                                        function()
                                        {
                                                tipo = 2;
                                                $("#frmUsuario").submit();
                                        }
                                    },
                                    {
                                        id:"Cancelar",
                                        text:"Cancelar",
                                        click:function()
                                        {
                                            $(".dialog-form").dialog("destroy");
                                            $(".dialog-form").dialog("close");
                                        }
                                    }
                            ]
                            });                     
                    if(id != 0 )
                    {
                        $("#Aplicar").hide();
                    }    
                    
            }
    });

}

function mostrarTablaPermisos(id)
{
  $('#divPermisos'+id).toggle();  
}

function permisosUsuario(id)
{
    $.ajax({
            url:"/catalogos/index/permisos/id/"+id,
            success:function result(data)
            {
				$(".dialog-form").html(data);        
				
					$("#frmPermisos").validate(
                    {
                       submitHandler: function(form) 
                       {
                         $(form).ajaxSubmit(
                         {
                            success:    function(response) 
                            { 
									alert('Los permisos del usuario han sido actualizados');
									location.reload();
                                    $(".dialog-form").dialog("destroy");
                                    $(".dialog-form").dialog("close");
                            }
                         });
                       }
                    });				
				
				
				            
                    $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Permisos de usuario",
                        position: [240,100],
                        modal: true,
                        buttons: 
                        [
                            {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                                    tipo = 2;
                                                    $("#frmPermisos").submit();
                                    }
                            },
                            {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                            $(".dialog-form").dialog("destroy");
                                            $(".dialog-form").dialog("close");
                                    }
                            }
                        ]
                    });                     
                    
            }
    });

}


function eliminarUsuario(id, status)
{
    if(status == 1)
    {
        if(confirm('¿En realidad desea deshabilitar este registro?'))
        {
            $.ajax({
                type: 'POST',
                url:"/catalogos/index/eliminar",
                data:{id:id},
                success:
                function respuesta(res)
                {
                    alert('El registro ha sido deshabilitado.');
                    $('#flexgrid').flexReload();
                }
            });
        }
    }else if(status == 0)
    {
        if(confirm('¿En realidad desea habilitar este registro?'))
        {
            $.ajax({
                type: 'POST',
                url:"/catalogos/index/habilitar",
                data:{id:id},
                success:
                function respuesta(res)
                {
                    alert('El registro ha sido habilitado.');
                    $('#flexgrid').flexReload();
                }
            });
        }
    }
}



$(document).ready(function() 
{   
    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/index/grid',
        dataType: 'xml',
        colModel : [
                
                {display: 'USUARIO',  name:'login',width : 880, sortable : true, align: 'center'},
                /*{display: 'PERMISOS',   width : 100, sortable : false, align: 'center'},*/
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "login",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1122,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function filtrarUsuarios()
{
    var filtro = "/catalogos/index/grid";
    var imprimir="/catalogos/index/imprimir";
    var exportar="/catalogos/index/exportar";
	
    if($("#fnombre").val() != "")
    {
        filtro += "/nombre/"+$("#fnombre").val();
        imprimir += "/nombre/"+$("#fnombre").val();
        exportar += "/nombre/"+$("#fnombre").val();
    }
	
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarUsuarios()
{
    $("#fnombre").attr('value','');
    var filtro = "/catalogos/index/grid";
	
    var imprimir="/catalogos/index/imprimir";
    var exportar="/catalogos/index/exportar";
    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);

    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}