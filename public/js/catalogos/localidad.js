$(document).ready(function()
{
	$("#flexigrid").flexigrid({
        url: "/backend/localidad/grid/"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"}
            ,{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"}            
            ,{display: "Localidad", name: "nombre", width: 321, sortable: true, align: "center"}
            ,{display: "Estado", name: "estado", width: 323, sortable: false, align: "center"}
            ,{display: "Municipio", name: "municipio", width: 321, sortable: false, align: "center"}
        ]
        ,sortname: "nombre"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){
        
        }
        ,onError: function(respuesta){
            ShowDialogBox("Ha ocurrido un error", "No se han podido mostrar las localidades, intentelo nuevamente", "Ok", "", false, "");
        }
    });

    $("#id_estado").change(function(e){
        $.ajax({
            url: "/backend/localidad/obtenermunicipios/estado/" + $("#id_estado").val(),
            type: "POST",
            data: frm,
            processData: false,  
            contentType: false
        }).done(function(data){
            $("#clave_municipio").html(data);
        });
    });

    $("#filtroEstado").change(function(e){
        $.ajax({
            url: "/backend/localidad/obtenermunicipios/estado/" + $("#filtroEstado").val(),
            type: "POST",
            processData: false,  
            contentType: false
        }).done(function(data){
            $("#filtroMunicipio").html(data);
        });
    });

	$("#btnGuardar").click(function(e){
        if($("#nombre").val().trim() == '')
            ShowDialogBox("Datos incompletos", "El nombre del localidad es requerido", "Ok", "", false, "");
        else if($("#id_estado").val() <= 0)
            ShowDialogBox("Datos incompletos", "El estado es requerido", "Ok", "", false, "");
        else if($("#clave_municipio").val() <= 0)
            ShowDialogBox("Datos incompletos", "El municipio es requerido", "Ok", "", false, "");
        else if($("#clave").val() <= 0)
            ShowDialogBox("Datos incompletos", "La clave del localidad es requerida", "Ok", "", false, "");
        else
        {
        	var frm = new FormData(document.getElementById("frm"));
    	    $.ajax({
    	    	url: "/backend/localidad/guardar/",
    	      	type: "POST",
    	      	data: frm,
    	      	processData: false,  
    	      	contentType: false
    	    }).done(function(data){
    	    	if(data > 0) 
    	    	{
    	    		if($("#id").val() > 0)
                        ShowDialogBox("Guardado", "El registro se ha editado correctamente", "Ok", "", true, "/backend/localidad/");
    	    		else
                    {
                        ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
                        limpiar();
                    }
    	    	}
    	    	else if(data == -1) 
    	    		ShowDialogBox("No se ha podido guardar", "El registro ya existe", "Ok", "", false, "");
    	    	else 
    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
    	    });
        }
 	});
});

function agregar(id=0)
{
	window.location = "/backend/localidad/agregar/id/" + id;
}

function filtrargrig()
{
    var nombre = $("#filtroNombre").val();
    var estado = $("#filtroEstado").val();
    var municipio = $("#filtroMunicipio").val();
    $('#flexigrid').flexOptions({url: "/backend/localidad/grid/nombre/" + nombre + "/estado/" + estado + "/municipio/" + municipio}).flexReload(); 
}

function limpiar()
{
	$("#nombre").val('');
    $("#id_estado").val('0');
    $("#clave").val('');
}

function limpiarFiltro()
{
    $("#filtroNombre").val('');
    $("#filtroEstado").val('0');
    $("#filtroMunicipio").val('0');
}

function deshabilitar(id)
{
	$.ajax({
    	url: "/backend/localidad/deshabilitar/id/" + id,
      	type: "POST",
      	processData: false,  
      	contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/localidad/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}

function habilitar(id)
{
    $.ajax({
        url: "/backend/localidad/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/localidad/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}