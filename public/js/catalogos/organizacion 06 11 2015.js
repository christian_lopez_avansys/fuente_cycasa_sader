$(document).ready(function()
{
	$("#flexigrid").flexigrid({
        url: "/backend/organizacion/grid/"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"},
			{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"},
			{display: "Organizacion", name: "nombre", width: 400, sortable: true, align: "center"},
			{display: "RFC", name: "rfc", width: 180, sortable: false, align: "center"},
			{display: "Domicilio", name: "domicilio", width: 390, sortable: false, align: "center"}
        ]
        ,sortname: "nombre"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){
        
        }
        ,onError: function(respuesta){
            ShowDialogBox("Ha ocurrido un error", "No se han podido mostrar las organizaciones, intentelo nuevamente", "Ok", "", false, "");
        }
    });

	$("#btnGuardar").click(function(e){
        if($("#nombre").val().trim() == '')
            ShowDialogBox("Datos incompletos", "El nombre es requerido", "Ok", "", false, "");
        else if($("#rfc").val().length!=12 && $("#rfc").val().length!=13){
			ShowDialogBox("Datos incompletos", "El RFC no tiene un formato valido", "Ok", "", false, "");
		}else if($("#domicilio").val().trim()==''){
			ShowDialogBox("Datos incompletos", "El Domicilio es requerido", "Ok", "", false, "");
		}else
        {
        	var frm = new FormData(document.getElementById("frm"));
    	    $.ajax({
    	    	url: "/backend/organizacion/guardar/",
    	      	type: "POST",
    	      	data: frm,
    	      	processData: false,  
    	      	contentType: false
    	    }).done(function(data){
    	    	if(data > 0) 
    	    	{
    	    		if($("#id").val() > 0)
                        ShowDialogBox("Guardado", "El registro se ha editado correctamente", "Ok", "", true, "/backend/organizacion/");
    	    		else
                    {
                        ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
                        limpiar();
                    }
    	    	}
    	    	else if(data == -1) 
    	    		ShowDialogBox("No se ha podido guardar", "El registro ya existe", "Ok", "", false, "");
    	    	else 
    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
    	    });
        }
 	});
});

function agregar(id=0)
{
	window.location = "/backend/organizacion/agregar/id/" + id;
}

function filtrargrig()
{
    var filtro = $("#filtroNombre").val();
    $('#flexigrid').flexOptions({url: "/backend/organizacion/grid/filtro/" + filtro}).flexReload(); 
}

function limpiar()
{
	$("#nombre").val('');
}

function limpiarFiltro()
{
    $("#filtroNombre").val('');
}

function deshabilitar(id)
{
	$.ajax({
    	url: "/backend/organizacion/deshabilitar/id/" + id,
      	type: "POST",
      	processData: false,  
      	contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/organizacion/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}

function habilitar(id)
{
    $.ajax({
        url: "/backend/organizacion/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/organizacion/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}