$(document).ready(function()
{
	$("#flexigrid").flexigrid({
        url: "/backend/productor/grid/"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"},
            {display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"},          
            {display: "Productor", name: "nombre", width: 200, sortable: true, align: "center"},
            {display: "RFC", name: "rfc", width: 100, sortable: true, align: "center"},
            {display: "Domicilio", name: "domicilio", width: 665, sortable: true, align: "center"}
        ]
        ,sortname: "nombre"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){
        
        }
        ,onError: function(respuesta){
            ShowDialogBox("Ha ocurrido un error", "No se han podido mostrar los productores, intentelo nuevamente", "Ok", "", false, "");
        }
    });

	$("#btnGuardar").click(function(e){
        if($("#nombre").val().trim() == '' || $("#rfc").val().trim() == '' || $("#curp").val().trim() == '' || $("#domicilio").val().trim() == '')
            ShowDialogBox("Datos incompletos", "Por favor verifique que todos los campos esten llenados correctamente", "Ok", "", false, "");
        else
        {
        	var frm = new FormData(document.getElementById("frm"));
    	    $.ajax({
    	    	url: "/backend/productor/guardar/",
    	      	type: "POST",
    	      	data: frm,
    	      	processData: false,  
    	      	contentType: false
    	    }).done(function(data){
    	    	if(data > 0) 
    	    	{
    	    		if($("#id").val() > 0)
                        ShowDialogBox("Guardado", "El registro se ha editado correctamente", "Ok", "", true, "/backend/productor/");
    	    		else
                    {
                        ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
                        limpiar();
                    }
    	    	}
    	    	else if(data == -1) 
    	    		ShowDialogBox("No se ha podido guardar", "El registro ya existe, compruebe que algunos de los campos capturados no existan en otro registro", "Ok", "", false, "");
    	    	else 
    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
    	    });
        }
 	});
});

function agregar(id=0)
{
	window.location = "/backend/productor/agregar/id/" + id;
}

function filtrargrig()
{
    var filtro = $("#filtroNombre").val();
    $('#flexigrid').flexOptions({url: "/backend/productor/grid/filtro/" + filtro}).flexReload(); 
}

function limpiar()
{
	$("#nombre, #rfc, #curp, #domicilio").val('');
}

function limpiarFiltro()
{
    $("#filtroNombre").val('');
}

function deshabilitar(id)
{
	$.ajax({
    	url: "/backend/productor/deshabilitar/id/" + id,
      	type: "POST",
      	processData: false,  
      	contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/productor/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}

function habilitar(id)
{
    $.ajax({
        url: "/backend/productor/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/productor/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}