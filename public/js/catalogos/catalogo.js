$(document).ready(function()
{

		$("input").keypress(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				$("#btnGuardar").trigger("click");
			}
		});

	$("#flexigrid").flexigrid({
        url: "/backend/catalogos/grid/"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"},
			{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"},
			{display: "Nombre", name: "nombre", width: 765, sortable: true, align: "center"}
        ]
        ,sortname: "nombre"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){
        
        }
        ,onError: function(respuesta){
            ShowDialogBox("Ha ocurrido un error", "No se han podido mostrar los activos, intentelo nuevamente", "Ok", "", false, "");
        }
    });

	$("#btnGuardar").click(function(e){
        if($("#nombre").val().trim() == '')
        {
            ShowDialogBox("Datos incompletos", "El nombre es requerido", "Ok", "", false, "");
        }
		else{
        	var frm = new FormData(document.getElementById("frm"));
    	    $.ajax({
    	    	url: "/backend/catalogos/guardar/",
    	      	type: "POST",
    	      	data: frm,
    	      	processData: false,  
    	      	contentType: false
    	    }).done(function(data){
    	    	if(data > 0) 
    	    	{
    	    		if($("#id").val() > 0)
                        ShowDialogBox("Guardado", "El registro se ha editado correctamente", "Ok", "", true, "/backend/catalogos/");
    	    		else
                    {
                        ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
                        limpiar();
                    }
    	    	}
    	    	else if(data == -1) 
    	    		ShowDialogBox("No se ha podido guardar", "El registro ya existe", "Ok", "", false, "");
    	    	else 
    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
    	    });
        }
 	});
    
    $('#padre_id').change(function(){
        var padre = $('#padre_id').val();
        $.ajax({
            url: "/backend/catalogos/obteneropciones/id_catalogo/"+padre,
            type: "POST",
            processData: false,
            contentType: false
        }).done(function(data){
            $('#opcion_id').html(data);
        });
    });
});

function agregar(id=0)
{
	window.location = "/backend/catalogos/agregar/id/" + id;
}

function filtrargrid()
{
    var filtro = $("#filtroNombre").val()+"/marca/"+$("#filtroMarca").val();
    $('#flexigrid').flexOptions({url: "/backend/catalogos/grid/filtro/" + filtro}).flexReload(); 
}

function limpiar()
{
	$("#nombre").val('');
	
}

function limpiarFiltro()
{
    $("#filtroNombre").val('');
	$("#filtroMarca option:first").prop("selected","selected");
}

function deshabilitar(id)
{
	$.ajax({
    	url: "/backend/catalogos/deshabilitar/id/" + id,
      	type: "POST",
      	processData: false,  
      	contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/catalogos/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}

function habilitar(id)
{
    $.ajax({
        url: "/backend/catalogos/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/catalogos/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}