//alert("entro");

var num_cultivos = 1;
var act = false;
var arr = new Array();
var formularios = { 
	frm1: { cultivo: 1, vertice: 1 },
	frm2: null,
	frm3: null,
	frm4: null,
	frm5: null,
	frm6: null,
	frm7: { bio: 1, insect: 1 },
	frm8: null,
	frm9: { bio: 1, insect: 1 },
	frm10: { num: 1, tb1: { fuente: 1, quimica: 1, organica: 1, mineral: 1, acondicionador: 1 } },
	frm11: { pre: 1, post: 1, des: 1 },
	frm12: { granulado: 1, semilla: 1, recarga: 1 },
	frm13: { num: 1, tb1: { marca: 1 } },
	frm14: null,
	frm15: { granulado: 1, semilla: 1, recarga: 1 },
	frm16: null,
	frm17: null,
	frm18: null
}

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function()
{

	$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });

    $('.btn-menu').click(function() {
        var check = this;
        $('.btn-menu').each(function(){ 
            if(this != check)
            {
                $(this).css('background-color', color_uncheck);
            }
        });
        $(this).css('background-color', color_check);
    });

 	AparecerFormulario("DatosGenerales");
	
	$("#flexigrid").flexigrid({
        url: "/backend/diagnosticos/grid"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"}
            ,{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"}            
            ,{display: "Dependencia", name: "integradora", width: 120, sortable: true, align: "center"}
            ,{display: "Estado", name: "estado", width: 80, sortable: true, align: "center"}
			,{display: "Integradora", name: "integradora", width: 120, sortable: true, align: "center"}
			,{display: "Organización", name: "orgnizadora", width: 130, sortable: true, align: "center"}
			,{display: "Agroconsultor", name: "asesor", width: 120, sortable: true, align: "center"}
            ,{display: "Municipio", name: "municipio", width: 80, sortable: true, align: "center"}
			,{display: "Localidad", name: "localidad", width: 130, sortable: true, align: "center"}
			,{display: "Potrero", name: "potrero", width: 100, sortable: true, align: "center"}
			,{display: "Parcela", name: "parcela", width: 100, sortable: true, align: "center"}
			,{display: "Año de apoyo", name: "ano", width: 80, sortable: true, align: "center"}
        ]
        ,sortname: ""
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){

        }
        ,onError: function(respuesta){

            _mensaje("#_mensaje-1", "Ocurri&oacute; un error al tratar de seleccionar los contactos, int&eacute;ntelo de nuevo");
        }
    });

	
	$.fn.datepicker.defaults.language="es";
	$('.date').datepicker({ format: 'dd/mm/yyyy' });
	
	$(document).on("click","#eliminarFila",function(){
		var parent = $(this).parents().get(2);
		$(parent).remove();
	});

	//HABILITAR RENGLON SI CLICKEAN EN EL RADIO BUTTON
	$(document).on("click", "input[type='radio']", function(){
		var parent = $(this).parents().get(2);
		var label = $(this).parents().get(0);
		
		if(parent.getAttribute("id")==null){//verificamos si existe el id tempporal
			idTemp="temp_"+Math.floor((Math.random() * 1000) + 1);
			parent.setAttribute("id",""+idTemp);			
		}
		else
		{
			idTemp=parent.getAttribute("id");
		}
		
		if(label.outerHTML.indexOf('Si')>=0){
			$('#'+idTemp+' :input').not('input[type="radio"], #'+idTemp+' .date :input').removeAttr("disabled");
			$('#'+idTemp+' .input-group-addon').removeClass("hide");	
			
			
		}
		else
		{
			$('#'+idTemp+' :input').not('input[type="radio"]').attr("disabled","disabled");
			$('#'+idTemp+' .input-group-addon').addClass("hide");
		}		
	});
	
	$('#tb_preparacion :input, #tb_renta :input, #tb_riego :input').not('input[type="radio"]').attr("disabled","disabled");

	$("#despues2").attr("disabled", "disabled");
	$("#despues2").hide();

	$("#hubo_resiembra").change(function()
	{
		if($("#hubo_resiembra").val() == 1)
		{
			$("#despues2").removeAttr("disabled");
			$("#despues2").show();
		}
		else
		{
			$(".row2").remove();
			$("#despues2").attr("disabled", "disabled");
			$("#despues2").hide();
		}
	});

	$("#tabla_resiembra").attr("disabled", "disabled");
	$("#tabla_resiembra").hide();

	$("#hubo_resiembra_resiembra").change(function()
	{
		if($("#hubo_resiembra_resiembra").val() == 1)
		{
			$("#tabla_resiembra").removeAttr("disabled");
			$("#tabla_resiembra").show();
		}
		else
		{
			$("#tabla_resiembra").attr("disabled", "disabled");
			$("#tabla_resiembra").hide();
		}
	});
	

	$('#tabla-costos-indirectos :input, #tabla-ingresos-esperados :input').removeAttr("disabled");
	$('#tabla-rentabilidad :input').attr("disabled", "disabled");
});

function agregarTabla(tipo, form)
{

	var count = 0;
	$('.' + tipo).each(function(){ count ++; });
	var div = "aderido_" + tipo + "_" + (count + 1);

	$("#" + form + " #tabla_div_" + tipo + " #despues_" + tipo + "_1").after( 
		$("#" + form + " #generico").clone().removeClass("hide").attr("id", div).addClass(tipo)
	);

	count += 1;

	$("#" + div + " #nombre_").attr("id", tipo + "_nombre_" + count);
	$("#" + div + " #eliminar-tabla").attr("id", tipo + "eliminar_tabla" + count).attr("onClick", "eliminarExtra('" + div + "')");
	$("#" + div + " #primera_nombre_").attr("id", tipo + "_primera_nombre_" + count);
	$("#" + div + " #tipo_unidad_").attr("id", tipo + "_tipo_unidad_" + count);
	$("#" + div + " #despues__interno_").attr("id", "despues_" + tipo + "_interno_" + count);
	$("#" + div + " #semilla_alto_cantidad").attr("id", tipo + "_semilla_alto_cantidad_" + count);
	$("#" + div + " #semilla_alto_pu").attr("id", tipo + "_semilla_alto_pu_" + count);
	$("#" + div + " #semilla_alto_importe").attr("id", tipo + "_semilla_alto_importe_" + count);
	$("#" + div + " #semilla_alto_frealizacion").attr("id", tipo + "_semilla_alto_frealizacion_" + count);
	$("#" + div + " #semilla_alto_innovacion").attr("id", tipo + "_semilla_alto_innovacion_" + count);
	$("#" + div + " #agrega_fila_").attr("id", tipo + "_agrega_fila_" + count).attr("onClick", "agregarFilaExtra('" + tipo + "','frm-8', " + count + ")");
	$("#" + div + " #mezclado_cantidad").attr("id", tipo + "_mezclado_cantidad_" + count);
	$("#" + div + " #mezclado_pu").attr("id", tipo + "_mezclado_pu_" + count);
	$("#" + div + " #mezclado_importe").attr("id", tipo + "_mezclado_importe_" + count);
	$("#" + div + " #mezclado_frealizacion").attr("id", tipo + "_mezclado_frealizacion_" + count);
	$("#" + div + " #mezclado_innovacion").attr("id", tipo + "_mezclado_innovacion_" + count);
	$("#" + div + " #tipo_ap_select").attr("id", tipo + "_tipo_ap_select_" + count).attr("onChange", "evaluaCambio('" + tipo + "_tipo_ap_select_" + count + "', '" + tipo + "', " + count + ")");
	$("#" + div + " #manual_div_").attr("id", "manual_div_" + tipo + "_" + count);
	$("#" + div + " #manual_div_no_jornales_").attr("id", "manual_div_no_jornales_" + tipo + "_" + count);
	$("#" + div + " #manual_div_costo_jornal_").attr("id", "manual_div_costo_jornal_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_").attr("id", "mecanizada_div_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_").attr("id", "mecanizada_div_select_" + tipo + "_" + count).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_" + tipo + "_" + count + "', '" + tipo + "', " + count + ")");
	$("#" + div + " #mecanizada_div_select_div_propia_").attr("id", "mecanizada_div_select_div_propia_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_propia_combustible_").attr("id", "mecanizada_div_select_div_propia_combustible_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_propia_operador_").attr("id", "mecanizada_div_select_div_propia_operador_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_rentada_").attr("id", "mecanizada_div_select_div_rentada_" + tipo + "_" + count);
	$("#" + div + " #mecanizada_div_select_div_rentada_costo_").attr("id", "mecanizada_div_select_div_rentada_costo_" + tipo + "_" + count);

}

function agregarFilaExtra(tipo, form, num)
{
	var ini = 1;
	if(tipo == 'frm-11') ini = 0;
	var count = 0;
	$('.' + tipo + "_interno_" + num).each(function(){ count ++; });

	$("#" + form + " #tabla_div_" + tipo + " #despues_" + tipo + "_interno_" + num).after( 
		$("#" + form + " #generico-tabla tbody tr:eq(1)").clone().removeClass("hide").addClass(tipo + "_interno_" + num).attr("id", "despues_" + tipo + "_interno_aderido_" + (num+1))
	);

	$("#despues_" + tipo + "_interno_aderido_" + (num+1) + " #eliminar-fila").attr("id", tipo + "_eliminar_fila_" + (num+1)).attr("onClick", "eliminarExtra('" + "despues_" + tipo + "_interno_aderido_" + (num+1) + "')");
}

function agregarTabla2()
{

	var count = 0;
	$('.foliar').each(function(){ count ++; });
	var div1 = "despues_fsiembra_" + (count + 1);
	var div2 = "despues_foliar_" + (count + 1);

	$("#frm-11 #tabla_div_siembra #despues_foliar_" + count).after( 
		$("#frm-11 #generico-siembra").clone().removeClass("hide").attr("id", div1).addClass("fsiembra")
	);

	$("#frm-11 #tabla_div_siembra #" + div1).after( 
		$("#frm-11 #generico-foliar").clone().removeClass("hide").attr("id", div2).addClass("foliar")
	);

	count += 1;
	var tipo = "fsiembra";

	$("#frm-11 #" + div1 + " #nombre_").attr("id", tipo + "_nombre_" + count);
	$("#frm-11 #" + div1 + " #eliminar-tabla").attr("id", tipo + "eliminar_tabla" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");
	$("#frm-11 #" + div1 + " #primera_nombre_").attr("id", tipo + "_primera_nombre_" + count);
	$("#frm-11 #" + div1 + " #tipo_unidad_").attr("id", tipo + "_tipo_unidad_" + count);
	$("#frm-11 #" + div1 + " #despues_interno_").attr("id", "despues_" + tipo + "_interno_1_" + count);
	$("#frm-11 #" + div1 + " #semilla_alto_cantidad").attr("id", tipo + "_semilla_alto_cantidad_" + count);
	$("#frm-11 #" + div1 + " #semilla_alto_pu").attr("id", tipo + "_semilla_alto_pu_" + count);
	$("#frm-11 #" + div1 + " #semilla_alto_importe").attr("id", tipo + "_semilla_alto_importe_" + count);
	$("#frm-11 #" + div1 + " #semilla_alto_frealizacion").attr("id", tipo + "_semilla_alto_frealizacion_" + count);
	$("#frm-11 #" + div1 + " #semilla_alto_innovacion").attr("id", tipo + "_semilla_alto_innovacion_" + count);
	$("#frm-11 #" + div1 + " #agrega_fila_").attr("id", tipo + "_agrega_fila_" + count).attr("onClick", "agregarFilaExtra2('" + tipo + "', '', " + count + ", 1)");
	$("#frm-11 #" + div1 + " #mezclado_cantidad").attr("id", tipo + "_mezclado_cantidad_" + count);
	$("#frm-11 #" + div1 + " #mezclado_pu").attr("id", tipo + "_mezclado_pu_" + count);
	$("#frm-11 #" + div1 + " #mezclado_importe").attr("id", tipo + "_mezclado_importe_" + count);
	$("#frm-11 #" + div1 + " #mezclado_frealizacion").attr("id", tipo + "_mezclado_frealizacion_" + count);
	$("#frm-11 #" + div1 + " #mezclado_innovacion").attr("id", tipo + "_mezclado_innovacion_" + count);
	$("#frm-11 #" + div1 + " #tipo_ap_select").attr("id", tipo + "_tipo_ap_select_" + count).attr("onChange", "evaluaCambio('" + tipo + "_tipo_ap_select_" + count + "', '" + tipo + "', " + count + ")");
	$("#frm-11 #" + div1 + " #manual_div_").attr("id", "manual_div_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #manual_div_no_jornales_").attr("id", "manual_div_no_jornales_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #manual_div_costo_jornal_").attr("id", "manual_div_costo_jornal_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #mecanizada_div_").attr("id", "mecanizada_div_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #mecanizada_div_select_").attr("id", "mecanizada_div_select_" + tipo + "_" + count).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_" + tipo + "_" + count + "', '" + tipo + "', " + count + ")");
	$("#frm-11 #" + div1 + " #mecanizada_div_select_div_propia_").attr("id", "mecanizada_div_select_div_propia_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #mecanizada_div_select_div_propia_combustible_").attr("id", "mecanizada_div_select_div_propia_combustible_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #mecanizada_div_select_div_propia_operador_").attr("id", "mecanizada_div_select_div_propia_operador_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #mecanizada_div_select_div_rentada_").attr("id", "mecanizada_div_select_div_rentada_" + tipo + "_" + count);
	$("#frm-11 #" + div1 + " #mecanizada_div_select_div_rentada_costo_").attr("id", "mecanizada_div_select_div_rentada_costo_" + tipo + "_" + count);

	var tab = "tabla-foliar-" + count;
	$("#frm-11 #" + div2 + " #tabla-foliar").attr("id", tab);
	$("#frm-11 #" + div2 + " #" + tab + " #generico-quimica #agregar").attr("id", "quimica_interno_1").attr("onClick", "agregarFilaExtra2('foliar', 'quimica', " + count + ", 1)");
	$("#frm-11 #" + div2 + " #" + tab + " #generico-organica #agregar").attr("id", "organica_interno_1").attr("onClick", "agregarFilaExtra2('foliar', 'organica', " + count + ", 1)");
	$("#frm-11 #" + div2 + " #" + tab + " #generico-mineral #agregar").attr("id", "mineral_interno_1").attr("onClick", "agregarFilaExtra2('foliar', 'mineral', " + count + ", 1)");
	$("#frm-11 #" + div2 + " #" + tab + " #generico-acondicionador #agregar").attr("id", "acondicionador_interno_1").attr("onClick", "agregarFilaExtra2('foliar', 'acondicionador', " + count + ", 1)");

	$("#frm-11 #" + div2 + " #tabla-foliar").attr("id", tab);
	$("#frm-11 #" + div2 + " #" + tab + " #generico-quimica").attr("id", "despues_quimica_interno_1_" + count);
	$("#frm-11 #" + div2 + " #" + tab + " #generico-organica").attr("id", "despues_organica_interno_1_" + count);
	$("#frm-11 #" + div2 + " #" + tab + " #generico-mineral").attr("id", "despues_mineral_interno_1_" + count);
	$("#frm-11 #" + div2 + " #" + tab + " #generico-acondicionador").attr("id", "despues_acondicionador_interno_1_" + count);

}

function agregarFilaExtra2(tipo, subtipo, num, subnum)
{
	
	if(subtipo == '')
	{
		var div = "despues_" + tipo + "_interno_" + (subnum + 1) + "_" + num;

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #despues_" + tipo + "_interno_" + subnum + "_" + num).after( 
			$("#frm-11 #generico-campo-fuente tbody tr:eq(0)").clone().removeClass("hide").addClass(tipo).attr("id", div)
		);

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #" + div + " #eliminar-fila").attr("id", subtipo + "_eliminar_fila_" + (subnum+1)).attr("onClick", "eliminarExtra('" + div + "')");
	}
	else
	{
		var div = "despues_" + subtipo + "_interno_" + (subnum + 1) + "_" + num;
		var ind = 0;
		if(subtipo == 'quimica') ind = 1;
		else if(subtipo == 'organica') ind = 2;
		else if(subtipo == 'mineral') ind = 3;
		else ind = 4;

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #despues_" + subtipo + "_interno_" + subnum + "_" + num).after( 
			$("#frm-11 #generico-foliar #tabla-foliar tbody tr:eq(" + ind + ")").clone().removeClass("hide").addClass(tipo + "_interno_" + (subnum+1)).attr("id", div)
		);

		$("#frm-11 #tabla_div_siembra #despues_" + tipo + "_" + num + " #" + div + " #agregar").attr("id", subtipo + "_eliminar_fila_" + (subnum+1)).attr("onClick", "eliminarExtra('" + div + "')").removeClass("btn-warning").removeClass("fa-plus-circle").addClass("fa-times-circle").addClass("btn-danger");
	}
}

// Evento que selecciona la fila y la elimina 
function eliminarExtra(id, form = null, count = null)
{
	$("#" + id).remove();
	if(form == 'frm-1') $("#sem_despues_" + count).remove();
}

function eliminarExtra2(p1, p2, p3)
{
	if(p1 != null) $("#" + p1).remove();
	if(p2 != null) $("#" + p2).remove();
	if(p3 != null) $("#" + p3).remove();
}

function eliminarExtra3(p1, p2)
{
	if(p1 != null) $("#" + p1).remove();
	if(p2 != null) $("#" + p2).remove();
}

// Evento que selecciona la fila y la elimina 
function eliminarFila(event, form = null, count = null)
{
	var parent = $(this).parents().get(2);
	$(parent).remove();
}

function agregarFila(tipo, form, id, clase = null)
{
	//si presionamos el boton de agregar fila
	if(id==''){
		if(tipo == '')
			equal = 1;
		else if(tipo == 1)
			equal = 1;
		else if(tipo == 2)
			equal=2;
		else if(tipo == 3)
			equal=2;
		else
			equal=3;
	}
	else
		equal=id;	
	

	if(form == 'frm-1')
	{
		// FORM 1
		var count = 0;
		$('.' + clase).each(function(){ count ++; });
		var div = clase + "_despues_" + (count + 1);

		$("#" + form + " #tabla tbody #" + clase + "_despues_" + count).after(
			$("#" + form + " #tabla tbody #fila_generica_" + clase).clone().removeClass("hide").attr("id", div).addClass(clase)
		);

		count += 1;

		$("#" + div + " #select_cultivo_dg_").attr("id", "select_cultivo_dg_" + count);
		$("#" + div + " #hect_cultivo_dg_").attr("id", "hect_cultivo_dg_" + count);
		$("#" + div + " #eliminarFila_dg_").attr("id", "eliminarFila_dg_" + count).attr("onClick", "eliminarExtra('" + div + "', '" + form + "', " + count + ")");

		// FORM 5
		count = 0;
		clase = "sem";
		form = "frm-5";
		$('.' + clase).each(function(){ count ++; });
		var div = clase + "_despues_" + (count + 1);

		$("#" + form + " #tabla tbody #" + clase + "_despues_" + count).after(
			$("#" + form + " #tabla tbody #fila_generica_1").clone().removeClass("hide").attr("id", div).addClass(clase)
		);

		count += 1;

		$("#" + div + " #" + clase + "cultivo_select_").attr("id", clase + "cultivo_select_" + count);
		$("#" + div + " #" + clase + "tipo_select_").attr("id", clase + "tipo_select_" + count);
		$("#" + div + " #" + clase + "marca_select_").attr("id", clase + "marca_select_" + count);
		$("#" + div + " #" + clase + "hib_select_").attr("id", clase + "hib_select_" + count);
		$("#" + div + " #" + clase + "hib_nombre_select_").attr("id", clase + "hib_nombre_select_" + count);
		$("#" + div + " #" + clase + "hectareas_sembradas_").attr("id", clase + "hectareas_sembradas_" + count);
		$("#" + div + " #" + clase + "semilla_alto_cantidad_").attr("id", clase + "semilla_alto_cantidad_" + count);
		$("#" + div + " #" + clase + "semilla_alto_pu_").attr("id", clase + "semilla_alto_pu_" + count);
		$("#" + div + " #" + clase + "semilla_alto_importe_").attr("id", clase + "semilla_alto_importe_" + count);
		$("#" + div + " #" + clase + "semilla_alto_frealizacion_").attr("id", clase + "semilla_alto_frealizacion_" + count);
		$("#" + div + " #" + clase + "semilla_alto_innovacion_").attr("id", clase + "semilla_alto_innovacion_" + count);

	}
	else if(form == 'frm-12')
	{
		var count = 0;
		$('.cdmpre-' + clase).each(function(){ count ++; });
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_coad_" + (count + 1);
		var div3 = "despues_" + clase + "_apli_" + (count + 1);

		$("#" + form + " #tabla tbody #despues_" + clase + "_apli_" + count).after(
			$("#" + form + " #tabla tbody #fila_generica_marca").clone().removeClass("hide").attr("id", div1).addClass("cdmpre-" + clase)
		);

		$("#" + form + " #tabla tbody #" + div1).after(
			$("#" + form + " #tabla tbody #fila_generica_coad").clone().removeClass("hide").attr("id", div2)
		);

		$("#" + form + " #tabla tbody #" + div2).after(
			$("#" + form + " #tabla tbody #fila_generica_apli").clone().removeClass("hide").attr("id", div3)
		);

		count += 1;

		arr[0] = div1; arr[1] = div2; arr[2] = div3;
		
		$("#" + div1 + " #eliminarFila_").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra2('" + div1 + "', '" + div2 + "', '" + div3 + "')");

		$("#" + div3 + " #tipo_ap_select_gen").attr("id", "tipo_ap_select_" + clase + "_" + count).attr("onChange", "evaluaCambio('tipo_ap_select_" + clase + "_" + count + "', '" + clase + "', " + count + ")");
		$("#" + div3 + " #manual_div_gen").attr("id", "manual_div_" + clase + "_" + count);
		$("#" + div3 + " #manual_div_no_jornales_gen").attr("id", "manual_div_no_jornales_" + clase + "_" + count);
		$("#" + div3 + " #manual_div_costo_jornal_gen").attr("id", "manual_div_costo_jornal_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_gen").attr("id", "mecanizada_div_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_gen").attr("id", "mecanizada_div_select_" + clase + "_" + count).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_" + clase + "_" + count + "', '" + clase + "', " + count + ")");
		$("#" + div3 + " #mecanizada_div_select_div_propia_gen").attr("id", "mecanizada_div_select_div_propia_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_propia_combustible_gen").attr("id", "mecanizada_div_select_div_propia_combustible_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_propia_operador_gen").attr("id", "mecanizada_div_select_div_propia_operador_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_rentada_gen").attr("id", "mecanizada_div_select_div_rentada_" + clase + "_" + count);
		$("#" + div3 + " #mecanizada_div_select_div_rentada_costo_gen").attr("id", "mecanizada_div_select_div_rentada_costo_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_gen").attr("id", "aerea_div_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_gen").attr("id", "aerea_div_select_" + clase + "_" + count).attr("onChange", "evaluaCambioAplicacion('aerea_div_select_" + clase + "_" + count + "', '" + clase + "', " + count + ")");
		$("#" + div3 + " #aerea_div_select_div_propia_gen").attr("id", "aerea_div_select_div_propia_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_propia_combustible_gen").attr("id", "aerea_div_select_div_propia_combustible_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_propia_operador_gen").attr("id", "aerea_div_select_div_propia_operador_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_rentada_gen").attr("id", "aerea_div_select_div_rentada_" + clase + "_" + count);
		$("#" + div3 + " #aerea_div_select_div_rentada_costo_gen").attr("id", "aerea_div_select_div_rentada_costo_" + clase + "_" + count);
	}
	else if(form == 'frm-13')
	{
		var count = 0;
		$('.cdpds-' + clase).each(function(){ count ++; });
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_apli_" + (count + 1);

		$("#" + form + " #tabla tbody #despues_" + clase + "_marca_" + count).after(
			$("#" + form + " #tabla tbody #fila-general-" + clase).clone().removeClass("hide").attr("id", div1).addClass("cdpds-" + clase)
		);

		count += 1;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else if(form == 'frm-16')
	{
		var count = 0;
		$('.tdebya-' + clase).each(function(){ count ++; });
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_apli_" + (count + 1);

		$("#" + form + " #tabla tbody #despues_" + clase + "_marca_" + count).after(
			$("#" + form + " #tabla tbody #fila-general-" + clase).clone().removeClass("hide").attr("id", div1).addClass("tdebya-" + clase)
		);

		count += 1;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else if(form == 'frm-14')
	{
		var count = 0;
		$('.cdpf-' + clase).each(function(){ count ++; });
		var div1 = "despues_" + clase + "_marca_" + (count + 1);
		var div2 = "despues_" + clase + "_coad_" + (count + 1);
		var div3 = "despues_" + clase + "_apli_" + (count + 1);

		$("#" + form + " #tabla tbody #despues_" + clase + "_coad_" + count).after(
			$("#" + form + " #tabla tbody #fila_generica_marca").clone().removeClass("hide").attr("id", div1).addClass("cdpf-" + clase)
		);

		$("#" + form + " #tabla tbody #" + div1).after(
			$("#" + form + " #tabla tbody #fila_generica_coad").clone().removeClass("hide").attr("id", div2)
		);

		count += 1;

		arr[0] = div1; arr[1] = div2;
		
		$("#" + div1 + " #eliminarFila").attr("id", "eliminarFila_" + count).attr("onClick", "eliminarExtra3('" + div1 + "', '" + div2 + "')");

	}
	else
	{
		if(tipo == '')
		{
			$("#"+form+" #tabla tbody #despues"+tipo+id).after( 
				$("#"+form+" #tabla tbody tr:eq("+equal+")").clone().removeAttr("id class")
			);
		}
		else
		{
			$("#"+form+" #tabla tbody #despues"+tipo+id).after( 
				$("#"+form+" #tabla tbody tr:eq("+equal+")").clone().removeAttr("id class").addClass("row"+tipo)
			);
		}
	}
	$('.date').datepicker({ format: 'dd/mm/yyyy' });
}


function agregarTabla3(tb)
{
	var count = 0;
	$('.tabfol').each(function(){ count ++; });

	$("#frm-14 #tablafol_" + count).after(
		$("#frm-14 #tabla-generica-fol").clone().removeClass("hide").attr("id", "tablafol_" + (count+1)).addClass("tabfol")
	);

	tb = count;

	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_").attr("id", "despues_fol_marca_" + (tb+1) + "_1").addClass("fol-" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_coad_").attr("id", "despues_fol_coad_" + (tb+1) + "_1");
	$("#frm-14 #tablafol_" + (tb+1) + " #despues_fol_marca_" + (tb+1) + "_1 #agre").attr("id", "agre" + (tb+1) + "_1").attr("onClick", "agregarFila3(" + (tb+1) + ", 1)");
	$("#frm-14 #tablafol_" + (tb+1) + " #eliminarFila").attr("id", "eliminartab_" + (tb+1)).attr("onClick", "eliminarExtra3('tablafol_" + (tb+1) + "', null)");

	$("#frm-14 #tablafol_" + (tb+1) + " #tipo_ap_select_gen").attr("id", "tipo_ap_select_fol_" + (tb+1)).attr("onChange", "evaluaCambio('tipo_ap_select_fol_" + (tb+1) + "', 'fol', " + (tb+1) + ")");
	$("#frm-14 #tablafol_" + (tb+1) + " #manual_div_gen").attr("id", "manual_div_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #manual_div_no_jornales_gen").attr("id", "manual_div_no_jornales_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #manual_div_costo_jornal_gen").attr("id", "manual_div_costo_jornal_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_gen").attr("id", "mecanizada_div_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_gen").attr("id", "mecanizada_div_select_fol_" + (tb+1)).attr("onChange", "evaluaCambioAplicacion('mecanizada_div_select_fol_" + (tb+1) + "', 'fol', " + (tb+1) + ")");
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_propia_gen").attr("id", "mecanizada_div_select_div_propia_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_propia_combustible_gen").attr("id", "mecanizada_div_select_div_propia_combustible_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_propia_operador_gen").attr("id", "mecanizada_div_select_div_propia_operador_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_rentada_gen").attr("id", "mecanizada_div_select_div_rentada_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #mecanizada_div_select_div_rentada_costo_gen").attr("id", "mecanizada_div_select_div_rentada_costo_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_gen").attr("id", "aerea_div_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_gen").attr("id", "aerea_div_select_fol_" + (tb+1)).attr("onChange", "evaluaCambioAplicacion('aerea_div_select_fol_" + (tb+1) + "', 'fol', " + (tb+1) + ")");
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_propia_gen").attr("id", "aerea_div_select_div_propia_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_propia_combustible_gen").attr("id", "aerea_div_select_div_propia_combustible_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_propia_operador_gen").attr("id", "aerea_div_select_div_propia_operador_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_rentada_gen").attr("id", "aerea_div_select_div_rentada_fol_" + (tb+1));
	$("#frm-14 #tablafol_" + (tb+1) + " #aerea_div_select_div_rentada_costo_gen").attr("id", "aerea_div_select_div_rentada_costo_fol_" + (tb+1));
}

function agregarFila3(tb, fil)
{

	var count = 0;
	$('.fol-' + tb).each(function(){ count ++; });
	count++;

	$("#frm-14 #tablafol_" + tb + " tbody #despues_fol_coad_" + tb + "_1").after(
		$("#frm-14 #tablafol_1 tbody #fila_generica_marca").clone().removeClass("hide").attr("id", "despues_fol_marca_" + tb + "_" + count).addClass("fol-" + tb)
	);

	$("#frm-14 #tablafol_" + tb + " tbody #despues_fol_marca_" + tb + "_" + count).after(
		$("#frm-14 #tablafol_1 tbody #fila_generica_coad").clone().removeClass("hide").attr("id", "despues_fol_coad_" + tb + "_" + count)
	);

	$("#frm-14 #tablafol_" + tb + " #despues_fol_marca_" + tb + "_" + count + " #eliminarFila").attr("id", "elimina_" + tb + "_" + count).attr("onClick", "eliminarExtra3('" + "despues_fol_marca_" + tb + "_" + count + "','" + "despues_fol_coad_" + tb + "_" + count + "')");
}

function AparecerFormulario(parcial){
	if(parcial == 'Fertilizacion')
	{

		if($("#hubo_resiembra_resiembra").val() == 1)
			$("#tabla_div_resiembra").removeClass("hide");
		else
			$("#tabla_div_resiembra").addClass("hide");
	}
	else if(parcial == 'Semilla'){
		if(act == false)
		{
			for(i=1; i < num_cultivos; i++)
			{
				agregarFila('', 'frm-5','');
			}
			act = true;
		}
	}
	else if(parcial == 'ReaccionDelSuelo'){
		//accedemos a la tabla donde esta el ph
		$('#DatosAnalisisDelSuelo tr').find('td:contains("PH")').each(function() {
			//buscamos la celda que contenga PH y accedemos a su hermano donde esta el input
			//para obtener su valor
				ph = $(this).siblings('td:eq(2)').find('input').val();
				ph= parseInt(ph);
				
				if(ph!=""){
					if(ph >= -3 && ph <= 3){
						//neutro
						$('#distintivo_ph').html(
						'<div class="small-box bg-green">'+
							'<div class="inner">'+
								'<h3>'+ph+'</h3>'+
								'<p>Valor de PH: Neutro</p>'+
							'</div>'+
							'<div class="icon">'+
								'<i class="ion ion-pie-graph"></i>'+
							'</div>'+
						'</div>'
						);
					}else if( (ph >= -5 && ph < -3) || (ph > 3 && ph <=5)){
						//alcalino
						$('#distintivo_ph').html(
						'<div class="small-box bg-yellow">'+
							'<div class="inner">'+
								'<h3>'+ph+'</h3>'+
								'<p>Valor de PH: Alcalino</p>'+
							'</div>'+
							'<div class="icon">'+
								'<i class="ion ion-pie-graph"></i>'+
							'</div>'+
						'</div>'
						);
					}else if( (ph <= -6 && ph < -5) || (ph > 5 ) ){
						//acido
						$('#distintivo_ph').html(
						'<div class="small-box bg-red">'+
							'<div class="inner">'+
								'<h3>'+ph+'</h3>'+
								'<p>Valor de PH: Acido</p>'+
							'</div>'+
							'<div class="icon">'+
								'<i class="ion ion-pie-graph"></i>'+
							'</div>'+
						'</div>'
						);
					}else{
					//Sin capturar
						$('#distintivo_ph').html(
						'<div class="small-box bg-aqua">'+
							'<div class="inner">'+
								'<h3>Valor de PH:</h3>'+
								'<p>Sin Capturar</p>'+
							'</div>'+
							'<div class="icon">'+
								'<i class="ion ion-pie-graph"></i>'+
							'</div>'+
						'</div>'
						);	
	}
				}
		});			
		
/*		*/
	}
	$("#parcial > div").each(function ()
	{
		$(this).addClass("hide");
	})

	$('#'+parcial).removeClass("hide");
}


function matriz(fmr, opc)
{
	var matriz, suelo;
	if(fmr == 1) { matriz = "matriz"; suelo = "analisis_suelo"; }
	else { matriz = "matriz_mdfds"; suelo = "analisis_suelo_mdfds"; }

	if(opc==1)
	{
		$('#' + matriz).fadeOut(1200,function(){});
		$('#' + matriz + ' :input').attr("disabled","disabled");
		$('#' + suelo).show();
	}
	else if(opc==0)
	{
		$('#' + matriz).fadeOut(1200,function(){});
		$('#' + matriz + ' :input').attr("disabled","disabled");
		$('#' + suelo).hide();
	}
	else if(opc==3)
	{
		$('#' + matriz + ' :input').removeAttr("disabled");
		$('#' + matriz).fadeIn(1200,function(){});
	}
	else
	{
		$('#' + matriz).fadeOut(1200,function(){});
		$('#' + matriz + ' :input').attr("disabled","disabled");
	}
}

function guardar(num)
{
	if(num == 6)
	{
		if($("#hubo_siembra_precision").val() == 1)
		{
			if($("#propia_cantidad").val() == '' && $("#rentada_cantidad").val() == '' && $("#manual_cantidad").val() == '')
			{
				alert("Al menos una cantidad de hectareas es requerida");
			}
			else
			{
				var val_propia = ($("#propia_cantidad").val() != '') ? $("#propia_cantidad").val() : 0;
				var val_rentada = ($("#rentada_cantidad").val() != '') ? $("#rentada_cantidad").val() : 0;
				var val_manual = ($("#manual_cantidad").val() != '') ? $("#manual_cantidad").val() : 0;
				var sum = parseFloat(val_propia) + parseFloat(val_rentada) + parseFloat(val_manual);
				
				if(sum > parseFloat($("#num_hectareas").val()))
					alert("La suma de la cantidad de hectareas no debe superar el Número de hectareas del predio");
				else if(sum < parseFloat($("#num_hectareas").val()))
					alert("La suma de la cantidad de hectareas no debe ser menor al Número de hectareas del predio");
			}
		}
	}
	else if(num == 7)
	{
		if($("#hubo_resiembra_resiembra").val() == 1)
		{
			if($("#propia_cantidad_resiembra").val() == '' && $("#rentada_cantidad_resiembra").val() == '' && $("#manual_cantidad_resiembra").val() == '')
			{
				alert("Al menos una cantidad de hectareas es requerida");
			}
			else
			{
				var val_propia = ($("#propia_cantidad_resiembra").val() != '') ? $("#propia_cantidad_resiembra").val() : 0;
				var val_rentada = ($("#rentada_cantidad_resiembra").val() != '') ? $("#rentada_cantidad_resiembra").val() : 0;
				var val_manual = ($("#manual_cantidad_resiembra").val() != '') ? $("#manual_cantidad_resiembra").val() : 0;
				var sum = parseFloat(val_propia) + parseFloat(val_rentada) + parseFloat(val_manual);
				
				if(sum > parseFloat($("#num_hectareas").val()))
					alert("La suma de la cantidad de hectareas no debe superar el Número de hectareas del predio");
				else if(sum < parseFloat($("#num_hectareas_resiembra").val()))
					alert("La suma de la cantidad de hectareas no debe ser menor al Número de hectareas del predio");
			}
		}
	}
}

function evaluaCambio(id, tipo, num)
{
	if($("#" + id).val() == 1)
	{
		$("#mecanizada_div_" + tipo + "_" + num).removeClass("hide");
		$("#manual_div_" + tipo + "_" + num).addClass("hide");
		$("#aerea_div_" + tipo + "_" + num).addClass("hide");
	}
	else if($("#" + id).val() == 2)
	{
		$("#manual_div_" + tipo + "_" + num).removeClass("hide");
		$("#mecanizada_div_" + tipo + "_" + num).addClass("hide");
		$("#aerea_div_" + tipo + "_" + num).addClass("hide");
	}
	else if($("#" + id).val() == 3)
	{
		$("#manual_div_" + tipo + "_" + num).addClass("hide");
		$("#mecanizada_div_" + tipo + "_" + num).addClass("hide");
		$("#aerea_div_" + tipo + "_" + num).removeClass("hide");
	}
}

function evaluaCambioAplicacion(id, tipo, num)
{
	if($("#" + id).val() == 1)
	{
		if(id.indexOf("aerea") > -1)
		{
			$("#aerea_div_select_div_propia_" + tipo + "_" + num).removeClass("hide");
			$("#aerea_div_select_div_rentada_" + tipo + "_" + num).addClass("hide");
		}
		else
		{
			$("#mecanizada_div_select_div_propia_" + tipo + "_" + num).removeClass("hide");
			$("#mecanizada_div_select_div_rentada_" + tipo + "_" + num).addClass("hide");
		}
	}
	else
	{
		if(id.indexOf("aerea") > -1)
		{
			$("#aerea_div_select_div_propia_" + tipo + "_" + num).addClass("hide");
			$("#aerea_div_select_div_rentada_" + tipo + "_" + num).removeClass("hide");
		}
		else
		{
			$("#mecanizada_div_select_div_propia_" + tipo + "_" + num).addClass("hide");
			$("#mecanizada_div_select_div_rentada_" + tipo + "_" + num).removeClass("hide");
		}
	}
}

function evaluaCambioFert(id, tipo, num)
{
	if($("#" + id).val() == 2)
	{
		$("#" + tipo + "_tipo_ap_div_" + num).removeClass("hide");
		$("#" + tipo + "_tipo_ap_cont_div_" + num).removeClass("hide");
	}
	else
	{
		$("#" + tipo + "_tipo_ap_div_" + num).addClass("hide");
		$("#" + tipo + "_tipo_ap_cont_div_" + num).addClass("hide");
	}
}

//function agregarTablaFinal(frm, clase, )