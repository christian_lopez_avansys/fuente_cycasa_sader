var tipo;
function agregarBanner(id, habilitar)
{
    $.ajax({
            url:"/catalogos/banner/agregar/id/"+id+"/editar/"+habilitar,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                $("#frmBanner").validate(
                {
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();
                      
                     $(form).ajaxSubmit(
                     {

                        success:    function(response) 
                        { 
                            if(response.trim()=="-1")
                            {
                                $("#Aplicar,#Aceptar").show(); 
                                alert("Este nombre de banner ya existe");
                            }
                            else if(response.trim()=="-9")
                            {
                                $("#Aplicar,#Aceptar").show(); 
                                alert("No es posible cargar la imagen.");
                            }
                            else if(response.trim()=="-10")
                            {
                                $("#Aplicar,#Aceptar").show(); 
                                alert("Debe seleccionar una imagen.");
                            }
                            else
                            {
                                $('#flexgrid').flexReload();
                                if(tipo==1)
                                {
                                    $('#frmBanner').resetForm();
    								$("#Aplicar,#Aceptar").show();
                                }
                                else
                                {
                                    $(".dialog-form").dialog("destroy");
                                    $(".dialog-form").dialog("close");
                                }
							 }
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar Banner",
                        position: [240,100],
                        modal: true,
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmBanner").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmBanner").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }

                if(habilitar == 1) 
                {
                    $("#Cancelar").hide();
                }
                    
            }
    });

}

function eliminarBanner(id, status)
{
    if(status == 1)
    {
        if(confirm('¿En realidad desea deshabilitar este registro?'))
        {
            $.ajax({
                type: 'POST',
                url:"/catalogos/banner/eliminar",
                data:{id:id},
                success:
                function respuesta(res)
                {
                    alert('El registro ha sido deshabilitado.');
                    $('#flexgrid').flexReload();
                }
            });
        }
    }else if(status == 0)
    {
        if(confirm('¿En realidad desea habilitar este registro?'))
        {
            $.ajax({
                type: 'POST',
                url:"/catalogos/banner/habilitar",
                data:{id:id},
                success:
                function respuesta(res)
                {
                    alert('El registro ha sido habilitado.');
                    $('#flexgrid').flexReload();
                }
            });
        }
    }
}

$(document).ready(function() 
{   

    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/banner/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 890, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1122,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
    
    
});

function filtrarBanners()
{
    var filtro = "/catalogos/banner/grid";
    var imprimir="/catalogos/banner/imprimir";
    var exportar="/catalogos/banner/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarBanners()
{
    $("#fnombre").attr('value','');

    var filtro = "/catalogos/banner/grid";
	
	var imprimir="/catalogos/banner/imprimir";
    var exportar="/catalogos/banner/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}