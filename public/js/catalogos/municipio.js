$(document).ready(function()
{
	$("#flexigrid").flexigrid({
        url: "/backend/municipio/grid/"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"}
            ,{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"}            
            ,{display: "Municipio", name: "nombre", width: 483, sortable: true, align: "center"}
            ,{display: "Estado", name: "estado", width: 482, sortable: false, align: "center"}
        ]
        ,sortname: "nombre"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){
        
        }
        ,onError: function(respuesta){
            ShowDialogBox("Ha ocurrido un error", "No se han podido mostrar los municipios, intentelo nuevamente", "Ok", "", false, "");
        }
    });

	$("#btnGuardar").click(function(e){
        if($("#nombre").val().trim() == '')
            ShowDialogBox("Datos incompletos", "El nombre del municipio es requerido", "Ok", "", false, "");
        else if($("#id_estado").val() <= 0)
            ShowDialogBox("Datos incompletos", "El estado es requerido", "Ok", "", false, "");
        else if($("#clave").val() <= 0)
            ShowDialogBox("Datos incompletos", "La clave del municipio es requerida", "Ok", "", false, "");
        else
        {
        	var frm = new FormData(document.getElementById("frm"));
    	    $.ajax({
    	    	url: "/backend/municipio/guardar/",
    	      	type: "POST",
    	      	data: frm,
    	      	processData: false,  
    	      	contentType: false
    	    }).done(function(data){
    	    	if(data > 0) 
    	    	{
    	    		if($("#id").val() > 0)
                        ShowDialogBox("Guardado", "El registro se ha editado correctamente", "Ok", "", true, "/backend/municipio/");
    	    		else
                    {
                        ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
                        limpiar();
                    }
    	    	}
    	    	else if(data == -1) 
    	    		ShowDialogBox("No se ha podido guardar", "El registro ya existe", "Ok", "", false, "");
    	    	else 
    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
    	    });
        }
 	});
});

function agregar(id=0)
{
	window.location = "/backend/municipio/agregar/id/" + id;
}

function filtrargrig()
{
    var nombre = $("#filtroNombre").val();
    var estado = $("#filtroEstado").val();
    $('#flexigrid').flexOptions({url: "/backend/municipio/grid/nombre/" + nombre + "/estado/" + estado}).flexReload(); 
}

function limpiar()
{
	$("#nombre").val('');
    $("#id_estado").val('0');
    $("#clave").val('');
}

function limpiarFiltro()
{
    $("#filtroNombre").val('');
    $("#filtroEstado").val('0');
}

function deshabilitar(id)
{
	$.ajax({
    	url: "/backend/municipio/deshabilitar/id/" + id,
      	type: "POST",
      	processData: false,  
      	contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/municipio/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}

function habilitar(id)
{
    $.ajax({
        url: "/backend/municipio/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/municipio/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}