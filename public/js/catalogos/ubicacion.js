var tipo;
var geocoder;
var mapaPropiedad;
var marker;
var bandera=false;
function agregarUbicacion(id, habilitar)
{
    $.ajax({
            url:"/catalogos/ubicacion/agregar/id/"+id+"/editar/"+habilitar,
            success:function result(data)
            {    
                $(".dialog-form").html(data);

                

                $("#frmUbicacion").validate(
                {
                   submitHandler: function(form) 
                   {
					  $("#Aplicar,#Aceptar").hide();
                      
                     $(form).ajaxSubmit(
                     {

                        success:    function(response) 
                        { 
                            if(response.trim()=="-1")
                            {
                                $("#Aplicar,#Aceptar").show(); 
                                alert("Este nombre de la gasolinera ya existe");
                            }
                            else if(response.trim()=="-10")
                            {
                                $("#Aplicar,#Aceptar").show(); 
                                alert("Debe de seleccionar la ubicación en el mapa");
                            }
                            else
                            {
                                $('#flexgrid').flexReload();
                                if(tipo==1)
                                {
                                    $('#frmUbicacion').resetForm();
    								$("#Aplicar,#Aceptar").show();
                                }
                                else
                                {
                                    $(".dialog-form").dialog("destroy");
                                    $(".dialog-form").dialog("close");
                                }
							 }
                        }
                     });
                   }
                });


                $(".dialog-form").dialog({
                        height:'auto',
                        width: 'auto',
                        resizable: false,
                        title: "Agregar Ubicación",
                        position: 'center',
                        modal: true,
                        resizeStop: function(event, ui) {google.maps.event.trigger(mapaPropiedad, 'resize')  },
                        open: function(event, ui) 
                        {
                            google.maps.event.trigger(mapaPropiedad, 'resize'); 
                            if($("#latitud").val()!='')
                            {
                                var ubicacionEditar = new google.maps.LatLng($("#latitud").val(), $("#longitud").val());
                                mapaPropiedad.setCenter(ubicacionEditar);
                            }
                            else
                            {
                                var ubicacionEditar = new google.maps.LatLng(23.221154981846556, -103.216552734375);
                                mapaPropiedad.setCenter(ubicacionEditar);
                            }
                        },
                        buttons: [{
                                id:"Aplicar",
                                text:"Aplicar",
                                click:function()
                                {
                                        tipo = 1;
                                        $("#frmUbicacion").submit();
                                }
                                },
                                {
                                    id:"Aceptar",
                                    text:"Aceptar",
                                    click:
                                    function()
                                    {
                                            tipo = 2;
                                            $("#frmUbicacion").submit();
                                    }
                                },
                                {
                                    id:"Cancelar",
                                    text:"Cancelar",
                                    click:function()
                                    {
                                        $(".dialog-form").dialog("destroy");
                                        $(".dialog-form").dialog("close");
                                    }
                                }
                        ]
                        });                     
                if(id != 0 )
                {
                    $("#Aplicar").hide();
                }

                if(habilitar == 1) 
                {
                    $("#Cancelar").hide();
                }
                    
            }
    });

}

function cargarCiudades($idEstado, $contenedor, $idCiudad, $editando)
{
    $.ajax({
        type: 'POST',
        url: '/catalogos/ubicacion/cargar-ciudades',
        data: {idEstado: $idEstado, idCiudad: $idCiudad, editando: $editando},
        success: function(resp)
        {
            $('#' + $contenedor).html(resp);
        },
        error: function()
        {

        },
        complete: function()
        {

        }
    });
}

function eliminarUbicacion(id, status)
{
    if(status == 1)
    {
        if(confirm('¿En realidad desea deshabilitar este registro?'))
        {
            $.ajax({
                type: 'POST',
                url:"/catalogos/ubicacion/eliminar",
                data:{id:id},
                success:
                function respuesta(res)
                {
                    alert('El registro ha sido deshabilitado.');
                    $('#flexgrid').flexReload();
                }
            });
        }
    }else if(status == 0)
    {
        if(confirm('¿En realidad desea habilitar este registro?'))
        {
            $.ajax({
                type: 'POST',
                url:"/catalogos/ubicacion/habilitar",
                data:{id:id},
                success:
                function respuesta(res)
                {
                    alert('El registro ha sido habilitado.');
                    $('#flexgrid').flexReload();
                }
            });
        }
    }
}

$(document).ready(function() 
{   

    $("#flexgrid").flexigrid(
    {
        url: '/catalogos/ubicacion/grid',
        dataType: 'xml',
        colModel : [
                {display: 'NOMBRE',  name:'nombre',width : 890, sortable : true, align: 'center'},
                {display: 'MODIFICAR',   width : 100, sortable : false, align: 'center'},
                {display: 'ELIMINAR',   width : 100, sortable : false, align: 'center'}
                ],
        sortname: "nombre",
        sortorder: "asc",
        usepager: true,
        useRp: false,
        rp: 10,
        width: 1122,
        height: 400
    });

    $("#flexgrid").ready(function(){
        $(".pReload").hide();
    });
});

function filtrarUbicaciones()
{
    var filtro = "/catalogos/ubicacion/grid";
    var imprimir="/catalogos/ubicacion/imprimir";
    var exportar="/catalogos/ubicacion/exportar";

    if($("#fnombre").val() != "")
    {
            filtro += "/nombre/"+$("#fnombre").val();
            imprimir += "/nombre/"+$("#fnombre").val();
            exportar += "/nombre/"+$("#fnombre").val();
    }

    $('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);
    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}

function limpiarUbicaciones()
{
    $("#fnombre").attr('value','');

    var filtro = "/catalogos/ubicacion/grid";
	
	var imprimir="/catalogos/ubicacion/imprimir";
    var exportar="/catalogos/ubicacion/exportar";
	
	$('#btnImprimir').attr("href",imprimir);
    $('#btnExportar').attr("href",exportar);


    $('#flexgrid').flexOptions({url: filtro}).flexReload(); 
}



function cargaMapa() 
{
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(23.221154981846556, -103.216552734375);
    var mapOptions = 
    {
        zoom: 4,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    }
    mapaPropiedad = new google.maps.Map(document.getElementById('mapaUbicacion'), mapOptions);

    google.maps.event.addListener(mapaPropiedad, 'click', function(event) {
        placeMarker(event.latLng);
    });

}

function marcadorPropiedad(lat,longitud) 
{
    var ubicacion = new google.maps.LatLng(lat, longitud);
    marker = new google.maps.Marker({
              position: ubicacion,
              map: mapaPropiedad
    });
    mapaPropiedad.setCenter(ubicacion);
    bandera=true;
}

function codeAddress() 
{        
    var address = $("#direccion").val()+','+$("#idCiudadField option[value='"+$("#idCiudadField").val()+"']").text()+','+$("#idCiudadField option[value='"+$("#idCiudadField").val()+"']").text()+','+$("#estado option[value='"+$("#estado").val()+"']").text();
    geocoder.geocode( { 'address': address}, function(results, status) 
    {
        if (status == google.maps.GeocoderStatus.OK) 
        {
          if($("#id").val()<=0)
            mapaPropiedad.setCenter(results[0].geometry.location);
          if($("#direccion").val()!='')
            mapaPropiedad.setZoom(18);
          else
            mapaPropiedad.setZoom(14);
          /*var marker = new google.maps.Marker({
              map: mapaPropiedad,
              position: results[0].geometry.location
          });*/
        } 
        else 
        {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
}

function placeMarker(location) 
{
  if($("#idCiudadField").val()>0)
  {
      if(bandera)
      {
        marker.setPosition(location);
      }
      else
      {
          marker = new google.maps.Marker({
              position: location,
              map: mapaPropiedad
          });
      }

      $("#latitud").attr("value",location.lat());
      $("#longitud").attr("value",location.lng());
      bandera=true;
  }
  else
    alert("Debe seleccionar una colonia para poder definir la ubicación exacta de la estación.");

}

function change(input, value)
{
    if(value == 1)
        $("#"+input).val("0");
    else
        $("#"+input).val("1");
}
