$(document).ready(function()
{
	$("#flexigrid").flexigrid({
        url: "/backend/cultivo/grid/"
        ,dataType: "xml"
        ,colModel: [
            {display: "Editar", name: "editar", width : 40, sortable: false, align: "center"},
			{display: "Cancelar", name: "cancelar", width: 60, sortable: false, align: "center"},
			{display: "Etapa Fenológica", name: "etapa", width: 150, sortable: false, align: "center"},
			{display: "Cultivo", name: "nombre", width: 815, sortable: true, align: "center"}
			
        ]
        ,sortname: "nombre"
        ,sortorder: "asc"
        ,usepager: true
        ,useRp: false
        ,singleSelect: true
        ,resizable: false
        ,showToggleBtn: false
        ,rp: 10
        ,width: 1070
        ,height: 350
        ,onSuccess: function(){
        
        }
        ,onError: function(respuesta){
            ShowDialogBox("Ha ocurrido un error", "No se han podido mostrar los cultivos, intentelo nuevamente", "Ok", "", false, "");
        }
    });
	

	$("#btnGuardar").click(function(e){
        if($("#nombre").val().trim() == '')
            ShowDialogBox("Datos incompletos", "El nombre es requerido", "Ok", "", false, "");
        else
        {
        	var frm = new FormData(document.getElementById("frm"));
    	    $.ajax({
    	    	url: "/backend/cultivo/guardar/",
    	      	type: "POST",
    	      	data: frm,
    	      	processData: false,  
    	      	contentType: false
    	    }).done(function(data){
    	    	if(data > 0) 
    	    	{
    	    		if($("#id").val() > 0)
                        ShowDialogBox("Guardado", "El registro se ha editado correctamente", "Ok", "", true, "/backend/cultivo/");
    	    		else
                    {
                        ShowDialogBox("Guardado", "El registro se ha guardado correctamente", "Ok", "", false, "");
                        limpiar();
                    }
    	    	}
    	    	else if(data == -1) 
    	    		ShowDialogBox("No se ha podido guardar", "El registro ya existe", "Ok", "", false, "");
    	    	else 
    	    		ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
    	    });
        }
 	});
});

function agregar(id=0)
{
	window.location = "/backend/cultivo/agregar/id/" + id;
}

function filtrargrig()
{
    var filtro = $("#filtroNombre").val();
    $('#flexigrid').flexOptions({url: "/backend/cultivo/grid/filtro/" + filtro}).flexReload(); 
}

function limpiar()
{
	$("#nombre").val('');
}

function limpiarFiltro()
{
    $("#filtroNombre").val('');
}

function deshabilitar(id)
{
	$.ajax({
    	url: "/backend/cultivo/deshabilitar/id/" + id,
      	type: "POST",
      	processData: false,  
      	contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/cultivo/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}


function habilitar(id)
{
    $.ajax({
        url: "/backend/cultivo/habilitar/id/" + id,
        type: "POST",
        processData: false,  
        contentType: false
    }).done(function(data){
        if(data > 0)
        {
            var filtro = $("#filtroNombre").val();
            $('#flexigrid').flexOptions({url: "/backend/cultivo/grid/filtro/" + filtro}).flexReload(); 
        }
        else
        {
            ShowDialogBox("Ha ocurrido un error", data, "Ok", "", false, "");
        }
    });
}
function etapaFenologica(id)
{
	$.ajax({
    	url: "/backend/cultivo/etapa-fenologica/",
      	type: "POST",
		data:{id:id}
    }).done(function(datas){
        ShowWindowBox("Etapa Fenológica", datas, "", "", "", "");
    });
}