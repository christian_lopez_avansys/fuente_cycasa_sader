<?php
/**
	* This class is under GPL Licencense Agreement
	* @author Juan Carlos Gonzalez Ulloa <jgonzalez@innox.com.mx>
	* Innovacion Inteligente S de RL de CV (Innox)
	* Lopez Mateos Sur 2077 - Z16
	* Col. Jardines de Plaza del Sol
	* Guadalajara, Jalisco
	* CP 44510
	* Mexico
	*
	* Class to read SHP files and modify the DBF related information
	* Just create the object and all the records will be saved in $shp->records
	* Each record has the "shp_d2ata" and "dbf_d2ata" arrays with the record information
	* You can modify the DBF information using the $shp_record->setDBFInformation($data)
	* The $data must be an array with the DBF's row data.
	*
	* Performance issues:
* Because the class opens and fetches all the information (records/dbf info)
	* from the file, the loading time and memory amount neede may be way to much.
	* Example:
*   15 seconds loading a 210907 points shape file
	*   60Mb memory limit needed
	*   Athlon XP 2200+
	*   Mandrake 10 OS
	*
	*
	*
	* Edited by David Granqvist March 2008 for better performance on large files
	* This version only get the information it really needs
	* Get one record at a time to save memory, means that you can work with very large files.
	* Does not load the information until you tell it too (saves time)
	* Added an option to not read the polygon points can be handy sometimes, and saves time :-)
	* 
	* Example:
		
	//sets the options to show the polygon points, 'noparts' => true would skip that and save time
	$options = array('noparts' => false);
	$shp = new ShapeFile2("../../php/ShapeFile2/file.shp",$options); 

	//Dump the ten first records
	$i = 0;
	while ($record = $shp->getNext() and $i<10) {
		$dbf_d2ata = $record->getDbfData();
		$shp_d2ata = $record->getShpData();
		//Dump the information
		var_d2ump($dbf_d2ata);
		var_d2ump($shp_d2ata);
		$i++;
	}
	* 
	*/

ini_set("memory_limit","30000M");
//set_time_limit("30");

// Configuration
define("SHOW_ERRORS2", true);
define("DEBUG2", false);


// Constants
define("XY_POINT_RECORD_LENGTH2", 16);


// Strings
define("ERROR_FILE_NOT_FOUND2", "SHP File not found [%s]");
define("INEXISTENT_RECORD_CLASS2", "Unable to determine shape record type [%i]");
define("INEXISTENT_FUNCTION2", "Unable to find reading function [%s]");
define("INEXISTENT_d2BF_FILE", "Unable to open (read/write) SHP's DBF file [%s]");
define("INCORRECT_d2BF_FILE", "Unable to read SHP's DBF file [%s]");
define("UNABLE_TO_WRITE_d2BF_FILE", "Unable to write DBF file [%s]");



class ShapeFile2{
	private $file_name;
	private $fp;
	//Used to fasten up the search between records;
	private $dbf_filename = null;
	//Starting position is 100 for the records
	private $fpos = 100;

	private $error_message = "";
	private $SHOW_ERRORS2   = SHOW_ERRORS2;

	private $shp_bounding_box = array();
	private $shp_type         = 0;

	public $records;

	function ShapeFile2($file_name,$options){
		$this->options = $options;

		$this->file_name = $file_name;
		//_d2("Opening [$file_name]");
		if(!is_readable($file_name)){
			return $this->setError( sprintf(ERROR_FILE_NOT_FOUND2, $file_name) );
		}

		$this->fp = fopen($this->file_name, "rb");
		
		$this->_fetchShpBasicConfiguration();

		//Set the dbf filename
		$this->dbf_filename = processdbffilename2($this->file_name);

	}
	
	
	public function getError(){
		return $this->error_message;
	}

	
	function __d2estruct()
	{
		$this->closeFile();
	}

	// Data fetchers
	private function _fetchShpBasicConfiguration(){
		//_d2("Reading basic information");
		fseek($this->fp, 32, SEEK_SET);
		$this->shp_type = readandunpack2("i", fread($this->fp, 4));
		//_d2("SHP type detected: ".$this->shp_type);

		$this->shp_bounding_box = readboundingbox2($this->fp);
		////_d2("SHP bounding box detected: miX=".$this->shp_bounding_box["xmin"]." miY=".$this->shp_bounding_box["ymin"]." maX=".$this->shp_bounding_box["xmax"]." maY=".$this->shp_bounding_box["ymax"]);
	}



	public function getNext(){
		if (!feof($this->fp)) {
			fseek($this->fp, $this->fpos);
			$shp_record = new ShapeRecord2($this->fp, $this->dbf_filename,$this->options);
			if($shp_record->getError() != ""){
				return false;
			}
			$this->fpos = $shp_record->getNextRecordPosition();
			return $shp_record;
		}
		return false;
	}
	
	/*Alpha, not working
	public function _resetFileReading(){
		rewind($this->fp);
		$this->fpos = 0;
		
		$this->_fetchShpBasicConfiguration();
	}*/

	/* Takes too much memory
	function _fetchRecords(){
		fseek($this->fp, 100);
		while(!feof($this->fp)){
			$shp_record = new ShapeRecord2($this->fp, $this->file_name);
			if($shp_record->error_message != ""){
				return false;
			}
			$this->records[] = $shp_record;
		}
	}
	*/

//Not Used
/*	private function getDBFHeader(){
		$dbf_d2ata = array();
		if(is_readable($dbf_d2ata)){
			$dbf = dbase_open($this->dbf_filename , 1);
			// solo en PHP5 $dbf_d2ata = dbase_get_header_info($dbf);
			echo dbase_get_header_info($dbf);
		}
	}
	*/

	// General functions        
	private function setError($error){
		$this->error_message = $error;
		if($this->SHOW_ERRORS2){
			echo $error."\n";
		}
		return false;
	}

	private function closeFile(){
		if($this->fp){
			fclose($this->fp);
		}
	}


}


/**
	* ShapeRecord2
	*
	*/    
class ShapeRecord2{
	private $fp;
	private $fpos = null; 
	
	private $dbf = null;

	private $record_number     = null;
	private $content_length    = null;
	private $record_shape_type = null;

	private $error_message     = "";

	private $shp_d2ata = array();
	private $dbf_d2ata = array();

	private $file_name = "";

	private $record_class = array(  0 => "RecordNull",
		1 => "RecordPoint",
		8 => "RecordMultiPoint",
		3 => "RecordPolyLine",
		5 => "RecordPolygon",
		13 => "RecordMultiPointZ",
		11 => "RecordPointZ");

	function ShapeRecord2(&$fp, $file_name,$options){
		$this->fp = $fp;
		$this->fpos = ftell($fp);
		$this->options = $options;

		//_d2("Shape record created at byte ".ftell($fp));
		
		if (feof($fp)) {
			echo "end ";
			exit;
		}
		$this->_readHeader();

		$this->file_name = $file_name;

	}
	
	public function getNextRecordPosition(){
		$nextRecordPosition = $this->fpos + ((4 + $this->content_length )* 2);
		return $nextRecordPosition;
	}

	private function _readHeader(){
		$this->record_number     = readandunpack2("N", fread($this->fp, 4));
		$this->content_length    = readandunpack2("N", fread($this->fp, 4));
		$this->record_shape_type = readandunpack2("i", fread($this->fp, 4));

		//_d2("Shape Record ID=".$this->record_number." ContentLength=".$this->content_length." RecordShapeType=".$this->record_shape_type."\nEnding byte ".ftell($this->fp)."\n");
	}

	private function getRecordClass(){
		if(!isset($this->record_class[$this->record_shape_type])){
			//_d2("Unable to find record class ($this->record_shape_type) [".getarray2($this->record_class)."]");
			return $this->setError( sprintf(INEXISTENT_RECORD_CLASS2, $this->record_shape_type) );
		}
		//_d2("Returning record class ($this->record_shape_type) ".$this->record_class[$this->record_shape_type]);
		return $this->record_class[$this->record_shape_type];
	}

	private function setError($error){
		$this->error_message = $error;
		return false;
	}

	public function getError(){
		return $this->error_message;
	}

	public function getShpData(){
		$function_name = "read".$this->getRecordClass();

		//_d2("Calling reading function [$function_name] starting at byte ".ftell($fp));

		if(function_exists($function_name)){
			$this->shp_d2ata = $function_name($this->fp,$this->options);
		} else {
			$this->setError( sprintf(INEXISTENT_FUNCTION2, $function_name) );
		}
		
		return $this->shp_d2ata;
	}
	
	public function getDbfData(){

		$this->_fetchDBFInformation();
		
		return $this->dbf_d2ata;
	}

	private function _openDBFFile($check_writeable = false){
		$check_function = $check_writeable ? "is_writable" : "is_readable";
		if($check_function($this->file_name)){
			$this->dbf = dbase_open($this->file_name, ($check_writeable ? 2 : 0));
			if(!$this->dbf){
				$this->setError( sprintf(INCORRECT_d2BF_FILE, $this->file_name) );
			}
		} else {
			$this->setError( sprintf(INEXISTENT_d2BF_FILE, $this->file_name) );
		}
	}

	private function _closeDBFFile(){
		if($this->dbf){
			dbase_close($this->dbf);
			$this->dbf = null;
		}
	}

	private function _fetchDBFInformation(){
		$this->_openDBFFile();
		if($this->dbf) {
			//En este punto salta un error si el registro 0 está vacio.
			//Ignoramos los errores, ja que aún así todo funciona perfecto.
			$this->dbf_d2ata = @dbase_get_record_with_names($this->dbf, $this->record_number);
		} else {
			$this->setError( sprintf(INCORRECT_d2BF_FILE, $this->file_name) );
		}
		$this->_closeDBFFile();
	}

	public function setDBFInformation($row_array){
		$this->_openDBFFile(true);
		if($this->dbf) {
			unset($row_array["deleted"]);

			if(!dbase_replace_record($this->dbf, array_values($row_array), $this->record_number)){
				$this->setError( sprintf(UNABLE_TO_WRITE_d2BF_FILE, $this->file_name) );
			} else {
				$this->dbf_d2ata = $row_array;
			}
		} else {
			$this->setError( sprintf(INCORRECT_d2BF_FILE, $this->file_name) );
		}
		$this->_closeDBFFile();
	}
}


/**
	* Reading functions
	*/    

function readrecordnull2(&$fp, $read_shape_type = false,$options = null){
	$data = array();
	if($read_shape_type) $data += readShapeType($fp);
	//_d2("Returning Null shp_d2ata array = ".getarray2($data));
	return $data;
}
$point_count = 0;
function readrecordpoint2(&$fp, $create_object = false,$options = null){
	global $point_count;
	$data = array();

	$data["x"] = readandunpack2("d", fread($fp, 8));
	$data["y"] = readandunpack2("d", fread($fp, 8));

	////_d2("Returning Point shp_d2ata array = ".getarray2($data));
	$point_count++;
	return $data;
}

function readrecordpoint2Z(&$fp, $create_object = false,$options = null){
	global $point_count;
	$data = array();

	$data["x"] = readandunpack2("d", fread($fp, 8));
	$data["y"] = readandunpack2("d", fread($fp, 8));
// 	$data["z"] = readandunpack2("d", fread($fp, 8));
// 	$data["m"] = readandunpack2("d", fread($fp, 8));

	////_d2("Returning Point shp_d2ata array = ".getarray2($data));
	$point_count++;
	return $data;
}

function readrecordpoint2ZSP($data, &$fp){
	
	$data["z"] = readandunpack2("d", fread($fp, 8));
	
	return $data;
}

function readrecordpoint2MSP($data, &$fp){
	
	$data["m"] = readandunpack2("d", fread($fp, 8));
	
	return $data;
}

function readrecordmultipoint2(&$fp,$options = null){
	$data = readboundingbox2($fp);
	$data["numpoints"] = readandunpack2("i", fread($fp, 4));
	//_d2("MultiPoint numpoints = ".$data["numpoints"]);

	for($i = 0; $i <= $data["numpoints"]; $i++){
		$data["points"][] = readrecordpoint2($fp);
	}

	//_d2("Returning MultiPoint shp_d2ata array = ".getarray2($data));
	return $data;
}

function readrecordpolyline2(&$fp,$options = null){
	$data = readboundingbox2($fp);
	$data["numparts"]  = readandunpack2("i", fread($fp, 4));
	$data["numpoints"] = readandunpack2("i", fread($fp, 4));

	//_d2("PolyLine numparts = ".$data["numparts"]." numpoints = ".$data["numpoints"]);
	if (isset($options['noparts']) && $options['noparts']==true) {
		//Skip the parts
		$points_initial_index = ftell($fp)+4*$data["numparts"];
		$points_read = $data["numpoints"];
	}
	else{
		for($i=0; $i<$data["numparts"]; $i++){
			$data["parts"][$i] = readandunpack2("i", fread($fp, 4));
			//_d2("PolyLine adding point index= ".$data["parts"][$i]);
		}

		$points_initial_index = ftell($fp);

		//_d2("Reading points; initial index = $points_initial_index");
		$points_read = 0;
		foreach($data["parts"] as $part_index => $point_index){
			//fseek($fp, $points_initial_index + $point_index);
			//_d2("Seeking initial index point [".($points_initial_index + $point_index)."]");
			if(!isset($data["parts"][$part_index]["points"]) || !is_array($data["parts"][$part_index]["points"])){
				$data["parts"][$part_index] = array();
				$data["parts"][$part_index]["points"] = array();
			}
			while( ! in_array( $points_read, $data["parts"]) && $points_read < $data["numpoints"] && !feof($fp)){
				$data["parts"][$part_index]["points"][] = readrecordpoint2($fp, true);
				$points_read++;
			}
		}
	}

	fseek($fp, $points_initial_index + ($points_read * XY_POINT_RECORD_LENGTH2));

	//_d2("Seeking end of points section [".($points_initial_index + ($points_read * XY_POINT_RECORD_LENGTH2))."]");
	return $data;
}

function readrecordmultipoint2Z(&$fp,$options = null){
	$data = readboundingbox2($fp);
	$data["numparts"]  = readandunpack2("i", fread($fp, 4));
	$data["numpoints"] = readandunpack2("i", fread($fp, 4));
// 	$fileX = 40 + (16*$data["numpoints"]);
// 	$fileY = $fileX + 16 + (8*$data["numpoints"]);
	$fileX = 44 + (4*$data["numparts"]);
	$fileY = $fileX + (16*$data["numpoints"]);
	$fileZ = $fileY + 16 + (8*$data["numpoints"]);
	/*
	Note: X = 44 + (4 * NumParts), Y = X + (16 * NumPoints), Z = Y + 16 + (8 * NumPoints)
	*/

	//_d2("PolyLine numparts = ".$data["numparts"]." numpoints = ".$data["numpoints"]);
	if (isset($options['noparts']) && $options['noparts']==true) {
		//Skip the parts
		$points_initial_index = ftell($fp)+4*$data["numparts"];
		$points_read = $data["numpoints"];
	}
	else{
		for($i=0; $i<$data["numparts"]; $i++){
			$data["parts"][$i] = readandunpack2("i", fread($fp, 4));
			//_d2("PolyLine adding point index= ".$data["parts"][$i]);
		}
		$points_initial_index = ftell($fp);

		//_d2("Reading points; initial index = $points_initial_index");
		$points_read = 0;
		foreach($data["parts"] as $part_index => $point_index){
			//fseek($fp, $points_initial_index + $point_index);
			//_d2("Seeking initial index point [".($points_initial_index + $point_index)."]");
			if(!isset($data["parts"][$part_index]["points"]) || !is_array($data["parts"][$part_index]["points"])){
				$data["parts"][$part_index] = array();
				$data["parts"][$part_index]["points"] = array();
			}
			while( ! in_array( $points_read, $data["parts"]) && $points_read < $data["numpoints"]/* && !feof($fp)*/){
				$data["parts"][$part_index]["points"][] = readrecordpoint2($fp, true);
				$points_read++;
			}
		}
		
		$data['Zmin'] = readandunpack2("d", fread($fp, 8));
		$data['Zmax'] = readandunpack2("d", fread($fp, 8));
		
		foreach($data["parts"] as $part_index => $point_index){
			foreach($point_index["points"] as $n => $p){
				$data["parts"][$part_index]['points'][$n] = readrecordpoint2ZSP($p, $fp, true);
			}
		}
		
		$data['Mmin'] = readandunpack2("d", fread($fp, 8));
		$data['Mmax'] = readandunpack2("d", fread($fp, 8));
		
		foreach($data["parts"] as $part_index => $point_index){
			foreach($point_index["points"] as $n => $p){
				$data["parts"][$part_index]['points'][$n] = readrecordpoint2MSP($p, $fp, true);
			}
		}
	}

	fseek($fp, $points_initial_index + ($points_read * XY_POINT_RECORD_LENGTH2));

	//_d2("Seeking end of points section [".($points_initial_index + ($points_read * XY_POINT_RECORD_LENGTH2))."]");
	return $data;
}

function readrecordpolygon2(&$fp,$options = null){
	//_d2("Polygon reading; applying readrecordpolyline2 function");
	return readrecordpolyline2($fp,$options);
}

/**
	* General functions
	*/    
function processdbffilename2($dbf_filename){
	//_d2("Received filename [$dbf_filename]");
	if(!strstr($dbf_filename, ".")){
		$dbf_filename .= ".dbf";
	}

	if(substr($dbf_filename, strlen($dbf_filename)-3, 3) != "dbf"){
		$dbf_filename = substr($dbf_filename, 0, strlen($dbf_filename)-3)."dbf";
	}
	//_d2("Ended up like [$dbf_filename]");
	return $dbf_filename;
}

function readboundingbox2(&$fp){
	$data = array();
	$data["xmin"] = readandunpack2("d",fread($fp, 8));
	$data["ymin"] = readandunpack2("d",fread($fp, 8));
	$data["xmax"] = readandunpack2("d",fread($fp, 8));
	$data["ymax"] = readandunpack2("d",fread($fp, 8));

	//_d2("Bounding box read: miX=".$data["xmin"]." miY=".$data["ymin"]." maX=".$data["xmax"]." maY=".$data["ymax"]);
	return $data;
}

function readandunpack2($type, $data){
	if(!$data) return $data;
	return current(unpack($type, $data));
}

function _d2($DEBUG2_text){
	if(DEBUG2){
		echo $DEBUG2_text."\n";
	}
}

function getarray2($array){
	ob_start();
	print_r($array);
	$contents = ob_get_contents();
	ob_get_clean();
	return $contents;
}
?>