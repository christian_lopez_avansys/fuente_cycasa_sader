<?php
require_once 'PHPExcel.php';
require_once 'PHPExcel/IOFactory.php';

class My_PHPExcel_Evaluacion extends PHPExcel
{
	private $objPHPExcel;
	
	public function __construct()
	{
		$this->objPHPExcel = new PHPExcel();
	}
	
	public function createExcel($data)
	{
		
		$f_size = 10;
			
		//Fuente y tama�o de fuente
		$this->objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$this->objPHPExcel->getDefaultStyle()->getFont()->setSize($f_size);
		$this->objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(36);
		$this->objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(30);
		$this->objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(24);
		
		//Instancia para insertar la imagen/logotipo en la hoja	
		$objDrawing = new PHPExcel_Worksheet_Drawing();
    	$objDrawing->setPath($_SERVER['DOCUMENT_ROOT'].'/biobolsa/public/images/header.jpg');
		$objDrawing->setHeight(56);
		$objDrawing->setWorksheet($this->objPHPExcel->getActiveSheet());
		
		//Poner los nombres de cada columna
		
		$this->objPHPExcel->getActiveSheet()->setCellValue('A5', utf8_encode('RESULTADOS DE LA EVALUACI�N'));
		$this->objPHPExcel->getActiveSheet()->mergeCells('A5:C5');
		$this->setBorders("A5:C5");
		$this->objPHPExcel->getActiveSheet()->getStyle("A5:C5")->getFont()->setBold(true);
		$this->objPHPExcel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		
		
		//Vaciar la informaci�n a la hoja de c�lculo
		foreach($data as $data)
		{
			foreach($data as $cell => $value)
			{
				$this->objPHPExcel->getActiveSheet()->setCellValue($cell, $value);
				$this->setBordersThin($cell);
				if(substr($cell, 0, 1) == "B")
				{
					$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}
			}
		}
		
		$this->objPHPExcel->getActiveSheet()->setTitle(utf8_encode("Evaluaci�n"));
		
		ob_end_clean();
        
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Evaluacion.xlsx"');
		
		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		$objWriter->save("php://output");		
		
		ob_end_clean();
	}
	
	//Funci�n para poner borde gruso a la celda
	public function setBorders($cell)
	{				
		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
	}

	//Funci�n para poner borde delgado a la celda
	public function setBordersThin($cell)
	{		
		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$this->objPHPExcel->getActiveSheet()->getStyle($cell)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	}
}
