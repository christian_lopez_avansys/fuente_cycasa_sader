<?php
class My_Comun{

	function __construct(){}

	/*// rutas
	const CARGAS_PROYECTOS = '/cargas/proyectos/';
	const CARGAS_ACTIVIDADDOCUMENTOS = '/cargas/actividad-documentos/';
	const CARGAS_ACTIVIDADTAREADOCUMENTOS = '/cargas/actividad-tarea-documentos/';
	const CARGAS_DOCUMENTOS = '/cargas/documentos/';*/

	public static function mensaje($numero){

		switch($numero)
		{
			case "-5"   : return "El registro ya existe"; break;
			case "1"    : return "El registro fue eliminado"; break;
			case "2"    : return "El registro fue deshabilitado"; break;
			case "3"    : return "Ocurrió un error al tratar de deshabilitar el registro, inténtelo de nuevo"; break;
			case "4"    : return "El registro fue habilitado"; break;
			case "5"    : return "Ocurrió un error al tratar de habilitar el registro, inténtelo de nuevo"; break;
			case "6"    : return "No se puede eliminar porque hay información dependiente"; break;
			default     : return "No se encontró la descripción del error. ".$numero; break;    
		}
	}

	 /**
     * Guarda un registro del modelo especificado.
     *
     * @param string $modelo Modelo
     * @param array $data arreglo de los datos a guardar
     * @param array $campo_unico arreglo asociativo campo=>valor de los datos a validar cómo únicos
     * @param integer $id del registro en caso de que se esté actualizando
     * @return mixed Resultado de �xito/error al tratar de eliminar el registro
     * @access public
     * @static
     */ 
    public static function guardar($model, $data = array(), $campo_unico = null, $id = "", $bitacora="nombre")
    {
    	$ModelTable = Doctrine_Core::getTable($model);
        
        if($campo_unico != null)
        {
            if(is_array($campo_unico))
            {
                foreach($campo_unico as $validar)
                {
	                $campo = $data[$validar];
	                $campo_unico = ucfirst(str_replace("-", "", $validar));
	                $findBy = "findOneBy$campo_unico";
	                $Model = $ModelTable->{$findBy}($campo);
	                if((@$Model->id != $id)&&(@$Model !== false))
	                {
	                    return -1; 
	                    exit;
	                }
                }
            }
            else 
            {
                $campo = $data[$campo_unico];
                $campo_unico = ucfirst($campo_unico);
                $findBy = "findOneBy$campo_unico";
                $Model = $ModelTable->{$findBy}($campo);
                if((@$Model->id != (int)$id)&&(@$Model !== false)){return -1; exit;}
            }
        }

        if(!is_numeric($id)){$Model = new $model(); unset($data['id']);}
        else{$Model = $ModelTable->findOneById((int)$id);}
            
        $data = str_replace(array("'","\"",),array("´","´"),$data);
            
        $Model->fromArray($data);   

        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        try
        {   
            $Model->save();  
            $conn->commit(); 
            return $Model->id;
        }
        catch(Exception $e)
        { 
            $conn->rollBack(); 
			return My_Comun::mensaje($e->getPortableCode()); 
        	//return $e;
        }
    }

	function aleatorio($maximo){
		
		$permitidos = "1234567890abcdefghijklmnopqrstuvwxyz";
		$i = 1;
		$_aleatorio = "";

		while($i <= $maximo){

			$_aleatorio .= $permitidos{mt_rand(0, strlen($permitidos))};
			$i++;
		}

		return $_aleatorio;        
	}

	public static function tienePermiso($permiso){   
		$permisos = explode("|", Zend_Auth::getInstance()->getIdentity()->permisos);                

		if(in_array($permiso, $permisos))
			return true;
		else
			return false;
	}

	public static function crearQuery($modelo, $query = null){

		$con = Doctrine_Manager::getInstance()->connection();
		if(is_null($query)){
			$q = Doctrine_Query::create()->from($modelo);
			return $q;
		} 
		else{
			$q = $con->execute($query)->fetchAll();
			return $q;
		}
	}
		
	public static function obtener($modelo, $campo, $valor, $final = "", $orden = ""){

		$filtro = $campo." = '".$valor."' ";

		if($final != "")
			$filtro .= $final;

		$q = Doctrine_Query::create()->from($modelo)->where($filtro);

		if($orden != "")
			$q->orderBy($orden);
		
		//echo $q->getSqlQuery(); exit;
		return $q->execute()->getFirst();
	}
	
	public static function obtenerFiltro($modelo, $filtro, $orden = ""){

		$q=Doctrine_Query::create()->from($modelo)->where($filtro);

		if($orden != "")
			$q->orderBy($orden);

		//echo $q->getSqlQuery(); //exit;

		return $q->execute();
	}

	/*public static function guardar($modelo, $datos = array(), $id = 0, $bitacora = array()){
		
		$tabla = Doctrine_Core::getTable($modelo);
		if(!is_numeric($id)){
			
			$Modelo = new $modelo(); 
			unset($datos['id']);
		}else{
			$Modelo = $tabla->findOneById((int)$id);
		}

		foreach($datos as $campo){

			if(!is_null($campo)){

				$campo = str_replace(array("'", "\""), array("´", "´"), $campo);
			}
		}
		
		$Modelo->fromArray($datos);

		try{

			$Modelo->save();

			foreach($bitacora as $bitacora_){
				
				if($bitacora_["id"] == "")
					Bitacora::guardar($Modelo->id, $modelo, $bitacora_["agregar"], $Modelo[$bitacora_["campo"]]);
				else{

					$registro = My_Comun::obtener($bitacora_["modelo"], "id", $bitacora_["id"]);

					Bitacora::guardar($Modelo->id, $modelo, $bitacora_["editar"], $registro[$bitacora_["campo"]]);
				}
			}

			return $Modelo->id;
		}catch(Exception $e){

			//echo $e->getMessage();

			try{
								
				return My_Comun::mensaje($e->getPortableCode()); 
			}catch(Exception $e1){
				
				return My_Comun::mensaje(-100); 
			}
		}
	}
*/
	public static function registrosGrid($modelo, $filtro)
	{
		### Incializamos el arreglo de registros
		$registros=array();
		
		### Recibimos los parámetros de paginación y ordenamiento.
		if (isset($_POST['page']) != ""){$page = $_POST['page'];}
		if (isset($_POST['sortname']) != ""){$sortname =$_POST['sortname'];}
		if (isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
		if (isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
		if (isset($_POST['query']) != ""){$query = $_POST['query'];}
		if (isset($_POST['rp']) != ""){$rp = $_POST['rp'];}
		
		
		$alias = $modelo;

		### Codificamos el filtro para evitar problemas con IE      
		$filtro= (My_Utf8::is_utf8($filtro))?$filtro:utf8_encode($filtro);
		
		
		### Creamos la consulta con el filtro pero sin parámetros de paginación para obtener el total de registros.
		//echo $filtro; //exit;
		$q=My_Comun::crearQuery($modelo)->where($filtro);
		//echo $q->getSqlQuery(); exit;
		
		$registros['total']=$q->count();
		$paginas=ceil($registros['total']/$rp);
		if($page>$paginas)
			$page=1;
			
		### Completamos la consulta con los datos de paginación y ordenamiento
		$offset = ($page-1)*$rp;
		$filtro .= ($qtype != '' && $query != '') ? " AND $alias.{$qtype} = '$query'" : '';
		$order = "$alias.{$sortname} $sortorder";
		
		### Ejecutamos la consulta
		$q = $q->orderBy($order)->limit($rp)->offset($offset);
		//echo $q->getSqlQuery(); exit;
		
		$registros['registros']=$q->execute();  
		$registros['pagina']=$page;  
		
		return $registros;
	}

/*	public static function registrosGridQuery($consulta){
		$registros = array();
		
		// Parámetros de paginación y ordenamiento que vienen del POST del JS donde se carga el DataGrid.
		if(isset($_POST['page']) != ""){$page = $_POST['page'];}
		if(isset($_POST['sortname']) != ""){$sortname = $_POST['sortname'];}
		if(isset($_POST['sortorder']) != ""){$sortorder = $_POST['sortorder'];}
		if(isset($_POST['qtype']) != ""){$qtype = $_POST['qtype'];}
		if(isset($_POST['query']) != ""){$query = $_POST['query'];}
		if(isset($_POST['rp']) != ""){$rp = $_POST['rp'];}

		//$consulta = (My_Utf8::is_utf8($consulta)) ? $consulta : utf8_encode($consulta);
		
		// Ejecutar la consulta sin parámetros de paginación ni ordenamiento; para obtener el total de registros.
		$q = My_Comun::crearQuery(null, $consulta);

		$registros['total'] = count($q);

		$paginas = ceil($registros['total'] / $rp);
		if($page > $paginas)
			$page = 1;
			
		// Completar la consulta con los parámetros de paginación y ordenamiento
		$consulta .= " order by ".$sortname." ".$sortorder;
		$offset = ($page - 1) * $rp;        
		$consulta .= " limit ".$offset.", ".$rp;

		//echo $consulta;

		$registros['registros'] = My_Comun::crearQuery(null, $consulta);
		$registros['pagina'] = $page;  
		
		return $registros;
	} */
	
	public static function armarGrid($registros, $grid){
		
		if(count($grid)>0){
			$columnas=array_keys($grid[0]);
		}

		$xml='<rows><page>'.$registros['pagina'].'</page><total>'.$registros['total'].'</total>';
		foreach($grid as $row){
			if(array_key_exists("id", $row))
				$xml .= '<row id="'.$row['id'].'">';
			else
				$xml .= '<row id="0">';

			foreach($columnas as $k=>$v)
			{
				if($v!='id')
				{
					$xml.='<cell><![CDATA['.$row[$v].']]></cell>';
				}
			}
			$xml .= '</row>';
		}
		
		echo $xml.="</rows>";        
	}

	/*public static function eliminar($modelo, $id, $bitacora = array()){   
		try{

			foreach($bitacora as $bitacora_){

				$registro = My_Comun::obtener($bitacora_["modelo"], "id", $bitacora_["id"]);
			}

			$q = Doctrine_Query::create()->delete($modelo)->where("id = ".$id);
			$q->execute();

			foreach($bitacora as $bitacora_){
				
				Bitacora::guardar($bitacora_["id"], $bitacora_["modelo"], $bitacora_["eliminar"], $registro[$bitacora_["campo"]]);
			}
			
			return My_Comun::mensaje(1);
		}catch (Exception $e){

			//echo $e->getMessage();
			
			if($e->getPortableCode()=="-3"){ // Error de integridad referencial
					try{
						
						$q = Doctrine_Query::create()->update($modelo)->set("status", "0")->where("id = ".$id);
						$q->execute();
						
						foreach($bitacora as $bitacora_){
					
							Bitacora::guardar($bitacora_["id"], $bitacora_["modelo"], $bitacora_["deshabilitar"], $registro[$bitacora_["campo"]]);
						}
						
						return My_Comun::mensaje(2);
					}catch (Exception $e1){
	
						echo $e1->getMessage();
					
						return My_Comun::mensaje(3);  
					}
				
			}else{
				return My_Comun::mensaje($e->getPortableCode()); 
			}
		}
	}

*/ 
	public static function deshabilitar($modelo, $id, $bitacora = array())
	{
		try
		{
			$q = Doctrine_Query::create()->update($modelo)->set("estatus", "0")->where("id = ".$id);
			$q->execute();  
			
			/*foreach($bitacora as $bitacora_)
			{
				$registro = My_Comun::obtener($bitacora_["modelo"], "id", $bitacora_["id"]);
				Bitacora::guardar($bitacora_["id"], $bitacora_["modelo"], $bitacora_["deshabilitar"], $registro[$bitacora_["campo"]]);
			}*/
			
			return 1;
		}
		catch (Exception $e)
		{
			return $e->getMessage();//My_Comun::mensaje(3); 
		}   
	}
	
	public static function habilitar($modelo, $id, $bitacora = array())
	{
		try
		{
			$q = Doctrine_Query::create()->update($modelo)->set("estatus", "1")->where("id = ".$id);
			$q->execute();  
			
			/*foreach($bitacora as $bitacora_)
			{
				$registro = My_Comun::obtener($bitacora_["modelo"], "id", $bitacora_["id"]);
				Bitacora::guardar($bitacora_["id"], $bitacora_["modelo"], $bitacora_["deshabilitar"], $registro[$bitacora_["campo"]]);
			}*/
			
			return 1;
		}
		catch (Exception $e)
		{
			return My_Comun::mensaje(3); 
		}   
	}
	
	public static function correo($asunto,$titulo, $cuerpo, $de="agroecologia@agricultura-eat.mx", $de_nombre="agroecologia@agricultura-eat.mx", $para="", $para_nombre, $copia = "", $adjunto = ""){

	$config = array("auth" => "login", "username" => "agroecologia@agricultura-eat.mx" , "password" => "i9&04Quz", "port" => 25);//587
	$transport = new Zend_Mail_Transport_Smtp("smtp.agricultura-eat.mx", $config);
        
        $cuerpo_ = "
                    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
                    <html xmlns=\"http://www.w3.org/1999/xhtml\">
                    <head><title>appgricola.com</title></head>
                    <body>
                        
                        <table style=\"width: 100%; font-family: Tahoma, Arial; font-size: 12px; border: none 0; border-spacing: 0; border-collapse: collapse; padding: 0;\" border=\"0\">
                            <tr>
                                <td style=\"height:3px; background-color: #14374A;\"></td>
                            </tr>
                            
                            <tr>
                                <td align=\"left\" style=\"padding: 10px;\">
								<img width=\"300\" src=\"http://agricultura-eat.mx/img/logo.jpeg\" alt=\"appgricola.com\" title=\"appgricola.com\"/></td>
                            </tr>
                            
                            <tr>
                                <td style=\"height:3px; background-color: #6B6C6C;\"></td>
                            </tr>
                            
                            <tr>
                                <td align=\"left\" valign=\"middle\" style=\"padding:10px; color:#14374A; font-size:18px;\">
                                    ".$titulo ."
				</td>
                            </tr>
                            
                            <tr>
                                <td align=\"left\" valign=\"top\" style=\"padding:20px; color:#444444; font-size:12px;\">
                                    ".$cuerpo."
				</td>
                            </tr>
                            
                            <tr>
                                <td align=\"left\" valign=\"top\" style=\"color:#444444; font-size:10px;\">
                                    Este correo electrónico ha sido enviado de la pagina de agricultura-eat.mx
                                </td>
                            </tr>
                            <tr><td style=\"height:3px; background-color: #14374A;\"></td></tr>
			</table>
                    </body>
		</html>";
        
		$mail = new Zend_Mail();
		$mail->setBodyHtml(utf8_decode($cuerpo_));
		$mail->setFrom($de,  utf8_decode($de_nombre));
		$mail->addTo($para, utf8_decode($para_nombre));	
		//$mail->setSubject(($titulo));
		$mail->setSubject(($asunto));

		if($copia != ""){
			$copia_ = explode(",", $copia);
			for($i = 0; $i <= count($copia_) - 1; $i++)
				$mail->addCc($copia_[$i], utf8_decode("Copia automática"));
		}

		if($adjunto != ""){
			$adjunto_ = explode(",", $adjunto);
			for($i = 0; $i <= count($adjunto_) - 1; $i++){
				$at = new Zend_Mime_Part(file_get_contents($adjunto_[$i]));
				$at->type = "application/pdf";
				$at->disposition = Zend_Mime::DISPOSITION_INLINE;
				$at->encoding = Zend_Mime::ENCODING_BASE64;
				$at->filename = "archivo".$i.".pdf";
				$mail->addAttachment($at);
			}
		}
	
		try{
			//$mail->send();
			$mail->send($transport);
		}catch(Exception $e){
			echo("$e error");
		}//try
		return 1;
	}

	/*

	public static function segundosAHora($segundos, $conSegundos = false){

		if($segundos == 0)
			return "00:00";

		$hora = intval($segundos / 3600);
		$minuto = intval((intval($segundos % 3600)) / 60);
		$segundo = $segundos % 60;

		$hora = strlen($hora) == 1 ? "0" . $hora : $hora;
		$minuto = strlen($minuto) == 1 ? "0" . $minuto : $minuto;
		$segundo = strlen($segundo) == 1 ? "0" . $segundo : $segundo;

		if($conSegundos)
			return $hora.":".$minuto.":".$segundo;
		else
			return $hora.":".$minuto;
	}

	public static function numeroASegundos($numero){

		if($numero == 0)
			return "0";

		return $numero * 3600;
	}

	public static function segundosANumero($segundos){

		if($segundos == 0)
			return "0";

		$hora = intval($segundos / 3600);
		$minuto = intval((intval($segundos % 3600)) / 60);

		if($minuto > 0)
			return $hora + ((($minuto * 100) / 60) / 100);
		else
			return $hora;
	}

	public static function FileSizeConvert($bytes){

		$bytes = floatval($bytes);

		$arBytes = array(
			0 => array(
				"UNIT" => "TB",
				"VALUE" => pow(1024, 4)
			),
			1 => array(
				"UNIT" => "GB",
				"VALUE" => pow(1024, 3)
			),
			2 => array(
				"UNIT" => "MB",
				"VALUE" => pow(1024, 2)
			),
			3 => array(
				"UNIT" => "KB",
				"VALUE" => 1024
			),
			4 => array(
				"UNIT" => "B",
				"VALUE" => 1
			),
		);

		foreach($arBytes as $arItem){

			if($bytes >= $arItem["VALUE"]){

				$result = $bytes / $arItem["VALUE"];
				$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
				break;
			}
		}

		return $result;
	}*/

	public static function comboMunicipio($idEstado)
    {
        $q=Doctrine_Query::create()->from('Municipio')->where('id_estado=?',$idEstado)->orderBy("nombre ASC");
        $d=$q->execute();
        $options='<option value="0">Seleccione un municipio...</option>';
        foreach($d as $municipio)
        {
            $s="";
            if($selected==$municipio->id)
               $s="selected";
                     
            $options.='<option value="'.$municipio->id_estado.'|'.$municipio->clave.'" '.$s.'>'.$municipio->nombre.'</option>';
        }
        echo $options;
    }

    public static function comboLocalidad($idEstado, $claveMunicipio)
    {
        $q=Doctrine_Query::create()->from('Localidad')->where('id_estado=?',$idEstado)->andWhere('clave_municipio=?',$claveMunicipio)->orderBy("nombre ASC");
        $d=$q->execute();
        $options='<option value="0">Seleccione una localidad...</option>';
        foreach($d as $localidad)
        {
            $s="";
            if($selected==$localidad->id)
               $s="selected";
                     
            $options.='<option value="'.$localidad->id.'" '.$s.'>'.My_Comun::reformarString(utf8_decode(utf8_encode($localidad->nombre))).'</option>';
        }
        echo $options;
    }

    public static function reformarString($string)
    {
		$string = str_replace("**u**", 'ü', $string);
		$string = str_replace("**A**", 'Á', $string);
        return $string;
    }
}
?>