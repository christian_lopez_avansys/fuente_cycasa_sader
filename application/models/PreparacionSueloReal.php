<?php

/**
 * PreparacionSueloReal
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class PreparacionSueloReal extends BasePreparacionSueloReal
{
	public static function guardar($relacion, $datos)
    {
        $tabla = Doctrine_Query::create()->from('RelacionDiagnostico')->where('id = ?', $relacion)->execute()->getFirst()->PreparacionSueloReal;
            // Subsoleo
            $subsoleo = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_subsoleo)->execute()->getFirst();
                $subsoleo->realizado = $datos['subsoleo_realizado'];
                if($datos['subsoleo_realizado'] == 1)
                {
                    $subsoleo->id_unidad = ($datos['subsoleo_id_unidad'] == "")?null:$datos['subsoleo_id_unidad'];
                    $subsoleo->cantidad = ($datos['subsoleo_cantidad'] == "")?null:$datos['subsoleo_cantidad'];
                    $subsoleo->precio = ($datos['subsoleo_precio'] == "")?null:$datos['subsoleo_precio'];
                    $subsoleo->importe = ($datos['subsoleo_importe'] == "")?null:$datos['subsoleo_importe'];
                    $subsoleo->fecha_realizacion = ($datos['subsoleo_fecha'] == "")?null:$datos['subsoleo_fecha'];
                    $subsoleo->innovacion = ($datos['subsoleo_innovacion'] == "")?null:$datos['subsoleo_innovacion'];
                }
                else
                {
                    $subsoleo->id_unidad = null;
                    $subsoleo->cantidad = null;
                    $subsoleo->precio = null;
                    $subsoleo->importe = null;
                    $subsoleo->fecha_realizacion = null;
                    $subsoleo->innovacion = null;
                }
            $subsoleo->save();

            // Cinceleo
            $cinceleo = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_cinceleo)->execute()->getFirst();
                $cinceleo->realizado = $datos['cinceleo_realizado'];
                if($datos['cinceleo_realizado'] == 1)
                {
                    $cinceleo->id_unidad = ($datos['cinceleo_id_unidad'] == "")?null:$datos['cinceleo_id_unidad'];
                    $cinceleo->cantidad = ($datos['cinceleo_cantidad'] == "")?null:$datos['cinceleo_cantidad'];
                    $cinceleo->precio = ($datos['cinceleo_precio'] == "")?null:$datos['cinceleo_precio'];
                    $cinceleo->importe = ($datos['cinceleo_importe'] == "")?null:$datos['cinceleo_importe'];
                    $cinceleo->fecha_realizacion = ($datos['cinceleo_fecha'] == "")?null:$datos['cinceleo_fecha'];
                    $cinceleo->innovacion = ($datos['cinceleo_innovacion'] == "")?null:$datos['cinceleo_innovacion'];
                }
                else
                {
                    $cinceleo->id_unidad = null;
                    $cinceleo->cantidad = null;
                    $cinceleo->precio = null;
                    $cinceleo->importe = null;
                    $cinceleo->fecha_realizacion = null;
                    $cinceleo->innovacion = ($datos['cinceleo_innovacion'] == "")?null:$datos['cinceleo_innovacion'];
                }
            $cinceleo->save();

            // Barbecho
            $barbecho = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_barbecho)->execute()->getFirst();
                $barbecho->realizado = $datos['barbecho_realizado'];
                if($datos['barbecho_realizado'] == 1)
                {
                    $barbecho->id_unidad = ($datos['barbecho_id_unidad'] == "")?null:$datos['barbecho_id_unidad'];
                    $barbecho->cantidad = ($datos['barbecho_cantidad'] == "")?null:$datos['barbecho_cantidad'];
                    $barbecho->precio = ($datos['barbecho_precio'] == "")?null:$datos['barbecho_precio'];
                    $barbecho->importe = ($datos['barbecho_importe'] == "")?null:$datos['barbecho_importe'];
                    $barbecho->fecha_realizacion = ($datos['barbecho_fecha'] == "")?null:$datos['barbecho_fecha'];
                    $barbecho->innovacion = ($datos['barbecho_innovacion'] == "")?null:$datos['barbecho_innovacion'];
                }
                else
                {
                    $barbecho->id_unidad = null;
                    $barbecho->cantidad = null;
                    $barbecho->precio = null;
                    $barbecho->importe = null;
                    $barbecho->fecha_realizacion = null;
                    $barbecho->innovacion = null; 
                }
            $barbecho->save();

            // Rastra
            $rastra = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_rastra)->execute()->getFirst();
                $rastra->realizado = $datos['rastra_realizado'];
                if($datos['rastra_realizado'] == 1)
                {
                    $rastra->id_unidad = ($datos['rastra_id_unidad'] == "")?null:$datos['rastra_id_unidad'];
                    $rastra->cantidad = ($datos['rastra_cantidad'] == "")?null:$datos['rastra_cantidad'];
                    $rastra->precio = ($datos['rastra_precio'] == "")?null:$datos['rastra_precio'];
                    $rastra->importe = ($datos['rastra_importe'] == "")?null:$datos['rastra_importe'];
                    $rastra->fecha_realizacion = ($datos['rastra_fecha'] == "")?null:$datos['rastra_fecha'];
                    $rastra->innovacion = ($datos['rastra_innovacion'] == "")?null:$datos['rastra_innovacion'];
                }
                else
                {
                    $rastra->id_unidad = null;
                    $rastra->cantidad = null;
                    $rastra->precio = null;
                    $rastra->importe = null;
                    $rastra->fecha_realizacion = null;
                    $rastra->innovacion = null; 
                }
            $rastra->save();

            // Cruza
            $cruza = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_cruza)->execute()->getFirst();
                $cruza->realizado = $datos['cruza_realizado'];
                if($datos['cruza_realizado'] == 1)
                {
                    $cruza->id_unidad = ($datos['cruza_id_unidad'] == "")?null:$datos['cruza_id_unidad'];
                    $cruza->cantidad = ($datos['cruza_cantidad'] == "")?null:$datos['cruza_cantidad'];
                    $cruza->precio = ($datos['cruza_precio'] == "")?null:$datos['cruza_precio'];
                    $cruza->importe = ($datos['cruza_importe'] == "")?null:$datos['cruza_importe'];
                    $cruza->fecha_realizacion = ($datos['cruza_fecha'] == "")?null:$datos['cruza_fecha'];
                    $cruza->innovacion = ($datos['cruza_innovacion'] == "")?null:$datos['cruza_innovacion'];
                }
                else
                {
                    $cruza->id_unidad = null;
                    $cruza->cantidad = null;
                    $cruza->precio = null;
                    $cruza->importe = null;
                    $cruza->fecha_realizacion = null;
                    $cruza->innovacion = null;
                }
            $cruza->save();

            // Laser
            $laser = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_laser)->execute()->getFirst();
                $laser->realizado = $datos['laser_realizado'];
                if($datos['laser_realizado'] == 1)
                {
                    $laser->id_unidad = ($datos['laser_id_unidad'] == "")?null:$datos['laser_id_unidad'];
                    $laser->cantidad = ($datos['laser_cantidad'] == "")?null:$datos['laser_cantidad'];
                    $laser->precio = ($datos['laser_precio'] == "")?null:$datos['laser_precio'];
                    $laser->importe = ($datos['laser_importe'] == "")?null:$datos['laser_importe'];
                    $laser->fecha_realizacion = ($datos['laser_fecha'] == "")?null:$datos['laser_fecha'];
                    $laser->innovacion = ($datos['laser_innovacion'] == "")?null:$datos['laser_innovacion'];
                }
                else
                {
                    $laser->id_unidad = null;
                    $laser->cantidad = null;
                    $laser->precio = null;
                    $laser->importe = null;
                    $laser->fecha_realizacion = null;
                    $laser->innovacion = null;
                }
            $laser->save();
            
            // Landplane
            $landplane = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_landplane)->execute()->getFirst(); 
                $landplane->realizado = $datos['landplane_realizado'];
                if($datos['landplane_realizado'] == 1)
                {
                    $landplane->id_unidad = ($datos['landplane_id_unidad'] == "")?null:$datos['landplane_id_unidad'];
                    $landplane->cantidad = ($datos['landplane_cantidad'] == "")?null:$datos['landplane_cantidad'];
                    $landplane->precio = ($datos['landplane_precio'] == "")?null:$datos['landplane_precio'];
                    $landplane->importe = ($datos['landplane_importe'] == "")?null:$datos['landplane_importe'];
                    $landplane->fecha_realizacion = ($datos['landplane_fecha'] == "")?null:$datos['landplane_fecha'];
                    $landplane->innovacion = ($datos['landplane_innovacion'] == "")?null:$datos['landplane_innovacion'];
                }
                else
                {
                    $landplane->id_unidad = null;
                    $landplane->cantidad = null;
                    $landplane->precio = null;
                    $landplane->importe = null;
                    $landplane->fecha_realizacion = null;
                    $landplane->innovacion = null;
                }
            $landplane->save();
            
        $tabla->save();
        return $tabla->id;
    }

    public static function obtener($relacion)
    {
        $tabla = Doctrine_Query::create()->from('RelacionDiagnostico')->where('id = ?', $relacion)->execute()->getFirst()->PreparacionSueloReal;
        $subsoleo = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_subsoleo)->execute()->getFirst();
        $cinceleo = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_cinceleo)->execute()->getFirst();
        $barbecho = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_barbecho)->execute()->getFirst();
        $rastra = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_rastra)->execute()->getFirst();
        $cruza = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_cruza)->execute()->getFirst();
        $laser = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_laser)->execute()->getFirst();
        $landplane = Doctrine_Query::create()->from('PreparacionSueloConcepto')->where('id = ?', $tabla->id_concepto_landplane)->execute()->getFirst(); 

        $resultados = array();
            $resultados['subsoleo_realizado'] = $subsoleo->realizado;
            $resultados['subsoleo_id_unidad'] = $subsoleo->id_unidad;
            $resultados['subsoleo_cantidad'] = $subsoleo->cantidad;
            $resultados['subsoleo_precio'] = $subsoleo->precio;
            $resultados['subsoleo_importe'] = $subsoleo->importe;
            $resultados['subsoleo_fecha'] = $subsoleo->fecha_realizacion;
            $resultados['subsoleo_innovacion'] = $subsoleo->innovacion;

            $resultados['cinceleo_realizado'] = $cinceleo->realizado;
            $resultados['cinceleo_id_unidad'] = $cinceleo->id_unidad;
            $resultados['cinceleo_cantidad'] = $cinceleo->cantidad;
            $resultados['cinceleo_precio'] = $cinceleo->precio;
            $resultados['cinceleo_importe'] = $cinceleo->importe;
            $resultados['cinceleo_fecha'] = $cinceleo->fecha_realizacion;
            $resultados['cinceleo_innovacion'] = $cinceleo->innovacion;

            $resultados['barbecho_realizado'] = $barbecho->realizado;
            $resultados['barbecho_id_unidad'] = $barbecho->id_unidad;
            $resultados['barbecho_cantidad'] = $barbecho->cantidad;
            $resultados['barbecho_precio'] = $barbecho->precio;
            $resultados['barbecho_importe'] = $barbecho->importe;
            $resultados['barbecho_fecha'] = $barbecho->fecha_realizacion;
            $resultados['barbecho_innovacion'] = $barbecho->innovacion;

            $resultados['rastra_realizado'] = $rastra->realizado;
            $resultados['rastra_id_unidad'] = $rastra->id_unidad;
            $resultados['rastra_cantidad'] = $rastra->cantidad;
            $resultados['rastra_precio'] = $rastra->precio;
            $resultados['rastra_importe'] = $rastra->importe;
            $resultados['rastra_fecha'] = $rastra->fecha_realizacion;
            $resultados['rastra_innovacion'] = $rastra->innovacion;

            $resultados['cruza_realizado'] = $cruza->realizado;
            $resultados['cruza_id_unidad'] = $cruza->id_unidad;
            $resultados['cruza_cantidad'] = $cruza->cantidad;
            $resultados['cruza_precio'] = $cruza->precio;
            $resultados['cruza_importe'] = $cruza->importe;
            $resultados['cruza_fecha'] = $cruza->fecha_realizacion;
            $resultados['cruza_innovacion'] = $cruza->innovacion;

            $resultados['laser_realizado'] = $laser->realizado;
            $resultados['laser_id_unidad'] = $laser->id_unidad;
            $resultados['laser_cantidad'] = $laser->cantidad;
            $resultados['laser_precio'] = $laser->precio;
            $resultados['laser_importe'] = $laser->importe;
            $resultados['laser_fecha'] = $laser->fecha_realizacion;
            $resultados['laser_innovacion'] = $laser->innovacion;

            $resultados['landplane_realizado'] = $landplane->realizado;
            $resultados['landplane_id_unidad'] = $landplane->id_unidad;
            $resultados['landplane_cantidad'] = $landplane->cantidad;
            $resultados['landplane_precio'] = $landplane->precio;
            $resultados['landplane_importe'] = $landplane->importe;
            $resultados['landplane_fecha'] = $landplane->fecha_realizacion;
            $resultados['landplane_innovacion'] = $landplane->innovacion;
        return json_encode($resultados);
    }
}