<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('RentaTierraDiagnostico', 'doctrine');

/**
 * BaseCultivo
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseRentaTierraDiagnostico extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('renta_tierra_diagnostico');

        $this->hasColumn('id', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             ));

        $this->hasColumn('rentado', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'autoincrement' => false,
             'notnull' => false,
             ));

        $this->hasColumn('id_unidad', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'autoincrement' => false,
             'notnull' => false,
             ));

        $this->hasColumn('cantidad', 'float', null, array(
             'type' => 'float',
             'length' => null,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             ));

        $this->hasColumn('precio', 'float', null, array(
             'type' => 'float',
             'length' => null,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             ));

        $this->hasColumn('importe', 'float', null, array(
             'type' => 'float',
             'length' => null,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             ));

        $this->hasColumn('fecha_realizacion', 'date', null, array(
             'type' => 'date',
             'length' => null,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'autoincrement' => false,
             'notnull' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}