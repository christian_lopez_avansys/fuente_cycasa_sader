<?php

/**
 * BaseBanner
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $nombre
 * @property string $imagen
 * @property integer $status
 * 
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseBanner extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('banner');

        $this->hasColumn('nombre', 'string', 50, array(
             'type' => 'string',
             'length' => '50',
             ));

        $this->hasColumn('imagen', 'string', 50, array(
             'type' => 'string',
             'length' => '50',
             ));

        $this->hasColumn('status', 'integer', 1, array(
             'type' => 'integer',
             'length' => '1',
             ));

        $this->option('type', 'INNODB');
        $this->option('collate', 'utf8_general_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}