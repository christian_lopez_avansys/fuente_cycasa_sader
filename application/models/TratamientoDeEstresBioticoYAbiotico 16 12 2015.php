<?php

/**
 * ControlPlagasSuelo
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class TratamientoDeEstresBioticoYAbiotico extends BaseTratamientoDeEstresBioticoYAbiotico
{
	public static function guardar($datos)
	{
		//error_reporting(E_ALL);
		$datos["tipo"] = $datos["tipo"]-1;
		//Primero guardamos los datos de la tabla padre
		//print_r($datos);exit;
		$id=My_Comun::guardar("TratamientoDeEstresBioticoYAbiotico",$datos, NULL, $datos['id']);
		
		//Borramos primero los registros
		$qBorrarInsecticida=Doctrine_Query::create()->delete("TratamientoDeEstresBioticoYAbioticoInsecticida")->where("id_tratamiento_de_estres_biotico_y_abiotico='".$datos['id']."'")->execute();
		//Guardamos los insecticidas
		for($i=3; $i<count($datos['marca_herbicida']);$i++)
		{
			$objInsecticida=new TratamientoDeEstresBioticoYAbioticoInsecticida();
				$objInsecticida->id_tratamiento_de_estres_biotico_y_abiotico=$id;
				$objInsecticida->marca_id=$datos['marca_herbicida'][$i];
				$objInsecticida->fuente_id=$datos['activo_herbicida'][$i];
				$objInsecticida->tipo=$datos['insecticida_tipo'][$i];
				$objInsecticida->unidad_id=$datos['tipo_unidad'][$i];
				$objInsecticida->cantidad=$datos['insecticida_cantidad'][$i];
				$objInsecticida->precio=$datos['insecticida_pu'][$i];
				$objInsecticida->importe=$datos['insecticida_importe'][$i];
				$objInsecticida->fecha=$datos['insecticida_frealizacion'][$i];
				$objInsecticida->etapa_fenologica=$datos['insecticida_etapa_fenologica'][$i];
				$objInsecticida->innovacion=$datos['insecticida_innovacion'][$i];
			$objInsecticida->save();


		}
		return $id;
	}


	public static function obtenerPorDatoGeneral($tipo,$id_general)
	{
		//print_r($id_general);exit;
		$tipo = $tipo-1;
		$string = Doctrine_Query::create()->from("TratamientoDeEstresBioticoYAbiotico")->where("id_dato_general='".$id_general."'")->andWhere('tipo = '. $tipo);
		//print_r($string->getSqlQuery());
		$q=Doctrine_Query::create()->from("TratamientoDeEstresBioticoYAbiotico")->where("id_dato_general='".$id_general."'")->andWhere('tipo = '. $tipo)->execute()->getFirst();
		return $q;
	}
	
	public static function congelarPorDatoGeneral($id_general,$tipo)
	{
		//print_r($id_general);exit;
		$tipo = $tipo-1;
		$t=Doctrine_Query::create()->from("TratamientoDeEstresBioticoYAbiotico")->where("id_dato_general='".$id_general."'")->andWhere('tipo = '. $tipo);
		$dato=$t->execute()->getFirst();
		if(is_object($dato)){
			Doctrine_Query::create()->update("TratamientoDeEstresBioticoYAbiotico")->set("congelado", "2")->where("id_dato_general='".$id_general."'")->andWhere('tipo = '. $tipo)->execute();
			return 1;
		}else{
			return 0;
		}
		
		
	}

}