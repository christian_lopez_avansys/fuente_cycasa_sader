<?php

/**
 * Activo
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class BiodiversidadAnimal extends BaseBiodiversidadAnimal
{
	public static function guardar($datos)
    {
        

        $registro=new BiodiversidadAnimal();
        $registro->fromArray($datos);
        $registro->save();

        $a['especie']=$registro->especie;
        $a['nombre_comun']=$registro->nombre_comun;
        $a['nombre_cientifico']=$registro->nombre_cientifico;
        $a['clasificacion']=$registro->BiodiversidadClasificacion->valor;
        $a['usos']=$registro->BiodiversidadUso->valor;
        $a['interesa_conserve']=($registro->interesa_conserve==1)?"Si":"No";
        $a['comentarios']=$registro->comentarios;

        return $a;

    }

    public static function obtenerRegistros($predio_id)
    {

        //Primero obtenemos el registro del sistema de producción actual a partir del predio
        $qRegistros=Doctrine_Query::create()->from('BiodiversidadAnimal')->where("predio_id='".$predio_id."'");
        $dRegistros=$qRegistros->execute();

        return $dRegistros;

    }

    public static function borrarRegistro($id)
    {

        Doctrine_Query::create()->delete('BiodiversidadAnimal')->where("id = '".$id."'")->execute();

    }
}