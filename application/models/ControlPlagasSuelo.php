<?php

/**
 * ControlPlagasSuelo
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class ControlPlagasSuelo extends BaseControlPlagasSuelo
{
	public static function guardar($datos)
	{
		//print_r($datos);exit;
		//Primero guardamos los datos de la tabla padre
		//print_r($datos);exit;
		$id=My_Comun::guardar("ControlPlagasSuelo",$datos, NULL, $datos['id']);
		//Borramos primero los registros
		$qBorrarInsecticida=Doctrine_Query::create()->delete("ControlPlagasSueloInsecticida")->where("id_control_plagas_suelo='".$datos['id']."'")->execute();

		//Guardamos los insecticidas
		for($i=2; $i<count($datos['marca_herbicida']);$i++)
		{
			$objInsecticida=new ControlPlagasSueloInsecticida();
				$objInsecticida->id_control_plagas_suelo=$id;
				$objInsecticida->empresa_id=$datos['empresa_herbicida'][$i];
				$objInsecticida->marca_id=$datos['marca_herbicida'][$i];
				//$objInsecticida->fuente_id=$datos['activo_herbicida'][$i];
				$objInsecticida->tipo=$datos['insecticida_tipo'][$i];
				$objInsecticida->unidad_id=$datos['tipo_unidad'][$i];
				$objInsecticida->cantidad=$datos['insecticida_cantidad'][$i];
				$objInsecticida->precio=$datos['insecticida_pu'][$i];
				$objInsecticida->importe=$datos['insecticida_importe'][$i];
				$objInsecticida->fecha=$datos['insecticida_frealizacion'][$i];
				$objInsecticida->etapa_fenologica=$datos['insecticida_etapa_fenologica'][$i];
				$objInsecticida->innovacion=$datos['insecticida_innovacion'][$i];
			$objInsecticida->save();


		}
		echo $id;
	}


	public static function obtenerPorDatoGeneral($id_general,$tipo,$pestana=1)
	{
		$q=Doctrine_Query::create()->from("ControlPlagasSuelo")->where("id_dato_general='".$id_general."'")->andWhere('tipo = '. $tipo)->andWhere("pestana='".$pestana."'")->execute()->getFirst();
		//print_r($q->getSQLQuery());

		return $q;
	}
	public static function congelar($datos)
	{
		//print_r($datos);exit;
		$q=Doctrine_Query::create()->update("ControlPlagasSuelo")->set("congelado","2")->where("id='".$datos["id"]."'")->andWhere('tipo = '. $tipo)->execute();
		return $q;
	}

}