<?php

/**
 * Predio
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Predio extends BasePredio
{
	public static function obtenerPorId($id)
  {
 		if($id == null)
     		$elemento = Doctrine_Query::create()->from('Predio')->orderBy('nombre asc')->execute();
     	else
     		$elemento = Doctrine_Query::create()->from('Predio')->where("id = ?", $id)->execute()->getFirst();
     return $elemento;
  }

  public static function obtenerRegiones(){
    $query = Doctrine_Query::create()->from('Predio')->orderBy('CAST(region as UNSIGNED) asc');
    $tipo = Usuario::obtenerTipo();
    
    if(Zend_Auth::getInstance()->hasIdentity()){
      $agroconsultores = Agroconsultor::obtenerUsuariosPorTipoIds($tipo, Zend_Auth::getInstance()->getIdentity()->id);
      switch($tipo){
        case 1:
          break;
        case 2:
        case 3:
          $predios = array();
          $consulta = Doctrine_Query::create()->from('DatoGeneral')->whereIn("id_agroconsultor", $agroconsultores)->execute();
          foreach ($consulta as $registro) {
            $predios[] = $registro->id_predio;
          }

          $query->whereIn('id', $predios);
          break;
      }
    }
    
    $elemento = $query->execute();
    $regiones = array();
    foreach($elemento as $predio) {
      if($predio->region)
        if(!in_array($predio->region, $regiones))
          $regiones[] = $predio->region;
    }

    return $regiones;
  }

  public static function obtenerClaves(){
    $elemento = Doctrine_Query::create()->from('Predio')->orderBy('clave asc')->execute();
    $claves = array();
    foreach($elemento as $predio) {
      if($predio->clave)
        if(!in_array($predio->clave, $claves))
          $claves[] = $predio->clave;
    }

    return $claves;
  }

  public static function obtenerTerritorios(){
    $tipo = Usuario::obtenerTipo();
    $agroconsultores = [];
    if(Zend_Auth::getInstance()->hasIdentity())
      $agroconsultores = Agroconsultor::obtenerUsuariosPorTipoIds($tipo, Zend_Auth::getInstance()->getIdentity()->id);
    
    $elemento = Doctrine_Query::create()->from('Usuario')->orderBy('CAST(territorio_funcional as UNSIGNED) asc');

    switch($tipo){
      case 1:
        break;
      case 2:
        $elemento = $elemento->whereIn('id', $agroconsultores);
      case 3:
        break;
    }

    $elemento = $elemento->execute();
    /*
    $elemento = Doctrine_Query::create()->from('Predio')->orderBy('territorio_funcional asc')->execute();
    $predios = array();
    foreach($elemento as $predio) {
      if($predio->territorio_funcional)
        if(!in_array($predio->territorio_funcional, $territorios))
          $predios[] = $predio->id;
    }

    $elemento = Doctrine_Query::create()->from('DatoGeneral')->orderBy('')->execute();
    */
    $territorios = array();
    
    foreach($elemento as $usuario) {
      if($usuario->territorio_funcional)
        if(!in_array($usuario->territorio_funcional, $territorios))
          $territorios[] = $usuario->territorio_funcional;
    }
    
    return $territorios;
  }

  public static function obtenerTipos(){
    $elemento = Doctrine_Query::create()->from('Predio')->orderBy('tipo_predio asc')->execute();
    $tipos = array();
    foreach($elemento as $predio) {
      if($predio->tipo_predio)
        if(!in_array($predio->tipo_predio, $tipos))
          $tipos[] = $predio->tipo_predio;
    }

    return $tipos;
  }

  public static function obtenerPorProductor($productor)
  {
     	$elementos = Doctrine_Query::create()->from('Predio')->where("id_productor = ?", $productor)->andWhere('estatus = 1')->execute();
     	return $elementos;
  }

  public static function obtenerInfoPorId($id){
    $resultados = array();
	//print_r($id);exit;
    $predio = My_Comun::obtener("Predio", "id", $id);
    if(is_object($predio))
    {
        $resultados['dependencia'] = Dependencia::obtenerPorId($predio->id_dependencia)->nombre;
        $resultados['integradora'] = Integradora::obtenerPorId($predio->id_integradora)->nombre;
        $resultados['organizacion'] = Organizacion::obtenerPorId($predio->id_organizacion)->nombre;
        $resultados['estado'] = Estado::obtenerEstadoPorId($predio->Localidad->id_estado)->nombre;
        $resultados['municipio'] = Municipio::obtenerMunicipioPorClave($predio->Localidad->id_estado, $predio->Localidad->clave_municipio)->nombre;
        $resultados['localidad'] = Localidad::obtenerLocalidadPorId($predio->Localidad->id)->nombre;
        $resultados['potrero'] = $predio->potrero;
        $resultados['superficie'] = $predio->superficie;
    }
    else
      $resultados = null;
    return $resultados;
  }

  public static function guardar($data)
    {       
      
      //Por alguna razón se utilizaba el mismo método para guardar los datos generales y los del predio.
      if(isset($data['curp_productor']))
      {

        $curp = Doctrine_Query::create()->from('Productor')->where("id = ?", $data['id_productor'])->execute()->getFirst(); 

        $data['curp']=$data['curp_productor'];

        $municipio =  explode('|',$data['municipio_id']);
        $data['municipio_id'] = $municipio[1];
        $productor = My_Comun::guardar("Productor", $data, null, $curp['id'], ""); 
        

        return $productor;
      }     
      foreach ($data as $key => $value) {
        if($data[$key] == "")
          unset($data[$key]);
      }

      /*if(isset($data['predio_id'])){        
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        try
        {
          Doctrine_Query::create()->delete('DatoGeneralCultivo')->where("predio_id = ".$data['predio_id'])->execute();
          Doctrine_Query::create()->delete('DatoGeneralVertice')->where("predio_id = ".$data['predio_id'])->execute();
        }
        catch (Exception $e)
        { 
            ### Rollback en caso de algún problema
            $conn->rollBack(); 
            ### Entregamos un mensaje de error
            return $e->getMessage();
        }        
        $conn->commit();
      }*/
      



      $data['nombre'] = $data['nombre_predio'];
      $data['potrero'] = $data['potrero'];
      $data['id_localidad'] = $data['localidad'];

      unset($data['nombre_predio']);

      $predio = My_Comun::guardar("Predio", $data, NULL, $data['predio_id']);
      $rPredio=My_Comun::obtener('Predio','id',$data['predio_id']);
      if($_FILES['kml_predio']['name'] != '' && $_FILES['kml_predio']['name'] != null)
      {
          $adapter = new Zend_File_Transfer_Adapter_Http();
          $adapter->setDestination("../public/kml_predio/");
          $files = $adapter->getFileInfo();

          if(count($files) > 0)
          {
              $extension = pathinfo($_FILES['kml_predio']['name'], PATHINFO_EXTENSION);
              //$nombre = $registro->id."_". date('YmdHis') .'.'.$extension;
              $nombre = $rPredio->id."_kml_predio.".$extension;
              $adapter->addFilter('Rename', array('target'=>"../public/kml_predio/". $nombre, 'overwrite' => true), $_FILES['kml_predio']['name']);
              $adapter->receive();
              $rPredio->kml = "/kml_predio/".$nombre;
              $rPredio->save();
          }
      }
      

      if(!is_numeric($predio)){
        echo "error"; exit;
      }

      $cultivos = array_values($data['cultivos']);
      

      foreach ($data['cultivos'] as $cultivo)
      { 
        if($cultivo['id']==0)
        {
          continue;
        }

          $registro_cultivo = new DatoGeneralCultivo();
          $registro_cultivo->predio_id = $predio;
          $registro_cultivo->tipo = $data['tipo'];
          $registro_cultivo->congelado = "2";
          $registro_cultivo->id_cultivo = ($cultivo['id_cultivo'] == '') ? null : $cultivo['id_cultivo'];
          $registro_cultivo->numero_hectareas = ($cultivo['hectareas'] == '') ? null : $cultivo['hectareas'];
          $registro_cultivo->save();
      }

      
      foreach ($data['vertices'] as $vertice)
      {        
          $registro_vertice = new DatoGeneralVertice();
          $registro_vertice->predio_id = $predio;
          $registro_vertice->tipo = $data['tipo'];
          $registro_vertice->latitud = ($vertice['latitud'] == '') ? null : $vertice['latitud'];
          $registro_vertice->longitud = ($vertice['longitud'] == '') ? null : $vertice['longitud'];
          $registro_vertice->save();
      }      
      
      return $predio;
    }
}