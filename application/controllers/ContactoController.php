<?php

class ContactoController extends Zend_Controller_Action
{

    public function init()
    {
       
    }

    public function indexAction()
    {
    	$this->view->enviado=false;
        if ((int)$this->_getParam('enviado') == 1)
            $this->view->enviado=true;
        /*else
            $this->view->enviado=false;*/
            
    }

    public function enviarAction()
    {
    	### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        //echo "<pre>"; print_r($_POST); exit;

        $msg=$this->view->partial("contacto/mensaje.phtml",array("post"=>$_POST));
   
        $mail = new Zend_Mail();
        $mail->addTo("administracion@gaxsa.com.mx");
        $mail->setSubject("Contacto | ValesFussion");
        $mail->setBodyHtml($msg);
        $mail->setFrom('correo@solucionesoftware.com.mx', 'Contacto');

        $sent = true;
        try 
        {
            $mail->send();
        } 
        catch (Exception $e)
        {
            $sent = false;
        }

        if($sent)
        {
        	header("Location: /contacto/index/enviado/1");
            
        } else {
            header("Location: /contacto/index/enviado/0");
        }
    }

}

