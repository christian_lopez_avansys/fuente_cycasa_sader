<?php

class UsuarioController extends Zend_Controller_Action{

    public function init(){
       //$this->view->headScript()->appendFile('/js/default/usuario.js');
    }

    public function indexAction(){
        $sess=new Zend_Session_Namespace('permisos');
		//$this->view->puedeAgregar=strpos($sess->permisos,"AGREGAR_USUARIO")!==false;
        
    }
	
	public function gridAction(){
		//deshabilitamos la vista
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender(TRUE);
			// en caso de que filtremos get inicio = 1
			if($this->_getParam("inicio") == "1")
            	$_POST["page"] = "1";

			$sess=new Zend_Session_Namespace('permisos');

			$filtro=" 1=1 ";//creamos una variable filtro para elaborar el filtro si se requiere en la consulta
			$nombre=$this->_getParam('nombre');
			if($nombre!=''){
				//se comprueba la cadena por cuestiones de seguridad con la base de datoso
				$nombre=explode(" ", trim($nombre));
				for($i=0; $i<=$nombre[$i]; $i++){
					$nombre[$i]=trim(str_replace(array("'","\"",),array("´","´"),$nombre[$i]));
					if($nombre[$i]!="")
						$filtro.=" AND (nombre LIKE '%".$nombre[$i]."%' || correo_electronico LIKE '".$nombre[$i]."') ";
				}//for
			}//if
			if($this->_getParam('status')!="")
				$filtro.=" AND status='".str_replace("'","´",$this->_getParam('status'))."' ";

			$registros = My_Comun::registrosGrid("Usuario", $filtro);
			$grid=array();
			$i=0;

			$permisos = My_Comun::tienePermiso("PERMISOS_USUARIO");
			$editar = My_Comun::tienePermiso("EDITAR_USUARIO");
			$eliminar = My_Comun::tienePermiso("ELIMINAR_USUARIO");
			//se recorren los registros y se validan permisos. 
			//con esto vamos creando las imagenes del flexigrid y acciones
			foreach($registros['registros'] as $registro){
				$grid[$i]['nombre']=$registro->nombre;
				$grid[$i]['correo']=$registro->correo_electronico;			

				if($registro->status == 0){
					if($permisos)
						$grid[$i]['permisos'] = '<img src="/imagenes/default/flexigrid/si-off.gif" />';
					else
						$grid[$i]['permisos'] = '<img src="/imagenes/default/flexigrid/no-aplica-off.gif" />';

					if($editar)
						$grid[$i]['editar'] = '<img src="/imagenes/default/flexigrid/editar-off.gif" />';
					else
						$grid[$i]['editar'] = '<img src="/imagenes/default/flexigrid/no-aplica-off.gif" />';

					if($eliminar)
						$grid[$i]['eliminar'] = '<span onclick="eliminar('.$registro->id.',false);" title="Habilitar">
													<img style="cursor:pointer;" 
													src="/imagenes/default/flexigrid/eliminar-off.gif" />
												</span>';
					else
						$grid[$i]['eliminar'] = '<img src="/imagenes/default/flexigrid/no-aplica-off.gif" />';
				}else{

					if($permisos)
						$grid[$i]['permisos'] = '<span onclick="permisos('.$registro->id.');" title="Permisos">
													<img style="cursor:pointer;" 
													src="/imagenes/default/flexigrid/si.gif" />
												</span>';
					else
						$grid[$i]['permisos'] = '<img src="/imagenes/default/flexigrid/no-aplica-off.gif" />';

					if($editar)
						$grid[$i]['editar'] = '<span onclick="agregar('.$registro->id.');" title="Editar">
													<img style="cursor:pointer;" 
													src="/imagenes/default/flexigrid/editar.gif" />
												</span>';
					else
						$grid[$i]['editar'] = '<img src="/imagenes/default/flexigrid/no-aplica.gif" />';

					if($eliminar)
						$grid[$i]['eliminar'] = '<span onclick="eliminar('.$registro->id.',true);" title="Eliminar">
													<img style="cursor:pointer;" 
													src="/imagenes/default/flexigrid/eliminar.gif" />
												</span>';
					else
						$grid[$i]['eliminar'] = '<img src="/imagenes/default/flexigrid/no-aplica.gif" />';						
				}
				
				$i++;
			}//foreach
			My_Comun::armarGrid($registros,$grid);
		}//function
		
	public function agregarAction(){
			$this->_helper->layout->disableLayout();
			$this->view->llave = My_Comun::aleatorio(20);
			
			//print_r($_POST);
			//print_r($_POST["id"]);
			if($_POST["id"]=="0"){
			}else{
				$this->view->registro=My_Comun::obtener("Usuario", "id", $_POST["id"]);
			}//if
	}//function

	public function guardarAction(){
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender(TRUE);

			if($_POST["id"] == ""){

				$_POST["permisos"] = "VER_DOCUMENTO";
			}

			if($_POST["usuario_superior_id"]=="")
				unset($_POST["usuario_superior_id"]);

			$_POST["por_proyecto"]=$_POST["por_proyecto"]=='on'?'1':'0';
			
			$bitacora = array();
			$bitacora[0]["modelo"] = "Usuario";
			$bitacora[0]["campo"] = "nombre";
			$bitacora[0]["id"] = $_POST["id"];
			$bitacora[0]["agregar"] = "Agregar usuario";
			$bitacora[0]["editar"] = "Editar usuario";
			
			$usuarioId = My_Comun::Guardar("Usuario", $_POST, $_POST["id"], $bitacora);
			echo($usuarioId);
		}//guardar
		
		public function permisosAction(){
			$this->_helper->layout->disableLayout();
			
			$this->view->registro=My_Comun::obtener('Usuario', "id", $_POST["id"]);
			$this->view->nombre  =$this->view->registro->nombre;
			$this->view->permisos=explode("|",$this->view->registro->permisos);
		}//function
		
		public function guardarpermisosAction(){
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender(TRUE);
			Usuario::guardarPermisos($_POST['permisos'],$_POST['id']);
		}//function

		public function eliminarAction(){
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender(TRUE);
			$regi=My_Comun::obtener("Usuario", "id", $_POST["id"]);
			
			$bitacora = array();
			$bitacora[0]["modelo"] = "Usuario";
			$bitacora[0]["campo"] = "nombre";
			$bitacora[0]["id"] = $_POST["id"];
			$bitacora[0]["deshabilitar"] = "Deshabilitar usuario";
			$bitacora[0]["habilitar"] = "Habilitar usuario";
			
			if($regi["status"]=="1"){
				echo(My_Comun::deshabilitar("Usuario",$_POST["id"],$bitacora));
			}else{
				echo(My_Comun::habilitar("Usuario",$_POST["id"],$bitacora));
			}//if
		}//function

		public function excelAction()
		{
		  ### Deshabilitamos el layout y la vista
		  $this->_helper->layout->disableLayout();
		  $this->_helper->viewRenderer->setNoRender(TRUE);

		  ini_set("display_errors", '1');
		  ini_set("memory_limit","256M");
		  error_reporting(E_ALL);

		  require 'My/PHPExcel/PHPExcel/IOFactory.php';
		  //$inputFileName = '../library/My/PHPExcel/PHPExcel/localidades13.xlsx';
		  $inputFileName = 'importacion.xlsx';

		  //  Read your Excel workbook
		  try {
		      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		      $objPHPExcel = $objReader->load($inputFileName);
		  } catch (Exception $e) {
		      die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
		  }

		  //  Get worksheet dimensions
		  $sheet = $objPHPExcel->getSheet(0);
		  $highestRow = $sheet->getHighestRow();
		  $highestColumn = $sheet->getHighestColumn();
		  $usuarios = array();
		  $dependencia = NULL;
		  for($row = 3; $row <= $highestRow; $row++)
		  {
		      //  Read a row of data into an array
		      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		      if($rowData[0][5] == 'Coordinador')
		        $dependencia = NULL;
		      $contrasena = My_Comun::aleatorio(10);
		      $observaciones = $rowData[0][12] ? true : false;
		      $usuario = new Usuario();
		      $usuario->territorio_funcional = $rowData[0][2];
		      $usuario->nombre = utf8_encode($rowData[0][6]);
		      $usuario->curp = $rowData[0][7];
		      $usuario->sexo = $rowData[0][8];
		      $usuario->correo_electronico = $rowData[0][9];
		      $usuario->correo_electronico_2 = $rowData[0][10];
		      $usuario->telefono = $rowData[0][11];
		      $usuario->contrasena = $contrasena;
		      $usuario->permisos = 'VER_BACKEND|VER_ACTIVO|VER_USUARIO|VER_BITACORA|VER_DIAGNOSTICOS|VER_DEPENDENCIA|VER_AGROCONSULTOR|VER_CULTIVO|VER_FUENTE|VER_INTEGRADORA|VER_MARCA_SEMILLA|VER_ORGANIZACION|VER_TIPOCULTIVO|VER_ESTADO|VER_MUNICIPIO|VER_LOCALIDAD|VER_UNIDAD|VER_PRODUCTOR|VER_PREDIO|VER_ETAPA_FENOLOGICA|VER_CULTIVO_ETAPA_FENOLOGICA|VER_TECNICAS|VER_USO';
		      $usuario->status = 1;
		      $usuario->tipo = 1;
		      $usuario->dependencia_id = $dependencia;
		      $usuario->save();
		      $correos = array(
		        'estrada.aguilar.ja@gmail.com',
		        'fx_clbaileyi02@msn.com'
		      );

		      $usuarios[] = array(
		        'email' => $usuario->correo_electronico,
		        'nombre' => utf8_encode($usuario->nombre),
		        'contrasena' => $usuario->contrasena,
		        'enviar' => $observaciones ? array($correos) : array($usuario->correo_electronico) 
		      );

		      if($rowData[0][5] == 'Coordinador')
		        $dependencia = $usuario->id;
		  }

		  file_put_contents('usuarios.txt', json_encode($usuarios));
		  //mysqli_close($link);
		  //echo json_encode($archivo);
		}

		public function enviarCorreosAction()
		{
			set_time_limit(600);
			ini_set("memory_limit","256M");

			### Deshabilitamos el layout y la vista
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNoRender(TRUE);
			$usuarios_txt = file_get_contents('usuarios.txt');
			$usuarios = json_decode($usuarios_txt);
			$correos_copia = 'cycasasc.admos@gmail.com,estrada.aguilar.ja@gmail.com';
			foreach ($usuarios as $usuario) {
			$cuerpo = 
			'<div>Usuario : '.$usuario->email.'</div>
			<div>Nombre : '.$usuario->nombre.'</div>
			<div>Contraseña : '.$usuario->contrasena.'</div>';
			My_Comun::correo('Cuenta creada', 'Su cuenta ha sido creada', $cuerpo, "recuperar@appgricola.com", "contacto@appgricola.com", $usuario->enviar, '', $correos_copia);
		  }
		}
}