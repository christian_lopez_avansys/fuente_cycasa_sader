<?php

class UbicacionController extends Zend_Controller_Action
{

    public function init()
    {
       
    }

    public function indexAction()
    {
       	$estaciones=array();
  		//Obtenemos las ciudades distintas en la que hay estaciones
  		$ciudades=Ubicacion::obtenerCiudadesConEstacionDeServicio();
  		foreach($ciudades as $ciudad)
  		{
  			$ubicaciones=Ubicacion::obtenerEstacionesDeCiudad($ciudad->municipio_id);
  			$estaciones[$ciudad->Municipio->nombre]=$ubicaciones;
  		}
  		$this->view->estaciones=$estaciones;
    }


}

