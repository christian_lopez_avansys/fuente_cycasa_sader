<?php

class Backend_PredioController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/predio.js');
    }

    public function indexAction()
    {
		$this->view->titulo = "Catalogos";
		$this->view->subtitulo = "Predios";
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		if($this->_getParam('filtro') != '' && $this->_getParam('filtro') != null) $filtros .= " AND nombre LIKE '%". $this->_getParam('filtro') ."%'";

		$registros = My_Comun::registrosGrid("Predio", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	

		if($this->_getParam('id') > 0) $this->view->registro = Predio::obtenerPorId($this->_getParam('id'));
		$this->view->dependencias = Dependencia::obtenerPorId(null);
		$this->view->integradoras = Integradora::obtenerPorId(null);
		$this->view->organizaciones = Organizacion::obtenerPorId(null);
		$this->view->estados = Estado::obtenerPorId(null);
		$this->view->productores = Productor::obtenerPorId(null);
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Predio", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Predio", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Predio", $_POST, "nombre", $_POST['id'], "");
	}

	public function obtenermunicipiosAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo Municipio::obtenerPorEstado($this->_getParam('estado'));
	}

	public function obtenerlocalidadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo Localidad::obtenerPorMunicipio($this->_getParam('municipio'));
	}

	public function obtenerinformacionAction()
	{
		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
       //	echo "<pre>"; print_r($_POST["id"]); exit;

		$resultados = Predio::obtenerInfoPorId($_POST['id']);
		if(!is_array($resultados))
		{
			$resultados = array();
            $resultados['noexiste'] = 'PREDIO NO ENCONTRADO';
        }
        echo json_encode($resultados);			
	}
}

?>