<?php
class Backend_LoginController extends Zend_Controller_Action
{
	
	public function init()
    {
        $this->_helper->layout()->setLayout('login');
		$this->view->headScript()->appendFile('/js/default/login.js');
		//$this->view->headScript()->appendFile('/js/comun.js');
    }

    public function indexAction()
	{
		$this->_helper->viewRenderer->setNoRender(TRUE);
        header("Location: /site");
	}
	
	public function ingresarAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
						//$this->_helper->layout->setLayout('default');
                   		//header("Location: /backend/admin");
        $adaptador = new ZC_Auth_Adapter($_POST['correo_electronico'], $_POST['contrasena']);
		
        $resultado = Zend_Auth::getInstance()->authenticate($adaptador);
		
		if(Zend_Auth::getInstance()->hasIdentity()){

            Bitacora::guardar("0", "Ingreso", "Ingreso al sistema", ""); 
            
            $sess = new Zend_Session_Namespace('permisos');
            $sess->permisos = Zend_Auth::getInstance()->getIdentity()->permisos;

            echo Zend_Auth::getInstance()->getIdentity()->id;
			header("location: ../../backend");
        }
        else{

            $usuario = $resultado->getIdentity();
            $mensajes = $resultado->getMessages();
            
            echo $usuario->id.'|'.$mensajes[0];
        }

    }
	
	public function salirAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        //Bitacora::guardar("0", "Salir", "Sali� del sistema"); 

        Zend_Auth::getInstance()->clearIdentity();
        
        header("Location: /site");
    }

    public function recuperaAction(){
    	//se ejecuta el controlador para obtener la vista,
		// y por javascript se hace un submit para ejecutar recuperarAction
        $this->_helper->layout->disableLayout();		
    }

    public function recuperarAction(){

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        	
        $usuario = My_Comun::obtener('Usuario', 'correo_electronico', $_POST['correo_electronico'], ' and status = 1');

		$asunto = "Recuperar contrase�a";
        $titulo = "Recuperar contrase&ntilde;a";		
        $cuerpo = "
            Usuario:&nbsp;".$usuario->correo_electronico."
            <br />
            Contrase&ntilde;a:&nbsp;".$usuario->contrasena."
        ";

		//echo $cuerpo; 
		
        echo My_Comun::correo($asunto, $titulo, $cuerpo, "contacto@appgricola.com", "", $usuario->correo_electronico, $usuario->nombre,"", "");
    }
}