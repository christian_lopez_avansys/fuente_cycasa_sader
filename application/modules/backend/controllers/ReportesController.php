<?php

class Backend_ReportesController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/default/diagnostico.js');
		$this->view->headScript()->appendFile('/js/reportes/predioreporte.js');
    }

    public function indexAction()
    {
	  	### Deshabilitamos el layout y la vista
      	//$this->_helper->layout->disableLayout();
	  	$this->view->titulo="Reportes";
	  	$this->view->subtitulo="";
	  
	  	$this->view->dependencias = Dependencia::obtenerDependenciasActivas();
		$this->view->estados = Municipio::obtenerEstadosActivos();
	  	$this->view->integradoras = Integradora::obtenerIntegradorasActivas();
	  	$this->view->claves = Predio::obtenerClaves();
	  	$this->view->tipos_predios = Predio::obtenerTipos();
	  	$this->view->territorios = Predio::obtenerTerritorios();
	  	$this->view->regiones = Predio::obtenerRegiones();
		
		
	  	$this->view->organizaciones = Organizacion::obtenerOrganizacionesActivas();
	  	$this->view->id_dato_general=$this->_getParam('id');
	  	$this->view->tipo_usuario = Usuario::obtenerTipo();
		if(Zend_Auth::getInstance()->hasIdentity()){
			//$this->view->tipo=Agroconsultor::obtenerUsuarioPorId(Zend_Auth::getInstance()->getIdentity()->id)->tipo;
			
			//if($this->view->tipo==1){
				//$this->view->agroconsultor = Agroconsultor::obtenerUsuarioHtml();

			//}
			$this->view->agroconsultor = Agroconsultor::obtenerUsuariosPorTipo($this->view->tipo_usuario, Zend_Auth::getInstance()->getIdentity()->id);
		}
		$this->view->Productor = Productor::obtenerProductores();
	  	//$this->view->tipo_usuario = 3;
	  	//$this->view->municipios = Municipio::obtenerMunicipiosActivos();
      	//$this->view->localidades = Localidad::obtenerLocalidadesActivas();   
    }
	
	public function reporteTabla($tabla_totales, $encabezados)
	{
		$conceptos = array(
			'nutricion' => 'Nutrición',
			'plantas_no_deseadas' => 'Plantas no deseadas',
			'plagas_suelo' => 'Plagas suelo',
			'plagas_foliares' => 'Plagas foliares',
			'control_enfermedades' => 'Control de enfermedades',
			'inductores' => 'Inductores',
			'totales' =>'Totales'
		);

		$html = '<table id="tabla_reporte_predios" width="100%" border="0" class="table table-bordered" style="font-size: 12px">';
		$html .= '<tr>';
			$html .= '<th scope="col" class="col-xs-2 text-center text-14">';
			$html .= '&nbsp;';
			$html .= '</th>';
		foreach($encabezados as $encabezado) {
			$html .= '<th>';
			$html .= $encabezado;
			$html .= '</th>';
		}
		$html .= '</tr>';
		foreach($conceptos as $concepto => $nombre) {
			$html .= '<tr>';
			$html .= '<td>';
			$html .= $nombre;
			$html .= '</td>';
			foreach($encabezados as $encabezado) {
				$html .= '<td align="center">';
				$html .= $tabla_totales[$concepto][$encabezado];
				$html .= '</td>';
			}
			$html .= '</tr>';
		}
		$html .= '</table>';
        return $html;
	}

	public function reportequeryAction(){
		$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
		//error_reporting(E_ERROR);
		$tipo = 1;
		$filtro = " DatoGeneral.estatus = 1 ";
		
		if($this->_getParam('agroconsultor'))			
            $filtro.=" AND DatoGeneral.id_agroconsultor='".$this->_getParam('agroconsultor')."' ";
        if($this->_getParam('productor'))			
            $filtro.=" AND DatoGeneral.Predio.id_productor='".$this->_getParam('productor')."' ";
		if($this->_getParam('estado'))			
            $filtro.=" AND DatoGeneral.Predio.Localidad.id_estado='".$this->_getParam('estado')."' ";
        if($this->_getParam('clave')!="")			
            $filtro.=" AND DatoGeneral.Predio.clave='".str_replace("'","�",$this->_getParam('clave'))."' ";
        if($this->_getParam('region')!="")			
            $filtro.=" AND DatoGeneral.Predio.region='".str_replace("'","�",$this->_getParam('region'))."' ";
	   	if($this->_getParam('territorio')!="")
            $filtro.=" AND DatoGeneral.Usuario.territorio_funcional='".str_replace("'","�",$this->_getParam('territorio'))."' ";
	   	if($this->_getParam('tipo_predio')!="")			
            $filtro.=" AND DatoGeneral.Predio.tipo_predio='".str_replace("'","�",$this->_getParam('tipo_predio'))."' ";
		if($this->_getParam('municipio')){
		   $clave = substr($this->_getParam('municipio'),strpos($this->_getParam('municipio'),"|")+1,strlen($this->_getParam('municipio')));		
		   $filtro.=" AND DatoGeneral.Predio.Localidad.clave_municipio='".str_replace("'","�",$clave)."'";			
		}
		if($this->_getParam('localidad')){
		   $filtro.=" AND DatoGeneral.Predio.id_localidad='".str_replace("'","�",$this->_getParam('localidad'))."' ";
		}
		$regDatoGeneral = Doctrine_Query::create()->from('DatoGeneral')
												->where($filtro)
												//->where('id = 407')
												->execute();
		$totales = array(
			'nutricion' => array(),
			'plantas_no_deseadas' => array(),
			'plagas_suelo' => array(),
			'plagas_foliares' => array(),
			'control_enfermedades' => array(),
			'inductores' => array(),
			'totales' => array()
		);

		$conceptos = array(
			'nutricion' => 'Nutrición',
			'plantas_no_deseadas' => 'Plantas no deseadas',
			'plagas_suelo' => 'Plagas suelo',
			'plagas_foliares' => 'Plagas foliares',
			'control_enfermedades' => 'Control de enfermedades',
			'inductores' => 'Inductores',
			'totales' =>'Totales'
		);
		$ids_coninfo = array();
		foreach($regDatoGeneral as $datoGeneral){
			//Nutricion
			$nutricion = Fertilizacion::obtener($tipo, $datoGeneral->id);
			$nutricion = json_decode($nutricion);
			if(isset($nutricion->ok)){
				$nutriciones = array();
				$nutriciones[] = $nutricion->antes;
				$nutriciones[] = $nutricion->enla;
				$nutriciones[] = $nutricion->durante;
				$nutriciones[] = $nutricion->rantes;
				$nutriciones[] = $nutricion->renla;
				$nutriciones[] = $nutricion->rdurante;
				foreach($nutriciones as $nutricion_ind) {
					if(isset($nutricion_ind->concepto)){
						foreach($nutricion_ind->concepto as $concepto) {
							if(!isset($totales['nutricion'][$concepto->empresa_id]))
								$totales['nutricion'][$concepto->empresa_id] = 0;
							$totales['nutricion'][$concepto->empresa_id]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}
					}

					if(isset($nutricion_ind->quimica)){
						foreach($nutricion_ind->quimica as $concepto) {
							if(!isset($totales['nutricion'][$concepto->empresa_id]))
								$totales['nutricion'][$concepto->empresa_id] = 0;
							$totales['nutricion'][$concepto->empresa_id]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}					
					}

					if(isset($nutricion_ind->organica)){
						foreach($nutricion_ind->organica as $concepto) {
							if(!isset($totales['nutricion'][$concepto->empresa_id]))
								$totales['nutricion'][$concepto->empresa_id] = 0;
							$totales['nutricion'][$concepto->empresa_id]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}					
					}

					if(isset($nutricion_ind->mineral)){
						foreach($nutricion_ind->mineral as $concepto) {
							if(!isset($totales['nutricion'][$concepto->empresa_id]))
								$totales['nutricion'][$concepto->empresa_id] = 0;
							$totales['nutricion'][$concepto->empresa_id]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}					
					}

					if(isset($nutricion_ind->acondicionador)){
						foreach($nutricion_ind->acondicionador as $concepto) {
							if(!isset($totales['nutricion'][$concepto->empresa_id]))
								$totales['nutricion'][$concepto->empresa_id] = 0;
							$totales['nutricion'][$concepto->empresa_id]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}					
					}
				}
			}
			//Control de maleza
			$plantas_no_deseadas = ControlDeMaleza::obtener($tipo, $datoGeneral->id);
			$plantas_no_deseadas = json_decode($plantas_no_deseadas);
			if(isset($plantas_no_deseadas->ok)){
				if(isset($plantas_no_deseadas->general->pre)){
					foreach($plantas_no_deseadas->general->pre as $concepto) {
						if(!isset($totales['plantas_no_deseadas'][$concepto->empresa_id]))
							$totales['plantas_no_deseadas'][$concepto->empresa_id] = 0;
						$totales['plantas_no_deseadas'][$concepto->empresa_id]++;
						if(!in_array($datoGeneral->id, $ids_coninfo))
							$ids_coninfo[] = $datoGeneral->id;
					}
				}

				if(isset($plantas_no_deseadas->general->post)){
					foreach($plantas_no_deseadas->general->post as $concepto) {
						if(!isset($totales['plantas_no_deseadas'][$concepto->empresa_id]))
							$totales['plantas_no_deseadas'][$concepto->empresa_id] = 0;
						$totales['plantas_no_deseadas'][$concepto->empresa_id]++;
						if(!in_array($datoGeneral->id, $ids_coninfo))
							$ids_coninfo[] = $datoGeneral->id;
					}
				}

				if(isset($plantas_no_deseadas->general->des)){
					foreach($plantas_no_deseadas->general->des as $concepto) {
						if(!isset($totales['plantas_no_deseadas'][$concepto->empresa_id]))
							$totales['plantas_no_deseadas'][$concepto->empresa_id] = 0;
						$totales['plantas_no_deseadas'][$concepto->empresa_id]++;
						if(!in_array($datoGeneral->id, $ids_coninfo))
							$ids_coninfo[] = $datoGeneral->id;
					}
				}
			}

			//Plagas suelo
			$plagas_suelo = array();
			if($plagasuelo = ControlPlagasSuelo::obtenerPorDatoGeneral($datoGeneral->id, $tipo, 1))
				$plagas_suelo = ControlPlagasSueloInsecticida::obtenerPorTipo($plagasuelo->id, "GRANULADO");
			
			if($plagas_suelo){
				foreach($plagas_suelo as $concepto) {
					if(!isset($totales['plagas_suelo'][$concepto->empresa_id]))
						$totales['plagas_suelo'][$concepto->empresa_id] = 0;
					$totales['plagas_suelo'][$concepto->empresa_id]++;
					if(!in_array($datoGeneral->id, $ids_coninfo))
						$ids_coninfo[] = $datoGeneral->id;
				}
			}
			//COntrol de enfermadades
			$control_enfermedades = array();
			if($control_enfermedad = ControlPlagasSuelo::obtenerPorDatoGeneral($datoGeneral->id, $tipo, 2)){
				$control_enfermedades = ControlPlagasSueloInsecticida::obtenerPorTipo($control_enfermedad->id, "GRANULADO");
			}
			
			if($control_enfermedades){
				foreach($control_enfermedades as $concepto) {
					if(!isset($totales['control_enfermedades'][$concepto->empresa_id]))
						$totales['control_enfermedades'][$concepto->empresa_id] = 0;
					$totales['control_enfermedades'][$concepto->empresa_id]++;
					if(!in_array($datoGeneral->id, $ids_coninfo))
						$ids_coninfo[] = $datoGeneral->id;
				}
			}

			//Plagas foliares
			$plagas_foliares = ControlPlagasFoliares::obtenerPorDatoGeneral($datoGeneral->id, $tipo);
			if($plagas_foliares){
				foreach($plagas_foliares as $concepto) {
					if(isset($concepto['dinamicos'])){
						foreach($concepto['dinamicos'] as $dinamico) {
							if(!isset($totales['plagas_foliares'][$dinamico['empresa_id']]))
								$totales['plagas_foliares'][$dinamico['empresa_id']] = 0;
							$totales['plagas_foliares'][$dinamico['empresa_id']]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}			
					}
				}
			}

			//Inductores
			$inductores = Doctrine_Query::create()->from("TratamientoDeEstresBioticoYAbiotico")->where("id_dato_general='".$datoGeneral->id."'")->andWhere('tipo = 0')->execute();
			if($inductores) {
				foreach($inductores as $inductor) {
					$inductores_g = TratamientoDeEstresBioticoYAbioticoInsecticida::obtenerPorTipo($inductor->id,"GRANULADO");
					$inductores_t = TratamientoDeEstresBioticoYAbioticoInsecticida::obtenerPorTipo($inductor->id,"TRATAMIENTO");
					$inductores_r = TratamientoDeEstresBioticoYAbioticoInsecticida::obtenerPorTipo($inductor->id,"RECARGA");
					//GRANULADO = RESISTENCIA = 1
					if($inductores_g){
						foreach($inductores_g as $inductor_ind) {
							if(!isset($totales['inductores'][1]))
								$totales['inductores'][1] = 0;
							$totales['inductores'][1]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}
					}
					//TRATAMIENTO = CRECIMIENTO = 2
					if($inductores_t){
						foreach($inductores_t as $inductor_ind) {
							if(!isset($totales['inductores'][2]))
								$totales['inductores'][2] = 0;
							$totales['inductores'][2]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}
					}
					//RECARGA = PRODUCTIVIDAD = 3
					if($inductores_r){
						foreach($inductores_r as $inductor_ind) {
							if(!isset($totales['inductores'][3]))
								$totales['inductores'][3] = 0;
							$totales['inductores'][3]++;
							if(!in_array($datoGeneral->id, $ids_coninfo))
								$ids_coninfo[] = $datoGeneral->id;
						}
					}
				}
			}
		}

		$encabezados = array('QUÍMICO', 'BIOLÓGICO', 'ORGÁNICO', 'MINERAL', 'HORMONAL', 'OTROS', 'INDUCTORES RESISTENCIA', 'INDUCTORES CRECIMIENTO', 'INDUCTORES PRODUCTIVIDAD');
		$tabla_totales = array();
		foreach($totales as $clave => $clave_obj) {
			foreach($clave_obj as $empresa => $cont) {
				if($clave == 'inductores'){
					$opcion = new stdClass();
					switch($empresa) {
						case 1:
							$opcion->valor = 'INDUCTORES RESISTENCIA';
							break;

						case 2:
							$opcion->valor = 'INDUCTORES CRECIMIENTO';
							break;

						case 3:
							$opcion->valor = 'INDUCTORES PRODUCTIVIDAD';
							break;
					}
				}
				else	
					$opcion = Doctrine_Query::create()->from('CatalogoDetalle')->where("id='".$empresa."'")->execute()->getFirst();
				if($opcion){
					if(!in_array($opcion->valor, $encabezados))
						$encabezados[] = $opcion->valor;
					
					$tabla_totales[$clave][$opcion->valor] = $cont;

					if(!isset($tabla_totales['totales'][$opcion->valor]))
						$tabla_totales['totales'][$opcion->valor] = 0;

					$tabla_totales['totales'][$opcion->valor] += $cont;
				}
			}
		}

		$tabla_totales_js = array();
		$tabla_totales_uni_js = array();
		$fila = array();
		$fila[] = 'Totales';
		$file_js = array();
		$fila_js[] = '';
		//echo json_encode($encabezados);
		foreach($encabezados as $encabezado) {
			$fila[] = isset($tabla_totales['totales'][$encabezado]) ? $tabla_totales['totales'][$encabezado] : 0;
			$fila_js[] = $encabezado;
		}
		$tabla_totales_js[] = $fila_js;
		$tabla_totales_uni_js[] = $fila_js;
		$tabla_totales_uni_js[] = $fila;

		//$fila[] = '';

		foreach($tabla_totales as $tipo => $totales) {
			$fila = array();
			if($tipo == 'totales')
				continue;
			
			$fila[] = $conceptos[$tipo];
			foreach($encabezados as $encabezado) {
				$fila[] = isset($totales[$encabezado]) ? $totales[$encabezado] : 0;
			}
			//$fila[] = '';
			$tabla_totales_js[] = $fila;
		}

		$html = $this->reporteTabla($tabla_totales, $encabezados);

		$totales_js_chart = array();
		$totales_js_chart[] = array('Concepto', 'Cantidad');
		for($i=1; $i < count($tabla_totales_uni_js[0]); $i++) { 
			$arreglo_temp = array();
			$arreglo_temp[] = $tabla_totales_uni_js[0][$i];
			$arreglo_temp[] = $tabla_totales_uni_js[1][$i];
			$totales_js_chart[] = $arreglo_temp;
		}

		$respuesta = array(
			'ids_con_info' => $ids_coninfo,
			'tabla_html' => $html,
			'tabla_js' => $tabla_totales_js,
			'tabla_object' => $tabla_totales,
			'tabla_encabezado' => $encabezados,
			'tabla_totales_js' => $totales_js_chart
		);

		echo json_encode($respuesta);
	}
}
?>