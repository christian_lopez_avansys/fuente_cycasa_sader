<?php

class Backend_LocalidadController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/localidad.js');
    }

    public function indexAction()
    {
		$this->view->titulo = "Catalogos";
		$this->view->subtitulo = "Localidades";
		$this->view->estados = Localidad::obtenerEstadosActivos();
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		$nombre = $this->_getParam('nombre');
		if($nombre != null && $nombre != '') 
			if(strpos($nombre, 'Á') !== false) 
				$nombre = str_replace("Á", '**A**', $nombre);
		if($nombre != null && $nombre != '') 
			if(strpos($nombre, 'ü') !== false) 
				$nombre = str_replace("ü", '**u**', $nombre);

		if($nombre != '' && $nombre != null) $filtros .= " AND nombre LIKE '%". $nombre ."%'";
		if($this->_getParam('estado') != '' && $this->_getParam('estado') != null && $this->_getParam('estado') > 0) $filtros .= " AND id_estado = ". $this->_getParam('estado');
		if($this->_getParam('municipio') != '' && $this->_getParam('municipio') != null && $this->_getParam('municipio') > 0) $filtros .= " AND clave_municipio = ". $this->_getParam('municipio');

		$registros = My_Comun::registrosGrid("Localidad", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = My_Comun::reformarString($registro->nombre);
      		$grid[$i]['estado'] = utf8_decode(Localidad::obtenerMunicipioPorLocalidad($registro->id_estado, $registro->clave_municipio)->Estado->nombre);
      		$grid[$i]['municipio'] = Localidad::obtenerMunicipioPorLocalidad($registro->id_estado, $registro->clave_municipio)->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	
		if($this->_getParam('id') > 0)
		{
			$this->view->registro = Localidad::obtenerLocalidadPorId($this->_getParam('id'));
			$this->view->municipios = Localidad::obtenerMunicipiosActivos($this->view->registro->id_estado);
		}
		$this->view->estados = Localidad::obtenerEstadosActivos();
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Localidad", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Localidad", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Localidad", $_POST, "nombre", $_POST['id'], "");
	}

	public function obtenermunicipiosAction()
	{
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$datos = '<option value="0">Seleccione un municipio</option>';
		$municipios = Localidad::obtenerMunicipiosActivos($this->_getParam('estado'));
		if(count($municipios) > 0)
		{
			foreach ($municipios as $municipio) {
            	$datos .= '<option value="'. $municipio->clave .'">'. $municipio->nombre .'</option>';
            }
		}
		echo $datos;
	}
}

?>