<?php

class Backend_MarcaSemillaController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/marcasemilla.js');
    }

    public function indexAction()
    {
		
		$this->view->titulo = "Catalogos";
		$this->view->subtitulo = "Marcas de semillas";
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		try{
			
		$filtros = "1=1";

		if($this->_getParam('filtro') != '' && $this->_getParam('filtro') != null) $filtros .= " AND nombre LIKE '%". $this->_getParam('filtro') ."%'";

		$registros = My_Comun::registrosGrid("Marca", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
		}catch(Exception $e){
			print_r($e->getMessage());
		}
    }

	public function agregarAction()
	{	

		if($this->_getParam('id') > 0) 
			$this->view->registro = Marca::obtenerPorId("",$this->_getParam('id'));
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Marca", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Marca", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Marca", $_POST, "nombre", $_POST['id'], "");
	}
}

?>