<?php
class Backend_IndexController extends Zend_Controller_Action
{
	
	public function init()
  {
    //$this->_helper->viewRenderer->setNoRender(TRUE);
  }

  public function indexAction()
	{
    //header("Location: /backend/diagnosticos");
    $this->_helper->layout->disableLayout();
    $this->view->estados = Estado::obtenerEstados();
    $this->view->productores = Productor::obtenerProductores();
    $this->view->predio_id=$this->_getParam('predio');
    

  }

  public function municipiosAction()
  {
     ### Deshabilitamos el layout y la vista
     $this->_helper->layout->disableLayout();
     $this->_helper->viewRenderer->setNoRender(TRUE);
     
     My_Comun::comboMunicipio($this->_getParam('id'));
  }

  public function localidadesAction()
  {
     ### Deshabilitamos el layout y la vista
     $this->_helper->layout->disableLayout();
     $this->_helper->viewRenderer->setNoRender(TRUE);



     $data = explode("|", $this->_getParam('id'));
     
     My_Comun::comboLocalidad($data[0], $data[1]);
  }

  public function curpAction()
  {
    ### Deshabilitamos el layout y la vista
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);

    $producto=Productor::obtenerPorCurp($this->_getParam('curp'));
    $resp=array();

    if(is_object($producto))
    {
      $resp['data']=true;
      $resp['apellido_paterno'] = $producto->apellido_paterno;
      $resp['apellido_materno'] = $producto->apellido_materno;
      $resp['nombre'] = $producto->nombre;
      $resp['genero'] = $producto->genero;
      $resp['estado_id'] = $producto->estado_id;
      $resp['municipio_id'] = $producto->municipio_id;
      $resp['localidad_id'] = $producto->localidad_id;
      $resp['ejido'] = $producto->ejido;
      $resp['pertenece_etnia'] = $producto->pertenece_etnia;
      $resp['etnia_id'] = $producto->etnia_id;
      $resp['sociedad_agricola'] = $producto->sociedad_agricola;
      $resp['sociedad_social'] = $producto->sociedad_social;
      $resp['sociedad_religiosa'] = $producto->sociedad_religiosa;
      $resp['persona_juridica_id'] = $producto->persona_juridica_id;
    }
    else
    {
      $resp['data']=false;
    }

    header('Content-Type: application/json');
    echo json_encode($resp);
  }

  public function guardarAction()
  {
    $productor = Productor::guardar($_POST);
    echo $productor;  exit;    

  }

  public function guardarpoligonoAction()
  {
    ### Deshabilitamos el layout y la vista
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(TRUE);

    //Borramos los vértices que existan previamiente
    $qBorrar=Doctrine_Query::create()->delete('DatoGeneralVertice')->where("predio_id='".$_POST['predio']."'")->execute();
    //Decodificamos el json de vértices
    $vertices=json_decode($_POST['vertices'], true);

    //Guardamos los vértices
    foreach ($vertices as $vertice)
    {        
        $registro_vertice = new DatoGeneralVertice();
        $registro_vertice->predio_id = $_POST['predio'];
        $registro_vertice->tipo = 0;
        $registro_vertice->latitud = $vertice['lat'];
        $registro_vertice->longitud = $vertice['lng'];
        $registro_vertice->save();
    }
    $resp=array();
    //Extraemos el dato id_dato_general
    $dPredio=Doctrine_Query::create()->from('Predio')->where("id='".$_POST['predio']."'")->execute()->getFirst();
    $resp['id_productor']=$dPredio->id_productor;
    $resp['id_predio']=$dPredio->id;


    header('Content-Type: application/json');
    echo json_encode($resp);
  }


}