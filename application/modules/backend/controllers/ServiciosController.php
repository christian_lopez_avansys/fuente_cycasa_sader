<?php

class Backend_ServiciosController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
    }

    public function obtenersubvariablesAction()
    {
      $resultado = array();
      $q=Doctrine_Query::create()->from('Variable')->where("parametro='".$this->_getParam('variable')."'");
      //echo $q->getSqlQuery();
      $d=$q->execute()->getFirst();
      /*$i=0;
      foreach($d as $variable)
      {
        $resultado['subvariable'][$i]['variable'] = strtolower($this->_getParam('variable'));
        $resultado['subvariable'][$i]['subvariable'] =  (isset($_POST['nombre'])) ? strtolower($this->_getParam('nombre')) : null;
        $resultado['subvariable'][$i]['valor'] = str_replace(" ", "", $variable->nombre);
        $resultado['subvariable'][$i]['color'] = $variable->color;
        $resultado['subvariable'][$i]['archivo'] = $variable->shp;
        $i++;
      }*/
      try 
      {
        
        $conexion=pg_connect("host=localhost dbname=avansys_democycasa user=appgricola password=A6b01uh0");
        $consulta = pg_exec($conexion, "SELECT nombre, color, archivo FROM shape WHERE shape_tipo_id = '". $d->id ."' ORDER BY archivo ASC");
        $i = 0;
        while ($variable = pg_fetch_array($consulta, NULL, PGSQL_ASSOC)){
          $resultado['subvariable'][$i]['variable'] = strtolower($this->_getParam('variable'));
          $resultado['subvariable'][$i]['subvariable'] =  (isset($_POST['nombre'])) ? strtolower($this->_getParam('nombre')) : null;
          $resultado['subvariable'][$i]['valor'] = str_replace(" ", "", $variable['nombre']);
          $resultado['subvariable'][$i]['color'] = $variable['color'];
          $resultado['subvariable'][$i]['archivo'] = $variable['archivo'];
          $i++;
        }
      } catch (Exception $e) {
        $resultado['error'] = "No se pudo obtener la informacion";
      }
      echo json_encode($resultado);
    }

    public function excelAction()
    {
      ### Deshabilitamos el layout y la vista
      $this->_helper->layout->disableLayout();
      $this->_helper->viewRenderer->setNoRender(TRUE);

      ini_set("display_errors", '1');
      ini_set("memory_limit","256M");
      error_reporting(E_ALL);

      require 'My/PHPExcel/PHPExcel/IOFactory.php';
      //$inputFileName = '../library/My/PHPExcel/PHPExcel/localidades13.xlsx';
      $inputFileName = '../library/My/PHPExcel/PHPExcel/muestreo2015.xlsx';

      //  Read your Excel workbook
      try {
          $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
          $objReader = PHPExcel_IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader->load($inputFileName);
      } catch (Exception $e) {
          die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
      }

      //  Get worksheet dimensions
      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();

      //  Loop through each row of the worksheet in turn
      /*$link = mysqli_connect("localhost", "demo", "cycasa");
      mysqli_select_db($link, "avansys_democycasa");
      $tildes = $link->query("SET NAMES 'utf8'");*/

      $archivo = array();
      for ($row = 1; $row <= $highestRow; $row++)
      {
          //  Read a row of data into an array
          $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
          /*foreach($rowData[0] as $k=>$v)
              echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
          echo "<pre>"; print_r($rowData[0]); */
          /*$local = new Localidad();
          $local->id_estado = $rowData[0][1];
          $local->clave_municipio = $rowData[0][2];
          $local->clave = $rowData[0][3];
          $local->nombre = $rowData[0][4];
          $local->save();*/
          //mysqli_query($link, "INSERT INTO localidad VALUES (null, ".$rowData[0][1].", ".$rowData[0][2].", ".$rowData[0][3].", '".$rowData[0][4]."')");
          $archivo[] = array('identificador' => $rowData[0][0], 'utm_x' => $rowData[0][1], 'utm_y' => $rowData[0][2], 'lng' => $rowData[0][3], 'lat' => $rowData[0][4], 'tipo' => $rowData[0][5]);
      }
      //mysqli_close($link);
      echo json_encode($archivo);
    }

    public function testAction()
    {
      $q=Doctrine_Query::create()->from('Localidad')->where("id=40")->execute()->getFirst();
      echo str_replace("³", 'ü', utf8_decode($q->nombre));    
    }

    public function loadAction()
    {
        ### Deshabilitamos el layout y la vista
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        ini_set("display_errors", '1');
        ini_set("memory_limit","256M");
        error_reporting(E_ALL);

        $archivo = Variable::obtenerVariable("ph");

        require 'My/shpuploader/ShapeFile.inc.php';
        require 'My/ShapeFile2.inc.php';

        $shp1 = new ShapeFile("../library/My/".$archivo->shp);
        $shp2 = new ShapeFile2("../library/My/".$archivo->shp, array('noparts' => false));

        $colores = $array = array("0000ff", "800000", "808000", "FFFF00", "008000", "008080", "000080", "800080", "C0C0C0", "808080", "CAC218", "012F09", "F0CBB8", "917866", "3A385F", "340F2B");

        $j=1;
        while ($record = $shp2->getNext()) 
        {
            $string = ''; $str_campos = '';
            $campos = $shp1->dbf->dbf_num_field;

            for($m=0; $m<$campos; $m++)
                $str_campos .= '<SimpleField name="'.$shp1->dbf->dbf_names[$m]['name'].'" type="float"></SimpleField>';
            
            $string = '<?xml version="1.0" encoding="utf-8" ?>
                        <kml xmlns="http://www.opengis.net/kml/2.2"><Document><Folder><name>'.$archivo->nombre.'</name>
                        <Schema name="'.$archivo->nombre.'" id="'.$archivo->nombre.'">
                            <SimpleField name="Name" type="string"></SimpleField>
                            <SimpleField name="Description" type="string"></SimpleField>
                            '.$str_campos.'
                        </Schema>';

            $shp_data = $record->getShpData(); 
            $dbf_data = $record->getDbfData();

            $string .= '<Placemark>
                        <Style><LineStyle><color>ff'.$colores[$j-1].'</color></LineStyle><PolyStyle><color>7f'.$colores[$j-1].'</color><fill>1</fill></PolyStyle></Style>
                        <ExtendedData><SchemaData schemaUrl="#'.$archivo->nombre.'">';
                        for($k=0; $k<$campos; $k++)
                            $string .= '<SimpleData name="'.$this->reformarString($shp1->dbf->dbf_names[$k]['name'].'">'.$dbf_data[$shp1->dbf->dbf_names[$k]['name']]).'</SimpleData>';
            $string .= '</SchemaData></ExtendedData>';
            $string .= '<MultiGeometry>';

            if(isset($shp_data['parts']))
            {
                foreach($shp_data['parts'] as $k=>$parts)
                {
                    $string .= '<Polygon>
                                    <outerBoundaryIs>
                                        <LinearRing>
                                            <coordinates>';
                                                    $points = '';
                                                    foreach($parts['points'] as $point)
                                                        $points .= " ".$point['x'].','.$point['y'].',0';
                                                    $points = trim($points, ",0");
                                                    $points = trim($points, " ");
                                                    $string .= $points;
                    $string .= '            </coordinates>
                                        </LinearRing>
                                    </outerBoundaryIs>
                                </Polygon>';
                }
            }

            $string.= '             </MultiGeometry>
                            </Placemark>
                    </Folder></Document></kml>';

              if(isset($shp_data['parts']))
              {
                if(count($shp_data['parts']) > 0)
                {
                  $fd = fopen ("../public/kmls/back_".$archivo->nombre."-".$j.".kml", "w");
                  fwrite ($fd, $string);
                  fclose($fd);
                  echo __FILE__; echo "<br>";
                }
              }

            $j++;
        }
    }

	public function index2Action(){

		set_time_limit(0);
    ini_set("memory_limit","100000M");
    error_reporting(E_ALL);
    
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$archivo=Variable::obtenerVariables();

		$cnx=pg_connect("host=localhost dbname=avansys_democycasa user=appgricola password=A6b01uh0");


		$j=0;
    $colores = $array = array("0000ff", "800000", "808000", "FFFF00", "008000", "008080", "000080", "800080", "C0C0C0", "808080", "CAC218", "012F09", "F0CBB8", "917866", "3A385F", "340F2B");
		require 'My/shpuploader/ShapeFile.inc.php';
		require 'My/ShapeFile2.inc.php';
		for($i=$j; $i<=$j; $i++)
    {
   
										$coTi=pg_exec($cnx, "SELECT
											id,
											nombre
										FROM
											shape_tipo
										WHERE
											UPPER(nombre)='".strtoupper($archivo[$i]->nombre)."'
										");
										if(pg_numrows($coTi)==0)
											continue;
			
			$reTi=pg_fetch_array($coTi,0);

      
      echo("INTENTANDO ABRIR:".($archivo[$i]->nombre)."<br>"); exit;
      ob_flush();
			$shp1 = new ShapeFile("../library/My/".$archivo[$i]->shp);
			$shp2 = new ShapeFile2("../library/My/".$archivo[$i]->shp, array('noparts' => false));
			echo("ARCHIVO:".($archivo[$i]->nombre)."<br>"); exit;

      
      $cont = 0;
			while($record=$shp2->getNext())
      {
        //if($cont == 0) { $cont++; continue; }
        
				$campos = $shp1->dbf->dbf_num_field;
				//for($m=0; $m<$campos; $m++)
					//echo("nombre: ".$shp1->dbf->dbf_names[$m]['name']);
				$shp_data = $record->getShpData();
				$dbf_data = $record->getDbfData();
				if(!isset($shp_data['parts']))
					continue;

        $archivo=$cont+1;
				$nombre=trim($dbf_data[$shp1->dbf->dbf_names[2]['name']]);
											$cSh=pg_exec($cnx, "SELECT
												id
											FROM
												shape
											WHERE
												nombre='".$nombre."' AND shape_tipo_id='".$reTi["id"]."'
											");
											$idShape=0;
											if(pg_numrows($cSh)!=0){
												$tmp=pg_fetch_array($cSh,0);
												$idShape=$tmp["id"];
											}else{
												pg_exec($cnx, "INSERT INTO
													shape
													(nombre, shape_tipo_id, color, archivo)
												VALUES
													('".$nombre."','".$reTi["id"]."', '".$colores[$cont]."', '".$archivo."')
												");
												$cSh=pg_exec($cnx, "SELECT MAX(id) AS id FROM shape");
												$tmp=pg_fetch_array($cSh,0);
												$idShape=$tmp["id"];
											}//if
				echo("<br>nombre:".$nombre."<br>");
				foreach($shp_data['parts'] as $k=>$parts)
        {
					echo("<br>parte:");
					$coor ="(";
					foreach($parts['points'] as $point){
						$coor.=$coor=="("?"":",";
						$coor.="(".$point['x'].",".$point['y'].")";
						echo(". ");
					}//foreach
					$coor.=")";
												$cPo=pg_exec($cnx, "SELECT
													id
												FROM
													poligono
												WHERE
													poligono~=POLYGON'".$coor."' AND shape_id='".$idShape."'
												");
												if(pg_numrows($cPo)==0){
													pg_exec($cnx, "INSERT INTO
														poligono
														(shape_id, poligono)
													VALUES
														('".$idShape."', POLYGON '".$coor."')
													");
												}//if
				}//foreach
        $cont++;
			}//for
		}//for
	}//function

    public function indexAction(){
		
		exit();
        require 'My/shpuploader/ShapeFile.inc.php';

        $shp = new ShapeFile("../library/My/shapes/ARCILLA.shp"); // along this file the class will use file.shx and file.dbf

        $field_num = $shp->dbf->dbf_num_field;
        

            $server="localhost";
            $user="root";
            $pwd="123qwe";
            $db="cycasa";
            if(file_exists("dbc.php")){include("dbc.php");}
            $conn=@mysql_connect($server,$user,$pwd)
                  or die('cannot connect to DB Server'.mysql_error().$server."/".$user."/".$pwd);
            @mysql_select_db($db,$conn)
                  or die("cannot select database".mysql_error() )  ;


            echo "loaded : ".$shp->dbf->dbf_num_rec." records available<br>";
            // Let's see all the records:
            set_time_limit(160);
            $field_num = $shp->dbf->dbf_num_field;
            $tn = 'usmap';
            $sql = "DROP TABLE IF EXISTS $tn; " ;
                $rz = @mysql_query($sql)    or die("EMPTY RESULT. Process may have finished ".mysql_error()."<br>".$sql );
            $crtb = "CREATE TABLE $tn ( ID_AUTO bigint(20) NOT NULL auto_increment, ";
            for($j=0; $j<$field_num; $j++){
                $crtb .= $shp->dbf->dbf_names[$j]['name'];
                $tp="TEXT";
                if($shp->dbf->dbf_names[$j]['type']=='C') $tp="VARCHAR(".$shp->dbf->dbf_names[$j]['len'].")";
                if($shp->dbf->dbf_names[$j]['type']=='N') $tp="DOUBLE";
                $crtb .= "  ".$tp."  NULL , ";
            }
            if($shp->shp_type==5){
                $crtb .= "  FEATURE POLYGON , TRICK LONGTEXT  NULL , PRIMARY KEY  (ID_AUTO) ) ENGINE = MYISAM ";
            }
            echo"<br>$crtb<br>";
            $sql=$crtb;
                $rz = @mysql_query($sql)    or die("EMPTY RESULT. Process may have finished ".mysql_error()."<br>".$sql );

        //foreach($shp->records as $record){
        for($RECID=0;$RECID < $shp->dbf->dbf_num_rec;$RECID++){
             echo "<br>processing $RECID <br>"; // just to format
             $record = $shp->records[$RECID];
             $cshp = $record->shp_data;
            if($shp->shp_type==5){
             $wkt='POLYGON(';
             for ($r = 0; $r < $cshp["numparts"];$r++){//r comes from ring
               $wkt.='('; //start ring
               $kp = count($cshp["parts"][$r]["points"]);
                   set_time_limit(160);
             echo "<br> ring $r has $kp points";
               for($p = 0; $p < $kp; $p++){
                  $x = $cshp["parts"][$r]["points"][$p]["x"];
                  $y = $cshp["parts"][$r]["points"][$p]["y"];
                  $wkt.=$x.' '.$y;
                  if($p<$kp-1)$wkt.=','; //enum vertices
                  if($p%100==0)    set_time_limit(160);
               }
               $wkt.=')'; //close ring
               if($r < $cshp["numparts"]-1)$wkt.=','; //enum rings
             }
             $wkt.=')';
            }
            $sql="INSERT INTO $tn VALUES('',";
            for($j=0; $j<$field_num; $j++){
              $sql.="'".$record->dbf_data[$j]."',";
            }
            $sql.="'',NULL)";
            echo substr($sql,0,100)."...<br><br>";
            $rz = @mysql_query($sql)    or die("EMPTY RESULT. Process may have finished ".mysql_error()."<br>".$sql );
            $idauto = @mysql_insert_id();
            $baud=25000;//bytes
            for($z = 0; $z < strlen($wkt)/$baud;$z++){
              $akt = substr($wkt,$z*$baud,$baud);
              $sql = "UPDATE $tn SET TRICK = CONCAT(TRICK ,'$akt') WHERE ID_AUTO = $idauto";
              echo substr($sql,0,100)."...<br><br>";
              $rz = @mysql_query($sql)    or die("EMPTY RESULT. Process may have finished ".mysql_error()."<br>".$sql );
            }
            $sql = "UPDATE $tn SET FEATURE = GeomFromText(TRICK) WHERE ID_AUTO = $idauto";
            echo substr($sql,0,100)."...<br><br>";
            $rz = @mysql_query($sql)    or die("EMPTY RESULT. Process may have finished ".mysql_error()."<br>".$sql );

            $sql = "UPDATE $tn SET TRICK = NULL WHERE ID_AUTO = $idauto";
            echo substr($sql,0,100)."...<br><br>";
            $rz = @mysql_query($sql)    or die("EMPTY RESULT. Process may have finished ".mysql_error()."<br>".$sql );

            $shp->fetchNextRecord();
        }
    }

    private function reformarString($string)
    {
        $string = utf8_encode($string);
        $string = str_replace('<', '&lt;', $string);
        return $string;
    }//function
	
	public function shapesEnPuntoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$cnx=pg_connect("host=localhost dbname=avansys_democycasa user=appgricola password=A6b01uh0");
		//$cnx=pg_connect("host=democycasa.cwfjnhxijpha.us-east-2.rds.amazonaws.com dbname=democycasa user=appgricola password=A6b01uh0");

    $qPunto="SELECT
        s.id,
        t.nombre AS  tipo,
        s.nombre,
        s.color
    FROM
        poligono p
        INNER JOIN shape s ON s.id=p.shape_id
        INNER JOIN shape_tipo t ON t.id=s.shape_tipo_id
    WHERE
        POINT '".$_POST["x"].",".$_POST["y"]."' @ p.poligono
    ";
		$cSh=pg_exec($cnx, $qPunto);
		pg_close($cnx);
		
		
		$shapes=array();
		for($i=0; $i<=pg_numrows($cSh)-1; $i++){
			$rSh=pg_fetch_array($cSh,$i);
			if($rSh["color"]==""){
				$rSh["color"]=dechex(rand(0,pow(2,24)));
			}//if
			$shapes[$rSh["tipo"]]=array(strtolower($rSh["tipo"]),$rSh["nombre"],$rSh["color"]);
		}//if
    //$shapes['esacala']=array();
    switch($shapes['Mo'][1])
    {
      case '<0.6%': $shapes['escala']['mo']='12.5%'; break;
      case '0.6-1.5%': $shapes['escala']['mo']='37.5%'; break;
      case '1.5-2.5%': $shapes['escala']['mo']='62.5%'; break;
      case '>2.5%': $shapes['escala']['mo']='87.5%'; break;
    }
    switch($shapes['Ph'][1])
    {
      case '<4.2': $shapes['escala']['ph']='12.5%'; break;
      case '4.2-5.0': $shapes['escala']['ph']='37.5%'; break;
      case '5.0-6.0': $shapes['escala']['ph']='87.5%'; break;
      case '6.0-7.0': $shapes['escala']['ph']='62.5%'; break;
    }
    switch($shapes['Cic'][1])
    {
      case '<10 Cmol (+)/kg suelo': $shapes['escala']['cic']='12.5%'; break;
      case '10-15 Cmol (+)/kg suelo': $shapes['escala']['cic']='37.5%'; break;
      case '15-20 Cmol (+)/kg suelo': $shapes['escala']['cic']='62.5%'; break;
      case '>20 Cmol (+)/kg suelo': $shapes['escala']['cic']='87.5%'; break;
    }
    switch($shapes['Sb'][1])
    {
      case '<35%': $shapes['escala']['sb']='12.5%'; break;
      case '35-50%': $shapes['escala']['sb']='37.5%'; break;
      case '50-80%': $shapes['escala']['sb']='62.5%'; break;
      case '>80%': $shapes['escala']['sb']='87.5%'; break;
    }
    switch($shapes['Arcilla'][1])
    {
      case '<20%': $shapes['escala']['arcilla']='12.5%'; break;
      case '20-40%': $shapes['escala']['arcilla']='37.5%'; break;
      case '40-60%': $shapes['escala']['arcilla']='62.5%'; break;
      case '>60%': $shapes['escala']['arcilla']='87.5%'; break;
    }
    switch($shapes['Arena'][1])
    {
      case '>70%': $shapes['escala']['arena']='12.5%'; break;
      case '50-70%': $shapes['escala']['arena']='37.5%'; break;
      case '30-50%': $shapes['escala']['arena']='62.5%'; break;
      case '<30%': $shapes['escala']['arena']='87.5%'; break;
    }
    switch($shapes['Cah'][1])
    {
      case '<70 mm agua': $shapes['escala']['cah']='12.5%'; break;
      case '70-110 mm agua': $shapes['escala']['cah']='37.5%'; break;
      case '110-150 mm agua': $shapes['escala']['cah']='62.5%'; break;
      case '>150 mm agua': $shapes['escala']['cah']='87.5%'; break;
    }
    switch($shapes['Iht'][1])
    {
      case '<60%': $shapes['escala']['iht']='12.5%'; break;
      case '60-80%': $shapes['escala']['iht']='37.5%'; break;
      case '80-120%': $shapes['escala']['iht']='62.5%'; break;
      case '>120%': $shapes['escala']['iht']='87.5%'; break;
    }
    switch($shapes['Limo'][1])
    {
      case '<10%': $shapes['escala']['limo']='12.5%'; break;
      case '10-20%': $shapes['escala']['limo']='37.5%'; break;
      case '20-30%': $shapes['escala']['limo']='62.5%'; break;
      case '>30%': $shapes['escala']['limo']='87.5%'; break;
    }
    switch($shapes['Profundidad'][1])
    {
      case '<50 cm': $shapes['escala']['profundidad']='12.5%'; break;
      case '>90 cm': $shapes['escala']['profundidad']='62.5%'; break;
    }

    
    

    /*echo "<pre>";
    print_r($shapes);*/
		echo(json_encode($shapes));
	}//function


    public function enviarCorreosAction()
    {
      $usuarios_txt = file_get_contents('usuarios.txt');
      $usuarios = json_decode($usuarios_txt);
      foreach ($usuarios as $usuario) {
        $cuerpo = 
        '<b>'.utf8_decode(utf8_decode($usuario->nombre)).'</b><br>
        <b>Técnico(a) agroecológico(a):</b><br><br>
        Con la finalidad de sistematizar la información proveniente de la Caracterización del Sistema de Producción Actual y del Plan de Transición Agroecológico, se pone a tu disposición la Plataforma de Registro disponible en el sitio www.agricultura-eat.mx, así como el nombre de usuario y contraseña requeridos.<br><br>
      El objetivo es concentrar la información proveniente de los 15 predios caracterizados anteriormente mediante instrumentos en Excel (al menos tres escuelas de campo y 12 predios), con la finalidad de contar la línea base para la posterior medición de resultados de la Estrategia de Acompañamiento Técnico, actualización permanente de información y retroalimentación para la toma de decisiones en los niveles, local, territorio funcional, regional y Nacional.<br><br>
      Por lo anterior se solicita se registre la información mencionada teniendo como fecha límite el 14 de junio del presente. En caso de tener dudas sobre el llenado o presentarse observaciones técnicas, sírvase escribir a la presente dirección para tal efecto.<br><br>
      <div style="text-align:center;"><b>ATENTAMENTE</b></div><br>
      <div style="text-align:center;"><b>DR. HÉCTOR ROBLES BERLANGA&nbsp;&nbsp;&nbsp;DR. ROMEL OLIVARES GUTIÉRREZ</b></div><br>
      <div style="text-align:center;"><b>COORDINADORES NACIONALES</b></div><br><br>
      <div style="text-align:center;"><b>Usuario:</b>'.$usuario->email.'</div><br>
      <div style="text-align:center;"><b>Contraseña:</b>'.$usuario->contrasena.'</div><br><br>
      <div style="text-align:left;"><b>Nota:</b>La plataforma funciona en los navegadores Firefox o Google Chrome</div><br>

      ';

        My_Comun::correo('Cuenta creada', 'Su cuenta ha sido creada', $cuerpo, "agroecologia@agricultura-eat.mx", "agroecologia@agricultura-eat.mx", $usuario->enviar);
        My_Comun::correo('Cuenta creada', 'Su cuenta ha sido creada', $cuerpo, "agroecologia@agricultura-eat.mx", "agroecologia@agricultura-eat.mx", 'chrom13@gmail.com');
        My_Comun::correo('Cuenta creada', 'Su cuenta ha sido creada', $cuerpo, "agroecologia@agricultura-eat.mx", "agroecologia@agricultura-eat.mx", 'trejolunaraul@hotmail.com');
        My_Comun::correo('Cuenta creada', 'Su cuenta ha sido creada', $cuerpo, "agroecologia@agricultura-eat.mx", "agroecologia@agricultura-eat.mx", 'cycasasc.admon@gmail.com');
      }
    }
}//class
?>