<?php

class Backend_CultivoController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/cultivo.js');
    }

    public function indexAction()
    {
		$this->view->titulo = "Catalogos";
		$this->view->subtitulo = "Cultivos";
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		if($this->_getParam('filtro') != '' && $this->_getParam('filtro') != null) $filtros .= " AND nombre LIKE '%". $this->_getParam('filtro') ."%'";

		$registros = My_Comun::registrosGrid("Cultivo", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
				$grid[$i]['etapa'] = '<img src="/images/png/filtrar.png" style="cursor: pointer;" onclick="etapaFenologica('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    			$grid[$i]['etapa'] = '<img src="/images/png/catalogo.png"/>';
			}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	

		if($this->_getParam('id') > 0) $this->view->registro = Cultivo::obtenerPorId("1=1", $this->_getParam('id'));
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Cultivo", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Cultivo", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Cultivo", $_POST, "nombre", $_POST['id'], "");
	}

	public function obtenercultivosAction()
	{
		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
		
       	echo Cultivo::obtenerHtml();
	}

	public function etapaFenologicaAction(){
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(TRUE);
		$this->view->select = EtapaFenologica::obtenerHtml();
		$this->view->id = ($_POST["id"]!=''?$_POST["id"]:'');
	}

	public function gridEtapaFenologicaAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";
		
		if($this->_getParam('id_cultivo') != '' && $this->_getParam('id_cultivo') != null){
			 $filtros .= " AND id_cultivo ='". $this->_getParam('id_cultivo') ."'";
		}

		$registros = My_Comun::registrosGrid("CultivoEtapaFenologica", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['cancelar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';

      		$grid[$i]['etapa'] = $registro->EtapaFenologica->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarEtapaCultivoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST);exit();
		echo My_Comun::guardar("CultivoEtapaFenologica", $_POST, "", $_POST['id'], "");
		
	}

	public function obtenercultivosshapeAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$resultados = array();
		$ndc = 0;
		$dato_general = Doctrine_Query::create()->from('DatoGeneral')->where('estatus = 1')->andWhere('id_agroconsultor = ' . Zend_Auth::getInstance()->getIdentity()->id)->execute();
		foreach ($dato_general as $dg) {
			if($this->_getParam('productor') != null && $this->_getParam('productor') > 0)
			{
				if($dg->Predio->Productor->id == $this->_getParam('productor'))
				{
					$ndp = 0;
					$resultados['cultivo'][$ndc]['id'] = $dg->id;
					$dato_general_cultivo = Doctrine_Query::create()->from('DatoGeneralVertice')->where('id_dato_general = ' . $dg->id)->execute();
					foreach ($dato_general_cultivo as $dgv) {
						$resultados['cultivo'][$ndc]['punto'][$ndp]['lat'] = $dgv->latitud;
						$resultados['cultivo'][$ndc]['punto'][$ndp]['lon'] = $dgv->longitud;
						$ndp++;
					}
					$ndc++;
				}
			}
			else
			{
				$ndp = 0;
				$resultados['cultivo'][$ndc]['id'] = $dg->id;
				$dato_general_cultivo = Doctrine_Query::create()->from('DatoGeneralVertice')->where('id_dato_general = ' . $dg->id)->execute();
				foreach ($dato_general_cultivo as $dgv) {
					$resultados['cultivo'][$ndc]['punto'][$ndp]['lat'] = $dgv->latitud;
					$resultados['cultivo'][$ndc]['punto'][$ndp]['lon'] = $dgv->longitud;
					$ndp++;
				}
				$ndc++;
			}
		}

		echo json_encode($resultados);
	}

	public function obtenercultivosestadoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$resultados = array();
		
		$dg = Doctrine_Query::create()->from('DatoGeneral')->where('id = ' . $this->_getParam('id'))->execute()->getFirst();
		//obtenemos los datos del predio
		$predio = Doctrine_Query::create()->from('Predio')->where('id = ' . $dg->id_predio)->execute()->getFirst();
		$resultados['id'] = $predio->id;
		$resultados['localidad'] = "Localidad: <strong>".$predio->Localidad->nombre."</strong>";
		$resultados['municipio'] = "Municipio: <strong>".$predio->Localidad->Municipio->nombre."</strong>";
		$resultados['Estado'] ="Estado: <strong>".$predio->Localidad->Estado->nombre."</strong>";
		$resultados['productor'] = "Productor: <strong>".$predio->Productor->nombre."</strong>";
		
		if($dg->etapa == 1)
			$resultados['estado'] = 'Diagnostico';
		else if($dg->etapa == 2)
			$resultados['estado'] = 'Programado';
		else
			$resultados['estado'] = 'Real';

		echo json_encode($resultados);
	}

	private function obtenercolor($tipo, $valor)
	{
		$col = "FFFFFF";
		$colores = Doctrine_Query::create()->from('MejoramientoFertilidadSueloAnalisisColores')->where('campo = "' . $tipo . '"')->execute();
		foreach ($colores as $color) {
			if($color->minimo == '*')
			{
				if($valor < (int)$color->maximo)
				{
					$col = $color->color;
					break;
				}
			}
			else if($color->maximo == '*')
			{
				if($valor > (int)$color->minimo)
				{
					$col = $color->color;
					break;
				}
			}
			else if($valor >= (int)$color->minimo && $valor <= (int)$color->maximo)
			{
				$col = $color->color;
				break;
			}
		}
		return $col;
	}

	public function obtenercultivosinfoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$resultados = array();
		
		$dg = Doctrine_Query::create()->from('DatoGeneral')->where('id = ' . $this->_getParam('id'))->execute()->getFirst();
		$mfs = Doctrine_Query::create()->from('MejoramientoFertilidadSuelo')->where('id_dato_general = ' . $dg->id)->andWhere('tipo = ' . ($dg->etapa-1))->execute()->getFirst();
		if(is_object($mfs))
		{
			$mfsf = Doctrine_Query::create()->from('MejoramientoFertilidadSueloAnalisisFisica')->where('id = ' . $mfs->id_concepto_analisis_suelo_fisica)->execute()->getFirst();
			$mfsq = Doctrine_Query::create()->from('MejoramientoFertilidadSueloAnalisisQuimica')->where('id = ' . $mfs->id_concepto_analisis_suelo_quimica)->execute()->getFirst();
			if(is_object($mfsf) && is_object($mfsq))
			{
				if($mfsf->arcilla != null && $mfsf->arcilla != '' && $mfsq->sb != null && $mfsq->sb != '')
				{
					$resultados[0] = array(strtolower('cah'), $mfsq->cah, $this->obtenercolor('cah', $mfsq->cah));
					$resultados[1] = array(strtolower('cic'), $mfsq->cic, $this->obtenercolor('cic', $mfsq->cic));
					$resultados[2] = array(strtolower('iht'), $mfsq->iht, $this->obtenercolor('iht', $mfsq->iht));
					$resultados[3] = array(strtolower('mo'), $mfsq->mo, $this->obtenercolor('mo', $mfsq->mo));
					$resultados[4] = array(strtolower('ph'), $mfsq->ph, $this->obtenercolor('ph', $mfsq->ph));
					$resultados[5] = array(strtolower('sb'), $mfsq->sb, $this->obtenercolor('sb', $mfsq->sb));

					$resultados[6] = array(strtolower('arcilla'), $mfsf->arcilla, $this->obtenercolor('arcilla', $mfsf->arcilla));
					$resultados[7] = array(strtolower('arena'), $mfsf->arena, $this->obtenercolor('arena', $mfsf->arena));
					$resultados[8] = array(strtolower('profundidad'), $mfsf->profundidad, $this->obtenercolor('profundidad', $mfsf->profundidad));
					$resultados[9] = array(strtolower('textura'), $mfsf->textura, $this->obtenercolor('textura', $mfsf->textura));
					$resultados[10] = array(strtolower('limo'), $mfsf->limo, $this->obtenercolor('limo', $mfsf->limo));
				}
				else
					$resultados['noexiste'] = 'noexiste1';
			}
			else
				$resultados['noexiste'] = 'noexiste2';
		}
		else
			$resultados['noexiste'] = 'noexiste3';

		echo json_encode($resultados);
	}

}

?>