<?php

class Backend_ProductorController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/productor.js');
    }

    public function indexAction()
    {
		$this->view->titulo = "Catalogos";
		$this->view->subtitulo = "Productores";
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		if($this->_getParam('filtro') != '' && $this->_getParam('filtro') != null) $filtros .= " AND nombre LIKE '%". $this->_getParam('filtro') ."%'";

		$registros = My_Comun::registrosGrid("Productor", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$grid[$i]['rfc'] = $registro->rfc;
      		$grid[$i]['domicilio'] = $registro->domicilio;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

    public function griddiagnosticoAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		if($this->_getParam('nombre') != '' && $this->_getParam('nombre') != null) 
			$filtros .= " AND nombre LIKE '%". $this->_getParam('nombre') ."%'";
		if($this->_getParam('rfc') != '' && $this->_getParam('rfc') != null) 
			$filtros .= " AND rfc LIKE '%". $this->_getParam('rfc') ."%'";
		if($this->_getParam('curp') != '' && $this->_getParam('curp') != null) 
			$filtros .= " AND curp LIKE '%". $this->_getParam('curp') ."%'";
		if($this->_getParam('domicilio') != '' && $this->_getParam('domicilio') != null) 
			$filtros .= " AND domicilio LIKE '%". $this->_getParam('domicilio') ."%'";

		$registros = My_Comun::registrosGrid("Productor", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    			$grid[$i]['seleccionar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="seleccionar_productor(\''. $registro->rfc .'\')"/>';
    		else
    			$grid[$i]['seleccionar'] = '<img src="/css/images/editar-off.gif"/>';

      		$grid[$i]['nombre'] = $registro->nombre;
      		$grid[$i]['rfc'] = $registro->rfc;
      		$grid[$i]['curp'] = $registro->curp;
      		$grid[$i]['domicilio'] = $registro->domicilio;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	
		if($this->_getParam('id') > 0) $this->view->registro = Productor::obtenerPorId($this->_getParam('id'));
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Productor", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Productor", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Productor", $_POST, array("nombre", "rfc", "domicilio", "curp"), $_POST['id'], "");
	}
}

?>