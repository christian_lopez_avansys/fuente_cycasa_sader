<?php

class Backend_DiagnosticosController extends Zend_Controller_Action
{
    public function init()
    {
    	
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/default/diagnostico.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_datosgenerales.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_rentatierra.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_acondicionamiento.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_preparacionsuelo.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_mejoramientofertilidad.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_riego.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_practicasiembra.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_practicaresiembra.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_siembra.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_resiembra.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_fertilizacion.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_control_malezas.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_costo_indirecto.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_ingreso_esperado.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_control_plagas_suelo.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_control_plagas_suelo_enfermedades.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_control_plagas_foliares.js');
		//$this->view->headScript()->appendFile('/js/default/diagnostico_control_enfermedades.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_tratamiento_biotico_abiotico.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_indice_de_rentabilidad.js');
		$this->view->headScript()->appendFile('/js/default/diagnostico_reporte_costos.js');
		$this->view->headScript()->appendFile('/js/caracterizacion/sistema-produccion-actual.js');
		$this->view->headScript()->appendFile('/js/caracterizacion/biodiversidad.js?v='.time());
		$this->view->headScript()->appendFile('/js/caracterizacion/cosecha.js');
		$this->view->headScript()->appendFile('/js/caracterizacion/financiamiento.js');
		
    }

    public function indexAction()
    {
	  	### Deshabilitamos el layout y la vista
      	//$this->_helper->layout->disableLayout();
	  	$this->view->titulo="Diagnósticos";
	  	$this->view->subtitulo="";
	  
	  	$this->view->dependencias = Dependencia::obtenerDependenciasActivas();
		$this->view->estados = Municipio::obtenerEstadosActivos();
	  	$this->view->integradoras = Integradora::obtenerIntegradorasActivas();
	  	$this->view->claves = Predio::obtenerClaves();
	  	$this->view->tipos_predios = Predio::obtenerTipos();
	  	$this->view->territorios = Predio::obtenerTerritorios();
	  	$this->view->regiones = Predio::obtenerRegiones();
		
		
	  	$this->view->organizaciones = Organizacion::obtenerOrganizacionesActivas();
	  	$this->view->id_dato_general=$this->_getParam('id');
	  	$this->view->tipo_usuario = Usuario::obtenerTipo();
		if(Zend_Auth::getInstance()->hasIdentity()){
			//$this->view->tipo=Agroconsultor::obtenerUsuarioPorId(Zend_Auth::getInstance()->getIdentity()->id)->tipo;
			
			//if($this->view->tipo==1){
				//$this->view->agroconsultor = Agroconsultor::obtenerUsuarioHtml();

			//}
			$this->view->agroconsultor = Agroconsultor::obtenerUsuariosPorTipo($this->view->tipo_usuario, Zend_Auth::getInstance()->getIdentity()->id);
		}
		$this->view->Productor = Productor::obtenerProductores();
	  	//$this->view->tipo_usuario = 3;
	  	//$this->view->municipios = Municipio::obtenerMunicipiosActivos();
      	//$this->view->localidades = Localidad::obtenerLocalidadesActivas();   
    }
	
	public function gridAction()
    {
	  	//error_reporting(E_ALL);
	  	### Deshabilitamos el layout y la vista
      	$this->_helper->layout->disableLayout();
	  	$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtro = " 1=1 ";
        $potrero = $this->_getParam('potrero');
		$ano = $this->_getParam('ano');
		
		if($this->_getParam('agroconsultor'))			
            $filtro.=" AND DatoGeneral.id_agroconsultor='".$this->_getParam('agroconsultor')."' ";
        
        if($this->_getParam('productor'))			
            $filtro.=" AND DatoGeneral.Predio.id_productor='".$this->_getParam('productor')."' ";

		if($this->_getParam('estado'))			
            $filtro.=" AND DatoGeneral.Predio.Localidad.id_estado='".$this->_getParam('estado')."' ";
        
        if($this->_getParam('clave')!="")			
            $filtro.=" AND DatoGeneral.Predio.clave='".str_replace("'","�",$this->_getParam('clave'))."' ";
        
        if($this->_getParam('region')!="")			
            $filtro.=" AND DatoGeneral.Predio.region='".str_replace("'","�",$this->_getParam('region'))."' ";
	    
	   	if($this->_getParam('territorio')!="")
            $filtro.=" AND DatoGeneral.Usuario.territorio_funcional='".str_replace("'","�",$this->_getParam('territorio'))."' ";
	   	
	   	if($this->_getParam('tipo_predio')!="")			
            $filtro.=" AND DatoGeneral.Predio.tipo_predio='".str_replace("'","�",$this->_getParam('tipo_predio'))."' ";
	         		
       //if($this->_getParam('integradora')!="")			
            //$filtro.=" AND DatoGeneral.Predio.id_integradora='".str_replace("'","�",$this->_getParam('integradora'))."' ";
        
		if($this->_getParam('municipio')!=""){
		   $clave = substr($this->_getParam('municipio'),strpos($this->_getParam('municipio'),"|")+1,strlen($this->_getParam('municipio')));		
           //print_r($clave);exit;
		   $filtro.=" AND DatoGeneral.Predio.Localidad.clave_municipio='".str_replace("'","�",$clave)."'";			
		}
		
		if($this->_getParam('localidad')!=""){
		   $filtro.=" AND DatoGeneral.Predio.id_localidad='".str_replace("'","�",$this->_getParam('localidad'))."' ";
			
		}
        //if($this->_getParam('organizacion')!="")			
            //$filtro.=" AND DatoGeneral.Predio.id_organizacion='".str_replace("'","�",$this->_getParam('organizacion'))."' ";
				
		if($potrero != ''){
            $potrero=explode(" ", trim($potrero));
            for($i=0; $i<=$potrero[$i]; $i++){
                $potrero[$i]=trim(str_replace(array("'","\"",),array("�","�"),$potrero[$i]));
				if($potrero[$i]!=""){				
                    //$filtro.=" AND (nombre LIKE '%".$nombre[$i]."%' ) ";
                    $filtro.=" AND (DatoGeneral.Predio.nombre LIKE '%".$potrero[$i]."%' ) ";
				}
            }//for
		}
		//print_r(Zend_Auth::getInstance()->getIdentity()->id);exit;
		$tipo = Usuario::obtenerTipo();
		$agroconsultores = Agroconsultor::obtenerUsuariosPorTipoIds($tipo, Zend_Auth::getInstance()->getIdentity()->id);
				
		switch($tipo){
			case 1:
				break;
			case 2:
				$filtro.=" AND  DatoGeneral.id_agroconsultor IN (".implode(",", $agroconsultores).")";
				break;
			case 3:
				$filtro.=" AND  DatoGeneral.id_agroconsultor = '".Zend_Auth::getInstance()->getIdentity()->id."'";
				break;
		}
		/*
		if(Zend_Auth::getInstance()->hasIdentity()){
			 if(Agroconsultor::obtenerUsuarioPorId(Zend_Auth::getInstance()->getIdentity()->id)->tipo==1){
				if($this->_getParam('productor')!=""){
					$filtro.=" AND  DatoGeneral.Predio.id_productor='".str_replace("'","�",$this->_getParam('productor'))."' "; 
				}
			 	if($this->_getParam('agroconsultor')!=""){
					$filtro.=" AND id_agroconsultor='".$this->_getParam('agroconsultor')."' "; 
				}
			 }else{
				 $filtro.=" AND id_agroconsultor='".Zend_Auth::getInstance()->getIdentity()->id."' ";
				 if($this->_getParam('productor')!=""){
					$filtro.=" AND  DatoGeneral.Predio.id_productor='".str_replace("'","�",$this->_getParam('productor'))."' "; 
				} 
			 }
		}
		*/			 		
        if($ano!=''){
            $ano=explode(" ", trim($ano));
            for($i=0; $i<=$ano[$i]; $i++){
                $ano[$i]=trim(str_replace(array("'","\"",),array("�","�"),$ano[$i]));
				if($ano[$i]!=""){				
                    //$filtro.=" AND (nombre LIKE '%".$nombre[$i]."%' ) ";
                    $filtro.=" AND (anio_apoyo LIKE '%".$ano[$i]."%' ) ";
				}
            }//for
		}
		 
		try
		{
       		$registros = My_Comun::registrosGrid("DatoGeneral", $filtro);
		}catch(Exception $e){
			print_r($e->getMessage());
		}  
        $grid=array();
		$i=0;

        foreach($registros['registros'] as $registro){
        	//print_r($registro->Localidad); exit;
       		if($registro->Predio->estatus == 0){
				$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif" />';
				$grid[$i]['eliminar'] = '<span onclick="habilitar('.$registro->Predio->id.');" title="Habilitar"><img style="cursor:pointer;" src="/css/images/eliminar-off.gif" /></span>';
			}else{
				$grid[$i]['editar'] = '<span onclick="agregar('.$registro->Predio->id_productor.', '.$registro->Predio->id.');" title="Editar"><img style="cursor:pointer;" src="/css/images/editar.gif" /></span>';
				$grid[$i]['eliminar'] = '<span onclick="deshabilitar('.$registro->id.');" title="Eliminar"><img style="cursor:pointer;" src="/css/images/eliminar.gif" /></span>';
			}

			if(!is_null($registro->Predio->id_localidad))
				$grid[$i]['estado']=$registro->Predio->Localidad->Estado->nombre;
			else
				$grid[$i]['estado']="Por definir";
			if(!is_null($registro->Predio->id_localidad))
				$grid[$i]['municipio']=Municipio::obtenerMunicipioPorClave($registro->Predio->Localidad->id_estado, $registro->Predio->Localidad->clave_municipio)->nombre;
			else
				$grid[$i]['municipio']="Por definir";
			if(!is_null($registro->Predio->id_localidad))
				$grid[$i]['localidad']=$registro->Predio->Localidad->nombre;
			else
				$grid[$i]['localidad']="Por definir";
			$grid[$i]['region']=$registro->Predio->region;
			$grid[$i]['territorio_funcional']=$registro->Usuario->territorio_funcional;
			$grid[$i]['agroconsultor']=$registro->Usuario->nombre;				
			$grid[$i]['datos_personales']=$registro->Predio->Productor->apellido_paterno.' '.$registro->Predio->Productor->apellido_materno.' '.$registro->Predio->Productor->nombre.' '.$registro->Predio->Productor->curp;
			$grid[$i]['tipo_predio']=$registro->Predio->tipo_predio;
			$grid[$i]['clave']=$registro->Predio->clave;
			$grid[$i]['folio']=$registro->Predio->folio_predio;
			$grid[$i]['nombre']=$registro->Predio->nombre;
			$grid[$i]['nombre_productor']=$registro->Predio->Productor->nombre;
			$grid[$i]['apellido_paterno']=$registro->Predio->Productor->apellido_paterno;
			$grid[$i]['apellido_materno']=$registro->Predio->Productor->apellido_materno;
			$grid[$i]['curp']=$registro->Predio->Productor->curp;
			$grid[$i]['genero']=($registro->Predio->Productor->genero==1)?"Femenino":"Masculino";
			$grid[$i]['ejido']=$registro->Predio->Productor->ejido;
            $i++;
		}//foreach
        My_Comun::armarGrid($registros,$grid); 
    }

	public function agregarAction()
	{
		$this->view->tipo = $this->_getParam('tipo');
		//error_reporting(E_ALL);
		//$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$this->view->titulo = "Agregar";
  		$this->view->subtitulo = "Manten tu informacion Actualizada";
  		if($this->_getParam('tipo') == 1)
  		{
  			$this->view->color = "#337ab7";
  			$this->view->uncolor = "#000099";
  		}
  		else if($this->_getParam('tipo') == 2)
  		{
  			$this->view->color = "#f0ad4e";
  			$this->view->uncolor = "#F09008";
  		}
  		else
  		{
  			$this->view->color = "#02B20E";
  			$this->view->uncolor = "#095A0F";
  		}

  		//Para la pestaña de Sistema de producción actual
  		$this->view->predio_id=$this->_getParam('predio');
  		$this->view->sistema_produccion_actual=SistemaProduccionActual::obtenerRegistro($this->view->predio_id);
  		$this->view->sistema_equipos=SistemaProduccionEquipo::obtenerTablas($this->_getParam('predio'));
  		$this->view->practica_conservaciones=SistemaProduccionConservacion::obtenerRegistros($this->_getParam('predio'));

  		$this->view->cultivos=Cultivo::obtenerHtml('1=1',0);
  		$this->view->datos_cultivos=DatoGeneralCultivo::obtenerPorPredio($this->view->predio_id,$this->view->tipo);

  		$this->view->sistema_eventos=SistemaProduccionEvento::obtenerTablas($this->_getParam('predio'));

  		//Para la pestaña de biodiversidad
  		$this->view->biodiversidad_plantas=BiodiversidadPlanta::obtenerRegistros($this->_getParam('predio'));
  		$this->view->biodiversidad_animales=BiodiversidadAnimal::obtenerRegistros($this->view->predio_id);
  		$this->view->biodiversidad_actividades=BiodiversidadActividades::obtenerRegistros($this->view->predio_id);

  		//Para la pestaña de financiamiento
  		$this->view->caracterizacion_financiamiento=Financiamiento::obtenerRegistro($this->view->predio_id);


		//print_r($this->view->sistema_produccion_actual);exit;
  		//Consultas para el catálogo de catálogos
  		$this->view->fuerza_trabajo=CatalogoDetalle::obtener(1);
  		$this->view->mano_obra=CatalogoDetalle::obtener(2);
  		$this->view->mano_obra=CatalogoDetalle::obtener(2);
  		$this->view->catalogo_biodiversidad_formas=CatalogoDetalle::obtener(5);
  		$this->view->catalogo_biodiversidad_usos=CatalogoDetalle::obtener(6);
  		$this->view->catalogo_biodiversidad_animales_clasificacion=CatalogoDetalle::obtener(7);
  		$this->view->catalogo_biodiversidad_animales_usos=CatalogoDetalle::obtener(8);
  		$this->view->catalogo_biodiversidad_actividades_fase_lunar=CatalogoDetalle::obtener(9);
  		$this->view->catalogo_cosecha_fuerza_trabajo=CatalogoDetalle::obtener(10);
  		$this->view->catalogo_cosecha_mano_obra=CatalogoDetalle::obtener(11);
  		$this->view->catalogo_cosecha_acarreo=CatalogoDetalle::obtener(12);
  		$this->view->catalogo_cosecha_almacenamiento=CatalogoDetalle::obtener(13);
  		$this->view->catalogo_cosecha_destino=CatalogoDetalle::obtener(14);
  		$this->view->tipo_manejo_actual=CatalogoDetalle::obtener(18);
  		$this->view->tipo_regimen_propiedad=CatalogoDetalle::obtener(19);
  		$this->view->tipo_regimen_agrario=CatalogoDetalle::obtener(20);
  		$this->view->tipo_regimen_hidrico=CatalogoDetalle::obtener(21);
  		$this->view->cat_textura_suelo=CatalogoDetalle::obtener(22);
  		$this->view->cat_pendiente_promedio=CatalogoDetalle::obtener(23);
  		$this->view->cat_tecnica_labranza=CatalogoDetalle::obtener(24);
  		$this->view->cat_disposicion_cultivo=CatalogoDetalle::obtener(25);
  		$this->view->cat_obtiene_semilla=CatalogoDetalle::obtener(26);
  		$this->view->cat_material_genetico=CatalogoDetalle::obtener(27);
  		$this->view->cat_propia_semilla=CatalogoDetalle::obtener(28);
  		$this->view->cat_fuente_manejo_sanitario=CatalogoDetalle::obtener(42);
  		$this->view->cat_tipo_inductores_resistencia=CatalogoDetalle::obtener(41);
  		$this->view->cat_tipo_inductores_crecimiento=CatalogoDetalle::obtener(40);
  		$this->view->cat_tipo_inductores_productividad=CatalogoDetalle::obtener(40);

  		//Catálogos para Nutrición (antes fertilización)
  		$this->view->cat_fuente_nutricion=CatalogoDetalle::obtener(32);

  		$regPredio=Doctrine_Query::create()->from('DatoGeneral')->where("id_predio='".$this->_getParam('predio')."'")->execute()->getFirst();
  		
		//$this->view->id = $this->_getParam('id');
		$this->view->id = $regPredio->id;
		
		//print_r(Localidad::obtenerLocalidadeshtml("1=1"));exit;
		$this->view->estado=Estado::obtenerHtml();
		$this->view->integradora = Integradora::obtenerHtml();
		$this->view->dependencia = Dependencia::obtenerHtml();
		$this->view->organizacion = Organizacion::obtenerHtml();

		$this->view->unidad=Unidad::obtener();
		//$this->view->empresas=Empresa::obtener();
		$this->view->empresas=Empresa::obtener();
		$this->view->marcas=Marca::obtener();
		
		$this->view->activos=Activo::obtener();
		$this->view->tecnicas=Tecnicas::obtener();
		try{
			$this->view->usos=Uso::obtenerPorId(" 1=1 ","");
		}catch(Exception $e){
			print_r($e->getMessage());exit;
		}
		
		//unidades tratamiento de control de plagas de suelo
		$plagasUnidades["gran"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","gran");
		$plagasUnidades["trat"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","trat");
		$plagasUnidades["rec"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","rec");
		$plagasUnidades["metarhizium"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","metarhizium");
		$plagasUnidades["Trichoderma"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","Trichoderma");
		$plagasUnidades["beauveria"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","beauveria");
		$plagasUnidades["acidificacion"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasDeSuelo","acidificacion");
		
		//unidades tratamiento de control de plagas foliares
		$foliaresUnidades["qob"]=$this->obtenerunidadesporformularioconcepto2("ControlDePlagasFoliares","qob");

		//unidades tratamiento de estres
		$tratamientoUnidades["gran2"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","gran2");
		$tratamientoUnidades["trat2"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","trat2");
		$tratamientoUnidades["rec2"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","rec2");
		$tratamientoUnidades["metarhizium"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","metarhizium");
		$tratamientoUnidades["Trichoderma"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","Trichoderma");
		$tratamientoUnidades["beauveria"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","beauveria");
		$tratamientoUnidades["acidificacion"]=$this->obtenerunidadesporformularioconcepto2("TratamientoDeEstresBioticoYAbiotico","acidificacion");
		
		$this->view->unidadTratamiento=$tratamientoUnidades;
		$this->view->unidadPlagas=$plagasUnidades;
		$this->view->unidadFoliar=$foliaresUnidades;
		//print_r($this->view->unidadFoliar);exit;
		//unidades control de enfermedades
		//["fungicida", "bactericida", "viricida", "bioestimulante"];
		$enfermedadesUnidades["fungicida"]=$this->obtenerunidadesporformularioconcepto2("ControlDeEnfermedades","fungicida");
		$enfermedadesUnidades["bactericida"]=$this->obtenerunidadesporformularioconcepto2("ControlDeEnfermedades","bactericida");
		$enfermedadesUnidades["viricida"]=$this->obtenerunidadesporformularioconcepto2("ControlDeEnfermedades","viricida");
		$enfermedadesUnidades["bioestimulante"]=$this->obtenerunidadesporformularioconcepto2("ControlDeEnfermedades","bioestimulante");
		$this->view->unidadEnfermedades=$enfermedadesUnidades;
		
		$this->view->etapas_fenologicas=EtapaFenologica::obtener();
		$this->view->unidades_pre=Unidad::obtenerHtmlConfiguracion(0,'ControlDeMaleza','pre');



		//Obtenemos lo relacionado con plagas de suelo
		if($this->_getParam('id')>0)
		{
			$this->view->control_plagas_suelo=ControlPlagasSuelo::obtenerPorDatoGeneral($this->view->id,$this->_getParam('tipo'));
			$this->view->control_plagas_suelo_insecticida_granulado=ControlPlagasSueloInsecticida::obtenerPorTipo($this->view->control_plagas_suelo->id,"GRANULADO");

			//Dado que Raul solicitó que la pestaña de Manejo fitosanitario (control de enfermedades)
			//fuera igual a la de plagas de suelo, se reutiliza el modelo y se diferencía por un campo
			//llamado "pestana"
			$this->view->control_plagas_suelo_enfermedades=ControlPlagasSuelo::obtenerPorDatoGeneral($this->view->id,$this->_getParam('tipo'),2);
			$this->view->control_plagas_suelo_insecticida_granulado_enfermedades=ControlPlagasSueloInsecticida::obtenerPorTipo($this->view->control_plagas_suelo_enfermedades->id,"GRANULADO");


			//$this->view->control_plagas_suelo_insecticida_tratamiento=ControlPlagasSueloInsecticida::obtenerPorTipo($this->view->control_plagas_suelo->id,"TRATAMIENTO");
			//$this->view->control_plagas_suelo_insecticida_recarga=ControlPlagasSueloInsecticida::obtenerPorTipo($this->view->control_plagas_suelo->id,"RECARGA");
			
			//obtenemos lo relacionado con control de enfermedades
			$this->view->control_enfermedades = ControlDeEnfermedades::obtener($this->_getParam('tipo'),$this->view->id);
			//obtenemos lo relacionado con tratamiento de estres biotico y abiotico
			$this->view->tratamiento_b_ab = TratamientoDeEstresBioticoYAbiotico::obtenerPorDatoGeneral($this->_getParam('tipo'), $this->view->id);
			$this->view->tratamiento_b_ab_insecticida_granulado = TratamientoDeEstresBioticoYAbioticoInsecticida::obtenerPorTipo($this->view->tratamiento_b_ab->id,"GRANULADO");
			$this->view->tratamiento_b_ab_insecticida_tratamiento = TratamientoDeEstresBioticoYAbioticoInsecticida::obtenerPorTipo($this->view->tratamiento_b_ab->id,"TRATAMIENTO");
			$this->view->tratamiento_b_ab_insecticida_recarga = TratamientoDeEstresBioticoYAbioticoInsecticida::obtenerPorTipo($this->view->tratamiento_b_ab->id,"RECARGA");

			$this->view->control_plagas_foliares=ControlPlagasFoliares::obtenerPorDatoGeneral($this->view->id,$this->_getParam('tipo'));

			$this->view->predio_id = $this->_getParam('predio');
			//print_r("hola mundo");exit;
		}
		//print_r("hola mundo");exit;
		/*if($this->_getParam('vertices'))
		{
			$this->view->verticesMapa = '';
			$arr = json_decode($this->_getParam('vertices'));
			$cnt = 0;
			foreach($arr as $key => $value) $cnt++;
			for ($i=0; $i < (int)$cnt/2; $i++) { 
				$var1 = 'lat_' . $i;
				$var2 = 'lng_' . $i;
				$this->view->verticesMapa .= '|' . $arr->$var1 . '::' . $arr->$var2;
			}
			$this->view->verticesMapa = trim($this->view->verticesMapa, '|');
		}*/
		$this->view->verticesMapa=DatoGeneralVertice::obtenerPorPredio($this->view->predio_id);


	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////// DATOS GENERALES /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardardatogeneralAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$resultados = array();
			$registro = DatoGeneral::guardar($_POST);
			if($registro > 0) 
				$resultados['ok'] = $registro;
			else 
				$resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function guardarcultivodatogeneralAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		$resultados = array();
		$registro = DatoGeneral::guardarCultivos($_POST);
		///print_r(count($registro)); exit;
		if(count($registro)== 0) 
			$resultados['ok'] = 1;
		else 
			$resultados['error'] = $registro;
		
		echo json_encode($resultados);
	}

	public function obtenerdatosdatogeneralAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		if($_POST['id'] > 0){
			$regDatoGeneral=Doctrine_Query::create()->from('DatoGeneral')->where("id='".$_POST['id']."'")->execute()->getFirst();
			$producto=Productor::obtenerPorId($regDatoGeneral->Predio->id_productor);
		    $resp=array();
		    $resultados = array();

		    if(is_object($producto))
		    {
			      $resp['data']=true;
			      $resp['id'] = $producto->id;
			      $resp['curp'] = $producto->curp;
			      $resp['apellido_paterno'] = $producto->apellido_paterno;
			      $resp['apellido_materno'] = $producto->apellido_materno;
			      $resp['nombre'] = $producto->nombre;
			      $resp['genero'] = $producto->genero;
			      $resp['estado_id'] = $producto->estado_id;
			      $resp['municipio_id'] = $producto->municipio_id;
			      $resp['localidad_id'] = $producto->localidad_id;
			      $resp['pertenece_etnia'] = $producto->pertenece_etnia;
			      $resp['etnia_id'] = $producto->etnia_id;
			      $resp['innovador'] = $producto->innovador;
			      $resp['sociedad_agricola'] = $producto->sociedad_agricola;
			      $resp['sociedad_social'] = $producto->sociedad_social;
			      $resp['sociedad_religiosa'] = $producto->sociedad_religiosa;
			      $resp['persona_juridica_id'] = $producto->persona_juridica_id;

			      $resp['predio'] = array();

			    if($_POST['predio_id']){
				      $predio = Predio::obtenerPorId($_POST['predio_id']);

//				      print_r($predio->Localidad->Estado->id); exit;

				      $resp['predio']['estado'] = $predio->Localidad->Estado->id;
				      $resp['predio']['municipio'] =  $predio->Localidad->clave_municipio;
				      $resp['predio']['localidad'] = $predio->Localidad->id;
				      $resp['predio']['region'] = $predio->region;
				      $resp['predio']['tipo_predio'] = $predio->tipo_predio;
				      $resp['predio']['clave'] = $predio->clave;
				      $resp['predio']['folio_predio'] = $predio->folio_predio;
				      $resp['predio']['nombre_predio'] = $predio->nombre;
				      $resp['predio']['numero_ciclos'] = $predio->numero_ciclos;
				      $resp['predio']['manejo_actual'] = $predio->manejo_actual;
				      $resp['predio']['superficie'] = $predio->superficie;
				      $resp['predio']['altitud'] = $predio->altitud;
				      $resp['predio']['regimen_propiedad']= $predio->regimen_propiedad;
				      $resp['predio']['regimen_agrario'] = $predio->regimen_agrario;
				      $resp['predio']['regimen_hidrico'] = $predio->regimen_hidrico;
				      $resp['predio']['nombre_tecnico'] = $predio->nombre_tecnico;
				      $resp['predio']['numero_modulo'] = $predio->numero_modulo;
				      $resp['predio']['kml'] = $predio->kml;

				    $contador_cultivos = 0;
		            $cultivos = DatoGeneralCultivo::obtenerPorPredio($_POST['predio_id']);
		            foreach ($cultivos as $cultivo) 
		            {
		                $resultados['cultivos'][$contador_cultivos]['id'] = $cultivo->id;
						$resultados['cultivos'][$contador_cultivos]['congelado'] = $cultivo->congelado;
		                $resultados['cultivos'][$contador_cultivos]['cultivos'] = Cultivo::obtenerHtml("1=1",$cultivo->id_cultivo);
		                $resultados['cultivos'][$contador_cultivos]['hectareas'] = $cultivo->numero_hectareas;
		                $contador_cultivos++;
		            }

		            $contador_vertices = 0;
		            $vertices = DatoGeneralVertice::obtenerPorPredio($_POST['predio_id']);
		            foreach ($vertices as $vertice) 
		            {
		                $resultados['vertices'][$contador_vertices]['id'] = $vertice->id;
		                $resultados['vertices'][$contador_vertices]['latitud'] = $vertice->latitud;
		                $resultados['vertices'][$contador_vertices]['longitud'] = $vertice->longitud;
		                $contador_vertices++;
		            }

		            if(is_array($resp)) $resp = array_merge($resultados, $resp);
		        }

		    }
		    else
		    {
		      $resp['data']=false;
		    }

		    echo json_encode($resp);
		}
			
		else
			echo RelacionDiagnostico::noData();
	}

	public function eliminarcultivogeneralAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		if($_POST['id'] > 0)
		{
			$id = DatoGeneralCultivo::eliminar($_POST['id']);
			//echo $id; exit();
			$resultados = array();
			if($id > 0) $resultados['ok'] = "";
			else $resultados['error'] = $id;
			echo json_encode($resultados);
		}
		else
			echo RelacionDiagnostico::noData();
	}

	public function eliminarverticegeneralAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		if($_POST['id'] > 0)
		{
			$id = DatoGeneralVertice::eliminar($_POST['id']);
			$resultados = array();
			if($id > 0) $resultados['ok'] = "";
			else $resultados['error'] = $resultados;
			echo json_encode($resultados);
		}
		else
			echo RelacionDiagnostico::noData();
	}
	/*
	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("DatoGeneral", $this->_getParam('id'), "");
		else echo -100;
	}
	*/
	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("DatoGeneral", $this->_getParam('id'), "");
		else echo -100;
	}

	public function verificartipocompletoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_POST); exit;

		$resultados = array();
			$registro = DatoGeneral::verificarFormCompleto($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['nocomplet'] = $registro;
		echo json_encode($resultados);
	}
	
	public function congelardatosgeneralesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = DatoGeneral::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////// RENTA DE LA TIERRA /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarrentadelatierraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		$resultados = array();
			$registro = RentaTierra::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatosrentadelatierraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		if($_POST['id'] > 0){
			echo RentaTierra::obtener($_POST['tipo'], $_POST['id']);
			//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		}else{
			echo RelacionDiagnostico::noData();
		}
	}

	public function congelarrentatierraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = RentaTierra::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// PRACTICA DE SIEMBRA /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarpracticadesiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;

		$resultados = array();
			$registro = PracticaSiembra::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatospracticadesiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		if($_POST['id'] > 0)
			echo PracticaSiembra::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarpracticasiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = PracticaSiembra::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// PRACTICA DE RESIEMBRA /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarpracticaderesiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;

		$resultados = array();
		$registro = PracticaResiembra::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatospracticaderesiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo PracticaResiembra::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarpracticaresiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = PracticaResiembra::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ACONDICIONAMIENTO PREDIO //////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardaracondicionamientodelpredioAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		$resultados = array();
		$registro = AcondicionamientoPredio::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatosacondicionamientodelpredioAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo AcondicionamientoPredio::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelaracondicionamientopredioAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = AcondicionamientoPredio::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////// PREPARACION DEL SUELO //////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarpreparaciondelsueloAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$resultados = array();
		$registro = PreparacionSuelo::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatospreparaciondelsueloAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo PreparacionSuelo::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarpreparacionsueloAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = PreparacionSuelo::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////// RIEGO ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarriegoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); print_r($_GET); exit;
		$resultados = array();
		$registro = Riego::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatosriegoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo Riego::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarriegoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = Riego::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// MEJORAMIENTO FERTILIDAD ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarmejoramientofertilidadAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); exit;
		$resultados = array();
		$registro = MejoramientoFertilidadSuelo::guardar($_POST);
			if($registro > 0) 
				$resultados['ok'] = $registro;
			else 
				$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatosmejoramientofertilidadAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo MejoramientoFertilidadSuelo::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarmejoramientofertilidadAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = MejoramientoFertilidadSuelo::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function eliminararchivoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		
		$resultados = array();
			$archivo->documento = $_POST["archivo"];
			//print_r($archivo->documento); exit;
			if($_POST["archivo"]!=""){
				$registro = MejoramientoFertilidadSuelo::eliminarArchivo($archivo);
				if($registro){ 
					$resultados['ok'] = "Archivo eliminado";
					MejoramientoFertilidadSuelo::actualizarDespuesDeEliminarArchivo($_POST["dato_general"],$_POST["tipo"]);
				}else 
					$resultados['error'] = "Error al eliminar el archivo";
			}
				
		echo json_encode($resultados);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// FERTILIZACION ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarfertilizacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$resultados = array();
		//print_r($_POST);exit;
		$registro = Fertilizacion::guardar($_POST);
		//print_r($registro);exit;
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatosfertilizacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo $_POST['tipo']." - ". $_POST['id']; exit();
		unset($_POST["tabla_1"]);
		$Datos = $_POST;
		
		if($_POST['id'] > 0)
			echo Fertilizacion::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarfertilizacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST); exit;
		
		$resultados = array();
		$registro = Fertilizacion::congelar($_POST);
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function eliminarfertilizacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
		$registro = Fertilizacion::eliminar($_POST["id"],($_POST["tipo"]-1));
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function eliminarconceptofertilizacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		//echo json_encode($_POST); exit;
		
		$resultados = array();
		$registro = Fertilizacion::eliminarconcepto($_POST["id"],($_POST["tipo"]-1));
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function eliminarconceptoffertilizacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		//echo json_encode($_POST); exit;
		
		$resultados = array();
		$registro = Fertilizacion::eliminarconceptof($_POST["id"],($_POST["tipo"]-1));
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}



	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// CONTROL DE MALEZAS ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarcontroldemalezaAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		unset($_POST["tabla_1"]);
		unset($_POST["oculto"]);
		//print_r($_POST);exit;
		$Datos = $_POST;
		$resultados = array();
		
		$registro = ControlDeMaleza::guardar($Datos);
		
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatoscontroldemalezaAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo $_POST['tipo']." - ". $_POST['id']; exit();
		//unset($_POST["tabla_1"]);
		//$Datos = $_POST;
		
		if($_POST['id'] > 0)
			echo ControlDeMaleza::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarcontroldemalezaAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = ControlDeMaleza::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function eliminarcontroldemalezaAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST["id"]); exit;
		
		$resultados = array();
		$registro = ControlDeMaleza::eliminar($_POST["id"]);
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// CONTROL DE ENFERMEDADES ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarcontroldeenfermedadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$Datos = $_POST;
		$resultados = array();
		//print_r($Datos);exit;
		$registro = ControlDeEnfermedades::guardar($Datos);
		
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatoscontroldeenfermedadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		if($_POST['id'] > 0)
			echo ControlDeEnfermedades::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelarcontroldeenfermedadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
				
		$resultados = array();
			$registro = ControlDeEnfermedades::congelar($_POST);
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// CONTROL DE PLAGAS FOLIARES ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarControlPlagasFoliaresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$id=ControlPlagasFoliares::guardar($_POST);
	}
	public function congelarControlPlagasFoliaresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST);exit();
		echo ControlPlagasFoliares::congelar($_POST["id_dato_general"],$_POST["tipo"]);
	}
	
	public function eliminarhijoplagafoliarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST);exit();
		echo ControlPlagasFoliares::eliminarHijoPorId($_POST["id"]);
	}
	public function eliminartablaplagafoliarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST);exit();
		echo ControlPlagasFoliares::eliminarTablaPorId($_POST["id"]);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// TRATAMIENTO DE ESTRES BIOTICO Y ABIOTICO /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarTratamientoDeEstresBioticoYAbioticoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		try{
			echo TratamientoDeEstresBioticoYAbiotico::guardar($_POST);
		}catch(Exception $e){
			print_r($e->getMessage());exit;
		}
		
	}
	public function congelarTratamientoDeEstresBioticoYAbioticoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		echo TratamientoDeEstresBioticoYAbiotico::congelarPorDatoGeneral($_POST["id_dato_general"],$_POST["tipo"]);
	}

	public function deshabilitarAction(){
		$id = $this->_getParam('id');
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		### Instanciamos la conexión para generar una transacción
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        //$id = 386;
        try {
        	$controlPlagasFoliares = Doctrine_Query::create()->from('ControlPlagasFoliares')->where('id_dato_general = ?', $id)->execute();
            foreach($controlPlagasFoliares as $controlPlaga){

        		ControlPlagasFoliares::eliminarTablaPorId($controlPlaga->id);
            }
        	
            $cultivo = Doctrine_Query::create()->from('DatoGeneralCultivo')->where('id = ?', $id)->execute()->getFirst();

            ############## BUSCAMOS LOS REGISTROS DE RESIEMBRA
      		$resiembras = Doctrine_Query::create()->from('Resiembra')->where('id_dato_general = ?', $id)->execute();
      		foreach($resiembras as $resiembra) {
      			$resiembras_cultivos = Doctrine_Query::create()->from('ResiembraCultivo')->where('id_resiembra = ?', $resiembra->id)->execute();
      			foreach($resiembras_cultivos as $resiembras_cultivo) {
            		$resiembras_inoculaciones = Doctrine_Query::create()->from('ResiembraInoculacion')->where('id_resiembra_cultivo = ?', $resiembras_cultivo->id)->execute();
      				foreach($resiembras_inoculaciones as $resiembras_inoculaciones) {
            			ResiembraInoculacion::eliminar($resiembras_inoculaciones->id);
      				}
					Doctrine_Query::create()->delete('ResiembraCultivo')->where("id = ".$resiembras_cultivo->id)->execute();
      			}
				Doctrine_Query::create()->delete('Resiembra')->where("id = ".$resiembra->id)->execute();
      		}
            ##################################################
            #### BUSCAMOS LOS REGISTROS DE ACONDICIONDICIONAMIENTO PREDIO
            $acondicionamientos = Doctrine_Query::create()->from('AcondicionamientoPredio')->where('id_dato_general = ?', $id)->execute();
      		foreach($acondicionamientos as $acondicionamiento) {
				Doctrine_Query::create()->delete('AcondicionamientoPredio')->where("id = ".$acondicionamiento->id)->execute();
				$acondicionamientos_conceptos = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')
					->where('id = ?', $acondicionamiento->id_concepto_canal)
					->orWhere('id = ?', $acondicionamiento->id_concepto_trazo)
					->orWhere('id = ?', $acondicionamiento->id_concepto_emparejamiento)
					->orWhere('id = ?', $acondicionamiento->id_concepto_contreo)
					->orWhere('id = ?', $acondicionamiento->id_concepto_despiedre)
					->orWhere('id = ?', $acondicionamiento->id_concepto_reforzamiento)
					->orWhere('id = ?', $acondicionamiento->id_concepto_otros)
					->execute();
      			foreach($acondicionamientos_conceptos as $acondicionamientos_concepto) {
					Doctrine_Query::create()->delete('AcondicionamientoPredioConcepto')->where("id = ".$acondicionamientos_concepto->id)->execute();
	      		}	
      		}
            #############################################################################
            ############## BUSCAMOS LOS REGISTROS DE SIEMBRA ##############
            $siembra = Doctrine_Query::create()->from('Siembra')->where('id_dato_general = ?', $id)->execute()->getFirst();
            if($siembra){
	            $siembra_cultivos = Doctrine_Query::create()->from('SiembraCultivo')->where('id_siembra = ?', $siembra->id)->execute();
	            ############# ELIMINAMOS LOS REGISTROS DE SIEMBRA ##############
				foreach($siembra_cultivos as $siembra_cultivo) {
	            	$siembra_inoculaciones = Doctrine_Query::create()->from('SiembraInoculacion')->where('id_siembra_cultivo = ?', $siembra_cultivo->id)->execute();

	            	foreach ($siembra_inoculaciones as $siembra_inoculacion) {
			            Doctrine_Query::create()->delete('SiembraInoculacion')
			                                    ->where("id = ". $siembra_inoculacion->id)
			                                    ->execute();
					}

					Doctrine_Query::create()->delete('SiembraCultivo')
			                                    ->where("id = ". $siembra_cultivo->id)
			                                    ->execute();

	            	$siembra_cultivo_conceptos = Doctrine_Query::create()->from('SiembraConcepto')->where('id = ?', $siembra_cultivo->id_siembra_concepto)->execute();

					foreach($siembra_cultivo_conceptos as $siembra_cultivo_concepto) {
			            Doctrine_Query::create()->delete('SiembraConcepto')
			                                    ->where("id = ". $siembra_cultivo_concepto->id)
			                                    ->execute();
					}
				}
				Doctrine_Query::create()->delete('Siembra')
	                                    ->where("id = ". $siembra->id)
	                                    ->execute();
            }
            ############# DATOGENERAL ##############
            Doctrine_Query::create()->delete('DatoGeneral')->where("id = ".$id)->execute();

            ### Hacemos commit a la transacción
            $conn->commit();
            //$conn->rollBack();
            echo 'true';
        } catch (Exception $e) {
            ### Rollback en caso de algún problema
            $conn->rollBack(); 
            ### Entregamos un mensaje de error
            echo $e->getMessage();
        }
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////// SIEMBRA ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarsiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST); exit;
		$resultados = array();
		$registro = Siembra::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatossiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo Siembra::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function eliminarinoculacionfrm7Action()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$resultados = array();
			if($_POST['id'] > 0)
			{
				$registro = SiembraInoculacion::eliminar($_POST['id']);
				if($registro > 0) $resultados['ok'] = $registro;
				else $resultados['error'] = $registro;
			}
			else
				$resultados['message'] = RelacionDiagnostico::noData();
		echo json_encode($resultados);
	}

	public function congelarsiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = Siembra::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////// SIEMBRA ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarresiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//print_r($_POST); exit;
		$resultados = array();
		$registro = Resiembra::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	public function tieneresiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); exit;
		$resultados = array();
		$registro = Resiembra::hayRegistros($_POST);
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	

	public function obtenerdatosresiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo Resiembra::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function eliminarinoculacionfrm9Action()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$resultados = array();
			if($_POST['id'] > 0)
			{
				$registro = ResiembraInoculacion::eliminar($_POST['id']);
				if($registro > 0) $resultados['ok'] = $registro;
				else $resultados['error'] = $registro;
			}
			else
				$resultados['message'] = RelacionDiagnostico::noData();
		echo json_encode($resultados);
	}

	public function congelarresiembraAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = Resiembra::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	
	public function comprobarhectareasAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		
		$hectareas = DatoGeneralCultivo::obtenerHectareas($_POST["id"],($_POST["tipo"]-1),$_POST["cultivo"]);
		//print_r($_POST);exit;
		if($_POST["hectareas"]>$hectareas){
			echo '1';
		}else{
			echo '0';
		}
		
	}
	public function obtenerhectareasdecultivoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		
		echo DatoGeneralCultivo::obtenerHectareas($_POST["id"],($_POST["tipo"]-1),$_POST["id_cultivo"]);
		
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// COSTO INDIRECTO ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardarcostoindirectoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); exit;
		$resultados = array();
		$registro = CostoIndirecto::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatoscostoindirectoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			$registro = CostoIndirecto::obtener($_POST['tipo'], $_POST['id']);
		else
			$registro = RelacionDiagnostico::noData();
			
		echo json_encode($registro);
	}

	public function congelarcostoindirectoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = CostoIndirecto::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// INGRESO ESPERADO //////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function obtenerIndiceDeRentaAction()
	{
		error_reporting(E_ERROR);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$con = Doctrine_Manager::getInstance()->connection();
		
		$formulario = array();
		$id_dato_general = $_POST['id_dato_general'];
		$tipo = $_POST['tipo']-1;
		try{
		
		
		//ingresos
		$ingresos = Doctrine_Query::create()->from('IngresoEsperado')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute();
		
		if(is_object($ingresos)){
			$banderaIgresos=true;
		}else{
			$banderaIgresos=false;
		}
		$costos_indirectos = Doctrine_Query::create()->from('CostoIndirecto')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($costos_indirectos)){
			$banderaCostos=true;
		}else{
			$banderaCostos=false;
		}
		if($_POST["funcion"]!="reporte_costos"){
			if($banderaIgresos==false || $banderaCostos==false){
				echo "0|0";exit;
			}
		}
		//$formulario["toneladas_por_hectarea"] =$ingresos->toneladas_por_hectarea;
		//print_r($ingresos->toneladas_por_hectarea);exit;
		$importe_parcial=0; 
		//renta de la tierra
		$renta_tierra = Doctrine_Query::create()->from('RentaTierra')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($renta_tierra)){
			$importe_parcial = $importe_parcial + $renta_tierra->importe;	
		}
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["renta_tierra"]=$renta_tierra->importe;
		
		//acondicionamiento del predio
		$acondicionamiento = Doctrine_Query::create()->from('AcondicionamientoPredio')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($acondicionamiento)){
			$canal = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_canal)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $canal->importe;
			$f = $f+$canal->importe;
			$trazo = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_trazo)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $trazo->importe;
			$f = $f+$trazo->importe;
			$emparejamiento = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_emparejamiento)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $emparejamiento->importe;
			$f = $f+$emparejamiento->importe;
			$contreo = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_contreo)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $contreo->importe;
			$f = $f+$contreo->importe;
			$despiedre = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_despiedre)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $despiedre->importe;
			$f = $f+$despiedre->importe;
			$reforzamiento = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_reforzamiento)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $reforzamiento->importe;
			$f = $f+$reforzamiento->importe;
			$otros = Doctrine_Query::create()->from('AcondicionamientoPredioConcepto')->where('id = ?', $acondicionamiento->id_concepto_otros)->execute()->getFirst();
			$importe_parcial = $importe_parcial + $otros->importe;
			$f = $f+$otros->importe;
		}
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["acondicionamiento_predio"]=$f;
		
		//mejoramiento fertilidad suelo
		$mejoramiento = Doctrine_Query::create()->from('MejoramientoFertilidadSuelo')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($mejoramiento)){
			$query="SELECT SUM(importe) as importe FROM mejoramiento_fertilidad_suelo_concepto WHERE id IN(".$mejoramiento->id_concepto_estiercol.",".$mejoramiento->id_concepto_muestreo_suelo.",".$mejoramiento->id_concepto_analisis_suelo.",".$mejoramiento->id_concepto_analisis_suelo_fisica.",".$mejoramiento->id_concepto_analisis_suelo_quimica.",".$mejoramiento->id_concepto_incorporacion_rastrojo.",".$mejoramiento->id_concepto_abonos_verdes.",".$mejoramiento->id_concepto_composta.",".$mejoramiento->id_concepto_lombricomposta.",".$mejoramiento->id_concepto_lixiviados.",".$mejoramiento->id_concepto_remineralizacion.",".$mejoramiento->id_concepto_descompactadores.",".$mejoramiento->id_concepto_microorganismos.",".$mejoramiento->id_concepto_cal.",".$mejoramiento->id_concepto_dolomita.",".$mejoramiento->id_concepto_yeso.",".$mejoramiento->id_concepto_otro.")";
			$d = $con->execute($query)->fetchAll();
			$importe_parcial = $importe_parcial + $d[0]["importe"];
			$mfi = $d[0]["importe"];
		}
			//llenamos la variable de formulario como un array asociativo para el reporte de costos
			$formulario["mejoramiento_fertilidad"]=$mfi;
		
		//preparacion del suelo
		$preparacion = Doctrine_Query::create()->from('PreparacionSuelo')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($preparacion)){
			$query="SELECT SUM(importe) as importe FROM preparacion_suelo_concepto WHERE id IN(".$preparacion->id_concepto_cinceleo.",".$preparacion->id_concepto_barbecho.",".$preparacion->id_concepto_rastra.",".$preparacion->id_concepto_cruza.",".$preparacion->id_concepto_laser.",".$preparacion->id_concepto_landplane.",".$preparacion->id_concepto_yunta.",".$preparacion->id_concepto_preparacion_manual.")";
			$d = $con->execute($query)->fetchAll();
			$importe_parcial = $importe_parcial + $d[0]["importe"];
			$psi = $d[0]["importe"];
		}
			//llenamos la variable de formulario como un array asociativo para el reporte de costos
			$formulario["preparacion_suelo"]= $psi;

		//riego
		$riego = Doctrine_Query::create()->from('Riego')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($riego)){
			$query="SELECT SUM(importe) as importe FROM riego_concepto WHERE id IN(".$riego->id_concepto_acondicionamiento.",".$riego->id_concepto_apertura_desague.",".$riego->id_concepto_apertura_zanjas.",".$riego->id_concepto_cuota.",".$riego->id_concepto_nacencia.",".$riego->id_concepto_auxilio.")";
			$d = $con->execute($query)->fetchAll();
			$importe_parcial = $importe_parcial + $d[0]["importe"];	
			$ri = $d[0]["importe"];
		}
			//llenamos la variable de formulario como un array asociativo para el reporte de costos
			$formulario["riego"]=$ri;
		//siembra
		$siembra = Doctrine_Query::create()->from('Siembra')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($siembra)){
			$cultivos = Doctrine_Query::create()->from('SiembraCultivo')->where('id_siembra = ?', $siembra->id)->execute(); 
			
			foreach($cultivos as $cultivo){
				//id_siembra_concepto
				//siembra_concepto
				$siembra_cultivo = Doctrine_Query::create()->from('SiembraConcepto')->where('id = ?', $cultivo->id_siembra_concepto)->execute()->getFirst();
				$importe_siembra_cultivo = $importe_siembra_cultivo + $siembra_cultivo->importe;
				
				$inoculacion_bio = Doctrine_Query::create()->from('SiembraInoculacion')->where('id_siembra_cultivo = ?', $cultivo->id)->andWhere('tipo = ?', "1")->execute();
				foreach($inoculacion_bio as $i_b){
					$bio = Doctrine_Query::create()->from('SiembraConcepto')->where('id = ?', $i_b->id_siembra_concepto)->execute()->getFirst();
					$importe_inoculacion_bio = $importe_inoculacion_bio + $bio->importe;
				}
				
				$inoculacion_ins = Doctrine_Query::create()->from('SiembraInoculacion')->where('id_siembra_cultivo = ?', $cultivo->id)->andWhere('tipo = ?', "2")->execute();
				foreach($inoculacion_ins as $i_i){
					$ins = Doctrine_Query::create()->from('SiembraConcepto')->where('id = ?', $i_i->id_siembra_concepto)->execute()->getFirst();
					$importe_inoculacion_ins = $importe_inoculacion_ins + $ins->importe;
				}
				
				$inoculacion_bios = Doctrine_Query::create()->from('SiembraInoculacion')->where('id_siembra_cultivo = ?', $cultivo->id)->andWhere('tipo = ?', "3")->execute();
				foreach($inoculacion_bios as $i_bs){
					$bios = Doctrine_Query::create()->from('SiembraConcepto')->where('id = ?', $i_bs->id_siembra_concepto)->execute()->getFirst();
					$importe_inoculacion_bios = $importe_inoculacion_bios + $bios->importe;
				}
				
				$inoculacion_insq = Doctrine_Query::create()->from('SiembraInoculacion')->where('id_siembra_cultivo = ?', $cultivo->id)->andWhere('tipo = ?', "4")->execute();
				foreach($inoculacion_insq as $i_iq){
					$insq = Doctrine_Query::create()->from('SiembraConcepto')->where('id = ?', $i_iq->id_siembra_concepto)->execute()->getFirst();
					$importe_inoculacion_insq = $importe_inoculacion_insq + $insq->importe;
				}
			}
			
		$importe_parcial =  $importe_parcial + $importe_siembra_cultivo + $importe_inoculacion_bio + $importe_inoculacion_ins;
		}
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["siembra"]=$importe_siembra_cultivo + $importe_inoculacion_bio + $importe_inoculacion_ins;
		
		
		//practicas de siembra
		$query="SELECT IF(aplicacion_tipo=2,(manual_no_jornales*manual_costo_jornal),(IF(mecanizada_tipo=1,(mecanizada_propia_costo_combustible+mecanizada_propia_costo_operador),mecanizada_rentada_monto)))AS MONTO FROM practica_siembra WHERE id_dato_general ='".$id_dato_general."' AND tipo ='".$tipo."'";
        $monto = $con->execute($query)->fetchAll();
		$importe_parcial = $importe_parcial+$monto[0]["MONTO"];
		
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["practica_siembra"]=$monto[0]["MONTO"];
		
		//resiembra
		$siembra = Doctrine_Query::create()->from('Resiembra')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		 if(is_object($siembra)){
			$siembra_cultivos = Doctrine_Query::create()->from('ResiembraCultivo')->where('id_resiembra = ?', $siembra->id)->execute();
			foreach ($siembra_cultivos as $siembra_cultivo){
				$siembra_inoculaciones = Doctrine_Query::create()->from('ResiembraInoculacion')->where('id_resiembra_cultivo = ?', $siembra_cultivo->id)->execute();
				foreach ($siembra_inoculaciones as $inoculacion){
					$importeResiembra = $importeResiembra + $inoculacion->ResiembraConcepto->importe;
					
				}
				$importeSiembraInoculacion = $importeSiembraInoculacion + $siembra_cultivo->ResiembraConcepto->importe;

			}
			$importe_parcial = $importe_parcial + $importeResiembra + $importeSiembraInoculacion;
			$r=$r + $importeResiembra + $importeSiembraInoculacion;
		 }
		 //llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["resiembra"]=$r;
		
		//practicas de resiembra
		$query="SELECT IF(aplicacion_tipo=2,(manual_no_jornales*manual_costo_jornal),(IF(mecanizada_tipo=1,(mecanizada_propia_costo_combustible+mecanizada_propia_costo_operador),mecanizada_rentada_monto)))AS MONTO FROM practica_resiembra WHERE id_dato_general ='".$id_dato_general."' AND tipo ='".$tipo."'";
         $monto = $con->execute($query)->fetchAll();
		$importe_parcial = $importe_parcial+$monto[0]["MONTO"];
		
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["practica_resiembra"]=$monto[0]["MONTO"];
		
		//fertilizacion
		$costo_fertilizacion=0;
		$importeTotal=0;
		$fertilizacion = Doctrine_Query::create()->from('Fertilizacion')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
			if(is_object($fertilizacion))
			{
				$fertilizaciones = Doctrine_Query::create()->from('FertilizacionFertilizacion')->where('id_fertilizacion = ?', $fertilizacion->id)->execute();
				foreach($fertilizaciones as $fert)
				{
					$conceptof= Doctrine_Query::create()->from('FertilizacionFoliar')->where('id_fertilizacion_fertilizacion = ?', $fert->id)->execute();
					foreach($conceptof as $cf)
					{
						$importef = $importef + $cf->importe;
					}
					$conceptos= Doctrine_Query::create()->from('FertilizacionFsiembra')->where('id_fertilizacion_fertilizacion = ?', $fert->id)->execute();
					foreach($conceptos as $cs)
					{
						$importes = $importes + $cs->importe;
					}
					$importeTotal = $importef + $importes;
					
					
					if($fert->tipo_aplicacion==1)
					{
						if($fert->tipo_mecanizacion==1)
							$aplicacion = $aplicacion+$fert->costo_combustible + $fert->costo_operador;
						if($fert->tipo_mecanizacion==2)
							$aplicacion = $aplicacion+$fert->monto_renta;
					}else if($fert->tipo_aplicacion==2)
					{
							$aplicacion = $aplicacion+($fert->numero_jornales * $fert->costo_jornal);
					}
					
					$importeMezclados = $importeMezclados + $fert->mezclado_importe;
					
				}
				$costo_fertilizacion = $costo_fertilizacion+ $aplicacion +$importeTotal + $importeMezclados;
				/*echo $importef;
				echo "-";
				echo $importes;
				echo "-";
				echo $importeMezclados;
				echo "-";
				echo $aplicacion;*/
				
			}
		$importe_parcial = $importe_parcial + $costo_fertilizacion;
		$cf = $costo_fertilizacion;
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["fertilizacion"]=$cf;
		
		
		//control de malezas
		$importeTotal =0;
		$control = Doctrine_Query::create()->from('ControlDeMaleza')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($control))
		{

			$control_maleza = Doctrine_Query::create()->from('ControlMalezaGeneral')->where('id_control_maleza = ?', $control->id)->execute();
			foreach($control_maleza as $cm)
			{


				$herbicida= Doctrine_Query::create()->from('ControlHerbicida')->where('id_control_maleza_general = ?', $cm->id)->execute();
				foreach($herbicida as $herb)
				{

					if($herb->aplicacion_aplicacion_tipo==1)
					{
						if($herb->aplicacion_tipo_mecanizacion==1){
							$ap = $herb->aplicacion_costo_combustible + $herb->aplicacion_costo_operador;
						}else{
							//rentada
							$ap = $herb->aplicacion_monto_renta;
						}
					}else if($herb->aplicacion_aplicacion_tipo==2){
						$ap = $herb->aplicacion_numero_jornales * $herb->aplicacion_costo_jornal;
					}else{
						if($herb->aplicacion_tipo_mecanizacion==1){
							$ap = $herb->aplicacion_costo_combustible + $herb->aplicacion_costo_operador;
						}else{
							//rentada
							$ap = $herb->aplicacion_monto_renta;
						}
					}
					
					$importeh = $importeh + $herb->marca_importe + $herb->coadyudante_importe + $ap;
					//print_r("control de maleza: ".$importeh);
				}

				

				$aplicacion=0;
				$acidificacion= Doctrine_Query::create()->from('ControlAcidificacion')->where('id_control_maleza_general = ?', $cm->id)->execute();
				foreach($acidificacion as $ac)
				{
					$importeac = $importeac + $ac->acidificacion_importe;
				}
				//print_r("acidificacion: ".$importeac);
				$importeTotal = $importeh + $importeac;
				//print_r("control de maleza acidificacion: ".$importeTotal);exit;

				//print_r("limpia: ".$aplicacion);exit;
				if($cm->limpia_aplicacion_tipo==1)
				{
					
					if($cm->limpia_tipo_mecanizacion==1){
						$aplicacion = $cm->limpia_costo_combustible + $cm->limpia_costo_operador;
					}else{
						//rentada
						$aplicacion = $cm->limpia_monto_renta;
					}
				}else if($cm->limpia_aplicacion_tipo==2){
					$aplicacion = $cm->limpia_numero_jornales*$cm->costo_jornal;
				}

				
	 			if($cm->escarda_aplicacion_tipo==1)
	 			{
					if($cm->escarda_tipo_mecanizacion==1){
						$aplicacion = $cm->escarda_costo_combustible + $cm->escarda_costo_operador;
					}else{
						//rentada
						$aplicacion = $cm->escarda_monto_renta;
					}
					
				}
				else if($cm->escarda_aplicacion_tipo==2){
					$aplicacion = $cm->escarda_numero_jornales * $cm->escarda_costo_jornal;
				}
				
				$costo_control_maleza = $costo_control_maleza+ $aplicacion +$importeTotal+$cm->limpia_importe+$cm->escarda_importe;;
				//print_r("fin: ".$aplicacion);exit;
			}

			$importe_parcial = $importe_parcial +$costo_control_maleza;
		}
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["control_malezas"]=$costo_control_maleza;

		//control de plagas de suelo
		$importeTotal =0;
		$importecp=0;

		

		$plagas = Doctrine_Query::create()->from('ControlPlagasSuelo')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', ($tipo+1))->andWhere('pestana=?','1')->execute()->getFirst();
		if(is_object($plagas))
		{
			$control_plagas = Doctrine_Query::create()->from('ControlPlagasSueloInsecticida')->where('id_control_plagas_suelo = ?', $plagas->id)->execute();
			foreach($control_plagas as $cp){
					$importecp = $importecp + $cp->importe;
			}

			//aplicacion granulado
			if($plagas->aplicacion_insecticida_granulado==1){
				if($plagas->tipo_mecanizacion_insecticida_granulado==1){//propio o renta{
					$aplicacioncp = $plagas->costo_combustible_insecticida_granulado+$plagas->costo_operador_insecticida_granulado;
				}else{
					//rentada
					$aplicacioncp = $plagas->monto_renta_insecticida_granulado;
				}
			}else if($plagas->aplicacion_insecticida_granulado==2){
				$aplicacioncp = $plagas->jornales_insecticida_granulado * $plagas->costo_jornal_insecticida_granulado;
			}
			/*//aplicacion tratamiento
			if($plagas->aplicacion_insecticida_tratamiento==1){
				if($plagas->tipo_mecanizacion_insecticida_tratamiento==1){//propio o renta{
					$aplicacioncp2 = $plagas->costo_combustible_insecticida_tratamiento +$plagas->costo_operador_insecticida_tratamiento ;
				}else{
					//rentada
					$aplicacioncp2 = $plagas->monto_renta_insecticida_tratamiento;
				}
			}else if($plagas->aplicacion_insecticida_tratamiento==2){
				$aplicacioncp2 = $plagas->jornales_insecticida_granulado * $plagas->costo_jornal_insecticida_granulado;
			}*/
			
			//aplicacion recarga
			if($plagas->aplicacion_insecticida_granulado_recarga==1){
				if($plagas->tipo_mecanizacion_insecticida_granulado_recarga==1){//propio o renta{
					$aplicacioncp3 = $plagas->costo_combustible_insecticida_granulado_recarga +$plagas->costo_operador_insecticida_granulado_recarga ;
				}else{
					//rentada
					$aplicacioncp3 = $plagas->monto_renta_insecticida_granulado_recarga;
				}
			}else if($plagas->aplicacion_insecticida_granulado_recarga==2){
				$aplicacioncp3 = $plagas->jornales_insecticida_granulado_recarga * $plagas->costo_jornal_insecticida_granulado_recarga;
			}
			//aplicacion acidificacion
			if($plagas->aplicacion_acidificacion==1){
				if($plagas->tipo_mecanizacion_insecticida_acidificacion==1){//propio o renta{
					$aplicacioncp4 = $plagas->costo_combustible_insecticida_acidificacion +$plagas->costo_operador_acidificacion;
				}else{
					//rentada
					$aplicacioncp4 = $plagas->monto_renta_insecticida_acidificacion;
				}
			}else if($plagas->aplicacion_acidificacion==2){
				$aplicacioncp4 = $plagas->jornales_acidificacion * $plagas->costo_jornal_acidificacion;
			}
			//aplicacion control biologico
			if($plagas->aplicacion_control_biologico==1){
				if($plagas->tipo_mecanizacion_control_biologico==1){//propio o renta{
					$aplicacioncp5 = $plagas->costo_combustible_control_biologico +$plagas->costo_operador_control_biologico ;
				}else{
					//rentada
					$aplicacioncp5 = $plagas->monto_renta_control_biologico;
				}
			}else if($plagas->aplicacion_control_biologico==2){
				$aplicacioncp5 = $plagas->jornales_control_biologico * $plagas->costo_jornal_control_biologico;
			}
			//print_r("tipo: ".$tipo);
			//print_r($plagas->id.": ".$plagas->importe_metarhizium ."-". $plagas->importe_trichoderma."-". $plagas->importe_beauveria ."-". $plagas->importe_acidificacion ."-".$aplicacioncp5 ."-".$aplicacioncp4 ."-".$aplicacioncp3 ."-".$aplicacioncp);exit();
			$importecp = $importecp + $plagas->importe_metarhizium + $plagas->importe_trichoderma+ $plagas->importe_beauveria + $plagas->importe_acidificacion +$aplicacioncp5 +$aplicacioncp4 +$aplicacioncp3 +$aplicacioncp;/* $aplicacioncp2 +*/ 
			
			$importe_parcial = $importe_parcial + $importecp;
		}
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["control_plagas_suelo"]=$importecp;

		//control de plagas foliares
		$foliares = Doctrine_Query::create()->from('ControlPlagasFoliares')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute();
		if(is_object($foliares)){
			$importeTotal =0;
			foreach($foliares as $foliar){
				$plagas_foliares = Doctrine_Query::create()->from("ControlPlagasFoliaresInsecticida")->where("id_control_plagas_foliares='".$foliar->id."'")->execute();
				foreach($plagas_foliares as $p_f){
					$importepf = $importepf + $p_f->marca_importe + $p_f->coadyudante_importe;
				}
				if($foliar->aplicacion==1 || $foliar->aplicacion==3){
					//print_r("aplicacion: ".$foliar->aplicacion." mecanizacion: ".$foliar->tipo_mecanizacion."".$foliar->tipo_mecanizacion_aerea);exit;
					if($foliar->tipo_mecanizacion==1 && $foliar->aplicacion==1){
						 $aplicacionpf = $foliar->costo_combustible + $foliar->costo_operador;
					}else if($foliar->tipo_mecanizacion_aerea==1 && $foliar->aplicacion==3){
						 $aplicacionpf = $foliar->costo_combustible_aerea + $foliar->costo_operador_aerea;
					}else {
						if($foliar->aplicacion==1){
							$aplicacionpf = $foliar->monto_renta;
						}else{
							$aplicacionpf =  $foliar->monto_renta_aerea;
						}
						
					}
				}else{
					$aplicacionpf = $foliar->numero_jornales*$foliar->costo_jornal;
				}
				$importeTotal = $importeTotal + $importepf + $aplicacionpf; 
			}
			$importe_parcial = $importe_parcial + $importeTotal;
		}
		
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["control_plagas_foliares"]=$importeTotal;
			
			
		//control de enfermedades
		$importecp=0;
		$aplicacioncp=0;
		$aplicacioncp3=0;
		$aplicacioncp4=0;
		$aplicacioncp5=0;
		$importe_parcial=0;
		$plagas = Doctrine_Query::create()->from('ControlPlagasSuelo')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', ($tipo+1))->andWhere('pestana=?','2')->execute()->getFirst();
		if(is_object($plagas))
		{
			$control_plagas = Doctrine_Query::create()->from('ControlPlagasSueloInsecticida')->where('id_control_plagas_suelo = ?', $plagas->id)->execute();
			foreach($control_plagas as $cp){
					$importecp = $importecp + $cp->importe;
			}

			//aplicacion granulado
			if($plagas->aplicacion_insecticida_granulado==1){
				if($plagas->tipo_mecanizacion_insecticida_granulado==1){//propio o renta{
					$aplicacioncp = $plagas->costo_combustible_insecticida_granulado+$plagas->costo_operador_insecticida_granulado;
				}else{
					//rentada
					$aplicacioncp = $plagas->monto_renta_insecticida_granulado;
				}
			}else if($plagas->aplicacion_insecticida_granulado==2){
				$aplicacioncp = $plagas->jornales_insecticida_granulado * $plagas->costo_jornal_insecticida_granulado;
			}
			/*//aplicacion tratamiento
			if($plagas->aplicacion_insecticida_tratamiento==1){
				if($plagas->tipo_mecanizacion_insecticida_tratamiento==1){//propio o renta{
					$aplicacioncp2 = $plagas->costo_combustible_insecticida_tratamiento +$plagas->costo_operador_insecticida_tratamiento ;
				}else{
					//rentada
					$aplicacioncp2 = $plagas->monto_renta_insecticida_tratamiento;
				}
			}else if($plagas->aplicacion_insecticida_tratamiento==2){
				$aplicacioncp2 = $plagas->jornales_insecticida_granulado * $plagas->costo_jornal_insecticida_granulado;
			}*/
			
			//aplicacion recarga
			if($plagas->aplicacion_insecticida_granulado_recarga==1){
				if($plagas->tipo_mecanizacion_insecticida_granulado_recarga==1){//propio o renta{
					$aplicacioncp3 = $plagas->costo_combustible_insecticida_granulado_recarga +$plagas->costo_operador_insecticida_granulado_recarga ;
				}else{
					//rentada
					$aplicacioncp3 = $plagas->monto_renta_insecticida_granulado_recarga;
				}
			}else if($plagas->aplicacion_insecticida_granulado_recarga==2){
				$aplicacioncp3 = $plagas->jornales_insecticida_granulado_recarga * $plagas->costo_jornal_insecticida_granulado_recarga;
			}
			//aplicacion acidificacion
			if($plagas->aplicacion_acidificacion==1){
				if($plagas->tipo_mecanizacion_insecticida_acidificacion==1){//propio o renta{
					$aplicacioncp4 = $plagas->costo_combustible_insecticida_acidificacion +$plagas->costo_operador_acidificacion;
				}else{
					//rentada
					$aplicacioncp4 = $plagas->monto_renta_insecticida_acidificacion;
				}
			}else if($plagas->aplicacion_acidificacion==2){
				$aplicacioncp4 = $plagas->jornales_acidificacion * $plagas->costo_jornal_acidificacion;
			}
			//aplicacion control biologico
			if($plagas->aplicacion_control_biologico==1){
				if($plagas->tipo_mecanizacion_control_biologico==1){//propio o renta{
					$aplicacioncp5 = $plagas->costo_combustible_control_biologico +$plagas->costo_operador_control_biologico ;
				}else{
					//rentada
					$aplicacioncp5 = $plagas->monto_renta_control_biologico;
				}
			}else if($plagas->aplicacion_control_biologico==2){
				$aplicacioncp5 = $plagas->jornales_control_biologico * $plagas->costo_jornal_control_biologico;
			}
			//print_r("tipo: ".$tipo);
			//print_r($plagas->id.": ".$plagas->importe_metarhizium ."-". $plagas->importe_trichoderma."-". $plagas->importe_beauveria ."-". $plagas->importe_acidificacion ."-".$aplicacioncp5 ."-".$aplicacioncp4 ."-".$aplicacioncp3 ."-".$aplicacioncp);exit();
			$importecp = $importecp + $plagas->importe_metarhizium + $plagas->importe_trichoderma+ $plagas->importe_beauveria + $plagas->importe_acidificacion +$aplicacioncp5 +$aplicacioncp4 +$aplicacioncp3 +$aplicacioncp;/* $aplicacioncp2 +*/ 
			
			$importe_parcial = $importe_parcial + $importecp;
		}
		//llenamos la variable de formulario como un array asociativo para el reporte de costos
		$formulario["control_enfermedades"]=$importecp;
		
		//tratamiento de estres biotico y abiotico
		$importeTotal =0;
		$importect =0;
		$tratamiento_estres = Doctrine_Query::create()->from('TratamientoDeEstresBioticoYAbiotico')->where('id_dato_general = ?', $id_dato_general)->andWhere('tipo = ?', $tipo)->execute()->getFirst();
		if(is_object($tratamiento_estres)){
			$trat_b_ab = Doctrine_Query::create()->from('TratamientoDeEstresBioticoYAbioticoInsecticida')->where('id_tratamiento_de_estres_biotico_y_abiotico = ?', $tratamiento_estres->id)->execute();
				foreach($trat_b_ab as $ct){
						$importect = $importect + $ct->importe;
				}
				//aplicacion granulado
				if($tratamiento_estres->aplicacion_insecticida_granulado==1){
					if($tratamiento_estres->tipo_mecanizacion_insecticida_granulado==1){//propio o renta{
						$aplicacionct = $tratamiento_estres->costo_combustible_insecticida_granulado+$tratamiento_estres->costo_operador_insecticida_granulado;
					}else{
						//rentada
						$aplicacionct = $tratamiento_estres->monto_renta_insecticida_granulado;
					}
				}else if($tratamiento_estres->aplicacion_insecticida_granulado==2){
					$aplicacionct = $tratamiento_estres->jornales_insecticida_granulado * $tratamiento_estres->costo_jornal_insecticida_granulado;
				}
				//aplicacion tratamiento
				if($tratamiento_estres->aplicacion_insecticida_tratamiento==1){
					if($tratamiento_estres->tipo_mecanizacion_insecticida_tratamiento==1){//propio o renta{
						$aplicacionct2 = $tratamiento_estres->costo_combustible_insecticida_tratamiento +$tratamiento_estres->costo_operador_insecticida_tratamiento ;
					}else{
						//rentada
						$aplicacionct2 = $tratamiento_estres->monto_renta_insecticida_tratamiento;
					}
				}else if($tratamiento_estres->aplicacion_insecticida_tratamiento==2){
					$aplicacionct2 = $tratamiento_estres->jornales_insecticida_tratamiento * $tratamiento_estres->costo_jornal_insecticida_tratamiento;
				}
				
				//aplicacion recarga
				if($tratamiento_estres->aplicacion_insecticida_granulado_recarga==1){
					if($tratamiento_estres->tipo_mecanizacion_insecticida_granulado_recarga==1){//propio o renta{
						$aplicacionct3 = $tratamiento_estres->costo_combustible_insecticida_granulado_recarga +$tratamiento_estres->costo_operador_insecticida_granulado_recarga ;
					}else{
						//rentada
						$aplicacionct3 = $tratamiento_estres->monto_renta_insecticida_granulado_recarga;
					}
				}else if($tratamiento_estres->aplicacion_insecticida_granulado_recarga==2){
					$aplicacionct3 = $tratamiento_estres->jornales_insecticida_granulado_recarga * $tratamiento_estres->costo_jornal_insecticida_granulado_recarga;
				}
				//aplicacion acidificacion
				if($tratamiento_estres->aplicacion_acidificacion==1){
					if($tratamiento_estres->tipo_mecanizacion_insecticida_acidificacion==1){//propio o renta{
						$aplicacionct4 = $tratamiento_estres->costo_combustible_insecticida_acidificacion +$tratamiento_estres->costo_operador_acidificacion ;
					}else{
						//rentada
						$aplicacionct4 = $tratamiento_estres->monto_renta_insecticida_acidificacion;
					}
				}else if($tratamiento_estres->aplicacion_acidificacion==2){
					$aplicacionct4 = $tratamiento_estres->jornales_acidificacion * $tratamiento_estres->costo_jornal_acidificacion;
				}
				//aplicacion control biologico
				if($tratamiento_estres->aplicacion_control_biologico==1){
					if($tratamiento_estres->tipo_mecanizacion_control_biologico==1){//propio o renta{
						$aplicacionct5 = $tratamiento_estres->costo_combustible_control_biologico +$tratamiento_estres->costo_operador_control_biologico ;
					}else{
						//rentada
						$aplicacionct5 = $tratamiento_estres->monto_renta_control_biologico;
					}
				}else if($tratamiento_estres->aplicacion_control_biologico==2){
					$aplicacionct5 = $tratamiento_estres->jornales_control_biologico * $tratamiento_estres->costo_jornal_control_biologico;
				}
				//print_r($tratamiento_estres->importe_metarhizium);exit;
				$importect = $importect + $tratamiento_estres->importe_metarhizium + $tratamiento_estres->importe_trichoderma+ $tratamiento_estres->importe_beauveria + $tratamiento_estres->importe_acidificacion +$aplicacionct5 +$aplicacionct4 +$aplicacionct3 + $aplicacionct2 + $aplicacionct;
				//print_r($aplicacionct5." - ".$aplicacionct4 ." - ".$aplicacionct3 ." - ". $aplicacionct2 ." - ". $aplicacionct);exit;
				$importe_parcial =$importe_parcial + $importect;
				//print_r($importe_parcial);exit;				
			}
				//llenamos la variable de formulario como un array asociativo para el reporte de costos
				$formulario["tratamiento_estres"]=$importect;

		
		
		$costo_directo = $importe_parcial;
		//print_r($costo_directo);exit;
		if($banderaIgresos==true && $banderaCostos==true){
			foreach($ingresos as $ingreso){
				if($ingreso->concepto!="indemnizacion_seguro" && $ingreso->concepto!="procampo" && $ingreso->concepto!="incentivo"){
					$ingreso_esperado = $ingreso_esperado + $ingreso->ingreso_total;
				}else{
					$costo_indirecto = $costo_indirecto + $ingreso->ingreso_total;
				}
				
			}
			//$ingreso_esperado = $ingresos->grano + $ingresos->elote + $ingresos->forraje_rastrojo + $ingresos->forraje_verde + $ingresos->hoja;
			//$ingreso_indirecto = $ingresos->indemnizacion_seguro +$ingresos->procampo + $ingresos->incentivo;
			
			//$costo_indirecto = $costos_indirectos->trilla_corte + $costos_indirectos->flete + $costos_indirectos->seguro_agricola + $costos_indirectos->asistencia_tecnica + $costos_indirectos->gasto_almacen + $costos_indirectos->cobertura_precio + $costos_indirectos->garantia_liquida;
			
			$costo_total = $costo_directo + $costo_indirecto;
			//print_r($costo_total ." ". $costo_directo ." ". $costo_indirecto);exit;
		}else{
			$ingreso_esperado =0;
			$ingreso_indirecto =0;
			$costo_indirecto =0;
			$costo_total =0;
		}
		$ingreso_total = $ingreso_indirecto + $ingreso_esperado;
		
		//echo $ingreso_esperado."*".$costo_directo."*".$ingreso_total."*".$costo_total;exit;
		$ingreso_1 = $ingreso_esperado - $costo_directo;
		$ingreso_2 = $ingreso_total - $costo_total;
		
		if($_POST["funcion"]=="reporte_costos"){
			
			
			
			
			//print_r($formulario);exit;
			echo json_encode($formulario);
		}else{
			echo $ingreso_1."|".$ingreso_2;
		}
				}catch(Exception $e){
					print_r($e->getMessage());exit;
				}
		
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// INGRESO ESPERADO //////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public function guardaringresoesperadoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($_POST); exit;
		$resultados = array();
		$registro = IngresoEsperado::guardar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function obtenerdatosingresoesperadoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id'] > 0)
			echo IngresoEsperado::obtener($_POST['tipo'], $_POST['id']);
		else
			echo RelacionDiagnostico::noData();
	}

	public function congelaringresoesperadoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = IngresoEsperado::congelar($_POST);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////// GENERALES ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	/*public function obtenercultivosAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		//echo "<pre>"; print_r($array); print_r($array); exit;
		
		$resultados = array();
			$registro = DatoGeneral::obtenerCultivos($_POST['id']);
			if($registro > 0) $resultados['ok'] = $registro;
			else $resultados['error'] = $registro;
		echo json_encode($resultados);
	}*/

	public function obtenercultivoscatalogoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$cultivos = DatoGeneralCultivo::obtenerPorDatoGeneral($_POST["id"],($_POST["tipo"]-1));
		$select .= '<option selected="selected" value="0">Seleccione un cultivo</option>';
		foreach($cultivos as $cultivo){
			$select .= '<option value="'.$cultivo->id_cultivo.'">'.$cultivo->Cultivo->nombre.'</option>';
		}
		echo $select;
		//echo Cultivo::obtenerHtml();
	}
	
	public function obtenerunicoAction()
	{
		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
		
		if($_POST['id']==NULL){
			echo '0';
			exit;
		}
		
       	$selector = "<option value='";		
       	switch ($_POST['tipo'])
       	{
       		case 'uso':
       			$r = Uso::obtenerPorId("1=1", $_POST['id']);
       			$selector .= $r->id . "' selected>" . $r->nombre . "</option>";
       			break;
       		case 'marca':
       			$r = Marca::obtenerPorId("1=1", $_POST['id']);
       			$selector .= $r->id . "' selected>" . $r->nombre . "</option>";
       			break;
       		case 'hov':
       			$r = HibridoVariedad::obtenerPorId("1=1", $_POST['id']);
       			$selector .= $r->id . "' selected>" . $r->nombre . "</option>";
       			break;
       		case 'unidad':
       			$r = Unidad::obtenerPorId("1=1", $_POST['id']);
       			$selector .= $r->id . "' selected>" . $r->nombre . "</option>";
       			break;
       		case 'activo':
       			$r = Activo::obtenerPorId("1=1", $_POST['id']);
       			$selector .= $r->id . "' selected>" . $r->nombre . "</option>";
       			break;
       		case 'cultivo':
       			$r = Cultivo::obtenerPorId("1=1", $_POST['id']);
       			$selector .= $r->id . "' selected>" . $r->nombre . "</option>";
       			break;
       		default:
       			echo "Error al encontrar el tipo"; 
       			exit;
       			break;
       	}
       	echo $selector;
	}

	public function obtenerproductorAction()
    {
       	### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$productor = My_Comun::obtener("Productor", "rfc", $_POST["rfc_productor"]);

		$resultados = array();
		if(is_object($productor))
		{
			$resultados['id'] = $productor->id;
            $resultados['nombre'] = $productor->nombre;
            $resultados['curp'] = $productor->curp; 
            $resultados['domicilio'] = $productor->domicilio; 
            $resultados['clave'] = $productor->id;

            $predios = Predio::obtenerPorProductor($productor->id);
            $predio_string = '<option value="0">Seleccione un predio</option>';
	        foreach ($predios as $predio) {
	          $predio_string .= '<option value="'. $predio->id .'">'. $predio->nombre .'</option>';
	        }
	        $resultados['predios'] = $predio_string;
        }
        else
        	$resultados['noexiste'] = 'RFC NO ENCONTRADO';
        echo json_encode($resultados);				   
    }	

	public function productorformAction()
	{
		### Deshabilitamos el layout ya que mostraremos la vista en un dialog
        $this->_helper->layout->disableLayout();
		$this->view->rfc = $this->_getParam("rfc");
		$this->view->dependencias = Dependencia::obtenerPorId(null);
		$this->view->integradoras = Integradora::obtenerPorId(null);
		$this->view->organizaciones = Organizacion::obtenerPorId(null);
		$this->view->estados = Estado::obtenerPorId(null);
	}

	public function obtenerunidadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$datos = '<option value="0">Seleccione una unidad</option>';
		$unidades = Unidad::obtenerPorId();
		if(count($unidades) > 0)
		{
			foreach ($unidades as $unidad) {
            	$datos .= '<option value="'. $unidad->id .'">'. $unidad->nombre .'</option>';
            }
		}
		echo $datos;
	}
	public function obtenerunidadesporformularioconceptoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$datos = '<option value="0">Seleccione una unidad</option>';
		$unidades = Unidad::obtenerPorFormularioYConcepto($_POST["formulario"],$_POST["concepto"]);
		if(count($unidades) > 0)
		{
			foreach ($unidades as $unidad) {
            	$datos .= '<option value="'. $unidad->id .'">'. $unidad->nombre .'</option>';
            }
		}
		echo $_POST["concepto"]."|".$datos;
	}
	public function obtenerunidadesporformularioconceptoriegoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$datos = '<option value="0">Seleccione una unidad</option>';
		$unidades = Unidad::obtenerPorFormularioYConcepto('Riego','acondicionamiento');
		if(count($unidades) > 0)
		{
			foreach ($unidades as $unidad) {
            	$datos .= '<option value="'. $unidad->id .'">'. $unidad->nombre .'</option>';
            }
		}

		echo $datos;
	}

	function obtenerunidadesporformularioconcepto2($formulario,$concepto)
	{
		$datos = '<option value="0">Seleccione una unidad</option>';
		$unidades = Unidad::obtenerIdsPorFormularioYConcepto($formulario,$concepto);

		/*if(count($unidades) > 0)
		{
			foreach ($unidades as $unidad) {
            	$datos .= '<option value="'. $unidad->id .'">'. $unidad->nombre .'</option>';
            }
		}*/
		return  $unidades;
	}

	public function obtenerusosAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo Uso::obtenerHtml();
	}

	public function obtenermarcasAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo Marca::obtenerHtml("tipo = "  . $_POST['tipo']);
	}

	public function obtenerhibridosovariedadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo HibridoVariedad::obtenerHtml("id_marca = " . $_POST['id_marca'] . " AND tipo = "  . $_POST['tipo']);
	}

	public function obteneractivosAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo MarcaActivo::obtenerHtml("id_marca = " . $_POST['id_marca']);
	}
	public function obtenermunicipiosporestadoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$items = Municipio::obtenerPorEstado($_POST["id_estado"]);
		echo $items;
	}
	public function obtenerlocalidadespormunicipioAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$items = Localidad::obtenerPorMunicipio($_POST["id_municipio"]);
		echo $items;
	}
	
	public function obtenerempresasAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		//echo "tipo = '" . $_POST['tipo']."' and id_empresa = '".$_POST['tipo']."'";
		
		echo Marca::obtenerHtml("tipo = '" . $_POST['tipo']."' and id_empresa = '".$_POST['id_empresa']."'");
	}

	public function obtenersubcatalogoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		//echo "tipo = '" . $_POST['tipo']."' and id_empresa = '".$_POST['tipo']."'";
		
		echo CatalogoDetalle::obtenerHtml($_POST['padre_id'], $_POST['id']);
	}

	public function guardarAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$predio = Predio::guardar($_POST);
		echo $predio; exit;

	}


	public function guardarsistemaproduccionactualAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$resultados = array();
		
		$_POST['normales_climatologicas']=json_encode($_POST['normal']);
		$_POST['ultimo_ano']=json_encode($_POST['ultimo']);
		$_POST['ano_actual']=json_encode($_POST['actual']);
		$registro = SistemaProduccionActual::guardar($_POST);
		//print_r($registro);exit;
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function guardarsistemaequipoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = SistemaProduccionEquipo::guardar($_POST);
		//print_r($registro);exit;
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function eliminarsistemaequipoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id']>0)
		{
			Doctrine_Query::create()->delete('SistemaProduccionEquipo')->where("id = '". $_POST['id']."'")->execute();
		}
	}


	public function guardarsistemaconservacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = SistemaProduccionConservacion::guardar($_POST);
		//print_r($registro);exit;
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function eliminarsistemaconservacionAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id']>0)
		{
			Doctrine_Query::create()->delete('SistemaProduccionConservacion')->where("id = '". $_POST['id']."'")->execute();
		}
	}


	public function guardarsistemacultivoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = SistemaProduccionActual::guardarCultivo($_POST);
		//print_r($registro);exit;
		
		echo json_encode($registro);
	}

	public function eliminarsistemacultivoAction()
	{
		//error_reporting(E_ALL);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$id = $_POST['id'];
		$respuesta = array();
		if($id>0)
		{
			try
			{
				/*
				$siembra_cultivo = Doctrine_Query::create()->from('SiembraCultivo')->where('id_dato_general_cultivo = ?', $id)->execute()->getFirst();
				$query = Doctrine_Query::create()->delete('SiembraCultivo')->where("id = ".$siembra_cultivo->id);//->execute();
				echo $query->getSqlQuery(); exit;
				*/
				//$cultivo = Doctrine_Query::create()->from('DatoGeneralCultivo')->where('id = ?', $id)->execute()->getFirst();
            	Doctrine_Query::create()->delete('DatoGeneralCultivo')->where("id = ".$id)->execute();
				### Hacemo commit a la transacción
				//$conn->commit();
				$respuesta['res'] = '200';
			}
			catch (Exception $e)
			{ 
				### Rollback en caso de algún problema
				$conn->rollBack(); 
				### Entregamos un mensaje de error
				$respuesta['res'] = '404';
				$respuesta['error'] = $e->getMessage();
			}
		}

		echo json_encode($respuesta);
	}

	public function guardarsistemaeventoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = SistemaProduccionEvento::guardar($_POST);
		//print_r($registro);exit;
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function eliminarsistemaeventoAction()
	{
		//error_reporting(E_ALL);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id']>0)
		{
			Doctrine_Query::create()->delete('SistemaProduccionEvento')->where("id = '". $_POST['id']."'")->execute();
		}
	}


	public function guardarbiodiversidadplantasAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = BiodiversidadPlanta::guardar($_POST);
		
		echo json_encode($registro);
	}

	public function eliminarbiodiversidadplantasAction()
	{
		error_reporting(E_ALL);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id']>0)
		{
			Doctrine_Query::create()->delete('BiodiversidadPlanta')->where("id = '". $_POST['id']."'")->execute();
		}
	}

	public function guardarbiodiversidadanimalesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = BiodiversidadAnimal::guardar($_POST);
		
		echo json_encode($registro);
	}

	public function eliminarbiodiversidadanimalesAction()
	{
		error_reporting(E_ALL);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id']>0)
		{
			Doctrine_Query::create()->delete('BiodiversidadAnimal')->where("id = '".$_POST['id']."'")->execute();
		}
	}

	public function guardarbiodiversidadactividadesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);


		$resultados = array();
		//print_r($_POST);exit;
		$registro = BiodiversidadActividades::guardar($_POST);
		
		echo json_encode($registro);
	}

	public function eliminarbiodiversidadactividadesAction()
	{
		error_reporting(E_ALL);
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['id']>0)
		{
			Doctrine_Query::create()->delete('BiodiversidadActividades')->where("id = '".$_POST['id']."'")->execute();
		}
	}


	public function guardarcaracterizacioncosechaAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		

		foreach($_POST['campo'] as $id=>$arreglo)
		{
			//Obtenemos el cultivo
			$cultivo=Doctrine_Query::create()->from('DatoGeneralCultivo')->where("id='".$id."'")->execute()->getFirst();
				$cultivo->fromArray($arreglo);
			$cultivo->save();
		}
		$resultados['ok'] = 1;
		echo json_encode($resultados);
	}

	public function guardarcaracterizacionfinanciamientoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$registro = Financiamiento::guardar($_POST);
		//print_r($registro);exit;
		if($registro > 0) 
			$resultados['ok'] = $registro;
		else 
			$resultados['error'] = $registro;
		echo json_encode($resultados);
	}

	public function cagartipofinanciamientoAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($_POST['tipo']==1)
			$registros=CatalogoDetalle::obtener(15);
		if($_POST['tipo']==2)
			$registros=CatalogoDetalle::obtener(16);

		$html='<option value="">Elija una opción</option>';

		foreach($registros as $valor)
		{
			$html.='<option value="'.$valor->id.'">'.$valor->valor.'</option>';
		}

		echo $html;
	}
		
	public function obteneragroconsultoresAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		$items = Agroconsultor::obtenerUsuariosPorTerritorio($this->_getParam('id_territorio'));
		echo $items;
	}
}

?>