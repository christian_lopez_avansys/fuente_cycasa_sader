<?php

class Backend_MunicipioController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/municipio.js');
    }

    public function indexAction()
    {
		$this->view->titulo = "Catalogos";
		$this->view->subtitulo = "Municipios";
		$this->view->estados = Municipio::obtenerEstadosActivos();
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		if($this->_getParam('nombre') != '' && $this->_getParam('nombre') != null) $filtros .= " AND nombre LIKE '%". $this->_getParam('nombre') ."%'";
		if($this->_getParam('estado') > 0) $filtros .= " AND id_estado = ". $this->_getParam('estado');

		$registros = My_Comun::registrosGrid("Municipio", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$grid[$i]['estado'] = utf8_decode($registro->Estado->nombre);

      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	

		if($this->_getParam('id') > 0) $this->view->registro = Municipio::obtenerMunicipioPorId($this->_getParam('id'));
		$this->view->estados = Municipio::obtenerEstadosActivos();
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Municipio", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Municipio", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Municipio", $_POST, "nombre", $_POST['id'], "");
	}
}

?>