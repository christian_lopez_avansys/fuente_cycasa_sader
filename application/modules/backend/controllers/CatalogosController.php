<?php

class Backend_CatalogosController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		$this->view->headScript()->appendFile('/js/catalogos/catalogo.js');
    }

    public function indexAction()
    {
		$this->view->titulo = "Catálogos";
		$this->view->subtitulo = "Catálogos";
		
		$this->view->marcas=Activo::obtenerPorId("1=1");
    }

	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";


		$registros = My_Comun::registrosGrid("Catalogo", $filtros);

		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	
		$this->view->catalogos=My_Comun::obtenerFiltro("Catalogo","estatus=1","nombre asc");
		if($this->_getParam('id') > 0){
			$this->view->registro = My_Comun::obtener('Catalogo','id',$this->_getParam('id'));

		}
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Catalogo", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Catalogo", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Catalogo", $_POST, "nombre", $_POST['id'], "");
	}

	public function obteneropcionesAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$catalogos = Catalogo::obtenerHtml($this->_getParam('id_catalogo'));
		echo $catalogos;
	}
}

?>