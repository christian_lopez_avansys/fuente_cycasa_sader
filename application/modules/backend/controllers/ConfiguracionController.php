<?php

class Backend_ConfiguracionController extends Zend_Controller_Action
{
    public function init()
    {
		$this->_helper->layout()->setLayout('backend');
		
		//$this->view->headScript()->appendFile('/js/multiselect.js');
		$this->view->headScript()->appendFile('/js/configuracion.js');
		$this->view->headLink()->appendStylesheet('/css/configuracion.css');
    }
	public function indexAction()
    {
		//$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(TRUE);
		//header("Location: /unidades-de-medida");
    }
    public function unidadesDeMedidaAction()
    {
		$this->view->titulo = "Configuración";
		$this->view->subtitulo = "Unidades de medida";
		
		$this->view->unidades = Unidad::obtenerHtml2();
    }
	public function guardarpestanaAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$arreglo = json_decode($_POST['datos']);
		$prefijo = $_POST['prefijo'];

		try{
			Configuracion::guardarUnidadesDeMedida($prefijo,$arreglo);
		}catch(Exception $e){
			$e->getMessage();
		}
    }
	public function obtenerunidadesAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$formulario = $_POST['formulario'];

		try{
			$unidades = Configuracion::obtenerUnidadesPorFormulario($formulario);
			
		}catch(Exception $e){
			$e->getMessage();
		}
		echo json_encode($unidades);
    }
	

/*	public function gridAction()
    {
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		$filtros = "1=1";

		if($this->_getParam('filtro') != '' && $this->_getParam('filtro') != null) $filtros .= " AND nombre LIKE '%". $this->_getParam('filtro') ."%'";

		$registros = My_Comun::registrosGrid("Fuente", $filtros);
		$i = 0;
		$grid=array();

		foreach($registros['registros'] as $registro)
    	{
    		if($registro->estatus == 1)
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar.gif" style="cursor: pointer;" onclick="agregar('. $registro->id .')"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/eliminar.gif" style="cursor: pointer;" onclick="deshabilitar('. $registro->id .')"/>';
    		}
    		else
    		{
    			$grid[$i]['editar'] = '<img src="/css/images/editar-off.gif"/>';
    			$grid[$i]['eliminar'] = '<img src="/css/images/check.png" style="cursor: pointer;" onclick="habilitar('. $registro->id .')"/>';
    		}

      		$grid[$i]['nombre'] = $registro->nombre;
      		$i++;
      	}

      	My_Comun::armarGrid($registros, $grid);
    }

	public function agregarAction()
	{	

		if($this->_getParam('id') > 0) $this->view->registro = Fuente::obtenerPorId("1=1", $this->_getParam('id'));
	}

	public function deshabilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::deshabilitar("Fuente", $this->_getParam('id'), "");
		else echo -100;
	}

	public function habilitarAction()
	{	
		### Deshabilitamos el layout y la vista
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if($this->_getParam('id') > 0) echo My_Comun::habilitar("Fuente", $this->_getParam('id'), "");
		else echo -100;
	}

	public function guardarAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		echo My_Comun::guardar("Fuente", $_POST, "nombre", $_POST['id'], "");
	}

	public function obtenerfuentesAction()
	{
		### Deshabilitamos el layout y la vista
		$this->_helper->layout->disableLayout();
       	$this->_helper->viewRenderer->setNoRender(TRUE);
       	echo Fuente::obtenerHtml();
	}
*/}

?>