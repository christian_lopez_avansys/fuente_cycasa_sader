<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application_Plugin_Login

 */
class Application_Plugin_Login extends Zend_Controller_Plugin_Abstract
{
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
    	//print_r($_POST); exit;
		$view = new Zend_View();	
    	$view->peticion = $request->isXmlHttpRequest();
    	$view->modulo = $request->getModuleName();	
		$view->controlador = $this->obtenerPermiso($request);
		$view->accion = $this->obtenerAccion($request);
		
		
		$layout = Zend_Layout::getMvcInstance();

		//echo "<pre>" . print_r($request->getParams()); exit;
		//echo $view->modulo . " -- " . $request->getControllerName() . " -- " . $request->getActionName(); exit;
				
		if($view->modulo == 'backend')
		{
			if($request->getControllerName() != "servicios"){
				if(Zend_Auth::getInstance()->hasIdentity())
				{
					//echo "entro"; exit();
					$request->setParams(array_merge($request->getParams(), array('loadjs' => 1)));
					$layout->setLayout('backend');
					
					if($view->controlador=="login")
						header("Location: /backend");
				}
				else
				{
					/*$par = array_merge($request->getParams(), array('loadjs' => 2));
					//echo "<pre>"; print_r($par); exit();
					$request->setParams($par);
					$layout->setLayout('login');*/
					header("Location: /sin-permiso");
				}
			}		
		}
		else
		{
			$layout->setLayout('login');
		}
		

		$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
		$viewRenderer->setView($view);

		Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }

    private function  obtenerPermiso($controlador){
		if(!$controlador->isXmlHttpRequest())
        {

        	//echo ("VER_" . strtoupper(str_replace("-", "_", $controlador->getControllerName()))); exit;
			//echo My_Comun::tienePermiso("VER_" . strtoupper(str_replace("-", "_", $controlador->getControllerName())));exit;
			if(My_Comun::tienePermiso("VER_" . strtoupper(str_replace("-", "_", $controlador->getControllerName()))) == false && $controlador->getControllerName() != "index" &&  $controlador->getControllerName() != "login" && $controlador->getControllerName() != "servicios")
				header("Location: /sin-permiso");
	
			return $controlador->getControllerName();
		}
		 return $controlador->getControllerName();
    }

    private function  obtenerAccion($accion){
		if(!$accion->isXmlHttpRequest())
        {
			switch($accion->getActionName()){
	
				case "index"	: return "ver"; break;
				case "agregar"  : return "agregar"; break;
				case "eliminar" : return "eliminar"; break;
				case "exportar" : return "ver"; break;
				default         : return $accion->getActionName(); break;	
			}
		}
		return $accion->getActionName();
    }
}

?>
