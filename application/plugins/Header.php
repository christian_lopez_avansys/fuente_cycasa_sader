<?php
  class Application_Plugin_Header extends Zend_Controller_Plugin_Abstract
  { 
	  public $module;
	  
	  public function preDispatch(Zend_Controller_Request_Abstract $request)
	  {
         $arr = $request->getParams();
		 $view = new Zend_View();
		 $view->modulo = $request->getModuleName();

         $view->headScript()->appendFile($view->baseUrl('/js/jQuery/jquery.js'));
         $view->headScript()->appendFile($view->baseUrl('/js/jquery-ui.js'));
         $view->headScript()->appendFile($view->baseUrl('/js/form/jquery.alphanumeric.js'));
         $view->headScript()->appendFile($view->baseUrl('/js/form/jquery.validate.min.js'));
         $view->headScript()->appendFile($view->baseUrl('/js/bootstrap/bootstrap2.3.2.js'));
         $view->headLink()->appendStylesheet($view->baseUrl('/css/bootstrap.css'));

		 if($view->modulo=="backend")
         {
             if($arr['loadjs'])
             {
                $view->headScript()->appendFile($view->baseUrl('/js/multiselect.js'));
                $view->headScript()->appendFile($view->baseUrl('/js/comun.js'));
                $view->headScript()->appendFile($view->baseUrl('/js/form/ajaxforms.js'));
        		$view->headScript()->appendFile($view->baseUrl('/js/bootstrap/bootstrap-datepicker.min.js'));
        		$view->headScript()->appendFile($view->baseUrl('/js/bootstrap/locales/bootstrap-datepicker.es.min.js'));
        		$view->headScript()->appendFile($view->baseUrl('/js/app.min.js'));
        		$view->headScript()->appendFile($view->baseUrl('/js/fastclick/fastclick.min.js'));
        		$view->headScript()->appendFile($view->baseUrl('/js/slimScroll/jquery.slimscroll.min.js'));
        		$view->headLink()->appendStylesheet($view->baseUrl('/css/datepicker3.css'));
                $view->headLink()->appendStylesheet($view->baseUrl('/css/global.css?'.time()));
                $view->headLink()->appendStylesheet($view->baseUrl('/css/dialog_mensaje.css'));
        		$view->headScript()->appendFile($view->baseUrl('/js/flexgrid/flexigrid.js'));
        		$view->headLink()->appendStylesheet($view->baseUrl('/css/flexigrid.css'));

                if($request->getActionName() == 'index' && $request->getControllerName() == 'index')
                    $view->headScript()->appendFile('/js/toggle.js');
                $view->headScript()->appendFile('/js/index.js?'.time());
            }
		 }
		 
		 $viewRenderer=new Zend_Controller_Action_Helper_ViewRenderer();
		 $viewRenderer->setView($view);
		 
		 Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
		 define("BASE_URL", $view->baseUrl());

		 
	  }
	  
	  public function routeStartup(Zend_Controller_Request_Abstract $request)
	  {


	  }
  }
?>
